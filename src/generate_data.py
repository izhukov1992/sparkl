#!/usr/bin/python
# -*- coding: utf-8 -*-

from datetime import datetime, timedelta
import django
import os
# import pandas as pd
# import re
from random import uniform, randint


from django.db.models import Q
from django.db.utils import IntegrityError

from sparkl_web.wsgi import application
from api.addresses.models import *
from api.profiles.constants import *
from api.profiles.models import *
from api.bookings.models import *
from api.business.models import *
from api.services.models import *
from api.navigation.models import *
from sparkl_web.google_oauth2_startservice import build_service
from api.schedule.constants import *
from api.schedule.google_calendar_helpers import *
from api.schedule.models import *

#from scripts.utils import *

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "sparkl_web.settings.dev")
django.setup()
from django.contrib.auth.models import User


try:
    User.objects.create_superuser('gaudencio',
                                  'pmgaudencio@gmail.com',
                                  'qwertyuiop')
except IntegrityError:
    pass

home_channel, _ = Channel.objects.get_or_create(name=u'Domicilios')

address_count = Address.objects.count()

# customer, _ = Customer.objects.get_or_create(
#     first_name="Maria",
#     last_name="Bigodes",
#     gender='M',
#     mobile_number="+351333333333",
#     address_id=1,
#     date_of_birth=datetime(1989, 11, 12),
#     vat="123456789",
#     facebook_id="123456789",
#     address=address3
# )

sp1, _ = ServiceProvider.objects.get_or_create(
    first_name="Carla Calos",
    last_name="Gomes",
    gender='F',
    mobile_number="+351111111111",
    address=Address.objects.get(pk=randint(0, address_count - 1)),
    status=ACTIVE,
    vat="123456789",
    facebook_id="123456789"
)


sp2, _ = ServiceProvider.objects.get_or_create(
    first_name="Ana Mani",
    last_name="da Silva",
    gender='F',
    mobile_number="+351222222222",
    address=Address.objects.get(pk=randint(0, address_count - 1)),
    status=ACTIVE,
    vat="123456789",
    facebook_id="123456789",
    travel_method=DRIVING
)

sp1_extra = sp1.extra_details.first()
sp1_extra.google_calendar_id = '47tkt4naok5f05b79tsjhlhn3s@group.calendar.google.com'
# sp1_extra.google_calendar_id = 'sparkl.pt_j60q26m2566q81ms1g83n4apho@group.calendar.google.com'
sp1_extra.save()

sp2_extra = sp2.extra_details.first()
sp2_extra.google_calendar_id = '606ci89bhv7b3vej28ioenga6k@group.calendar.google.com'
# sp2_extra.google_calendar_id = 'sparkl.pt_6peg9spmsclo4bk7c1cvl7jfoc@group.calendar.google.com'
sp2_extra.save()

print("Created service providers: {}...".format(
    [s for s in ServiceProvider.objects.all()]))


mani, _ = ServiceCategory.objects.get_or_create(name=u"Manicure")
pedi, _ = ServiceCategory.objects.get_or_create(name=u"Pedicure")
mup, _ = ServiceCategory.objects.get_or_create(name=u"Makeup")
hair, _ = ServiceCategory.objects.get_or_create(name=u"Hairstyle")
wax, _ = ServiceCategory.objects.get_or_create(name=u"Depilação")
he, _ = ServiceCategory.objects.get_or_create(name=u"Massagens")
wed, _ = ServiceCategory.objects.get_or_create(name=u"Noivas")
man, _ = ServiceCategory.objects.get_or_create(name=u"Homem")


print("Created service categories: {}...".format(
    [s for s in ServiceCategory.objects.all()]))

SECONDS_PER_MINUTE = 60
SECONDS_PER_HOUR = 3600
SECONDS_PER_DAY = 86400

s1, _ = Service.objects.get_or_create(
    name="Mani Normal",
    category=mani,
    price=12,
    time_duration=(45 * SECONDS_PER_MINUTE),
    repetition_period=(31 * SECONDS_PER_DAY))
s2, _ = Service.objects.get_or_create(
    name="Mani Longa Duração",
    category=mani,
    price=15,
    time_duration=(45 * SECONDS_PER_MINUTE),
    repetition_period=(31 * SECONDS_PER_DAY))
s3, _ = Service.objects.get_or_create(
    name="Mani Verniz Gel (Gelinho)",
    category=mani,
    price=20,
    time_duration=(50 * SECONDS_PER_MINUTE),
    repetition_period=(31 * SECONDS_PER_DAY))
s4, _ = Service.objects.get_or_create(
    name="Mani Imersão em Pó",
    category=mani,
    price=24,
    time_duration=(60 * SECONDS_PER_MINUTE),
    repetition_period=(31 * SECONDS_PER_DAY))
s8, _ = Service.objects.get_or_create(
    name="Pedi Normal",
    category=pedi,
    price=22,
    time_duration=(50 * SECONDS_PER_MINUTE),
    repetition_period=(31 * SECONDS_PER_DAY))
s9, _ = Service.objects.get_or_create(
    name="Pedi Longa Duração",
    category=pedi,
    price=25,
    time_duration=(50 * SECONDS_PER_MINUTE),
    repetition_period=(31 * SECONDS_PER_DAY))
s10, _ = Service.objects.get_or_create(
    name="Pedi Verniz Gel (Gelinho)",
    category=pedi,
    price=32,
    time_duration=SECONDS_PER_HOUR,
    repetition_period=(31 * SECONDS_PER_DAY))
s11, _ = Service.objects.get_or_create(
    name="Makeup",
    category=mup,
    price=60,
    time_duration=(50 * SECONDS_PER_MINUTE),
    repetition_period=(SECONDS_PER_DAY))
# s12, _ = Service.objects.get_or_create(
#     name="Festa",
#     category=mup,
#     price=80,
#     time_duration=(30 * SECONDS_PER_MINUTE),
#     repetition_period=(SECONDS_PER_DAY))
s13, _ = Service.objects.get_or_create(
    name="Makeup Premium",
    category=mup,
    price=165,
    time_duration=(50 * SECONDS_PER_MINUTE),
    repetition_period=(SECONDS_PER_DAY))
s14, _ = Service.objects.get_or_create(
    name="Brushing",
    category=hair,
    price=25,
    time_duration=(30 * SECONDS_PER_MINUTE),
    repetition_period=(2 * SECONDS_PER_DAY))
s15, _ = Service.objects.get_or_create(
    name="Ondulado",
    category=hair,
    price=30,
    time_duration=(40 * SECONDS_PER_MINUTE),
    repetition_period=(2 * SECONDS_PER_DAY))
s16, _ = Service.objects.get_or_create(
    name="Apanhado",
    category=hair,
    price=45,
    time_duration=(50 * SECONDS_PER_MINUTE),
    repetition_period=(2 * SECONDS_PER_DAY))
s17, _ = Service.objects.get_or_create(
    name="Hairstyle Premium",
    help_text="Hairstyle com as experts das estrelas portuguesas",
    category=hair,
    price=165,
    time_duration=(50 * SECONDS_PER_MINUTE),
    repetition_period=(2 * SECONDS_PER_DAY))
s18, _ = Service.objects.get_or_create(
    name="Buço",
    category=wax,
    price=7,
    time_duration=(10 * SECONDS_PER_MINUTE),
    repetition_period=(21 * SECONDS_PER_DAY))
s19, _ = Service.objects.get_or_create(
    name="Sobrancelhas",
    category=wax,
    price=9,
    time_duration=(20 * SECONDS_PER_MINUTE),
    repetition_period=(21 * SECONDS_PER_DAY))
s20, _ = Service.objects.get_or_create(
    name="Queixo",
    category=wax,
    price=7,
    time_duration=(10 * SECONDS_PER_MINUTE),
    repetition_period=(21 * SECONDS_PER_DAY))
s21, _ = Service.objects.get_or_create(
    name="Axilas",
    category=wax,
    price=9.5,
    time_duration=(20 * SECONDS_PER_MINUTE),
    repetition_period=(21 * SECONDS_PER_DAY))
s22, _ = Service.objects.get_or_create(
    name="Virilhas",
    category=wax,
    price=10,
    time_duration=(20 * SECONDS_PER_MINUTE),
    repetition_period=(21 * SECONDS_PER_DAY))
s23, _ = Service.objects.get_or_create(
    name="Virilha total",
    category=wax,
    price=20,
    time_duration=(30 * SECONDS_PER_MINUTE),
    repetition_period=(21 * SECONDS_PER_DAY))
s24, _ = Service.objects.get_or_create(
    name="Meia perna",
    category=wax,
    price=16,
    time_duration=(20 * SECONDS_PER_MINUTE),
    repetition_period=(21 * SECONDS_PER_DAY))
s25, _ = Service.objects.get_or_create(
    name="Perna inteira",
    category=wax,
    price=23,
    time_duration=(30 * SECONDS_PER_MINUTE),
    repetition_period=(21 * SECONDS_PER_DAY))
s26, _ = Service.objects.get_or_create(
    name="Glúteos",
    category=wax,
    price=12,
    time_duration=(20 * SECONDS_PER_MINUTE),
    repetition_period=(21 * SECONDS_PER_DAY))
s27, _ = Service.objects.get_or_create(
    name="Coxa",
    category=wax,
    price=18,
    time_duration=(20 * SECONDS_PER_MINUTE),
    repetition_period=(21 * SECONDS_PER_DAY))
Service.objects.get_or_create(
    name="Manicure",
    category=man,
    subcategory=mani,
    price=12,
    time_duration=(45 * SECONDS_PER_MINUTE),
    repetition_period=(21 * SECONDS_PER_DAY))
Service.objects.get_or_create(
    name="Pedicure",
    category=man,
    subcategory=pedi,
    price=22,
    time_duration=(50 * SECONDS_PER_MINUTE),
    repetition_period=(21 * SECONDS_PER_DAY))
Service.objects.get_or_create(
    name="Remoção de Calos",
    category=man,
    subcategory=pedi,
    price=3,
    time_duration=(15 * SECONDS_PER_MINUTE),
    repetition_period=(21 * SECONDS_PER_DAY))
Service.objects.get_or_create(
    name="Sobrancelhas",
    category=man,
    subcategory=wax,
    price=9,
    time_duration=(20 * SECONDS_PER_MINUTE),
    repetition_period=(21 * SECONDS_PER_DAY))
Service.objects.get_or_create(
    name="Axilas",
    category=man,
    subcategory=wax,
    price=9.5,
    time_duration=(20 * SECONDS_PER_MINUTE),
    repetition_period=(21 * SECONDS_PER_DAY))
Service.objects.get_or_create(
    name="Costas",
    category=man,
    subcategory=wax,
    price=15,
    time_duration=(20 * SECONDS_PER_MINUTE),
    repetition_period=(21 * SECONDS_PER_DAY))
Service.objects.get_or_create(
    name="Braços",
    category=man,
    subcategory=wax,
    price=15,
    time_duration=(20 * SECONDS_PER_MINUTE),
    repetition_period=(21 * SECONDS_PER_DAY))
Service.objects.get_or_create(
    name="Braços",
    category=man,
    subcategory=wax,
    price=15,
    time_duration=(20 * SECONDS_PER_MINUTE),
    repetition_period=(21 * SECONDS_PER_DAY))
Service.objects.get_or_create(
    name="Peito",
    category=man,
    subcategory=wax,
    price=15,
    time_duration=(20 * SECONDS_PER_MINUTE),
    repetition_period=(21 * SECONDS_PER_DAY))
Service.objects.get_or_create(
    name="Meia perna",
    category=man,
    subcategory=wax,
    price=15,
    time_duration=(20 * SECONDS_PER_MINUTE),
    repetition_period=(21 * SECONDS_PER_DAY))
Service.objects.get_or_create(
    name="Massagem Drenante",
    category=he,
    price=45,
    time_duration=(45 * SECONDS_PER_MINUTE),
    repetition_period=(21 * SECONDS_PER_DAY))
Service.objects.get_or_create(
    name="Massagem Modeladora",
    category=he,
    price=45,
    time_duration=(45 * SECONDS_PER_MINUTE),
    repetition_period=(21 * SECONDS_PER_DAY))
Service.objects.get_or_create(
    name="Massagem Relaxante",
    category=he,
    price=45,
    time_duration=(45 * SECONDS_PER_MINUTE),
    repetition_period=(21 * SECONDS_PER_DAY))
Service.objects.get_or_create(
    name="Massagem a Dois",
    category=he,
    price=45,
    time_duration=(50 * SECONDS_PER_MINUTE),
    repetition_period=(21 * SECONDS_PER_DAY))

# ADD-ONS
# Manicure
m_rem_sparkl, _ = Service.objects.get_or_create(
    name="Remover Verniz Gel/Dip Sparkl",
    category=mani,
    addon_only=True,
    price=0,
    time_duration=(15 * SECONDS_PER_MINUTE),
    repetition_period=(31 * SECONDS_PER_DAY))
m_rem_outro, _ = Service.objects.get_or_create(
    name="Remover Verniz Gel/Dip feito noutro estabelecimento",
    category=mani,
    addon_only=True,
    price=6,
    time_duration=(30 * SECONDS_PER_MINUTE),
    repetition_period=(12 * SECONDS_PER_DAY))
m_rem_gel, _ = Service.objects.get_or_create(
    name="Remover gel",
    category=mani,
    addon_only=True,
    price=6,
    time_duration=(30 * SECONDS_PER_MINUTE),
    repetition_period=(12 * SECONDS_PER_DAY))
m_rem_none, _ = Service.objects.get_or_create(
    name="Sem remoção de Verniz Gel/Dip",
    category=mani,
    addon_only=True,
    price=0,
    time_duration=0,
    repetition_period=(21 * SECONDS_PER_DAY))
m_agua, _ = Service.objects.get_or_create(
    name="Com água",
    category=mani,
    addon_only=True,
    price=0,
    time_duration=0,
    repetition_period=0)
m_agua_nao, _ = Service.objects.get_or_create(
    name="Sem água",
    category=mani,
    addon_only=True,
    price=0,
    time_duration=0,
    repetition_period=0)

# Pedicure
p_rem_sparkl, _ = Service.objects.get_or_create(
    name="Remover Verniz Gel/Dip Sparkl",
    category=pedi,
    addon_only=True,
    price=0,
    time_duration=(15 * SECONDS_PER_MINUTE),
    repetition_period=(31 * SECONDS_PER_DAY))
p_rem_outro, _ = Service.objects.get_or_create(
    name="Remover Verniz Gel/Dip feito noutro estabelecimento",
    category=pedi,
    addon_only=True,
    price=6,
    time_duration=(30 * SECONDS_PER_MINUTE),
    repetition_period=(12 * SECONDS_PER_DAY))
p_rem_gel, _ = Service.objects.get_or_create(
    name="Remover gel",
    category=pedi,
    addon_only=True,
    price=6,
    time_duration=(30 * SECONDS_PER_MINUTE),
    repetition_period=(12 * SECONDS_PER_DAY))
p_rem_none, _ = Service.objects.get_or_create(
    name="Sem remoção de Verniz Gel/Dip",
    category=pedi,
    addon_only=True,
    price=0,
    time_duration=0,
    repetition_period=(21 * SECONDS_PER_DAY))
p_agua, _ = Service.objects.get_or_create(
    name="Com água",
    category=pedi,
    addon_only=True,
    price=0,
    time_duration=0,
    repetition_period=0)
p_agua_nao, _ = Service.objects.get_or_create(
    name="Sem água",
    category=pedi,
    addon_only=True,
    price=0,
    time_duration=0,
    repetition_period=0)
m_peel, _ = Service.objects.get_or_create(
    name="Com cuticle peel",
    category=mani,
    addon_only=True,
    price=0,
    time_duration=0,
    repetition_period=0)
m_peel_nao, _ = Service.objects.get_or_create(
    name="Sem cuticle peel",
    category=mani,
    addon_only=True,
    price=0,
    time_duration=0,
    repetition_period=0)

# Outros
spa, _ = Service.objects.get_or_create(
    name="Spa",
    category=mani,
    addon_only=True,
    price=10,
    time_duration=(15 * SECONDS_PER_MINUTE),
    repetition_period=(21 * SECONDS_PER_DAY))
he_extra, _ = Service.objects.get_or_create(
    name="Massagem Extra",
    category=he,
    addon_only=True,
    price=4,
    time_duration=(10 * SECONDS_PER_MINUTE),
    repetition_period=(21 * SECONDS_PER_DAY))
mani_fr, _ = Service.objects.get_or_create(
    name="Mani Francesa",
    category=mani,
    addon_only=True,
    price=3,
    time_duration=(5 * SECONDS_PER_MINUTE),
    repetition_period=(21 * SECONDS_PER_DAY))
pedi_fr, _ = Service.objects.get_or_create(
    name="Pedi Francesa",
    category=pedi,
    addon_only=True,
    price=3,
    time_duration=(5 * SECONDS_PER_MINUTE),
    repetition_period=(21 * SECONDS_PER_DAY))
p_balbcare, _ = Service.objects.get_or_create(
    name="Balbcare",
    category=pedi,
    addon_only=True,
    price=0,
    time_duration=0,
    repetition_period=0)

m_remocao_gel_choices, _ = ExtraServiceGroup.objects.get_or_create(
    name="Manicure Remoção de Gel",
    fallback_choice=m_rem_none,
    help_text="")
m_remocao_gel_choices.choices.add(m_rem_sparkl,
                                  m_rem_outro,
                                  m_rem_gel,
                                  m_rem_none)

p_remocao_gel_choices, _ = ExtraServiceGroup.objects.get_or_create(
    name="Pedicure Remoção de Gel",
    fallback_choice=p_rem_none,
    help_text="")
p_remocao_gel_choices.choices.add(p_rem_sparkl,
                                  p_rem_outro,
                                  p_rem_gel,
                                  p_rem_none)

m_agua_choices, _ = ServicePreferencesGroup.objects.get_or_create(
    name="Manicure Água",
    default_choice=m_agua_nao,
    yes_choice=m_agua,
    no_choice=m_agua_nao,
    help_text="")

m_peel_choices, _ = ServicePreferencesGroup.objects.get_or_create(
    name="Manicure Cuticle Peel",
    default_choice=m_peel_nao,
    yes_choice=m_peel,
    no_choice=m_peel_nao,
    help_text="")

p_agua_choices, _ = ServicePreferencesGroup.objects.get_or_create(
    name="Pedicure Água",
    default_choice=p_agua_nao,
    yes_choice=p_agua,
    no_choice=p_agua_nao,
    help_text="")

for s in Service.objects.filter(category=mani, addon_only=False):
    s.required_addons.add(m_remocao_gel_choices)
    s.related_preferences.add(m_agua_choices, m_peel_choices)
    s.related_services.add(spa, he_extra, mani_fr)

for s in Service.objects.filter(category=pedi, addon_only=False):
    s.required_addons.add(p_remocao_gel_choices)
    s.related_services.add(spa, he_extra, pedi_fr)
    s.related_preferences.add(p_agua_choices)
    if not s.pk == s10.pk:
        s.related_services.add(p_balbcare)

for s in Service.objects.filter(Q(category=wax) | Q(category=he)):
    s.interoperable = False
    s.save()


print("Created services: {}...".format(
    [s for s in Service.objects.all()]))


try:
    for s in Service.objects.all():
        ServiceDelivery.objects.get_or_create(service_provider=sp1,
                                              service=s)

        ServiceDelivery.objects.get_or_create(
            service_provider=sp2,
            time_duration=s.time_duration +
            int(uniform(5, 20)) * SECONDS_PER_MINUTE,
            service=s)
except IntegrityError:
    pass

print("Created {} service deliveries...".format(
    ServiceDelivery.objects.count()))

tomorrow = datetime.now() + timedelta(days=1)
work_start_time = tomorrow.replace(hour=9, minute=0, second=0, microsecond=0)
work_end_time = tomorrow.replace(hour=21, minute=0, second=0, microsecond=0)

google_calendar = build_service()

for sp in [sp1, sp2]:
    slot, _ = Slot.objects.get_or_create(
        slot_from=work_start_time,
        slot_until=work_end_time,
        service_provider=sp)
    # Slot.objects.create(
    #     slot_from=break_start_time,
    #     slot_until=break_end_time,
    #     service_provider=sp,
    #     slot_type=OFF)
    create_availability_event(google_calendar, slot)

# booking_enq = BookingEnquiry.objects.create(
#     preferred_datetime=tomorrow.replace(hour=14, minute=0, second=0),
#     customer=customer,
#     address=address3
# )
# # services should take 50 + 15 min
# booking_enq.services.add(s3, s7)
# booking_enq.prepare_booking(created=True)

# print("Created {} with services {}...".format(
#     booking_enq, booking_enq.services.all()))
