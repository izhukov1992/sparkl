#!/usr/bin/python
# -*- coding: utf-8 -*-


import django
from django.conf import settings
import os
import pandas as pd
import re

from sparkl_web.wsgi import application
from api.addresses.models import (
    Area,
    Address,
    PostalCode
)

from scripts.utils import *

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "sparkl_web.settings.dev")
django.setup()

csv_file = os.path.join(settings.BASE_DIR, 'fixtures/addresses_ctt.csv')
print("Importing offline data...")


def massage_numbers(stretch, number):
    stretch = massage_null(stretch)
    number = massage_null(number)
    if number:
        return [number]
    elif stretch and (stretch.lower().startswith("pares de") or
                      stretch.lower().startswith("impares de")):
        branch = filter(None, re.compile("\D").split(stretch))
        return [str(n) for n in xrange(int(branch[0]),
                                       int(branch[1]) + 1, 2)]
    elif stretch:
        return [stretch]
    else:
        return []


df = pd.read_csv(csv_file)

for row in df.itertuples():
    area, _ = Area.objects.get_or_create(code=massage_null(row.area_code),
                                         name=massage_null(row.area_name))

    postalcode, _ = PostalCode.objects.get_or_create(
        code=massage_null(row.postalcode_code),
        extension=massage_null(row.postalcode_extension),
        name=massage_null(row.postalcode_name))

    # FIXME: check why branch_code is suffixing a .0 (5)
    address, address_was_created = Address.objects.get_or_create(
        discrict_code=massage_null(row.district_code),
        county_code=massage_null(row.county_code),
        area=area,
        branch_code=massage_null(row.branch_code),
        branch_name=fix_whitespaces(" ".join([massage_null(row.branch_type),
                                              massage_null(row.first_prep),
                                              massage_null(row.title),
                                              massage_null(row.second_prep),
                                              massage_null(row.branch_name)])),
        venue_name=massage_null(row.name) or massage_null(row.venue),
        postal_code=postalcode,
        number=None,
        source="CTT")

    numbers = massage_numbers(row.stretch, row.number)
    if len(numbers) > 1:
        try:
            address_values = address.__dict__
            address_values.pop('id', None)
            address_values.pop('_state', None)
            address_values.pop('_area_cache', None)
            address_values.pop('_postal_code_cache', None)
            address_list = []
            for number in numbers:
                address_values['number'] = number
                address_list.append(Address(**address_values))
            Address.objects.bulk_create(address_list)
            if address_was_created and hasattr(address, 'pk'):
                address.delete()
        except Exception as e:
            print(e)
            import ipdb; ipdb.set_trace()
    elif len(numbers) == 1:
        address.number = numbers[0]
        address.save()
    else:
        address.save()

    print("Row {}...".format(row.Index))

print("{} addresses imported successfully.".format(Address.objects.count()))
