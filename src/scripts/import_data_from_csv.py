#!/usr/bin/python
# -*- coding: utf-8 -*-

import django
import pandas as pd
import os
from sparkl_web.wsgi import application

from api.bookings.models import Booking
from api.profiles.models import (
    Customer,
    ServiceProvider
)
from api.services.models import (
    Service,
    ServiceCategory,
    Venue,
    Channel
)

from scripts.utils import *

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "sparkl_web.settings.dev")
django.setup()

filename = "fixtures/report_analysis_2017_11_24.csv"


df = pd.read_csv(filename)
print("Importing offline data...")

for row in df.itertuples():
    customer, _ = Customer.objects.get_or_create(
        mobile_number=massage_null(row.contact))
    for attr in ['name', 'address', 'area', 'nif']:
        if not getattr(customer, attr):
            setattr(customer, attr, massage_null(getattr(row, attr)))
    customer.save()
    # TODO: split addresses in Address, Area and PostalCode (3)

    venue, _ = Venue.objects.get_or_create(
        name=massage_srt(massage_null(row.venue)))

    services = []
    for category in ['manicure', 'pedicure', 'waxing']:
        service = getattr(row, category)
        if not pd.isna(service):
            c, _ = ServiceCategory.objects.get_or_create(
                name=massage_srt(category))
            s, _ = Service.objects.get_or_create(
                name=massage_srt(service), category=c)
            services.append(s)
    if (not pd.isna(row.gel) and
            row.gel == "sim" or
            row.gel == "remover gel"):
        c, _ = ServiceCategory.objects.get_or_create(name="Manicure")
        s, _ = Service.objects.get_or_create(name="Gel", category=c)
        services.append(s)
    if (not pd.isna(row.calluses) and
            row.gel == "sim" or
            row.gel == "calos"):
        c = ServiceCategory.objects.get_or_create(name="Pedicure")
        s, _ = Service.objects.get_or_create(name="Calos", category=c)
        services.append(s)

    service_provider, _ = ServiceProvider.objects.get_or_create(
        name=massage_srt(row.service_provider))

    paid = True if row.paid == "sim" else False
    night_tax = True if not pd.isna(row.night_taxation) and \
        row.night_taxation == "sim" else False
    rep = True if (not pd.isna(row.repeated) and
                   row.repeated.lower() == "sim") else False

    channel, _ = Channel.objects.get_or_create(name=row.channel.title())
    percentage = row.percentage if not pd.isna(row.percentage) else 0

    booking = Booking.objects.create(
        row_idx=str(row.Index),
        datetime=parse_datetime(row.date, row.month, row.time),
        customer=customer,
        service_provider=service_provider,
        services_count=row.services_count,
        amount_paid=massage_float(row.paid_value),
        amount_revenue=massage_float(row.amount_revenue),
        amount_profit=massage_float(row.amount_profit),
        visits_count=massage_float(row.visits_count),
        opportunities=massage_null(row.opportunity),
        channel=channel,
        percentage=percentage,
        paid=paid,
        venue=venue,
        repeated=rep,
        reservation_weekday=massage_srt(
            massage_null(row.reservation_weekday)),
        feedback=massage_null(row.feedback),
        details=massage_null(row.details),
        type_of_service=massage_null(row.type_of_service).lower())
    booking.services.add(*services)
    print("Row {}...".format(row.Index))

print("Done.")
