#!/usr/bin/python
# -*- coding: utf-8 -*-

from datetime import datetime
import pandas as pd
import re


month_dict = {
    'janeiro': 1,
    'fevereiro': 2,
    'março': 3,
    'abril': 4,
    'maio': 5,
    'junho': 6,
    'julho': 7,
    'agosto': 8,
    'setembro': 9,
    'outubro': 10,
    'novembro': 11,
    'dezembro': 12,
}


def parse_datetime(date, month, time):
    if not time.split("h")[1]:
        time = time + "00"
    elif len(time.split("-")) == 2:
        time = time.split("-")[0]
    elif len(time.split(" e ")) == 2:
        time = time.split(" e ")[0]
    elif not time:
        time = "09h00"
    return datetime.strptime("{}-{}-2017  {}".format(
        date, month_dict.get(month.lower()), time), '%d-%m-%Y %Hh%M')


def massage_null(value):
    return value if not pd.isna(value) else ""


def massage_float(f):
    if pd.isna(f):
        return 0.0
    return f if isinstance(f, float) else float(f.replace(',', '.'))


def massage_srt(s):
    return " ".join(re.compile("\s+").split(s)).title()


def split_name(name):
    n = name.split(' ')
    if len(n) > 1:
        return " ".join(n[:len(n)]), n[-1]
    else:
        return name, ""


def fix_whitespaces(s):
    return re.sub(' +', ' ', s)
