#!/usr/bin/python
# -*- coding: utf-8 -*-

import django
import os
from sparkl_web.wsgi import application

from api.profiles.models import Customer
from api.bookings.models import Booking

from difflib import get_close_matches
from pprint import pprint

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "sparkl_web.settings.dev")
django.setup()

all_customers_name = [c.name for c in Customer.objects.all()]

# d = {}

# for name in all_customers_name:
#     matches = get_close_matches(name, all_customers_name)
#     if matches:
#         d[name] = matches

# for name, matches in d.iteritems():
#     pprint("{}: {}".format(name.encode('utf-8'), ', '.join(matches)))

# find possible duplicates
possible_dups = {}
all_customers = Customer.objects.all()

for customer in all_customers:
    if len(Customer.objects.filter(mobile_number=customer.mobile_number)) > 1:
        if possible_dups.get(customer.mobile_number):
            possible_dups[customer.mobile_number].add(customer.pk)
        else:
            possible_dups[customer.mobile_number] = {customer.pk}

# discard duplicates, assing first customer
# for key, customers in possible_dups.iteritems():
#     bookings = Booking.objects.filter(
#         customer__in=Customer.objects.filter(pk__in=customers))
#     new_c = customers.pop()
#     for b in bookings:
#         b.customer = new_c
#         b.save()

# booking_list = {}
# for key, bookings in possible_dups.iteritems():
#     booking_list[key] = [int(b.row_idx) + 2 for b in bookings]


def grade_customer_data(customers):
    customer_rating = {}
    for customer in customers:
        rating = 0
        for attr in ['name', 'address', 'area', 'nif']:
            if getattr(customer, attr):
                rating += 1
        customer_rating[customer] = rating
    import operator
    return max(customer_rating.iteritems(),
               key=operator.itemgetter(1))[0]


def get_customer_details(mobile_number):
    customers = Customer.objects.filter(mobile_number=mobile_number)
    # print("{} found.\n".format(len(customers)))
    # [c.inspect_details() for c in customers]
    # if len(customers) > 1:
    #     print(20*'*')
    #     print("Zis iz ze bezt guess:\n")
    #     grade_customer_data(customers).inspect_details()
    #     print(20*'*')
    return grade_customer_data(customers)


def find_related_bookings(mobile_number):
    bookings = Booking.objects.filter(
        customer__in=Customer.objects.filter(mobile_number=mobile_number))
    # print([b.row_idx + 2 for b in bookings])
    return bookings


def correct_bookings(bookings, new_c):
    for b in bookings:
        b.customer = new_c
        b.save()


for customer_mobile_number, dups in possible_dups.iteritems():
    customer = get_customer_details(customer_mobile_number)
    bookings = find_related_bookings(customer_mobile_number)
    correct_bookings(bookings, customer)

"""
get customer details
decide best customer
find bookings
assing customer to all bookings
"""
