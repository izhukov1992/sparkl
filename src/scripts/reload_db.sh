#!/bin/bash

stty sane
find . -name \*.pyc -type f -delete
# find -type d -wholename ./api/accounts/migrations -exec rm -rf {} \;
# find -type d -wholename ./api/addresses/migrations -exec rm -rf {} \;
# find -type d -wholename ./api/profiles/migrations -exec rm -rf {} \;
# find -type d -wholename ./api/schedule/migrations -exec rm -rf {} \;
# find -type d -wholename ./api/services/migrations -exec rm -rf {} \;
# find -type d -wholename ./api/navigation/migrations -exec rm -rf {} \;
# find -type d -wholename ./api/bookings/migrations -exec rm -rf {} \;
# find -type d -wholename ./api/business/migrations -exec rm -rf {} \;

./manage.py flush

# ./manage.py makemigrations accounts
./manage.py makemigrations addresses
./manage.py makemigrations profiles
./manage.py makemigrations schedule
./manage.py makemigrations services
./manage.py makemigrations navigation
./manage.py makemigrations bookings
./manage.py makemigrations business
./manage.py makemigrations payments
./manage.py migrate

# ./manage.py loaddata fixtures/addresses_initial_data.json
# python ../generate_data.py
