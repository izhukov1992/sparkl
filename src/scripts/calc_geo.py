# -*- coding: utf-8 -*-
from geopy.distance import vincenty
import requests


gmaps_key = "AIzaSyCCSquQjr_RxRmBk9tu-hoyL6Ng01fO9sg"
gmaps_url = "https://maps.googleapis.com/maps/api/"
gmaps_geocode_url = gmaps_url + "geocode/json?"
gmaps_distance_url = gmaps_url + "distancematrix/json?"

service_providers_geo = {
    'Ines': 'R. Domingos Saraiva 14, 2725-079 Algueirão-Mem Martins, Portugal',
    'Helena': 'Rotunda Arnaldo Dias, 2675-232 Odivelas, Portugal',
    'Thaina': 'Av. Praia da Vitória 44, 1000-117 Lisboa, Portugal'
}
address = "R. Domingos Sequeira 30, 1250-096 Lisboa"


class ServiceProvider(object):
    def __init__(self, name, location):
        self._name = name
        self._location = location

    def get_name(self):
        return self._name

    def get_location(self):
        return self._location


class TestGeo(object):
    def __init__(self, location_dict, address):
        self._location_dict = location_dict
        self._sp_coords = {}
        self._address = address

    def ask_gmaps(self, url, params):
        resp = requests.get(url, params=params)
        # if resp.get('status') == "ZERO_RESULTS":
        #     print("Search returned zero results for '{}'.".format(
        #         params.get('address')))
        #     exit(1)

        return resp.json()

    def closest_service_provider(self, service_providers):
        return min(service_providers, key=service_providers.get)

    def coords_from_gmaps_resp(self, resp):
        coords = resp.get('results')[0].get('geometry').get('location')
        return (coords.get('lat'), coords.get('lng'))

    def itinerary_from_gmaps_resp(self, resp):
        # coords = resp.get('rows')
        # [0].get('geometry').get('location')
        return resp.get('rows')

    def calc_itinerary(self, address_from, address_to):
        result = self.ask_gmaps(gmaps_distance_url,
                                {'key': gmaps_key,
                                 'units': 'metric',
                                 'origins': "{},{}".format(*address_from),
                                 'destinations': "{},{}".format(*address_to)})
        return result

    def run_each(self, service_provider, location, destination):
        gmaps_coord_from = self.ask_gmaps(gmaps_geocode_url,
                                          {'key': gmaps_key,
                                           'address': location})
        coord_from = self.coords_from_gmaps_resp(gmaps_coord_from)
        self._sp_coords[service_provider] = coord_from

        if not isinstance(self._address, tuple):
            gmaps_coord_to = self.ask_gmaps(gmaps_geocode_url,
                                            {'key': gmaps_key,
                                             'address': destination})
            coord_to = self.coords_from_gmaps_resp(gmaps_coord_to)
            self._address = coord_to
        else:
            coord_to = self._address

        d = vincenty(coord_from, coord_to)
        return d.km

    def get_distance_matrix(self, service_providers_calc):
        # abs(subrtracao de distancias) < 1km ?
        tt = len(service_providers_calc) - 1
        t = tt
        while tt > 1:
            t += tt - 1
            tt -= 1
        print(t)

    def run(self):
        service_providers_calc = {}
        for service_provider, location in self._location_dict.iteritems():
            service_providers_calc[service_provider] = self.run_each(
                service_provider, location, self._address)
        result = self.closest_service_provider(service_providers_calc)

        print(service_providers_calc)
        print(result)
        print(self._sp_coords)
        print(self._address)

        itenirary = self.calc_itinerary(self._sp_coords[result],
                                        self._address)

        print(itenirary)
        self.get_distance_matrix(service_providers_calc)

        return result


if __name__ == "__main__":
    g = TestGeo(service_providers_geo, address)
    g.run()
