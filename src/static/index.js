import React from 'react';
import ReactDOM from 'react-dom';
import createHistory from 'history/createBrowserHistory';

import Root from './containers/Root/Root';
import configureStore from './store/configureStore';
import registerSW from './registerSW';

const initialState = {};
const target = document.getElementById('root');

const history = createHistory();
const { store, persistor } = configureStore(initialState, history);
registerSW();

const node = (
    <Root store={store} persistor={persistor} history={history} />
);

const token = sessionStorage.getItem('token');
let user = {};
try {
    user = JSON.parse(sessionStorage.getItem('user'));
} catch (e) {
    // Failed to parse
}

ReactDOM.render(node, target);
