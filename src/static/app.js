import React from 'react';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import classNames from 'classnames';
import PropTypes from 'prop-types';

import { Header, Footer } from './components';
import { hideModal } from './actions/modal';
import { PATH_POSTAL, PATH_CONFIRM, PATH_CATEGORIES } from './constants';
import Routes from './routes';

import './styles/Elements.css';
import './styles/style.css';

class App extends React.Component {
  componentWillMount() {
    this.props.hideModal();
  }

  static propTypes = {
    location: PropTypes.shape({
      pathname: PropTypes.string
    })
  };

  getHeader() {
    if (this.props.location && this.props.location.pathname !== PATH_POSTAL && this.props.location.pathname !== PATH_CONFIRM) {
      return (<Header />);
    }
    else {
      return null;
    }
  }
  
  getFooter() {
    if (this.props.location && this.props.location.pathname !== PATH_POSTAL && this.props.location.pathname !== PATH_CONFIRM && this.props.location.pathname !== PATH_CATEGORIES) {
      return (<Footer onBack={this.props.onBack} onNext={this.props.onNext} checkNext={this.props.checkNext} />);
    }
    else {
      return null;
    }
  }

  render() {
    return (
      <main>
        {this.getHeader()}
        {this.props.children}
        {this.getFooter()}
      </main>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    location: state.routing.location
  };
};

export default connect(mapStateToProps, { hideModal })(App);
export { App as AppNotConnected };
