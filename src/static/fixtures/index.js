export const services = [
  {
    "category": {
      "id": 1,
      "weight": 0,
      "name": "Manicure",
      "overlay_img": "images/ima_service_manicure.jpg"
    },
    "name": "Mani Normal",
    "weight": 0,
    "time_duration": 2700,
    "required_addons": [
      {
        "fallback_if_any_service": [
          {
            "id": 2
          },
          {
            "id": 3
          }
        ],
        "name": "Manicure Remoção de Gel",
        "fallback_choice": {
          "id": 40
        },
        "choices": [
          {
            "id": 37
          },
          {
            "id": 38
          },
          {
            "id": 39
          },
          {
            "id": 40
          }
        ],
        "fallback_if_all_services": [
          {
            "id": 4
          },
        ],
        "help_text": "",
        "id": 1
      }
    ],
    "price": 12,
    "related_services": [
      {
        "id": 51
      },
      {
        "id": 52
      },
      {
        "id": 53
      },
      {
        "id": 2
      }
    ],
    "addon_only": false,
    "help_text": "This example text was added just in purposes of testing to see possible negative effects",
    "id": 1
  },
  {
    "category": {
      "id": 1,
      "weight": 0,
      "name": "Manicure",
      "overlay_img": "images/ima_service_manicure.jpg"
    },
    "name": "Mani Longa Duração",
    "weight": 0,
    "time_duration": 2700,
    "required_addons": [
      {
        "fallback_if_any_service": [

        ],
        "name": "Manicure Remoção de Gel",
        "fallback_choice": {
          "id": 40
        },
        "choices": [
          {
            "id": 37
          },
          {
            "id": 38
          },
          {
            "id": 39
          },
          {
            "id": 40
          }
        ],
        "fallback_if_all_services": [

        ],
        "help_text": "",
        "id": 1
      }
    ],
    "price": 15,
    "related_services": [
      {
        "id": 51
      },
      {
        "id": 52
      },
      {
        "id": 53
      }
    ],
    "addon_only": false,
    "help_text": "This example text was added just in purposes of testing to see possible negative effects",
    "id": 2
  },
  {
    "category": {
      "id": 1,
      "weight": 0,
      "name": "Manicure",
      "overlay_img": "images/ima_service_manicure.jpg"
    },
    "name": "Mani Verniz Gel (Gelinho)",
    "weight": 0,
    "time_duration": 3000,
    "required_addons": [
      {
        "fallback_if_any_service": [

        ],
        "name": "Manicure Remoção de Gel",
        "fallback_choice": {
          "id": 40
        },
        "choices": [
          {
            "id": 37
          },
          {
            "id": 38
          },
          {
            "id": 39
          },
          {
            "id": 40
          }
        ],
        "fallback_if_all_services": [

        ],
        "help_text": "",
        "id": 1
      }
    ],
    "price": 20,
    "related_services": [
      {
        "id": 51
      },
      {
        "id": 52
      },
      {
        "id": 53
      }
    ],
    "addon_only": false,
    "help_text": "This example text was added just in purposes of testing to see possible negative effects",
    "id": 3
  },
  {
    "category": {
      "id": 1,
      "weight": 0,
      "name": "Manicure",
      "overlay_img": "images/ima_service_manicure.jpg"
    },
    "name": "Mani Imersão em Pó",
    "weight": 0,
    "time_duration": 3600,
    "required_addons": [
      {
        "fallback_if_any_service": [

        ],
        "name": "Manicure Remoção de Gel",
        "fallback_choice": {
          "id": 40
        },
        "choices": [
          {
            "id": 37
          },
          {
            "id": 38
          },
          {
            "id": 39
          },
          {
            "id": 40
          }
        ],
        "fallback_if_all_services": [

        ],
        "help_text": "",
        "id": 1
      }
    ],
    "price": 24,
    "related_services": [
      {
        "id": 51
      },
      {
        "id": 52
      },
      {
        "id": 53
      }
    ],
    "addon_only": false,
    "help_text": "This example text was added just in purposes of testing to see possible negative effects",
    "id": 4
  },
  {
    "category": {
      "id": 2,
      "weight": 0,
      "name": "Pedicure",
      "overlay_img": null
    },
    "name": "Pedi Normal",
    "weight": 0,
    "time_duration": 3000,
    "required_addons": [
      {
        "fallback_if_any_service": [

        ],
        "name": "Pedicure Remoção de Gel",
        "fallback_choice": {
          "id": 46
        },
        "choices": [
          {
            "id": 43
          },
          {
            "id": 44
          },
          {
            "id": 45
          },
          {
            "id": 46
          }
        ],
        "fallback_if_all_services": [

        ],
        "help_text": "",
        "id": 2
      }
    ],
    "price": 22,
    "related_services": [
      {
        "id": 51
      },
      {
        "id": 52
      },
      {
        "id": 54
      },
      {
        "id": 55
      }
    ],
    "addon_only": false,
    "help_text": "This example text was added just in purposes of testing to see possible negative effects",
    "id": 5
  },
  {
    "category": {
      "id": 2,
      "weight": 0,
      "name": "Pedicure",
      "overlay_img": null
    },
    "name": "Pedi Longa Duração",
    "weight": 0,
    "time_duration": 3000,
    "required_addons": [
      {
        "fallback_if_any_service": [

        ],
        "name": "Pedicure Remoção de Gel",
        "fallback_choice": {
          "id": 46
        },
        "choices": [
          {
            "id": 43
          },
          {
            "id": 44
          },
          {
            "id": 45
          },
          {
            "id": 46
          }
        ],
        "fallback_if_all_services": [

        ],
        "help_text": "",
        "id": 2
      }
    ],
    "price": 25,
    "related_services": [
      {
        "id": 51
      },
      {
        "id": 52
      },
      {
        "id": 54
      },
      {
        "id": 55
      }
    ],
    "addon_only": false,
    "help_text": "This example text was added just in purposes of testing to see possible negative effects",
    "id": 6
  },
  {
    "category": {
      "id": 2,
      "weight": 0,
      "name": "Pedicure",
      "overlay_img": null
    },
    "name": "Pedi Verniz Gel (Gelinho)",
    "weight": 0,
    "time_duration": 3600,
    "required_addons": [
      {
        "fallback_if_any_service": [

        ],
        "name": "Pedicure Remoção de Gel",
        "fallback_choice": {
          "id": 46
        },
        "choices": [
          {
            "id": 43
          },
          {
            "id": 44
          },
          {
            "id": 45
          },
          {
            "id": 46
          }
        ],
        "fallback_if_all_services": [

        ],
        "help_text": "",
        "id": 2
      }
    ],
    "price": 32,
    "related_services": [
      {
        "id": 51
      },
      {
        "id": 52
      },
      {
        "id": 54
      }
    ],
    "addon_only": false,
    "help_text": "This example text was added just in purposes of testing to see possible negative effects",
    "id": 7
  },
  {
    "category": {
      "id": 3,
      "weight": 0,
      "name": "Makeup",
      "overlay_img": "images/ima_service_makeup.jpg"
    },
    "name": "Makeup",
    "weight": 0,
    "time_duration": 3000,
    "help_text": "This example text was added just in purposes of testing to see possible negative effects",
    "price": 60,
    "id": 8,
    "addon_only": false
  },
  {
    "category": {
      "id": 3,
      "weight": 0,
      "name": "Makeup",
      "overlay_img": "images/ima_service_makeup.jpg"
    },
    "name": "Makeup Premium",
    "weight": 0,
    "time_duration": 3000,
    "help_text": "This example text was added just in purposes of testing to see possible negative effects",
    "price": 165,
    "id": 9,
    "addon_only": false
  },
  {
    "category": {
      "id": 4,
      "weight": 0,
      "name": "Hairstyle",
      "overlay_img": "images/ima_service_hairstyle.jpg"
    },
    "name": "Brushing",
    "weight": 0,
    "time_duration": 1800,
    "help_text": "This example text was added just in purposes of testing to see possible negative effects",
    "price": 25,
    "id": 10,
    "addon_only": false
  },
  {
    "category": {
      "id": 4,
      "weight": 0,
      "name": "Hairstyle",
      "overlay_img": "images/ima_service_hairstyle.jpg"
    },
    "name": "Ondulado",
    "weight": 0,
    "time_duration": 2400,
    "help_text": "This example text was added just in purposes of testing to see possible negative effects",
    "price": 30,
    "id": 11,
    "addon_only": false
  },
  {
    "category": {
      "id": 4,
      "weight": 0,
      "name": "Hairstyle",
      "overlay_img": "images/ima_service_hairstyle.jpg"
    },
    "name": "Apanhado",
    "weight": 0,
    "time_duration": 3000,
    "help_text": "This example text was added just in purposes of testing to see possible negative effects",
    "price": 45,
    "id": 12,
    "addon_only": false
  },
  {
    "category": {
      "id": 4,
      "weight": 0,
      "name": "Hairstyle",
      "overlay_img": "images/ima_service_hairstyle.jpg"
    },
    "name": "Hairstyle Premium",
    "weight": 0,
    "time_duration": 3000,
    "help_text": "Hairstyle com as experts das estrelas portuguesas",
    "price": 165,
    "id": 13,
    "addon_only": false
  },
  {
    "category": {
      "id": 5,
      "weight": 0,
      "name": "Depilação",
      "overlay_img": "images/ima_service_depilacao.jpg"
    },
    "name": "Buço",
    "weight": 0,
    "time_duration": 600,
    "help_text": "This example text was added just in purposes of testing to see possible negative effects",
    "price": 7,
    "id": 14,
    "addon_only": false
  },
  {
    "category": {
      "id": 5,
      "weight": 0,
      "name": "Depilação",
      "overlay_img": "images/ima_service_depilacao.jpg"
    },
    "name": "Sobrancelhas",
    "weight": 0,
    "time_duration": 1200,
    "help_text": "This example text was added just in purposes of testing to see possible negative effects",
    "price": 9,
    "id": 15,
    "addon_only": false
  },
  {
    "category": {
      "id": 5,
      "weight": 0,
      "name": "Depilação",
      "overlay_img": "images/ima_service_depilacao.jpg"
    },
    "name": "Queixo",
    "weight": 0,
    "time_duration": 600,
    "help_text": "This example text was added just in purposes of testing to see possible negative effects",
    "price": 7,
    "id": 16,
    "addon_only": false
  },
  {
    "category": {
      "id": 5,
      "weight": 0,
      "name": "Depilação",
      "overlay_img": "images/ima_service_depilacao.jpg"
    },
    "name": "Axilas",
    "weight": 0,
    "time_duration": 1200,
    "help_text": "This example text was added just in purposes of testing to see possible negative effects",
    "price": 9.5,
    "id": 17,
    "addon_only": false
  },
  {
    "category": {
      "id": 5,
      "weight": 0,
      "name": "Depilação",
      "overlay_img": "images/ima_service_depilacao.jpg"
    },
    "name": "Virilhas",
    "weight": 0,
    "time_duration": 1200,
    "help_text": "This example text was added just in purposes of testing to see possible negative effects",
    "price": 10,
    "id": 18,
    "addon_only": false
  },
  {
    "category": {
      "id": 5,
      "weight": 0,
      "name": "Depilação",
      "overlay_img": "images/ima_service_depilacao.jpg"
    },
    "name": "Virilha total",
    "weight": 0,
    "time_duration": 1800,
    "help_text": "This example text was added just in purposes of testing to see possible negative effects",
    "price": 20,
    "id": 19,
    "addon_only": false
  },
  {
    "category": {
      "id": 5,
      "weight": 0,
      "name": "Depilação",
      "overlay_img": "images/ima_service_depilacao.jpg"
    },
    "name": "Meia perna",
    "weight": 0,
    "time_duration": 1200,
    "help_text": "This example text was added just in purposes of testing to see possible negative effects",
    "price": 16,
    "id": 20,
    "addon_only": false
  },
  {
    "category": {
      "id": 5,
      "weight": 0,
      "name": "Depilação",
      "overlay_img": "images/ima_service_depilacao.jpg"
    },
    "name": "Perna inteira",
    "weight": 0,
    "time_duration": 1800,
    "help_text": "This example text was added just in purposes of testing to see possible negative effects",
    "price": 23,
    "id": 21,
    "addon_only": false
  },
  {
    "category": {
      "id": 5,
      "weight": 0,
      "name": "Depilação",
      "overlay_img": "images/ima_service_depilacao.jpg"
    },
    "name": "Glúteos",
    "weight": 0,
    "time_duration": 1200,
    "help_text": "This example text was added just in purposes of testing to see possible negative effects",
    "price": 12,
    "id": 22,
    "addon_only": false
  },
  {
    "category": {
      "id": 5,
      "weight": 0,
      "name": "Depilação",
      "overlay_img": "images/ima_service_depilacao.jpg"
    },
    "name": "Coxa",
    "weight": 0,
    "time_duration": 1200,
    "help_text": "This example text was added just in purposes of testing to see possible negative effects",
    "price": 18,
    "id": 23,
    "addon_only": false
  },
  {
    "category": {
      "id": 8,
      "weight": 0,
      "name": "Homem",
      "overlay_img": "images/ima_service_noivas.jpg"
    },
    "subcategory": {
      "id": 1,
      "weight": 0,
      "name": "Manicure"
    },
    "name": "Manicure",
    "weight": 0,
    "time_duration": 2700,
    "help_text": "This example text was added just in purposes of testing to see possible negative effects",
    "price": 12,
    "id": 24,
    "addon_only": false
  },
  {
    "category": {
      "id": 8,
      "weight": 0,
      "name": "Homem",
      "overlay_img": "images/ima_service_noivas.jpg"
    },
    "subcategory": {
      "id": 2,
      "weight": 0,
      "name": "Pedicure"
    },
    "name": "Pedicure",
    "weight": 0,
    "time_duration": 3000,
    "help_text": "This example text was added just in purposes of testing to see possible negative effects",
    "price": 22,
    "id": 25,
    "addon_only": false
  },
  {
    "category": {
      "id": 8,
      "weight": 0,
      "name": "Homem",
      "overlay_img": "images/ima_service_noivas.jpg"
    },
    "subcategory": {
      "id": 2,
      "weight": 0,
      "name": "Pedicure"
    },
    "name": "Remoção de Calos",
    "weight": 0,
    "time_duration": 900,
    "help_text": "This example text was added just in purposes of testing to see possible negative effects",
    "price": 3,
    "id": 26,
    "addon_only": false
  },
  {
    "category": {
      "id": 8,
      "weight": 0,
      "name": "Homem",
      "overlay_img": "images/ima_service_noivas.jpg"
    },
    "subcategory": {
      "id": 5,
      "weight": 0,
      "name": "Depilação"
    },
    "name": "Sobrancelhas",
    "weight": 0,
    "time_duration": 1200,
    "help_text": "This example text was added just in purposes of testing to see possible negative effects",
    "price": 9,
    "id": 27,
    "addon_only": false
  },
  {
    "category": {
      "id": 8,
      "weight": 0,
      "name": "Homem",
      "overlay_img": "images/ima_service_noivas.jpg"
    },
    "subcategory": {
      "id": 5,
      "weight": 0,
      "name": "Depilação"
    },
    "name": "Axilas",
    "weight": 0,
    "time_duration": 1200,
    "help_text": "This example text was added just in purposes of testing to see possible negative effects",
    "price": 9.5,
    "id": 28,
    "addon_only": false
  },
  {
    "category": {
      "id": 8,
      "weight": 0,
      "name": "Homem",
      "overlay_img": "images/ima_service_noivas.jpg"
    },
    "subcategory": {
      "id": 5,
      "weight": 0,
      "name": "Depilação"
    },
    "name": "Costas",
    "weight": 0,
    "time_duration": 1200,
    "help_text": "This example text was added just in purposes of testing to see possible negative effects",
    "price": 15,
    "id": 29,
    "addon_only": false
  },
  {
    "category": {
      "id": 8,
      "weight": 0,
      "name": "Homem",
      "overlay_img": "images/ima_service_noivas.jpg"
    },
    "subcategory": {
      "id": 5,
      "weight": 0,
      "name": "Depilação"
    },
    "name": "Braços",
    "weight": 0,
    "time_duration": 1200,
    "help_text": "This example text was added just in purposes of testing to see possible negative effects",
    "price": 15,
    "id": 30,
    "addon_only": false
  },
  {
    "category": {
      "id": 8,
      "weight": 0,
      "name": "Homem",
      "overlay_img": "images/ima_service_noivas.jpg"
    },
    "subcategory": {
      "id": 5,
      "weight": 0,
      "name": "Depilação"
    },
    "name": "Peito",
    "weight": 0,
    "time_duration": 1200,
    "help_text": "This example text was added just in purposes of testing to see possible negative effects",
    "price": 15,
    "id": 31,
    "addon_only": false
  },
  {
    "category": {
      "id": 8,
      "weight": 0,
      "name": "Homem",
      "overlay_img": "images/ima_service_noivas.jpg"
    },
    "subcategory": {
      "id": 5,
      "weight": 0,
      "name": "Depilação"
    },
    "name": "Meia perna",
    "weight": 0,
    "time_duration": 1200,
    "help_text": "This example text was added just in purposes of testing to see possible negative effects",
    "price": 15,
    "id": 32,
    "addon_only": false
  },
  {
    "category": {
      "id": 6,
      "weight": 0,
      "name": "Massagens",
      "overlay_img": "images/ima_service_massagens.jpg"
    },
    "name": "Massagem Drenante",
    "weight": 0,
    "time_duration": 2700,
    "help_text": "This example text was added just in purposes of testing to see possible negative effects",
    "price": 45,
    "id": 33,
    "addon_only": false
  },
  {
    "category": {
      "id": 6,
      "weight": 0,
      "name": "Massagens",
      "overlay_img": "images/ima_service_massagens.jpg"
    },
    "name": "Massagem Modeladora",
    "weight": 0,
    "time_duration": 2700,
    "help_text": "This example text was added just in purposes of testing to see possible negative effects",
    "price": 45,
    "id": 34,
    "addon_only": false
  },
  {
    "category": {
      "id": 6,
      "weight": 0,
      "name": "Massagens",
      "overlay_img": "images/ima_service_massagens.jpg"
    },
    "name": "Massagem Relaxante",
    "weight": 0,
    "time_duration": 2700,
    "help_text": "This example text was added just in purposes of testing to see possible negative effects",
    "price": 45,
    "id": 35,
    "addon_only": false
  },
  {
    "category": {
      "id": 6,
      "weight": 0,
      "name": "Massagens",
      "overlay_img": "images/ima_service_massagens.jpg"
    },
    "name": "Massagem a Dois",
    "weight": 0,
    "time_duration": 3000,
    "help_text": "This example text was added just in purposes of testing to see possible negative effects",
    "price": 45,
    "id": 36,
    "addon_only": false
  },
  {
    "category": {
      "id": 1,
      "weight": 0,
      "name": "Manicure",
      "overlay_img": "images/ima_service_manicure.jpg"
    },
    "name": "Remover Verniz Gel/Dip Sparkl",
    "weight": 0,
    "time_duration": 900,
    "help_text": "This example text was added just in purposes of testing to see possible negative effects",
    "price": 0,
    "id": 37,
    "addon_only": true
  },
  {
    "category": {
      "id": 1,
      "weight": 0,
      "name": "Manicure",
      "overlay_img": "images/ima_service_manicure.jpg"
    },
    "name": "Remover Verniz Gel/Dip feito noutro estabelecimento",
    "weight": 0,
    "time_duration": 1800,
    "help_text": "This example text was added just in purposes of testing to see possible negative effects",
    "price": 6,
    "id": 38,
    "addon_only": true
  },
  {
    "category": {
      "id": 1,
      "weight": 0,
      "name": "Manicure",
      "overlay_img": "images/ima_service_manicure.jpg"
    },
    "name": "Remover gel",
    "weight": 0,
    "time_duration": 1800,
    "help_text": "This example text was added just in purposes of testing to see possible negative effects",
    "price": 6,
    "id": 39,
    "addon_only": true
  },
  {
    "category": {
      "id": 1,
      "weight": 0,
      "name": "Manicure",
      "overlay_img": "images/ima_service_manicure.jpg"
    },
    "name": "Sem remoção de Verniz Gel/Dip",
    "weight": 0,
    "time_duration": 0,
    "help_text": "This example text was added just in purposes of testing to see possible negative effects",
    "price": 0,
    "id": 40,
    "addon_only": true
  },
  {
    "category": {
      "id": 1,
      "weight": 0,
      "name": "Manicure",
      "overlay_img": "images/ima_service_manicure.jpg"
    },
    "name": "Com água",
    "weight": 0,
    "time_duration": 0,
    "help_text": "This example text was added just in purposes of testing to see possible negative effects",
    "price": 0,
    "id": 41,
    "addon_only": true
  },
  {
    "category": {
      "id": 1,
      "weight": 0,
      "name": "Manicure",
      "overlay_img": "images/ima_service_manicure.jpg"
    },
    "name": "Sem água",
    "weight": 0,
    "time_duration": 0,
    "help_text": "This example text was added just in purposes of testing to see possible negative effects",
    "price": 0,
    "id": 42,
    "addon_only": true
  },
  {
    "category": {
      "id": 2,
      "weight": 0,
      "name": "Pedicure",
      "overlay_img": null
    },
    "name": "Remover Verniz Gel/Dip Sparkl",
    "weight": 0,
    "time_duration": 900,
    "help_text": "This example text was added just in purposes of testing to see possible negative effects",
    "price": 0,
    "id": 43,
    "addon_only": true
  },
  {
    "category": {
      "id": 2,
      "weight": 0,
      "name": "Pedicure",
      "overlay_img": null
    },
    "name": "Remover Verniz Gel/Dip feito noutro estabelecimento",
    "weight": 0,
    "time_duration": 1800,
    "help_text": "This example text was added just in purposes of testing to see possible negative effects",
    "price": 6,
    "id": 44,
    "addon_only": true
  },
  {
    "category": {
      "id": 2,
      "weight": 0,
      "name": "Pedicure",
      "overlay_img": null
    },
    "name": "Remover gel",
    "weight": 0,
    "time_duration": 1800,
    "help_text": "This example text was added just in purposes of testing to see possible negative effects",
    "price": 6,
    "id": 45,
    "addon_only": true
  },
  {
    "category": {
      "id": 2,
      "weight": 0,
      "name": "Pedicure",
      "overlay_img": null
    },
    "name": "Sem remoção de Verniz Gel/Dip",
    "weight": 0,
    "time_duration": 0,
    "help_text": "This example text was added just in purposes of testing to see possible negative effects",
    "price": 0,
    "id": 46,
    "addon_only": true
  },
  {
    "category": {
      "id": 2,
      "weight": 0,
      "name": "Pedicure",
      "overlay_img": null
    },
    "name": "Com água",
    "weight": 0,
    "time_duration": 0,
    "help_text": "This example text was added just in purposes of testing to see possible negative effects",
    "price": 0,
    "id": 47,
    "addon_only": true
  },
  {
    "category": {
      "id": 2,
      "weight": 0,
      "name": "Pedicure",
      "overlay_img": null
    },
    "name": "Sem água",
    "weight": 0,
    "time_duration": 0,
    "help_text": "This example text was added just in purposes of testing to see possible negative effects",
    "price": 0,
    "id": 48,
    "addon_only": true
  },
  {
    "category": {
      "id": 1,
      "weight": 0,
      "name": "Manicure",
      "overlay_img": "images/ima_service_manicure.jpg"
    },
    "name": "Com cuticle peel",
    "weight": 0,
    "time_duration": 0,
    "help_text": "This example text was added just in purposes of testing to see possible negative effects",
    "price": 0,
    "id": 49,
    "addon_only": true
  },
  {
    "category": {
      "id": 1,
      "weight": 0,
      "name": "Manicure",
      "overlay_img": "images/ima_service_manicure.jpg"
    },
    "name": "Sem cuticle peel",
    "weight": 0,
    "time_duration": 0,
    "help_text": "This example text was added just in purposes of testing to see possible negative effects",
    "price": 0,
    "id": 50,
    "addon_only": true
  },
  {
    "category": {
      "id": 1,
      "weight": 0,
      "name": "Manicure",
      "overlay_img": "images/ima_service_manicure.jpg"
    },
    "name": "Spa",
    "weight": 0,
    "time_duration": 900,
    "help_text": "This example text was added just in purposes of testing to see possible negative effects",
    "price": 10,
    "id": 51,
    "addon_only": true
  },
  {
    "category": {
      "id": 6,
      "weight": 0,
      "name": "Massagens",
      "overlay_img": "images/ima_service_massagens.jpg"
    },
    "name": "Massagem Extra",
    "weight": 0,
    "time_duration": 600,
    "help_text": "This example text was added just in purposes of testing to see possible negative effects",
    "price": 4,
    "id": 52,
    "addon_only": true
  },
  {
    "category": {
      "id": 1,
      "weight": 0,
      "name": "Manicure",
      "overlay_img": "images/ima_service_manicure.jpg"
    },
    "name": "Mani Francesa",
    "weight": 0,
    "time_duration": 300,
    "help_text": "This example text was added just in purposes of testing to see possible negative effects",
    "price": 3,
    "id": 53,
    "addon_only": true
  },
  {
    "category": {
      "id": 2,
      "weight": 0,
      "name": "Pedicure",
      "overlay_img": null
    },
    "name": "Pedi Francesa",
    "weight": 0,
    "time_duration": 300,
    "help_text": "This example text was added just in purposes of testing to see possible negative effects",
    "price": 3,
    "id": 54,
    "addon_only": true
  },
  {
    "category": {
      "id": 2,
      "weight": 0,
      "name": "Pedicure",
      "overlay_img": null
    },
    "name": "Balbcare",
    "weight": 0,
    "time_duration": 0,
    "help_text": "This example text was added just in purposes of testing to see possible negative effects",
    "price": 0,
    "id": 55,
    "addon_only": true
  },
  {
    "category": {
      "id": 99,
      "weight": 0,
      "name": "Empty category",
      "overlay_img": "images/ima_service_manicure.jpg"
    }
  }
];

export const customerDetails = {
  "customer": {
    "first_name": "Ilya",
    "last_name": "Zhukov",
    "mobile_number": "+7-904-900-00-00",
    "nif": "123456789",
    "token": "cjgn06r9ya2ysod9hef4",
    "email": "oleg.zharkikh.work@gmail.com",
    "postal_code": {
      "code": "1000",
      "id": 3,
      "extension": "117",
      "name": "Lisboa"
    },
    "fullAddress": "",
    "address": {
      "discrict_code": "",
      "county_code": "",
      "branch_code": "",
      "branch_name": "",
      "branch_extra": "",
      "venue_name": "",
      "number": "",
      "area": {
        "code": "",
        "name": "LISBOA",
        "status": null
      },
      "postal_code": {
        "code": "",
        "extension": "",
        "name": ""
      },
      "source": ""
    }
  }
};

export const timeSlots = {
  "slots": [
    "2018-03-29T09:00:00",
    "2018-03-29T09:30:00",
    "2018-03-29T10:00:00",
    "2018-03-29T10:30:00",
    "2018-03-29T11:00:00",
    "2018-03-29T11:30:00",
    "2018-03-29T12:00:00",
    "2018-03-29T12:30:00",
    "2018-03-29T13:00:00",
    "2018-03-29T13:30:00",
    "2018-03-29T14:00:00",
    "2018-03-29T14:30:00",
    "2018-03-29T15:00:00",
    "2018-03-29T15:30:00",
    "2018-03-29T16:00:00",
    "2018-03-29T16:30:00",
    "2018-03-29T17:00:00",
    "2018-03-29T17:30:00"
  ]
};

export const profSlots = {
  "sps": [
    {
      "gender": "F",
      "first_name": "Ana Mani",
      "last_name": "da Silva",
      "id": "fbcd2cd2-1797-440d-90ca-9604dbc35658",
      "image": null
    },
    {
      "gender": "F",
      "first_name": "Carla Calos",
      "last_name": "Gomes",
      "id": "68d76f22-3844-4f43-94ed-fd79c9b5a995",
      "image": null
    },
    {
      "gender": "F",
      "first_name": "Carla Calos",
      "last_name": "Gomes",
      "id": "68d76f23-3844-4f43-94ed-fd79c9b5a995",
      "image": null
    },
    {
      "gender": "F",
      "first_name": "Santa Calos",
      "last_name": "Pereira",
      "id": "68d74518-3844-4f43-94ed-fd79c9b5a995",
      "image": null
    },
    {
      "gender": "F",
      "first_name": "Carla Calos",
      "last_name": "Gomes",
      "id": "68d56718-3844-4f43-94ed-fd79c9b5a995",
      "image": null
    },
    {
      "gender": "F",
      "first_name": "Carla Calos",
      "last_name": "Gomes",
      "id": "68d76f18-3842-4f43-94ed-fd79c9b5a995",
      "image": null
    },
    {
      "gender": "F",
      "first_name": "Carla Calos",
      "last_name": "Gomes",
      "id": "68d76f18-3844-4f99-94ed-fd79c9b5a995",
      "image": null
    },
    {
      "gender": "M",
      "first_name": "Carla Calos",
      "last_name": "Gomes",
      "id": "68d76f18-3844-4f43-94ed-fd79c9b5a995",
      "image": null
    },
    {
      "gender": "M",
      "first_name": "Carla Calos",
      "last_name": "Gomes",
      "id": "68d76f18-3844-4f43-94ed-fd79c97aa995",
      "image": null
    }
  ]
};

export const streetNumbers = {
  "numbers": [
    {
      "id": 106456,
      "number": "28"
    },
    {
      "id": 106455,
      "number": "26"
    },
    {
      "id": 106454,
      "number": "24"
    },
    {
      "id": 106453,
      "number": "22"
    },
    {
      "id": 106452,
      "number": "20"
    },
    {
      "id": 106451,
      "number": "18"
    },
    {
      "id": 106450,
      "number": "16"
    },
    {
      "id": 106449,
      "number": "14"
    },
    {
      "id": 106448,
      "number": "12"
    },
    {
      "id": 106447,
      "number": "10"
    },
    {
      "id": 106446,
      "number": "8"
    },
    {
      "id": 106445,
      "number": "6"
    },
    {
      "id": 106444,
      "number": "4"
    },
    {
      "id": 106443,
      "number": "2"
    }
  ]
};