import React from 'react';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { PATH_SERVICES, PATH_STATIC } from '../../constants';

class Category extends React.Component {
    constructor(props) {
        super(props);

        this.openCategory = this.openCategory.bind(this);
    }

    openCategory(e) {
        e.preventDefault();
        this.props.dispatch(push(`${PATH_SERVICES}/${this.props.id}`));
    }

    render() {
        return (
            <div className="item-type_service">
                <a className="bto-item-type_service" href="javascript:void(0)" onClick={this.openCategory}>
                    {this.props.overlayImg
                    ? <img src={`${PATH_STATIC}/${this.props.overlayImg}`} alt="" />
                    : <div className="empty"></div>
                    }
                    <div className="tit-services">{this.props.name}</div>
                </a>
            </div>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
    };
};

export default connect(mapStateToProps)(Category);
export { Category as CategoryNotConnected };
