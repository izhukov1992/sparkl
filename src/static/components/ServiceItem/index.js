import React from 'react';
import { connect } from 'react-redux';

import { getServicePrice } from '../../reducers';
import { AddSub } from '../../components';

class ServiceItem extends React.Component {
  constructor(props) {
    super(props);

    this.getRightContainer = this.getRightContainer.bind(this);
  }

  getRightContainer() {
    if (this.props.hideCounts) {
      return null;
    }

    return (
      <div className="container-right">
        <AddSub onSub={() => this.props.onDecrease(this.props.serviceItem)} onAdd={() => this.props.onIncrease(this.props.serviceItem)} value={this.props.serviceItem.quantity} />
      </div>
    );
  }
  render() {
    return (
      <li>
        <div className={'service-container-unid ' + (this.props.serviceItem.quantity ? 'active' : '') + (this.props.hideCounts ? ' hide-counts' : '')}>
          <div className="container-left">
            <div className="right background-item-services_left">
              <span className="txt-service marg-service-txt_left">{this.props.serviceItem.service.name}<small></small></span>
            </div>
            <div className="midle background-item-services_right">
              <span className="txt-price marg-service-txt_right">{getServicePrice(this.props.serviceItem)}<small>€</small></span>
            </div>
          </div>
          {this.getRightContainer()}
        </div>
      </li>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    dispatch
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ServiceItem);
export { ServiceItem as ServiceItemNotConnected };
