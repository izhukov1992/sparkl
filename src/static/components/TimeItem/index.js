import React from 'react';
import { connect } from 'react-redux';
import moment from 'moment';
import _ from 'lodash';

class TimeItem extends React.Component {
  render() {
    let classes = "time-slot-disable";

    if (this.props.timeItem.isActive) {
      classes = "time-slot";
      if (moment(this.props.timeItem.time).isSame(moment(_.result(this.props.activeTimeSlot, 'time')), 'minute')) {
        classes += " active";
      }
    }

    return (
      <p className={classes} onClick={this.props.timeItem.isActive ? () => this.props.onChange(this.props.timeItem) : null}>{moment(this.props.timeItem.time).format('HH:mm')}</p>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    activeTimeSlot: state.booking.activeTimeSlot
  };
};

export default connect(mapStateToProps)(TimeItem);
export { TimeItem as TimeItemNotConnected };
