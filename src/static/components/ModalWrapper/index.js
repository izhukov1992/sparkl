import React from 'react';
import { connect } from 'react-redux';

import { 
    CUSTOMER_DETAILS, CUSTOMER_DETAILS_VALID, CART_SUMMARY,
    SERVICE_REQUIRED_ADDONS, POPUP_TALK, LIVE_CHAT,
    ERROR, ADDRESS_NUMBER, POSTAL_CODE, PAYMENT_REFERENCE,
    PAYMENT_VISA
} from '../../constants';

import CustomerDetailsModal from '../CustomerDetailsModal';
import CustomerDetailsValidModal from '../CustomerDetailsValidModal';
import ServiceRequiredAddonsModal from '../ServiceRequiredAddonsModal';
import PopupTalkModal from '../PopupTalkModal';
import CartSummaryModal from '../CartSummaryModal';
import AddressNumberModal from '../AddressNumberModal';
import PostalCodeModal from '../PostalCodeModal';
import LiveChatModal from '../LiveChatModal';
import ErrorModal from '../ErrorModal';
import PaymentReferenceModal from '../PaymentReferenceModal';
import PaymentVisaModal from '../PaymentVisaModal';

const MODAL_COMPONENTS = {
    [CUSTOMER_DETAILS]: CustomerDetailsModal,
    [CUSTOMER_DETAILS_VALID]: CustomerDetailsValidModal,
    [SERVICE_REQUIRED_ADDONS]: ServiceRequiredAddonsModal,
    [POPUP_TALK]: PopupTalkModal,
    [CART_SUMMARY]: CartSummaryModal,
    [LIVE_CHAT]: LiveChatModal,
    [ERROR]: ErrorModal,
    [ADDRESS_NUMBER]: AddressNumberModal,
    [POSTAL_CODE]: PostalCodeModal,
    [PAYMENT_REFERENCE]: PaymentReferenceModal,    
    [PAYMENT_VISA]: PaymentVisaModal
}

class ModalWrapper extends React.Component {
    render() {
        return this.getBody();
    }

    getBody() {
        if (!this.props.modalType) {
            return null;
        }

        const SpecificModal = MODAL_COMPONENTS[this.props.modalType];
        if (!SpecificModal) {
            return null;
        }

        return (
            <SpecificModal {...this.props.modalProps} setModal={this.props.setModal} 
                cartModalCallback={this.props.cartModalCallback} getModalServices={this.props.getModalServices} 
            />
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        modalType: state.modal.modalType,
        modalProps: state.modal.modalProps
    };
};

export default connect(mapStateToProps)(ModalWrapper);
export { ModalWrapper as ModalNotConnected };