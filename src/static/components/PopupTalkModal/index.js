import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';

import { GridWrapper, Button } from '../index';
import { hideModal } from '../../actions/modal';
import { PATH_CATEGORIES, img_info } from '../../constants';
import { FB_CART_URL } from '../../utils/config';

class PopupTalkModal extends React.Component {
  constructor(props) {
    super(props);

    this.returnToCategoriesScreen = this.returnToCategoriesScreen.bind(this);
    this.openFBMessengerCart = this.openFBMessengerCart.bind(this);
  }

  returnToCategoriesScreen = (e) => {
    e && e.preventDefault();
    this.props.hideModal();
    this.props.dispatch(push(`${PATH_CATEGORIES}`));
  }

  openFBMessengerCart = (e) => {
    e.preventDefault();
    window.location.href = FB_CART_URL;
  }

  render() {
    return (
      <GridWrapper classes="overlay-box">
        <div className="content">
          <h1 className="tit-big-overlay">ASSISTENTE SPARKL</h1>
          <p className="padding-txt">Irá ser atendida(o) pela nossa assistente para poder agendar o seu serviço.</p>
          <Button label="VOLTAR" onClick={this.returnToCategoriesScreen} small_style left_style />
          <Button label="AGENDAR" onClick={this.openFBMessengerCart} small_style right_style />
        </div>
      </GridWrapper >
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    dispatch,
    hideModal: bindActionCreators(hideModal, dispatch)
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(PopupTalkModal);
export { PopupTalkModal as PopupTalkModalNotConnected };