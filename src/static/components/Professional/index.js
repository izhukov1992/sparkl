import React from 'react';
import { connect } from 'react-redux';

import { PATH_STATIC, avatar_default } from '../../constants';

class Professional extends React.Component {
  constructor(props) {
    super(props);

    this.getFullName = this.getFullName.bind(this);
  }

  getFullName() {
    return `${this.props.professional.first_name} ${this.props.professional.last_name}`;
  }

  render() {
    let classes = "professional";

    if (this.props.activeProfSlot === this.props.professional) {
      classes += " active";
    }

    return (
      <div className={classes} onClick={() => this.props.onChange(this.props.professional)}>
        {this.props.professional.image
        ? <img src={`${PATH_STATIC}/{this.props.professional.image}`} width="83" height="83" />
        : <img src={avatar_default} width="83" height="83" />
        }
        <p className="name-prof">{this.getFullName()}</p>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    activeProfSlot: state.booking.activeProfSlot
  };
};

export default connect(mapStateToProps)(Professional);
export { Professional as ProfessionalNotConnected };
