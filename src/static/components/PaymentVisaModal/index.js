import React from 'react';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';

import { PATH_CONFIRM, HIDE_MODAL } from '../../constants';
import { GridWrapper } from '../../components';

class PaymentVisaModal extends React.Component {
  constructor(props) {
    super(props);

    this.getPaymentFormOptions = this.getPaymentFormOptions.bind(this);
  }

  componentDidMount() {
    const script = document.createElement("script");

    script.src = `https://test.oppwa.com/v1/paymentWidgets.js?checkoutId=${this.props.id}`;
    script.async = true;

    document.body.appendChild(script);
  }

  getPaymentFormOptions() {
    return `
      var wpwlOptions = {
        style: "card",
        expiryDate: ${this.props.expiration_datetime}
      }    
    `;
  }

  getTargetAddress() {
    return `${location.protocol}//${location.hostname}${location.port ? (':'  +location.port) : ''}/confirm`;
  }

  render() {
    return (
      <GridWrapper classes={"overlay-box"}>
        <script>{this.getPaymentFormOptions()}</script>
        <form action={location.protocol+'//'+location.hostname+(location.port ? ':'+location.port: '') + '/confirm'} className="paymentWidgets" data-brands={this.props.payment_method}></form>
      </GridWrapper>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  let { id, order_id, payment_method, status, amount } = ownProps;

  return {
    id,
    order_id,
    payment_method,
    status,
    amount
  }
};

export default connect(mapStateToProps)(PaymentVisaModal);
export { PaymentVisaModal as PaymentVisaModalNotConnected };