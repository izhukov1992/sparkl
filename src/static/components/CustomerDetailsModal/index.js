import React from 'react';
import { connect } from 'react-redux';
import InputMask from 'react-input-mask';

import { GridWrapper, Button } from '../index';
import { hideModal } from '../../actions/modal';
import { saveCustomerDetails } from '../../actions/customerDetails';

class CustomerDetailsModal extends React.Component {
    constructor(props) {
        super(props);

        let { first_name, last_name, mobile_number, email, full_address } = this.props.customerDetails;

        this.state = {
            first_name: first_name || '',
            last_name: last_name || '',
            mobile_number: mobile_number || '',
            email: email || '',
            full_address: full_address || ''
        }

        this.handleChange = this.handleChange.bind(this);
        this.returnToPostalScreen = this.returnToPostalScreen.bind(this);
        this.updateCustomerDetails = this.updateCustomerDetails.bind(this);
    }

    handleChange(e) {
        let newValue = e.target.value;
    
        if (e.target.name === 'mobile_number') {
          if (/^[0-8]$/.test(newValue)) {
            newValue = '+351' + newValue;
          }
    
          if (/^((\+[1-9]\d{0,14})|(\+)|(9\d{0,14}))?$/.test(newValue)) {
            this.setState({[e.target.name]: newValue});
          }
        } else {
          this.setState({[e.target.name]: newValue});
        }
      }

    returnToPostalScreen = () => {
        this.props.hideModal();
    }

    updateCustomerDetails = (e) => {
        e.preventDefault();

        let { first_name, last_name, mobile_number, email, full_address } = this.state;
        let { postal_code, token } = this.props.customerDetails;
        this.props.saveCustomerDetails({
            first_name,
            last_name,
            mobile_number,
            email,
            full_address: full_address,
            token: token,
            postal_code: postal_code
        });
    }

    render() {
        return (
            <GridWrapper requireOverlayImg classes="overlay-box">
                <div className="box-popup_content">
                    <h1 className="tit-small-overlay">INFELIZMENTE AINDA NÃO ESTAMOS NA SUA ZONA.</h1>
                    <p className="padding-txt-form">Envie-nos os seus contactos para ser a primeira a saber quando estivermos!</p>
                </div>
                <div className="content_popup_form">
                    <form onSubmit={this.updateCustomerDetails}>
                        <div className="max-width-container">
                            <div className="form-container_pop-up">
                                <div className="form-container-single group">
                                    <div className="col-popup span_1_of_2">
                                        <input type="text" name="first_name" value={this.state.first_name} onChange={this.handleChange} placeholder="Nome" required="required" className="form-field" />
                                    </div>
                                    <div className="col-popup span_1_of_2">
                                        <input type="text" name="last_name" value={this.state.last_name} onChange={this.handleChange} placeholder="Apelido" required="required" className="form-field" />
                                    </div>
                                </div>
                                <div className="form-container-single group">
                                    <div className="col-popup span_2_of_2">
                                        <input type="email" name="email" value={this.state.email} onChange={this.handleChange} placeholder="Email" required="required" className="form-field" />
                                    </div>
                                </div>
                                <div className="form-container-single group">
                                    <div className="col-popup span_2_of_2">
                                        <input type="text" name="full_address" minLength={5} required="required" value={this.state.full_address} onChange={this.handleChange} placeholder="Morada" required="required" className="form-field" />
                                    </div>
                                </div>
                                <div className="form-container-single group">
                                    <div className="col-popup span_2_of_2">
                                        <input type="text" name="mobile_number" pattern="^(\+[1-9]\d{1,14})|(0\d{1,14})$" placeholder="Telefone" required="required" className="form-field" value={this.state.mobile_number} onChange={this.handleChange} />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <Button label="CANCELAR" onClick={this.returnToPostalScreen} small_style left_style />
                        <Button label="ENVIAR" button_submit small_style right_style />
                    </form>
                </div>
            </GridWrapper>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    let { customerDetails } = state;
    return {
        customerDetails
    }
};

export default connect(mapStateToProps, { hideModal, saveCustomerDetails })(CustomerDetailsModal);
export { CustomerDetailsModal as CustomerDetailsModalNotConnected };