import React from 'react'; 
import { connect } from 'react-redux'; 
import { push } from 'react-router-redux'; 
import moment from 'moment'; 
 
import { PATH_CONFIRM, HIDE_MODAL } from '../../constants'; 
import { GridWrapper, Button } from '../../components'; 
import { getTotalCartPrice } from '../../reducers'; 
 
class PaymentReferenceModal extends React.Component { 
  constructor(props) { 
    super(props); 
 
    this.openConfirmScreen = this.openConfirmScreen.bind(this); 
    this.getExpirationText = this.getExpirationText.bind(this); 
    this.getFormattedReferenceCode = this.getFormattedReferenceCode.bind(this); 
  } 
 
  openConfirmScreen(e) { 
    e.preventDefault(); 
    this.props.dispatch({ 
      type: HIDE_MODAL 
    }); 
    this.props.dispatch(push(`${PATH_CONFIRM}`)); 
  } 
 
  getExpirationText() { 
    if (!this.props.expiration_datetime) { 
      return null; 
    } 
 
    let timeStampMoment = moment(this.props.expiration_datetime); 
 
    let time = timeStampMoment.format('HH:mm'), 
      day = timeStampMoment.format('DD'), 
      month = timeStampMoment.format('MMMM'), 
      year = timeStampMoment.format('YYYY'); 
 
    return `Para finalizar o seu agendamento terá de fazer o pagamento até às ${time} de ${day} ÀS ${month} de ${year}`; 
  } 
 
  getFormattedReferenceCode() { 
    return this.props.entity.replace(/\./g, ' '); 
  } 
 
  render() { 
    return ( 
      <GridWrapper classes={"overlay-box"}> 
        <div className="content content-shadow"> 
          <h1 className="tit-big-overlay">OBRIGADA PELA SUA RESERVA!</h1> 
 
          <div> 
            <p className="padding-txt">{this.getExpirationText()}</p> 
 
            <p className="form-field-order payment"> 
              <span className="field-name">Entidade:</span> 
              <span className="field-value">{this.getFormattedReferenceCode()}</span> 
            </p> 
            <p className="form-field-order payment"> 
              <span className="field-name">Referencia:</span> 
              <span className="field-value">{this.props.mb_ref}</span> 
            </p> 
            <p className="form-field-order payment"> 
              <span className="field-name">Valor:</span> 
              <span className="field-value">{this.props.amount}€</span> 
            </p> 
          </div> 
 
          <Button label="SAIR" big_style onClick={this.openConfirmScreen} /> 
        </div> 
      </GridWrapper> 
    ); 
  } 
} 
 
const mapStateToProps = (state, ownProps) => { 
  return { 
  } 
}; 
 
export default connect(mapStateToProps)(PaymentReferenceModal); 
export { PaymentReferenceModal as PaymentReferenceModalNotConnected };