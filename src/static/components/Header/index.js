import React from 'react';
import { connect } from 'react-redux';

import { SHOW_MODAL, CART_SUMMARY, PATH_CATEGORIES, sparkl_logo, cart } from '../../constants';
import { getCartTotalAmount } from '../../reducers';

class Header extends React.Component {
    constructor(props) {
        super(props);

        this.openCartModal = this.openCartModal.bind(this);
    }

    openCartModal() {
        this.props.dispatch({
            type: SHOW_MODAL,
            modalType: CART_SUMMARY,
            modalProps: {
            }
        });
    }

    getBasketButton() {
        return (
            <a className="bto-cart" href="javascript:void(0)" onClick={this.openCartModal}><img src={cart} alt="cart" />
                {this.props.cartCount > 0 &&
                    <div className="number_cart">{this.props.cartCount}</div>
                }
            </a>
        );
    }

    render() {
        return (
            <header>
                <div className="header_wrapper">
                    <div className="header_grid">
                        <img src={sparkl_logo} alt="SPARKL" className="logo" />
                        {this.getBasketButton()}
                    </div>
                </div>
            </header>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        location: state.routing.location,
        cartCount: getCartTotalAmount(state)
    };
};

export default connect(mapStateToProps)(Header);
export { Header as HeaderNotConnected };
