import React from 'react';
import { connect } from 'react-redux';
import { GridWrapper, Button } from '../index';
import { hideModal } from '../../actions/modal';
import { img_info } from '../../constants';

class ServiceInfoModal extends React.Component {
  constructor(props) {
    super(props);

    this.returnToServicesScreen = this.returnToServicesScreen.bind(this);
  }

  getFormattedDuration(duration) {
    return Math.ceil(Number(duration) / 60) + ' min';
  }

  returnToServicesScreen = (e) => {
    e && e.preventDefault();
    this.props.hideModal();
    this.props.infoBtnCallback();
  }

  render() {
    return (
      <GridWrapper classes="overlay-box">
          <div className="content">
            <img src={img_info} alt="SPARKL" className="img-info-service" />
            <div>
              <svg className="wave_top_popup" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 380 26" style={{enableBackground: 'new 0 0 380 26'}} xmlSpace="preserve" >
                <path className="wave_section" d="M364,23.4C345.7,22.3,342.5,0,320.6,0h-262C36.6,0,33.5,22.3,15.1,23.4H0V26h380v-2.6H364z" />
              </svg>
              <p className="sub-tit-section">{this.props.service.name}</p>
              <p className="sub-tit-time">{this.getFormattedDuration(this.props.service.time_duration)}</p>
            </div>
            <div className="padding-txt-time">
              <p>{this.props.service.help_text}</p>
            </div>
            <Button label="FECHAR" onClick={this.returnToServicesScreen} big_style no_decor_style />
          </div>
      </GridWrapper >
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  let { services } = state;
  return {
    services,
    infoBtnCallback: ownProps.infoBtnCallback || (() => {})
  }
};

export default connect(mapStateToProps, { hideModal })(ServiceInfoModal);
export { ServiceInfoModal as ServiceInfoModalNotConnected };