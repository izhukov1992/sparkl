import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { GridWrapper, Button } from '../index';
import { hideModal } from '../../actions/modal';
import * as errorMsgs from '../../constants';

class ErrorModal extends React.Component {
  render() {
    return (
      <GridWrapper classes="overlay-box">
        <div className="content">
          <h1 className="tit-big-overlay">UPS...</h1>
          <p className="padding-txt">{errorMsgs[this.props.msgType]}</p>
          <Button label="FECHAR" onClick={this.props.hideModal} big_style no_decor_style />
        </div>
      </GridWrapper >
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    dispatch,
    hideModal: bindActionCreators(hideModal, dispatch)
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ErrorModal);
export { ErrorModal as ErrorModalNotConnected };