import React from 'react';
import { connect } from 'react-redux';
import moment from 'moment';

class DateItem extends React.Component {
  constructor(props) {
    super(props);

    this.handleClick = this.handleClick.bind(this);
  }

  render() {
    let classes='';

    if (moment(this.props.activeDateSlot, 'MM-DD-YYYY').isSame(this.props.dateItem, 'day')) {
      classes += "select-day";
    } else {
      classes += "no-select-day";
    }

    if (moment(this.props.dateItem, 'MM-DD-YYYY').isSameOrBefore(moment(), 'day')) {
      classes += " old-day";
    }

    return (
      <div className={classes} onClick={this.handleClick}>
        <p className="mouth">{this.props.dateItem.format('MMM').toUpperCase()}</p>
        <p className="day-number">{this.props.dateItem.format('DD')}</p>
        <p className="day-txt">{this.props.dateItem.format('ddd').toUpperCase()}</p>
      </div>
    );
  }

  handleClick(e) {
    e.preventDefault();

    if (!moment(this.props.dateItem, 'MM-DD-YYYY').isSameOrBefore(moment(), 'day')) {
      this.props.onChange(this.props.dateItem);
    }
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    activeDateSlot: state.booking.activeDateSlot
  };
};

export default connect(mapStateToProps)(DateItem);
export { DateItem as DateItemNotConnected };
