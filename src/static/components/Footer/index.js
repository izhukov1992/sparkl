import React from 'react';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import _ from 'lodash';

import { LIVE_CHAT, SHOW_MODAL, PATH_CATEGORIES, PATH_SERVICES, PATH_CART, PATH_SESSION, PATH_BOOKING, PATH_CHECKOUT, backward, forward, chat, home } from '../../constants';

class Footer extends React.Component {
  constructor(props) {
    super(props);

    this.openLiveChatModal = this.openLiveChatModal.bind(this);
    this.onHome = this.onHome.bind(this);
  }

  openLiveChatModal() {
    this.props.dispatch({
        type: SHOW_MODAL,
        modalType: LIVE_CHAT,
        modalProps: {
      }
    });
  }

  getNextButton() {
    if (!this.props.checkNext()) {
      return null;
    }

    let nextBtnText = 'seguinte';
    if (this.props.location.pathname === PATH_CHECKOUT) {
      nextBtnText = 'confirmar';
    }

    return (
      <a className="bto-forward" href="javascript:void(0)" onClick={this.props.onNext}><img src={forward} alt="forward" /></a>
    );
  }

  onHome(e) {
    e.preventDefault();
    this.props.dispatch(push(`${PATH_CATEGORIES}`));
  }

  render() {
    return (
      <div className="nav_container">
        <a className="bto-backward" href="javascript:void(0)" onClick={this.props.onBack}><img src={backward} alt="backward" /></a>
        <a className="bto-chat" href="javascript:void(0)" onClick={this.openLiveChatModal}><img src={chat} alt="chat" /></a>
        <a className="bto-home" href="javascript:void(0)" onClick={this.onHome}><img src={home} alt="home" /></a>
        {this.getNextButton()}
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    location: state.routing.location,
    rootRendererCount: state.helper.rootRendererCount
  };
};

export default connect(mapStateToProps)(Footer);
export { Footer as FooterNotConnected };
