import React from 'react';
import { connect } from 'react-redux';

import { GridWrapper } from '../../components';
import loadingIcon from '../../img/spinner.svg';

class LoadingSpinner extends React.Component {
  render() {
    if (!this.props.showIcon) {
      return null;
    }

    return (      
      <GridWrapper classes={"overlay-box"}>
        <img src={loadingIcon} className="spinner"></img>
      </GridWrapper>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    showIcon: state.helper.fetchInProgress
  }
};

export default connect(mapStateToProps)(LoadingSpinner);
export { LoadingSpinner as LoadingSpinnerNotConnected };