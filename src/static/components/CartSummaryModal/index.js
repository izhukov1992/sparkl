import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { push } from 'react-router-redux';
import moment from 'moment';

import { GridWrapper, Button, ServiceItem } from '../index';
import { hideModal } from '../../actions/modal';
import { updateCart } from '../../actions/shoppingCart';
import { PATH_CATEGORIES, PATH_CART, PATH_SESSION, PATH_BOOKING, PATH_CHECKOUT, img_info, nightFeeService } from '../../constants';

class CartSummaryModal extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      cartItems: this.props.cartItems.map((cartItem) => {
        return {
          ...cartItem
        }
      }),
      actionCounter: 0
    };

    this.goToCategoriesList = this.goToCategoriesList.bind(this);
    this.closeModal = this.closeModal.bind(this);
    this.onIncrease = this.onIncrease.bind(this);
    this.onDecrease = this.onDecrease.bind(this);
    this.getTotalCartPrice = this.getTotalCartPrice.bind(this);
    this.getNightFeeItem = this.getNightFeeItem.bind(this);
  }

  componentWillMount() {
    this.props.setModal(true);
  }

  goToCategoriesList(e) {
    e.preventDefault();
    this.props.hideModal();
    this.props.dispatch(push(`${PATH_CATEGORIES}`));
  }

  closeModal(e) {
    e.preventDefault();

    let changedServices = this.state.cartItems.filter((cartItem) => {
      let index = this.props.cartItems.findIndex((item) => {
        return item.service.id === cartItem.service.id;
      });

      if (index === -1) {
        return true;
      }

      return this.props.cartItems[index].quantity !== cartItem.quantity;
    });

    this.props.dispatch(updateCart({
      serviceItems: changedServices,
      successCallback: () => this.props.hideModal(),
      cartCallback: (this.props.location.pathname === PATH_CART) ? this.props.cartModalCallback :  (() => {}),
      actionCounter: this.state.actionCounter,
      increment: false
    }));
  }

  onIncrease(item) {
    if (!item) {
      return;
    }

    this.setState({
      cartItems: this.state.cartItems.map((cartItem) => {
        return {
          ...cartItem,
          quantity: (cartItem === item) ? ++cartItem.quantity : cartItem.quantity
        }
      }),
      actionCounter: ++this.state.actionCounter
    });
  }

  onDecrease(item) {
    if (!item || item.quantity === 0) {
      return;
    }

    this.setState({
      cartItems: this.state.cartItems.map((cartItem) => {
        return {
          ...cartItem,
          quantity: (cartItem === item) ? --cartItem.quantity : cartItem.quantity
        };
      }),
      actionCounter: ++this.state.actionCounter
    });
  }

  getTotalCartPrice() {
    var result = this.state.cartItems.reduce((total, cartItem) => {
      return total + cartItem.service.price * cartItem.quantity;
    }, 0);

    // Add night fee price
    if (this.props.activeTimeSlot && moment(this.props.activeTimeSlot.time).hours() >= 20) {
      result += nightFeeService.service.price;
    }

    return result;
  }  

  getNightFeeItem() {
    if (!this.props.activeTimeSlot || !this.props.activeTimeSlot.time) {
      return null;
    }

    if (moment(this.props.activeTimeSlot.time).hours() >= 20) {
      return <ServiceItem key={nightFeeService.service.id} serviceItem={nightFeeService} onIncrease={this.onIncrease} onDecrease={this.onDecrease} hideCounts={true} />
    } else {
      return null;
    }
  }  

  areCountsVisible() {
    if ([ PATH_SESSION, PATH_BOOKING, PATH_CHECKOUT ].indexOf(this.props.location.pathname) !== -1) {
      return false
    }

    return true;
  }

  render() {
    return (
      <GridWrapper classes="overlay-box">
        <div className="padding-cart">
        <div className="content_popup_pref-service-cart">
          <h1 className="sub-tit-section">A SUA ENCOMENDA</h1>
        </div>
        <div className="content_popup_form">
          <form>
            <ul className="table-services_pop-up spacer-section no-bullets item-pointer">
              {this.state.cartItems.map(cartItem => (
                <ServiceItem key={cartItem.service.id} serviceItem={cartItem} onIncrease={this.onIncrease} onDecrease={this.onDecrease} hideCounts={!this.areCountsVisible()} />
              ))}
              {this.getNightFeeItem()}
            </ul>

            <section className="center max-width-container">
              <p className="item-total_popup item-total-dashed-top">TOTAL: {this.getTotalCartPrice()}€</p>
              <Button onClick={this.goToCategoriesList} label="ADICIONAR MAIS SERVIÇOS" add_txt_style />
            </section>

            <Button label="FECHAR" onClick={this.closeModal} big_style no_decor_style margin_top_style />
          </form>
        </div>
        </div>
      </GridWrapper >
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  let services = [];
  let location = state.routing.location;

  if (location.pathname === PATH_CART) {
    services = ownProps.getModalServices().filter((service) => service.quantity);
  } else {
    services = state.shoppingCart.services;
  }

  return {
    location: location,
    cartItems: services.filter((item) => {
      return item && item.service.price;
    }).map((item) => ({
      ...item,
      alwaysIncrement: false
    })),
    activeTimeSlot: state.booking.activeTimeSlot
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    dispatch,
    hideModal: bindActionCreators(hideModal, dispatch)
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(CartSummaryModal);
export { CartSummaryModal as CartSummaryModalNotConnected };