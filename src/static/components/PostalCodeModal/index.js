import React from 'react';
import { connect } from 'react-redux';
import InputMask from 'react-input-mask';
import { bindActionCreators } from 'redux';

import { GridWrapper, Button } from '../index';
import { hideModal } from '../../actions/modal';
import { checkSlotsAvailability } from '../../actions/booking';
import { SET_POSTAL_CODE } from '../../constants';

class PostalCodeModal extends React.Component {
    constructor(props) {
        super(props);

        let { postal_code } = this.props.customerDetails;
        this.state = {
            postalCode: (postal_code.code && postal_code.extension) ? `${postal_code.code}-${postal_code.extension}` : ''
        }

        this.handleChange = this.handleChange.bind(this);
        this.submitPostalCode = this.submitPostalCode.bind(this);
    }

    handleChange(e) {
        this.setState({
            postalCode: e.target.value
        });
    }

    submitPostalCode(e) {
        e.preventDefault();

        this.props.dispatch({
            type: SET_POSTAL_CODE,
            postal_code: this.state.postalCode
        });

        this.props.checkSlotsAvailability(true);
    }

    render() {
        return (
            <GridWrapper classes="overlay-box">
                <div className="content_input grey">
                    <h1 className="tit-medium-overlay">DESEJA ALTERAR O SEU CÓDIGO POSTAL?</h1>
                </div>
                <div className="content_postal-code">
                    <form onSubmit={this.submitPostalCode}>
                        <InputMask mask="9999-999" pattern="[0-9]{4}-[0-9]{3}" required placeholder="Inserir Código Postal" className="postal-code input-text" maskChar={null} value={this.state.postalCode} onChange={this.handleChange} />
                        <svg className="wave_bottom" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                            viewBox="0 0 310 22" style={{enableBackground: 'new 0 0 310 22'}} xmlSpace="preserve">
                            <path className="wave_field" d="M297,2.2C282.1,3.1,279.4,22,261.5,22H47.8C29.9,22,27.3,3.1,12.3,2.2H0V0h310v2.2H297z" />
                        </svg>
                        <Button label="CANCELAR" onClick={this.props.hideModal} small_style left_style />
                        <Button label="ALTERAR" small_style right_style button_submit />
                    </form>
                </div>
            </GridWrapper>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    let { customerDetails } = state;
    return {
        customerDetails
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        dispatch,
        hideModal: bindActionCreators(hideModal, dispatch),
        checkSlotsAvailability: bindActionCreators(checkSlotsAvailability, dispatch),
    };
  };

export default connect(mapStateToProps, mapDispatchToProps)(PostalCodeModal);
export { PostalCodeModal as PostalCodeModalNotConnected };