import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { GridWrapper, Button } from '../index';
import { hideModal } from '../../actions/modal';
import { FB_CART_URL } from '../../utils/config';

class LiveChatModal extends React.Component {
  constructor(props) {
    super(props);

    this.openFBMessengerCart = this.openFBMessengerCart.bind(this);
  }

  openFBMessengerCart = (e) => {
    e.preventDefault();
    window.location.href = FB_CART_URL;
  }

  render() {
    return (
      <GridWrapper classes="overlay-box">
        <div className="content">
          <h1 className="tit-big-overlay">FALE CONNOSCO!</h1>
          <p className="padding-txt">O nosso Live Chat funciona das 9:00 até às 22:00.</p>
          <Button label="VOLTAR" onClick={this.props.hideModal} small_style left_style />
          <Button label="FALAR" onClick={this.openFBMessengerCart} small_style right_style />
        </div>
      </GridWrapper >
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    dispatch,
    hideModal: bindActionCreators(hideModal, dispatch)
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(LiveChatModal);
export { LiveChatModal as LiveChatModalNotConnected };