import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import _ from 'lodash';

import { GridWrapper, Button } from '../index';
import { hideModal } from '../../actions/modal';
import { saveCustomerDetails } from '../../actions/customerDetails';
import { CHANGE_ADDRESS_DETAILS, arrow_address } from '../../constants';

class AddressNumberModal extends React.Component {
    constructor(props) {
        super(props);

        let { address, numbers } = this.props.customerDetails;

        this.state = {
            address_unit: address.address_unit,
            numbers: numbers,
            number: (!_.isUndefined(address.number) && address.number !== '') ? address.number : numbers[0]
        };

        this.handleChange = this.handleChange.bind(this);
        this.setPrevNumber = this.setPrevNumber.bind(this);
        this.setNextNumber = this.setNextNumber.bind(this);
        this.updateAddressDetails = this.updateAddressDetails.bind(this);
    }

    handleChange(e) {
        this.setState({ [e.target.name]: e.target.value });
    }

    setPrevNumber() {
        if (!this.state.numbers || !this.state.numbers.length || this.state.numbers.length < 2) {
            return;
        }

        let index = this.state.numbers.findIndex((numberInfo) => {
            return numberInfo.number === _.result(this.state.number, 'number');
        });
        if (index === -1) {
            return;
        }

        let prevNumber = (index === 0) ? this.state.numbers[this.state.numbers.length - 1] : this.state.numbers[index - 1];

        this.setState({
            number: prevNumber
        });
    }

    setNextNumber() {
        if (!this.state.numbers || !this.state.numbers.length || this.state.numbers.length < 2) {
            return;
        }

        let index = this.state.numbers.findIndex((numberInfo) => {
            return numberInfo.number === _.result(this.state.number, 'number');
        });
        if (index === -1) {
            return;
        }

        let nextNumber = (index === (this.state.numbers.length - 1)) ? this.state.numbers[0] : this.state.numbers[index + 1];

        this.setState({
            number: nextNumber
        });
    }

    updateAddressDetails = (e) => {
        e.preventDefault();

        let { address_unit, number } = this.state;

        this.props.dispatch({
            type: CHANGE_ADDRESS_DETAILS,
            address_unit,
            number
        });

        this.props.dispatch(this.props.hideModal());
    }

    render() {
        return (
            <GridWrapper classes="overlay-box">
                <div className="content_popup_pref-service">
                    <h1 className="sub-tit-section">LOCAL DO SERVIÇO?</h1>

                    <div className="">
                        <div className="col span_2_of_2 margin-top">
                            <p className="form-field-order_popup">{this.props.customerDetails.address.branch_name}</p>
                            <p className="popup-address_txt_rose">NÚMERO DE PORTA</p>
                        </div>
                    </div>

                    <form className="center margin-top" onSubmit={this.updateAddressDetails}>
                        <img src={arrow_address} alt="" className="arrow-up" onClick={this.setPrevNumber} />
                        <p className="popup-address_number">{_.result(this.state.number, 'number')}</p>
                        <img src={arrow_address} alt="" className="arrow-down" onClick={this.setNextNumber} />
                        <div className="form-container-single-adress group">
                            <div className="col span_2_of_2">
                                <input type="text" name="address_unit" value={this.state.address_unit} placeholder="Lote / Edifício / Andar" onChange={this.handleChange} className="form-field" />
                            </div>
                        </div>
                        <Button label="SEGUINTE" button_submit big_style no_decor_style />
                    </form>
                </div>
            </GridWrapper>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    let { customerDetails } = state;
    return {
        customerDetails
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        dispatch,
        hideModal: bindActionCreators(hideModal, dispatch)
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(AddressNumberModal);
export { AddressNumberModal as AddressNumberModalNotConnected };