import React from 'react';
import { connect } from 'react-redux';

import { PATH_STATIC, arrow } from '../../constants';

class CategorySlider extends React.Component {
  render() {
    return (
      <div className="image-section">
        <div className="item-type_service">
          {this.props.category.overlay_img
          ? <img src={`${PATH_STATIC}/${this.props.category.overlay_img}`} alt="" />
          : <div className="empty"></div>
          }
          <h1 className="tit-services">{this.props.category.name}</h1>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
  };
};

export default connect(mapStateToProps)(CategorySlider);
export { CategorySlider as CategorySliderNotConnected };
