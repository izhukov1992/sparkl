import React from 'react';
import { connect } from 'react-redux';
import { GridWrapper, Button } from '../index';
import { hideModal } from '../../actions/modal';
import { finger } from '../../constants';

class CustomerDetailsValidModal extends React.Component {
    constructor(props) {
        super(props);

        this.returnToPostalScreen = this.returnToPostalScreen.bind(this);
    }

    returnToPostalScreen = () => {
        this.props.hideModal();
    }

    render() {
        return (
            <GridWrapper requireOverlayImg classes="overlay-box">
                <div className="content center">
                    <img src={finger} alt="SPARKL" className="finger" />
                    <p className="txt-thanks_popup">OBRIGADA PELO SEU CONTACTO!</p>
                    <Button label="FECHAR" onClick={this.returnToPostalScreen} big_style no_decor_style />
                </div>
            </GridWrapper>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    let { customerDetails } = state;
    return {
        customerDetails
    }
};

export default connect(mapStateToProps, { hideModal })(CustomerDetailsValidModal);
export { CustomerDetailsValidModal as CustomerDetailsValidModalNotConnected };