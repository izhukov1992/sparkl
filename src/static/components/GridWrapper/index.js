import React from 'react';
import { connect } from 'react-redux';

import { hideModal } from '../../actions/modal';

class GridWrapper extends React.Component {
    constructor(props) {
        super(props);

        this.gridClasses = '';
    }

    componentDidMount() {
        this.setGridClasses();
    }

    componentDidUpdate() {
        this.setGridClasses();
    }

    setGridClasses() {
        if (!this.grid) {
            return;
        }

        let oldGridClasses = this.gridClasses;

        let viewportHeight = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
        let gridHeight = Math.max(this.grid.clientHeight, this.grid.offsetHeight);

        let heightDiff = viewportHeight - gridHeight;

        if (heightDiff < 0) {
            this.gridClasses = 'top-margin-modal';
        } else {
            this.gridClasses = '';
        }

        if (oldGridClasses !== this.gridClasses) {
            this.forceUpdate();
        }
    }

    render() {
        return (
            <div className={this.props.requireOverlayImg ? "overlay_wrapper_img" : "overlay_wrapper"}>
                <div className={"grid " + this.gridClasses} ref={grid => { this.grid = grid }} >
                    <div className={this.props.classes}>
                        <article ref={node => { this.node = node }}>
                            {this.props.children}
                        </article>
                    </div>
                </div>
            </div>
        )
    }
}

export default connect()(GridWrapper);
export { GridWrapper as GridWrapperNotConnected };