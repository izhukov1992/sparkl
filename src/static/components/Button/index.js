import React from 'react';
import { connect } from 'react-redux';

class Button extends React.Component {
  render() {
    const classes = [];

    if (this.props.big_style) {
      classes.push('bto-big');
    }

    if (this.props.no_decor_style) {
      classes.push('no-decor');
    }    

    if (this.props.add_style) {
      classes.push('bto-add');
    }

    if (this.props.add_txt_style) {
      classes.push('bto-add-txt');
    }

    if (this.props.small_style) {
      classes.push('bto-small');

      if (this.props.left_style) {
        classes.push('bto-small-left');
      } else if (this.props.right_style) {
        classes.push('bto-small-right');
      }
    }

    if (this.props.margin_top_style) {
      classes.push('margin-top');
    }

    return (
      <div>
        {this.props.button_submit
            ? <input type="submit" className={classes.length ? classes.join(' ') : ''} value={this.props.label} />
            : <a href="javascript:void(0)" onClick={this.props.onClick} className={classes.length ? classes.join(' ') : ''}>{this.props.label}</a>
        }
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
  };
};

export default connect(mapStateToProps)(Button);
export { Button as ButtonNotConnected };
