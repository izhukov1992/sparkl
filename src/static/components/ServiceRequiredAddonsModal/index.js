import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import _ from 'lodash';

import { GridWrapper, Button, ServiceItem } from '../index';
import { addRequiredAddonsToCart } from '../../actions/shoppingCart';
import { getRequiredAddonsForPendingServices } from '../../reducers/services';

class ServiceRequiredAddonsModal extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      requiredAddons: this.props.requiredAddons.map((requiredAddon) => {
        return {
          ...requiredAddon
        }
      }),
      actionCounter: this.props.actionCounter
    };

    this.confirmRequiredAddons = this.confirmRequiredAddons.bind(this);
    this.onIncrease = this.onIncrease.bind(this);
    this.onDecrease = this.onDecrease.bind(this);
  }

  confirmRequiredAddons = (e) => {
    e.preventDefault();

    let { pendingServiceItems, nextScreen, successCallback, cartCallback, increment } = this.props;
    let { requiredAddons, actionCounter } = this.state;

    this.props.addRequiredAddonsToCart({
      pendingServices: pendingServiceItems,
      requiredAddons,
      actionCounter,
      nextScreen,
      successCallback,
      cartCallback,
      increment
    });
  }

  onIncrease(item) {
    if (!item) {
      return;
    }

    this.setState({
      requiredAddons: this.state.requiredAddons.map((requiredAddon) => {
        return {
          ...requiredAddon,
          quantity: (requiredAddon === item) ? ++requiredAddon.quantity : requiredAddon.quantity
        }
      }),
      actionCounter: ++this.state.actionCounter
    });
  }

  onDecrease(item) {
    if (!item || item.quantity === 0) {
      return;
    }

    this.setState({
      requiredAddons: this.state.requiredAddons.map((requiredAddon) => {
        return {
          ...requiredAddon,
          quantity: (requiredAddon === item) ? --requiredAddon.quantity : requiredAddon.quantity          
        }
      }),
      actionCounter: --this.state.actionCounter
    });
  }

  render() {
    return (
      <GridWrapper classes="overlay-box">
        <div className="content_popup_pref-service">
          <h1 className="sub-tit-section">NECESSITA DE REMOÇÃO?</h1>
          <form className="popup-form">
            <ul className="table-services_pop-up spacer-section no-bullets item-pointer">
              {this.state.requiredAddons.map(requiredAddon => (
                <ServiceItem key={requiredAddon.service.id} serviceItem={requiredAddon} onIncrease={this.onIncrease} onDecrease={this.onDecrease} />
              ))}
            </ul>
            <Button label="SEGUINTE" onClick={this.confirmRequiredAddons} big_style no_decor_style />
          </form>
        </div>
      </GridWrapper >
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    requiredAddons: getRequiredAddonsForPendingServices(state, ownProps.pendingServiceItems)
  }
};

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    addRequiredAddonsToCart: bindActionCreators(addRequiredAddonsToCart, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ServiceRequiredAddonsModal);
export { ServiceRequiredAddonsModal as ServiceRequiredAddonsModalNotConnected };