import React from 'react';
import { connect } from 'react-redux';

class AddSub extends React.Component {
  render() {
    return (
      <div className="left cart-qty">
        <a className="bto-circle" href="javascript:void(0)" onClick={this.props.onSub}><i className="fa fa-minus"></i></a>
        <input className="item-cart-qty" type="text" readOnly name="qty" value={this.props.value} />
        <a className="bto-circle" href="javascript:void(0)" onClick={this.props.onAdd}><i className="fa fa-plus"></i></a>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
  };
};

export default connect(mapStateToProps)(AddSub);
export { AddSub as AddSubNotConnected };
