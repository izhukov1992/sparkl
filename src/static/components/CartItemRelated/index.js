import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { pedi_small } from '../../constants';

class CartItemRelated extends React.Component {
  render() {
    return (
      <li>
        <div className="service-container_add-cart left-txt" onClick={() => this.props.handleClick(this.props.relatedCartItem)}>
          <div className="service-container">
            <a className="bto" href="javascript:void(0)">
              <div className="bto-service_container-left_no-padd">
                <div className="ima_serv_cart">
                  <img src={pedi_small} alt="" className="add-servive-cart" />
                </div>
                <div className="vert-center_add-cart">
                  <div className="vert-center_container">
                    <p className="txt-service-add bold">
                      <span>{this.props.relatedCartItem.service.name}</span>
                      <small></small>
                    </p>
                    <p className="txt-price">{this.props.relatedCartItem.service.price}<small>€</small></p>
                  </div>
                </div>
              </div>
            </a>
          </div>
        </div>
      </li>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    dispatch
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(CartItemRelated);
export { CartItemRelated as CartItemRelatedNotConnected };
