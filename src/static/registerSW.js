import runtime from 'serviceworker-webpack-plugin/lib/runtime';

export default () => {
    if ('serviceWorker' in navigator) {
        const registration = runtime.register({
            scope: '.'
        });
    }
}
