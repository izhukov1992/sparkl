import _ from 'lodash';

export function checkHttpStatus(response) {
    if (response.status >= 200 && response.status < 300) {
        return response;
    }

    const error = new Error(response.statusText);
    error.response = response;
    throw error;
}

export function parseJSON(response) {
    let json = response.json();
    let { status } = response;

    return Promise.resolve(json).then(data => ({
        status,
        json: data
    }));
}

export function removeArrayDuplicates(array, prop) {
    if (!_.isArray(array) || !prop) {
        return array;
    }

    return array.filter(function (obj, pos, arr) {
        if (!_.isObject(obj)) {
            return false;
        }

        return arr.map(function (mapObj) {
            return mapObj[prop];
        }).indexOf(obj[prop]) === pos;
    });
}
