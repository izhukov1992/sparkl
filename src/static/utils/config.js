export const SERVER_URL = '';
export const API_URL = 'api/v1';
export const FB_CART_URL = 'https://m.me/sparklpt';

// config should use named export as there can be different exports,
// just need to export default also because of eslint rules
export { SERVER_URL as default };
