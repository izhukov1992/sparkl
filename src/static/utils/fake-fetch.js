import * as fixtures from '../fixtures';
import _ from 'lodash';
import moment from 'moment';

const fakeFetch = (url, opts) => {
    return new Promise((resolve, reject) => {
        // wrap in timeout to simulate server api call
        setTimeout(() => {
            // check postal code info
            if (url.endsWith('/order/assign_postal_code') && opts.method === 'POST') {
                // get parameters from post request
                let params = JSON.parse(opts.body);

                if (!params['postal_code'] || params['postal_code'].length !== 8 || params['postal_code'].startsWith("9")) {
                    resolve({
                        ok: true,
                        json: () => [],
                        status: 200
                    });
                }

                let { token, postal_code, address } = fixtures.customerDetails.customer;

                resolve({
                    ok: true,
                    json: () => ({
                        customer: {
                            token,
                            postal_code: {
                                id: 1,
                                code: params['postal_code'].substr(0, 4),
                                extension: params['postal_code'].substr(5, 3)
                            },
                            address: {
                                address_unit: '',
                                number: '',
                                area: {
                                    ...address.area
                                }
                            }
                        }
                    }),
                    status: 200
                });

                return;
            }

            // get categories/services data
            if (url.endsWith('/service') && opts.method === 'GET') {
                // Return hardcoded services
                resolve({
                    ok: true,
                    json: () => fixtures.services,
                    status: 200
                });

                return;
            }

            // post pending order details
            if (url.endsWith('/order/update_cart') && opts.method === 'POST') {
                // get parameters from post request
                let params = JSON.parse(opts.body);
                // Return hardcoded services
                if (!params || _.isEmpty(params) || params.totalPrice < 12) {
                    resolve({
                        ok: true,
                        json: () => '',
                        status: 403
                    });
                } else {
                    resolve({
                        ok: true,
                        json: () => ({
                            ...fixtures.timeSlots,
                            ...fixtures.profSlots
                        }),
                        status: 200
                    });
                }

                return;
            }

            // get availableTimeSlot
            if (url.indexOf('/order/get_available_slots') !== -1 && opts.method === 'GET') {
                let match = url.match(/date=(.*)(&sp=)?/);

                let startDateTime = moment(match[1], 'MM-DD-YYYY').add(8, 'hours');
                // Return hardcoded timeslots
                resolve({
                    ok: true,
                    json: () => ({
                        "slots": [
                            moment(startDateTime).add(30, 'minutes').format(),
                            moment(startDateTime).add(90, 'minutes').format(),
                            moment(startDateTime).add(120, 'minutes').format(),
                            moment(startDateTime).add(180, 'minutes').format(),
                            moment(startDateTime).add(210, 'minutes').format(),
                            moment(startDateTime).add(270, 'minutes').format(),
                            moment(startDateTime).add(330, 'minutes').format(),
                            moment(startDateTime).add(690, 'minutes').format(),
                            moment(startDateTime).add(720, 'minutes').format(),
                            moment(startDateTime).add(750, 'minutes').format()
                        ]
                    }),
                    status: 200
                });

                return;
            }

            // confirm the availability of slots
            if (url.endsWith('/booking_enquiry/assign_datetime') && opts.method === 'POST') {
                // get parameters from post request
                let params = JSON.parse(opts.body);

                resolve({
                    ok: true,
                    json: () => Object.assign({}, params.postal_code ? {
                        datetime: moment()
                    } : {}),
                    status: 200
                });

                return;
            }

            // save customer details
            if (url.endsWith('/booking_enquiry/inquire') && opts.method === 'POST') {
                // get parameters from post request
                let params = JSON.parse(opts.body);

                if (_.isEmpty(params)) {
                    resolve({
                        ok: true,
                        json: () => '',
                        status: 400
                    });
                } else {
                    let { token, postal_code, first_name, last_name, email, mobile_number, full_address } = params;
                    resolve({
                        ok: true,
                        json: () => ({
                            customer: {
                                token,
                                postal_code,
                                first_name,
                                last_name,
                                email,
                                mobile_number,
                                full_address
                            }
                        }),
                        status: 201
                    });
                }

                return;
            }

            // get categories/services data
            if (url.indexOf('/address/get_numbers?postalcode=' !== -1) && opts.method === 'GET') {
                // Return hardcoded street numbers
                resolve({
                    ok: true,
                    json: () => ({
                        ...fixtures.streetNumbers,
                        address: {
                            branch_name: 'Avenida dos Defensores de Chaves'
                        }
                    }),
                    status: 200
                });

                return;
            }

            // confirm the availability of date/time slots
            if (url.endsWith('/booking_enquiry/end/') && opts.method === 'POST') {
                // get parameters from post request
                let params = JSON.parse(opts.body);

                resolve({
                    ok: true,
                    json: () => ({
                        expiration_datetime: "2018-04-20T16:30:00",
                        entity_id: "12345",
                        code: "000.100.110",
                        checkout_id: 'sample_checkout_id'
                    }),
                    status: 200
                });

                return;
            }

            // pass through any requests not handled above
            fetch(url, opts).then(response => resolve(response));

        }, 50);
    });
}

export default fakeFetch;