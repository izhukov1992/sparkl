export const AUTH_LOGIN_USER_REQUEST = 'AUTH_LOGIN_USER_REQUEST';
export const AUTH_LOGIN_USER_FAILURE = 'AUTH_LOGIN_USER_FAILURE';
export const AUTH_LOGIN_USER_SUCCESS = 'AUTH_LOGIN_USER_SUCCESS';
export const AUTH_LOGOUT_USER = 'AUTH_LOGOUT_USER';

export const DATA_FETCH_PROTECTED_DATA_REQUEST = 'DATA_FETCH_PROTECTED_DATA_REQUEST';
export const DATA_RECEIVE_PROTECTED_DATA = 'DATA_RECEIVE_PROTECTED_DATA';

export const LOAD_ALL_SERVICES = 'LOAD_ALL_SERVICES';
export const VERIFY_POSTAL_CODE = 'VERIFY_POSTAL_CODE';
export const SAVE_CUSTOMER_DETAILS = 'SAVE_CUSTOMER_DETAILS';
export const ADD_SERVICE = 'ADD_SERVICE';
export const ADD_PENDING_SERVICE = 'ADD_PENDING_SERVICE';
export const REMOVE_PENDING_SERVICE = 'REMOVE_PENDING_SERVICE';
export const CONFIRM_PENDING_SERVICE = 'CONFIRM_PENDING_SERVICE';
export const CONFIRM_CHECKOUT = 'CONFIRM_CHECKOUT';
export const POST_ORDER = 'POST_ORDER';
export const SET_POSTAL_CODE = 'SET_POSTAL_CODE';
export const UPDATE_CART = 'UPDATE_CART';
export const UPDATE_APP = 'UPDATE_APP';

export const CUSTOMER_DETAILS = 'CUSTOMER_DETAILS';
export const CUSTOMER_DETAILS_VALID = 'CUSTOMER_DETAILS_VALID';
export const SERVICE_INFO = 'SERVICE_INFO';
export const SERVICE_REQUIRED_ADDONS = 'SERVICE_REQUIRED_ADDONS';
export const POPUP_TALK = 'POPUP_TALK';
export const CART_SUMMARY = 'CART_SUMMARY';
export const LIVE_CHAT = 'LIVE_CHAT';
export const PAYMENT_REFERENCE = 'PAYMENT_REFERENCE';
export const PAYMENT_MBWAY = 'PAYMENT_MBWAY';
export const PAYMENT_VISA = 'PAYMENT_VISA';

export const CHANGE_ACTIVE_DATE = 'CHANGE_ACTIVE_DATE';
export const CHANGE_ACTIVE_TIME = 'CHANGE_ACTIVE_TIME';
export const CHANGE_ACTIVE_PROFS = 'CHANGE_ACTIVE_PROFS';
export const LOAD_DATES = 'LOAD_DATES';
export const LOAD_TIMES = 'LOAD_TIMES';
export const LOAD_PROFS = 'LOAD_PROFS';
export const RESET_PROF_FILTER = 'RESET_PROF_FILTER';
export const RESET_AVAILABLE_TIME_SLOTS = 'RESET_AVAILABLE_TIME_SLOTS';
export const SET_AVAILABLE_TIME_SLOTS = 'SET_AVAILABLE_TIME_SLOTS';
export const QUERY_TIME_SLOTS = 'QUERY_TIME_SLOTS';
export const LOAD_INITIAL_ORDER_DATA = 'LOAD_INITIAL_ORDER_DATA';
export const CHECK_SLOTS_AVAILABILITY = 'CHECK_SLOTS_AVAILABILITY';
export const RESET_ACTIVE_TIME = 'RESET_ACTIVE_TIME';
export const CHANGE_ADDRESS_UNIT = 'CHANGE_ADDRESS_UNIT';
export const POSTAL_CODE = 'POSTAL_CODE';
export const ADDRESS_NUMBER = 'ADDRESS_NUMBER';
export const CHANGE_BOOKING_DETAILS = 'CHANGE_BOOKING_DETAILS';
export const CHANGE_ADDRESS_DETAILS = 'CHANGE_ADDRESS_DETAILS';
export const SET_BRANCH_INFO = 'SET_BRANCH_INFO';
export const RESET_SLOTS = 'RESET_SLOTS';
export const SET_PAYMENT_METHOD = 'SET_PAYMENT_METHOD';

export const FETCH_STARTED = 'FETCH_STARTED';
export const FETCH_COMPLETED = 'FETCH_COMPLETED';

export const SHOW_MODAL = 'SHOW_MODAL';
export const HIDE_MODAL = 'HIDE_MODAL';

export const START = '_START';
export const SUCCESS = '_SUCCESS';
export const FAIL = '_FAIL';
export const ERROR = 'ERROR';

export const PATH_ROOT = '/';
export const PATH_OTHER = '*';
export const PATH_POSTAL = '/postal';
export const PATH_CATEGORIES = '/categories';
export const PATH_SERVICES = '/services';
export const PATH_ADDONS = '/addons';
export const PATH_CART = '/cart';
export const PATH_SESSION = '/session';
export const PATH_BOOKING = '/booking';
export const PATH_CHECKOUT = '/checkout';
export const PATH_CONFIRM = '/confirm';
export const PATH_STATIC = '/static';

export backward from '../img/backward.svg';
export forward from '../img/forward.svg';
export chat from '../img/chat.svg';
export home from '../img/home.svg';
export sparkl_logo from '../img/sparkl_logo.svg';
export cart from '../img/cart.svg';
export pedi_small from '../img/ima_pedi_small.jpg';
export girl from '../img/ima_girl.jpg';
export prof_joana from '../img/prof_joana.jpg';
export arrow from '../img/arrow.svg';
export manicure from '../img/ima_service_manicure.jpg';
export info from '../img/info.svg';
export finger from '../img/finger.svg';
export img_info from '../img/ima_info_makeup.jpg';
export avatar_default from '../img/avatar_default.jpg'; 
export pattern_background from '../img/pattern_background_no-image.gif';
export arrow_address from '../img/arrow_address.svg';

import img_manicure from '../img/ima_service_manicure.jpg';
import img_pedicure from '../img/ima_service_pedicure.jpg';
import img_makeup from '../img/ima_service_makeup.jpg';
import img_hairstyle from '../img/ima_service_hairstyle.jpg';
import img_depilacao from '../img/ima_service_depilacao.jpg';
import img_massagens from '../img/ima_service_massagens.jpg';
import img_noivas from '../img/ima_service_noivas.jpg';
import img_masculino from '../img/ima_service_masculino.jpg';

// Error messages
export const AMOUNT_NOT_CONFIRMED = 'O valor do seu  agendamento tera de ser superior a 12€';
export const NO_CART_SERVICES = 'Estes add-ons têm de ser marcados juntamente com os serviços correspondentes';
export const TIME_SLOT_BUSY = 'A data selecionada já não se encontra disponível';

export const nightFeeService = {
    service: {
        id: 999999,
        name: 'Taxa noturna',
        price: 3
    },
    quantity: 1
};