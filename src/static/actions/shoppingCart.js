import _ from 'lodash';
import { push } from 'react-router-redux';

import {
  CONFIRM_CHECKOUT, SHOW_MODAL, HIDE_MODAL,
  SERVICE_REQUIRED_ADDONS, START, SUCCESS, 
  FAIL, RESET_SLOTS, UPDATE_CART
} from '../constants';
import { getDefaultRequiredAddon } from '../reducers';

export function updateCart(data) {
  return (dispatch, getState) => {
    let { serviceItems, nextScreen, actionCounter, successCallback, cartCallback, increment } = data;

    if (!serviceItems || !serviceItems.length) {
      if (nextScreen) {
        return dispatch(push(nextScreen));
      } else if (successCallback && _.isFunction(successCallback)) {
        successCallback();
      } else {
        return;
      }
    }

    let filteredServices = serviceItems.filter((serviceItem) => {
      if (!serviceItem || !serviceItem.service || !serviceItem.quantity) {
        return false;
      }

      const { required_addons } = serviceItem.service;
      const requiredAddonsLength = _.result(serviceItem.service.required_addons, 'length', 0);

      if (!requiredAddonsLength) {
        return false;
      }

      const { defaultAddon, applyFallback } = getDefaultRequiredAddon(getState(), serviceItem.service.id, serviceItems);
      if (applyFallback) {
        serviceItem = {
          ...serviceItem,
          applyFallback,
          fallbackAddon: defaultAddon
        };
        return false;
      } else {
        return true;
      }
    });

    if (filteredServices.length) {
      dispatch({
        type: SHOW_MODAL,
        modalType: SERVICE_REQUIRED_ADDONS,
        modalProps: {
          pendingServiceItems: serviceItems,
          nextScreen,
          actionCounter,
          successCallback,
          cartCallback,
          increment
        }
      });
    } else {
      dispatch({
        type: UPDATE_CART,
        pendingServiceItems: serviceItems,
        increment
      });

      if (actionCounter) {
        dispatch({
          type: RESET_SLOTS
        });
      }

      nextScreen && dispatch(push(nextScreen));

      if (successCallback && _.isFunction(successCallback)) {
        successCallback();
      }

      if (cartCallback && _.isFunction(cartCallback)) {
        cartCallback(serviceItems);
      }
    }
  }
}

export function addRequiredAddonsToCart(data) {
  return (dispatch, getState) => {
    let { pendingServices, requiredAddons = [], nextScreen, actionCounter, successCallback, cartCallback, increment } = data;
    requiredAddons.forEach((addon) => {
      addon.alwaysIncrement = true;
    });

    let allServices = [...(pendingServices || []), ...(requiredAddons || [])];

    dispatch({
      type: UPDATE_CART,
      pendingServiceItems: allServices,
      increment
    });

    dispatch({
      type: HIDE_MODAL
    });

    if (actionCounter) {
      dispatch({
        type: RESET_SLOTS
      });
    }

    nextScreen && dispatch(push(nextScreen));

    if (successCallback && _.isFunction(successCallback)) {
      successCallback();
    }

    if (cartCallback && _.isFunction(cartCallback)) {
      cartCallback(allServices);
    }
  }
}