import { push } from 'react-router-redux';

import { LOAD_ALL_SERVICES, POST_ORDER, LOAD_INITIAL_ORDER_DATA, PATH_SESSION, ERROR, SHOW_MODAL } from '../constants';
import { getCartOrder, getTotalCartPrice } from '../reducers';

export function loadAllServices() {
    return {
        type: LOAD_ALL_SERVICES,
        callAPI: {
            url: '/service',
            method: 'GET'
        },
    }
}

export function postPendingOrder() {
    return (dispatch, getState) => {
        const { shoppingCart } = getState();
        let relatedAddons = shoppingCart.services.filter((serviceInfo) => {
            let { service } = serviceInfo; 
            return service && (service.addon_only === true);
        });

        if (relatedAddons.length === shoppingCart.services.length) {
            return dispatch({
                type: SHOW_MODAL,
                modalType: ERROR,
                modalProps: {
                    msgType: 'NO_CART_SERVICES'
                }
            });
        }

        const orderDetails = getCartOrder(getState());
        dispatch({
            type: POST_ORDER,
            callAPI: {
                url: '/order/update_cart',
                method: 'POST',
                body: {
                    services: orderDetails,
                    totalPrice: getTotalCartPrice(getState())
                }
            },
            successCallback: (response) => {
                if (response && response.status === 200) {
                    dispatch({
                        type: LOAD_INITIAL_ORDER_DATA,
                        data: response.json
                    });
                    dispatch(push(`${PATH_SESSION}`));
                }

                return response;
            },
            failCallback: (error) => {
                error.response && (error.response.status === 403) && dispatch({
                    type: SHOW_MODAL,
                    modalType: ERROR,
                    modalProps: {
                        msgType: 'AMOUNT_NOT_CONFIRMED'
                    }
                });

                return error;
            }
        });
    }
}