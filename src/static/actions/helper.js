import { UPDATE_APP } from '../constants';

export function updateApp() {
  return (dispatch) => {
    setTimeout(() => {
      dispatch({
        type: UPDATE_APP
      });
    });
  }


}