import moment from 'moment';
import { push } from 'react-router-redux';

import {
    CHANGE_ACTIVE_DATE, CHANGE_ACTIVE_TIME, CHANGE_ACTIVE_PROFS,
    RESET_ACTIVE_PROF, LOAD_DATES, LOAD_TIMES,
    LOAD_PROFS, RESET_AVAILABLE_TIME_SLOTS,
    QUERY_TIME_SLOTS, SET_AVAILABLE_TIME_SLOTS,
    RESET_PROF_FILTER, PATH_BOOKING, CHECK_SLOTS_AVAILABILITY,
    SHOW_MODAL, ERROR, RESET_ACTIVE_TIME, CUSTOMER_DETAILS,
    PATH_SESSION, PATH_CHECKOUT, HIDE_MODAL, PATH_CONFIRM,
    CONFIRM_CHECKOUT, SET_PAYMENT_METHOD, PAYMENT_REFERENCE,
    PAYMENT_VISA

} from '../constants';

export function changeActiveDate(dateSlot) {
    return {
        type: CHANGE_ACTIVE_DATE,
        dateSlot
    }
}

export function changeActiveTime(timeSlot) {
    return {
        type: CHANGE_ACTIVE_TIME,
        timeSlot
    }
}

export function toggleActiveProfs(profSlot) {
    return {
        type: CHANGE_ACTIVE_PROFS,
        profSlot
    }
}

export function resetProfFilter(profSlot) {
    return {
        type: RESET_PROF_FILTER
    }
}

export function resetProf() {
    return {
        type: RESET_ACTIVE_PROF
    }
}

export function resetActiveTime() {
    return {
        type: RESET_ACTIVE_TIME
    }
}

export function loadDates() {
    return {
        type: LOAD_DATES
    }
}

export function loadTimes() {
    return (dispatch, getState) => {
        const { booking } = getState();

        // 1. Reset available slots info
        dispatch({
            type: RESET_AVAILABLE_TIME_SLOTS
        });

        let date = moment(booking.activeDateSlot, 'MM-DD-YYYY').format('MM-DD-YYYY');
        let sp = (booking.activeProfSlot) ? [booking.activeProfSlot.id] : '';

        // 2. Query BE to get a list of available time slots by date and chosen professional
        dispatch({
            type: QUERY_TIME_SLOTS,
            callAPI: {
                url: `/order/get_available_slots?date=${date}&sp=${sp}`,
                method: 'GET'
            },
            successCallback: (response) => {
                if (response && response.status === 200) {
                    // Populate available slots info
                    dispatch({
                        type: SET_AVAILABLE_TIME_SLOTS,
                        slots: response.json.slots
                    });

                    // 
                    dispatch({
                        type: LOAD_TIMES
                    });
                }

                return response;
            }
        });
    }
}

export function checkSlotsAvailability(includePostalCode) {
    return (dispatch, getState) => {
        const { booking, customerDetails } = getState();

        let body = Object.assign({}, {
            datetime: moment(booking.activeTimeSlot.time).format(),
            sps: (booking.activeProfSlot) ? booking.activeProfSlot.id : undefined,
            token: customerDetails.token
        }, includePostalCode ? {
            postal_code: `${customerDetails.postal_code.code}-${customerDetails.postal_code.extension}`
        } : {});

        let successCallback = (() => { }), failCallback = (() => { });

        if (includePostalCode) {
            successCallback = (response) => {
                if (response && response.status === 200) {
                    if (response.json && !response.json.datetime) {
                        dispatch(push(`${PATH_SESSION}`));
                    }

                    dispatch({
                        type: HIDE_MODAL
                    });
                } else {
                    dispatch({
                        type: SHOW_MODAL,
                        modalType: CUSTOMER_DETAILS
                    });
                }

                return response;
            };
        } else {
            successCallback = (response) => {
                if (response && response.status === 200) {
                    dispatch(push(`${PATH_BOOKING}`));
                }

                return response;
            };

            failCallback = (error) => {
                error.response && (error.response.status === 410) && dispatch({
                    type: SHOW_MODAL,
                    modalType: ERROR,
                    modalProps: {
                        msgType: 'TIME_SLOT_BUSY'
                    }
                });

                dispatch({
                    type: LOAD_TIMES
                });

                return error;
            };
        }

        dispatch({
            type: CHECK_SLOTS_AVAILABILITY,
            callAPI: {
                url: '/booking_enquiry/assign_datetime',
                method: 'POST',
                body
            },
            successCallback,
            failCallback
        });
    }
}

export function completeOrder(paymentMethod) {
    return (dispatch, getState) => {
        const { booking, customerDetails } = getState();
        const { first_name, last_name, mobile_number, nif, address, comment, source, token } = customerDetails;

        let body = {
            first_name, last_name, mobile_number, nif, comment, source, token,
            address_id: address.id,
            address_unit: address.address_unit,
            payment_method: paymentMethod
        };

        dispatch({
            type: SET_PAYMENT_METHOD,
            paymentMethod
        });

        dispatch({
            type: CONFIRM_CHECKOUT,
            callAPI: {
                url: '/payment/get_fake_checkout_id/',
                method: 'POST',
                body
            },
            successCallback: (response) => {
                if (response && response.status === 200) {
                    let { expiration_datetime, entity_id, code, payment_method, id, amount, mb_ref, entity } = response.json;
                    expiration_datetime = expiration_datetime || "2018-04-20T16:30:00";

                    if (paymentMethod === 'VISA' || paymentMethod === 'MBWAY') {
                        dispatch({
                            type: SHOW_MODAL,
                            modalType: PAYMENT_VISA,
                            modalProps: {
                                id,
                                payment_method,
                                code,
                                entity_id,
                                expiration_datetime
                            }
                        });
                    } else {
                        dispatch({
                            type: SHOW_MODAL,
                            modalType: PAYMENT_REFERENCE,
                            modalProps: {
                                mb_ref,
                                entity,
                                amount,
                                expiration_datetime
                            }
                        });
                    }
                }

                return response;
            },
            failCallback: (error) => {
                dispatch({
                    type: SHOW_MODAL,
                    modalType: ERROR,
                    modalProps: {
                        msgType: 'TIME_SLOT_BUSY'
                    }
                });
            }
        });
    }
}