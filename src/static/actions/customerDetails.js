import {
    VERIFY_POSTAL_CODE, SHOW_MODAL, CUSTOMER_DETAILS, ADDRESS_NUMBER,
    CUSTOMER_DETAILS_VALID, SAVE_CUSTOMER_DETAILS, PATH_CATEGORIES,
    PATH_CART, SET_POSTAL_CODE, LOAD_ADDRESSES, SET_BRANCH_INFO
} from '../constants';
import { push, replace } from 'react-router-redux'

export function verifyPostalCodeAndCart(postal_code, cart) {
    return (dispatch) => {
        dispatch({
            type: VERIFY_POSTAL_CODE,
            callAPI: {
                url: '/order/assign_postal_code',
                method: 'POST',
                body: {
                    "postal_code": postal_code
                }
            },
            successCallback: (response) => {
                dispatch({
                    type: SET_POSTAL_CODE,
                    postal_code
                });
                if (response && response.json && response.json.customer) {
                    if (cart.services.length) {
                        dispatch(push(`${PATH_CART}`));
                    }
                    else {
                        dispatch(push(`${PATH_CATEGORIES}`));
                    }
                } else {
                    dispatch({
                        type: SHOW_MODAL,
                        modalType: CUSTOMER_DETAILS
                    });
                }

                return response;
            }
        })
    }
}

export function saveCustomerDetails(details) {
    return (dispatch) => {
        dispatch({
            type: SAVE_CUSTOMER_DETAILS,
            callAPI: {
                url: '/booking_enquiry/inquire',
                method: 'POST',
                body: details
            },
            successCallback: (response) => {
                if (response && response.status === 201) {
                    dispatch({
                        type: SHOW_MODAL,
                        modalType: CUSTOMER_DETAILS_VALID
                    });
                }

                return response;
            }
        })
    }
}

export function loadAddresses() {
    return (dispatch, getState) => {
        let { customerDetails } = getState();
        let postal_code = `${customerDetails.postal_code.code}-${customerDetails.postal_code.extension}`;

        dispatch({
            type: LOAD_ADDRESSES,
            callAPI: {
                url: `/address/get_numbers?postalcode=${postal_code}`,
                method: 'GET'
            },
            successCallback: (response) => {
                if (response && response.json && response.status === 200) {
                    let { numbers, address } = response.json;
                    dispatch({
                        type: SET_BRANCH_INFO,
                        data: {
                            numbers,
                            branch_name: address.branch_name
                        }
                    });

                    dispatch({
                        type: SHOW_MODAL,
                        modalType: ADDRESS_NUMBER
                    });
                }

                return response;
            }
        });
    };

}