import { START, SUCCESS, FAIL, FETCH_STARTED, FETCH_COMPLETED } from '../constants'
import { SERVER_URL, API_URL } from '../utils/config';
import { checkHttpStatus, parseJSON } from '../utils';

import realFetch from 'isomorphic-fetch';
import fakeFetch from '../utils/fake-fetch';

let fetch;
if (process.env.NODE_ENV === 'development') { 
    fetch = fakeFetch; 
} else { 
    fetch = realFetch; 
} 

export default store => next => action => {
    const { callAPI, token, type, successCallback, failCallback, ...rest } = action;

    if (!callAPI) return next(action);

    let isFetchInProgress = true;

    next({
        ...rest, type: type + START
    });

    setTimeout(() => {
        if (isFetchInProgress) {
            next({
                type: FETCH_STARTED
            });
        }
    }, 1500);

    let serverUrl = SERVER_URL || '';

    fetch(`${serverUrl}/${API_URL}${callAPI.url}`, {
        method: callAPI.method,
        credentials: 'include',
        body: JSON.stringify(callAPI.body),
        headers: {
            Accept: 'application/json',
            Authorization: `Token ${token}`,
            'content-type': 'application/json'
        }
    })
        .then(checkHttpStatus)
        .then(parseJSON) 
        .then((response) => { 
            next({ ...rest, type: type + SUCCESS, response: response.json });
            successCallback && successCallback(response);
            isFetchInProgress = false;
            next({
                type: FETCH_COMPLETED
            });
        })
        .catch((error) => {
            next({ ...rest, type: type + FAIL, error });
            failCallback && failCallback(error);
            isFetchInProgress = false;
            next({
                type: FETCH_COMPLETED
            });
        });
}