// When the service worker is first added to a computer.
self.addEventListener('install', event => {
})

// After the install event.
self.addEventListener('activate', event => {
})

self.addEventListener('message', event => {
})

self.addEventListener('fetch', event => {
})