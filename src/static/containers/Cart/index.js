import React from 'react';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import moment from 'moment';

import { ServiceItem, CartItemRelated, Button } from '../../components';
import { getRelatedAddons } from '../../reducers';
import { postPendingOrder } from '../../actions/services';
import { updateCart } from '../../actions/shoppingCart';
import { updateApp } from '../../actions/helper';
import { PATH_ADDONS, PATH_SESSION, PATH_CATEGORIES, PATH_SERVICES, girl, nightFeeService } from '../../constants';

class Cart extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      cartItems: this.props.cartItems.map((cartItem) => {
        return {
          ...cartItem
        }
      }),
      actionCounter: 0
    };

    this.back = this.back.bind(this);
    this.next = this.next.bind(this);
    this.checkNext = this.checkNext.bind(this);
    this.goToCategoriesList = this.goToCategoriesList.bind(this);
    this.getAddonsSection = this.getAddonsSection.bind(this);
    this.onIncrease = this.onIncrease.bind(this);
    this.onDecrease = this.onDecrease.bind(this);
    this.getTotalCartPrice = this.getTotalCartPrice.bind(this);
    this.handleRelatedItemClick = this.handleRelatedItemClick.bind(this);
    this.getNightFeeItem = this.getNightFeeItem.bind(this);
    this.getModalServices = this.getModalServices.bind(this);
    this.cartModalCallback = this.cartModalCallback.bind(this);
  }

  componentDidMount() {
    this.props.setBack(this.back);
    this.props.setNext(this.next);
    this.props.setCheckNext(this.checkNext);
    this.props.setGetModalServices(this.getModalServices);
    this.props.setCartModalCallback(this.cartModalCallback);
  }

  back(e) {
    e.preventDefault();

    if (this.props.relatedCartItems.length) {
      this.props.dispatch(push(`${PATH_ADDONS}`));
    } else {
      this.props.dispatch(push(`${PATH_SERVICES}`));
    }
  }

  next(e) {
    e.preventDefault();

    let changedServices = this.state.cartItems.filter((cartItem) => {
      let index = this.props.cartItems.findIndex((item) => {
        return item.service.id === cartItem.service.id;
      });

      if (index === -1) {
        return true;
      }

      return this.props.cartItems[index].quantity !== cartItem.quantity;    
    });

    this.props.dispatch(updateCart({
      serviceItems: changedServices, 
      successCallback: () => this.props.dispatch(postPendingOrder()),
      actionCounter: this.state.actionCounter,
      increment: false
    }));
  }

  checkNext() {
    return (this.state.cartItems.reduce((counter, item) => {
      return counter + item.quantity;
    }, 0)) && (this.getTotalCartPrice() >= 12);
  }

  getModalServices() {
    return [...this.state.cartItems];
  }

  cartModalCallback(services) {
    if (!services || !services.length) {
      return;
    }
    
    this.setState({
      cartItems: this.props.state.shoppingCart.services.filter((item) => {
        return item && item.service.price;
      }).map((item) => ({
        ...item,
        alwaysIncrement: false
      })),
      actionCounter: 0
    });
  }

  goToCategoriesList(e) {
    e.preventDefault();
    this.props.dispatch(push(`${PATH_CATEGORIES}`));
  }

  onIncrease(item) {
    if (!item) {
      return;
    }

    this.setState({
      cartItems: this.state.cartItems.map((cartItem) => {
        return {
          ...cartItem,
          quantity: (cartItem === item) ? ++cartItem.quantity : cartItem.quantity
        }
      }),
      actionCounter: ++this.state.actionCounter
    });

    this.props.dispatch(updateApp());
  }

  onDecrease(item) {
    if (!item || item.quantity === 0) {
      return;
    }

    this.setState({
      cartItems: this.state.cartItems.map((cartItem) => {
        return {
          ...cartItem,
          quantity: (cartItem === item) ? --cartItem.quantity : cartItem.quantity
        };
      }),
      actionCounter: ++this.state.actionCounter
    });

    this.props.dispatch(updateApp());
  }

  getTotalCartPrice() {
    var result = this.state.cartItems.reduce((total, cartItem) => {
      return total + cartItem.service.price * cartItem.quantity;
    }, 0);

    // Add night fee price
    if (this.props.activeTimeSlot && moment(this.props.activeTimeSlot.time).hours() >= 20) {
      result += nightFeeService.service.price;
    }

    return result;
  }

  handleRelatedItemClick(relatedItem) {
    if (!relatedItem || !relatedItem.service) {
      return;
    }

    let index = this.state.cartItems.findIndex((cartItem) => {
      return cartItem.service.id === relatedItem.service.id;
    });

    if (index === -1) {
      this.setState({
        cartItems: [...this.state.cartItems, {
          ...relatedItem,
          quantity: 0
        }]
      });
    } else {
      this.setState({
        cartItems: this.state.cartItems.map((cartItem, itemIndex) => {
          return {
            ...cartItem,
            quantity: (itemIndex === index) ? ++cartItem.quantity : cartItem.quantity
          }
        })
      });
    }
  }

  getNightFeeItem() {
    if (!this.props.activeTimeSlot || !this.props.activeTimeSlot.time) {
      return null;
    }

    if (moment(this.props.activeTimeSlot.time).hours() >= 20) {
      return <ServiceItem key={nightFeeService.service.id} serviceItem={nightFeeService} onIncrease={this.onIncrease} onDecrease={this.onDecrease} hideCounts={true} />
    } else {
      return null;
    }
  }

  render() {
    return (
      <article className="padding-top">
        <div className="item-type_service-geral-small">
          <img src={girl} alt="" />
        </div>

        <section>
          <svg className="wave_top" id="Layer_1" x="0px" y="0px"
            viewBox="0 0 380 26" enableBackground="new 0 0 380 26">
            <path className="wave_section" d="M364,23.4C345.7,22.3,342.5,0,320.6,0h-262C36.6,0,33.5,22.3,15.1,23.4H0V26h380v-2.6H364z" />
          </svg>
          <h2 className="sub-tit-section">A SUA ENCOMENDA</h2>
          <form>
            <ul className="table-services spacer-section no-bullets">
              {this.state.cartItems.map(cartItem => (
                <ServiceItem key={cartItem.service.id} serviceItem={cartItem} onIncrease={this.onIncrease} onDecrease={this.onDecrease} />
              ))}
              {this.getNightFeeItem()}
            </ul>

            <section className="center max-width-container">
              <p className="item-total item-total-dashed-top">TOTAL: {this.getTotalCartPrice()}€</p>
              <Button onClick={this.goToCategoriesList} label="ADICIONAR MAIS SERVIÇOS" add_style />
            </section>
            {this.getAddonsSection()}
          </form>
        </section>
      </article>
    );
  }

  getAddonsSection() {
    if (!this.props.relatedCartItems.length) {
      return null;
    }

    return (
      <section className="center spacer-section padding-bottom">
        <p className="sub-tit-section">OUTRAS OFERTAS PARA SI</p>

        <ul className="table-services spacer-section no-bullets">
          {this.props.relatedCartItems.map(relatedCartItem => (
            <CartItemRelated key={relatedCartItem.service.id} relatedCartItem={relatedCartItem} handleClick={this.handleRelatedItemClick}/>
          ))}
        </ul>
      </section>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    cartItems: state.shoppingCart.services.filter((item) => {
      return item && item.service.price;
    }).map((item) => ({
      ...item,
      alwaysIncrement: false
    })),
    state: state,
    relatedCartItems: getRelatedAddons(state),
    activeTimeSlot: state.booking.activeTimeSlot
  };
};

export default connect(mapStateToProps)(Cart);
export { Cart as CartNotConnected };