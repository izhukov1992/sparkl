import React from 'react';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';

import { getCategoryById } from '../../reducers/services'  ;
import { getServiceItemsByCategoryId, getCartTotalAmount } from '../../reducers';
import { updateCart } from '../../actions/shoppingCart';
import { updateApp } from '../../actions/helper';
import { CategorySlider, ServiceItem } from '../../components';
import { PATH_CATEGORIES, PATH_ADDONS, SHOW_MODAL, POPUP_TALK } from '../../constants';

class Services extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      serviceItems: this.props.serviceItems.map((serviceItem) => {
        return {
          ...serviceItem
        }
      }),
      actionCounter: 0
    };

    this.back = this.back.bind(this);
    this.next = this.next.bind(this);
    this.checkNext = this.checkNext.bind(this);
    this.onIncrease = this.onIncrease.bind(this);
    this.onDecrease = this.onDecrease.bind(this);
  }

  componentWillMount() {
    if (!this.state.serviceItems.length) {
      this.props.dispatch({
        type: SHOW_MODAL,
        modalType: POPUP_TALK,
        modalProps: {
        }
      });
    }
  }

  componentDidMount() {
    this.props.setBack(this.back);
    this.props.setNext(this.next);
    this.props.setCheckNext(this.checkNext);
  }

  back(e) {
    e.preventDefault();
    this.props.dispatch(push(`${PATH_CATEGORIES}`));
  }

  next(e) {
    e.preventDefault();

    this.props.dispatch(updateCart({
      serviceItems: this.state.serviceItems, 
      nextScreen: PATH_ADDONS,
      actionCounter: this.state.actionCounter,
      increment: true
    }));
  }

  checkNext() {
    let pendingServicesCount = this.state.serviceItems.reduce((counter, item) => {
      return counter + item.quantity;
    }, 0);

    return pendingServicesCount + this.props.cartCount;
  }

  onIncrease(item) {
    if (!item) {
      return;
    }

    this.setState({
      serviceItems: this.state.serviceItems.map((serviceItem) => {
        return {
          ...serviceItem,
          quantity: (serviceItem === item) ? ++serviceItem.quantity : serviceItem.quantity
        }
      }),
      actionCounter: ++this.state.actionCounter
    });

    this.props.dispatch(updateApp());
  }

  onDecrease(item) {
    if (!item || item.quantity === 0) {
      return;
    }

    this.setState({
      serviceItems: this.state.serviceItems.map((serviceItem) => {
        return {
          ...serviceItem,
          quantity: (serviceItem === item) ? --serviceItem.quantity : serviceItem.quantity
        };
      }),
      actionCounter: ++this.state.actionCounter
    });

    this.props.dispatch(updateApp());
  }

  render() {
    return (
      <article className="padding-top padding-bottom">
        <CategorySlider category={this.props.category} />

        <section>
          <svg className="wave_top" id="Layer_1" x="0px" y="0px"
            viewBox="0 0 380 26" enableBackground="new 0 0 380 26">
            <path className="wave_section" d="M364,23.4C345.7,22.3,342.5,0,320.6,0h-262C36.6,0,33.5,22.3,15.1,23.4H0V26h380v-2.6H364z" />
          </svg>
          <h2 className="sub-tit-section">Que serviços deseja?</h2>
          <form>
            <ul className="table-services spacer-section no-bullets item-pointer">
              {this.state.serviceItems.map(serviceItem => (
                <ServiceItem key={serviceItem.service.id} serviceItem={serviceItem} onIncrease={this.onIncrease} onDecrease={this.onDecrease} />
              ))}
            </ul>
          </form>
        </section>
      </article>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    category: getCategoryById(state, ownProps.categoryId), 
    serviceItems: getServiceItemsByCategoryId(state, ownProps.categoryId),
    cartCount: getCartTotalAmount(state)
  };
};

export default connect(mapStateToProps)(Services);
export { Services as ServicesNotConnected };
