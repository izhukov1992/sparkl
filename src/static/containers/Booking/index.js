import React from 'react';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import InputMask from 'react-input-mask';
import _ from 'lodash';

import { 
  PATH_SESSION, PATH_CHECKOUT, SHOW_MODAL, 
  CHANGE_ADDRESS_UNIT, POSTAL_CODE, ADDRESS_NUMBER, 
  CHANGE_BOOKING_DETAILS, girl

} from '../../constants';
import { updateApp } from '../../actions/helper';
import { loadAddresses } from '../../actions/customerDetails';

class Booking extends React.Component {
  constructor(props) {
    super(props);

    let { first_name, last_name, mobile_number, email, comment, nif } = this.props.customerDetails;

    this.state = {
      first_name: first_name || '',
      last_name: last_name || '',
      mobile_number: mobile_number || '',
      email: email || '',
      comment: comment || '',
      nif: nif || '',
      isConfirmed: false
  }

    this.back = this.back.bind(this);
    this.next = this.next.bind(this);
    this.checkNext = this.checkNext.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.getFullAddress = this.getFullAddress.bind(this);
    this.getPostalCode = this.getPostalCode.bind(this);
    this.changeAddressUnit = this.changeAddressUnit.bind(this);
    this.openPostalCodeModal = this.openPostalCodeModal.bind(this);
    this.openStreetNumberModal = this.openStreetNumberModal.bind(this);
    this.toggleIsConfirmed = this.toggleIsConfirmed.bind(this);
  }

  back(e) {
    e.preventDefault();
    this.props.dispatch(push(`${PATH_SESSION}`));
  }

  next(e) {
    e.preventDefault();
    this.submitter.click();
  }

  checkNext() {
    if (!this.state.isConfirmed) {
      return false;
    }

    return true;
  }

  handleChange(e) {
    let newValue = e.target.value;

    if (e.target.name === 'mobile_number') {
      if (/^[0-8]$/.test(newValue)) {
        newValue = '+351' + newValue;
      }

      if (/^((\+[1-9]\d{0,14})|(\+)|(9\d{0,14}))?$/.test(newValue)) {
        this.setState({[e.target.name]: newValue});
      }
    } else {
      this.setState({[e.target.name]: newValue});
    }

    this.props.dispatch(updateApp());
  }

  handleSubmit(e) {
    e.preventDefault();

    this.props.dispatch({
      type: CHANGE_BOOKING_DETAILS,
      data: {
        ...this.state
      }
    });

    this.props.dispatch(push(`${PATH_CHECKOUT}`));
  }

  getFullAddress() {
    let { address } = this.props.customerDetails;
    return _.trim(`${address.branch_name || ''} ${_.result(address.number, 'number') || ''} ${address.address_unit || ''}`);
  }

  getPostalCode() {
    let { postal_code } = this.props.customerDetails;
    return `${postal_code.code}-${postal_code.extension}`;
  }

  toggleIsConfirmed(e) {
    this.setState({
      isConfirmed: !this.state.isConfirmed
    });
    this.props.dispatch(updateApp());
  }

  changeAddressUnit(e) {
    this.props.dispatch({
      type: CHANGE_ADDRESS_UNIT,
      newValue: e.target.value
    });

    this.props.dispatch(updateApp());
  }

  openPostalCodeModal() {
    this.props.dispatch({
      type: SHOW_MODAL,
      modalType: POSTAL_CODE
    });

    this.props.dispatch(updateApp());
  }

  openStreetNumberModal() {
    this.props.dispatch(loadAddresses());
    this.props.dispatch(updateApp());
  }

  componentDidMount() {
    this.props.setBack(this.back);
    this.props.setNext(this.next);
    this.props.setCheckNext(this.checkNext);
  }

  render() {
    return (
      <article className="padding-top">
        <div className="item-type_service-geral">
          <img src={girl} alt="" />
        </div>
        <section>
          <svg className="wave_top" id="Layer_1" x="0px" y="0px"
            viewBox="0 0 380 26" enableBackground="new 0 0 380 26">
            <path className="wave_section" d="M364,23.4C345.7,22.3,342.5,0,320.6,0h-262C36.6,0,33.5,22.3,15.1,23.4H0V26h380v-2.6H364z" />
          </svg>
          <h2 className="sub-tit-section">DADOS DA SUA RESERVA</h2>
          <form id="booking-form" onSubmit={this.handleSubmit}>
            <div className="max-width-container">
              <div className="form-container">
                <div className="form-container-single group">
                  <div className="col span_1_of_2">
                    <input type="text" name="first_name" placeholder="Nome" required="required" value={this.state.first_name} onChange={this.handleChange} className="form-field" />
                  </div>
                  <div className="col span_1_of_2">
                    <input type="text" name="last_name" placeholder="Apelido" required="required" value={this.state.last_name} onChange={this.handleChange} className="form-field" />
                  </div>
                </div>
                <div className="form-container-single group">
                  <div className="col span_2_of_2">
                    <input type="email" name="email" placeholder="Email" required="required" value={this.state.email} className="form-field" onChange={this.handleChange} />
                  </div>
                </div>
                <div className="form-container-single group">
                  <div className="col span_2_of_2">
                    <input type="text" name="full_address" placeholder="Morada" required="required" value={this.getFullAddress()} onFocus={this.openStreetNumberModal} onChange={() => {}} className="form-field" />
                  </div>
                </div>
                <div className="form-container-single group">
                  <div className="col span_1_of_2">
                    <input type="text" name="number" placeholder="Nº de porta" required="required" value={_.result(this.props.customerDetails.address.number, 'number')} onFocus={this.openStreetNumberModal} onChange={() => {}} className="form-field" />
                  </div>
                  <div className="col span_1_of_2">
                    <input type="text" name="address_unit" placeholder="Andar" value={this.props.customerDetails.address.address_unit} className="form-field" onChange={this.changeAddressUnit} />
                  </div>
                </div>
                <div className="form-container-single group">
                  <div className="col span_1_of_2">
                    <input type="text" name="postal_code" placeholder="Código postal" required="required" className="form-field" value={this.getPostalCode()} readOnly onFocus={this.openPostalCodeModal}/>
                  </div>
                  <div className="col span_1_of_2">
                    <input type="text" name="place" placeholder="Localidade" required="required" value={this.props.customerDetails.address.area.name} className="form-field" readOnly onFocus={this.openPostalCodeModal}/>
                  </div>
                </div>
                <div className="form-container-single group">
                  <div className="col span_1_of_2">
                    <input type="text" name="mobile_number" pattern="^(\+[1-9]\d{0,14})|(\+)|(9\d{0,14})?$" placeholder="Telefone" required="required" className="form-field" value={this.state.mobile_number} onChange={this.handleChange}/>
                  </div>
                  <div className="col span_1_of_2">
                    <input type="text" name="nif" placeholder="NIF" value={this.state.nif} className="form-field" onChange={this.handleChange} />
                  </div>
                </div>
                <div className="form-container-single group">
                  <div className="col span_2_of_2">
                    <textarea name="comment" className="form-field item-text-area" placeholder="COMENTÁRIO" maxLength="30" value={this.state.comment} onChange={this.handleChange}></textarea>
                  </div>
                </div>
                <div className="padding-bottom max-width-container center">
                  <label className="container-checkbox">
                    <p>Aceito termos e condições</p>
                    <input type="checkbox" name="isConfirmed" checked={this.state.isConfirmed ? "checked" : ""} onChange={this.toggleIsConfirmed} /><span className="checkmark"></span>
                  </label>
                </div>
                <input type="submit" name="submitButton" value="twoButton" ref={node => this.submitter = node} hidden />
              </div>
            </div>
          </form>
        </section>
      </article>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  let { customerDetails } = state;
  return {
      customerDetails
  }
};

export default connect(mapStateToProps)(Booking);
export { Booking as BookingNotConnected };
