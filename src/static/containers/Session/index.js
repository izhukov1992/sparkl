import React from 'react';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import moment from 'moment';
import Slider from 'react-slick'

import '../../styles/slick.css';
import '../../styles/slick-theme.css';
import { updateApp } from '../../actions/helper';

import {
  changeActiveDate, changeActiveTime, toggleActiveProfs,
  loadDates, loadTimes, resetProfFilter, checkSlotsAvailability
} from '../../actions/booking';
import { DateItem, TimeItem, Professional } from '../../components';
import { PATH_CART, PATH_BOOKING, girl } from '../../constants';

import 'moment/locale/pt-br';
moment.locale('pt-BR');

const daysResponsiveDetails = [
  {
    breakpoint: 350,
    settings: {
      slidesToShow: 5,
      slidesToScroll: 5
    }
  },
  {
    breakpoint: 450,
    settings: {
      slidesToShow: 7,
      slidesToScroll: 7
    }
  },
  {
    breakpoint: 550,
    settings: {
      slidesToShow: 9,
      slidesToScroll: 9
    }
  },
  {
    breakpoint: 700,
    settings: {
      slidesToShow: 11,
      slidesToScroll: 11
    }
  }
];

const profResponsiveDetails = [
  {
    breakpoint: 350,
    settings: {
      slidesToShow: 2,
      slidesToScroll: 2
    }
  },
  {
    breakpoint: 450,
    settings: {
      slidesToShow: 3,
      slidesToScroll: 3
    }
  },
  {
    breakpoint: 600,
    settings: {
      slidesToShow: 4,
      slidesToScroll: 4
    }
  },
  {
    breakpoint: 700,
    settings: {
      slidesToShow: 5,
      slidesToScroll: 5
    }
  }
];

class Session extends React.Component {
  constructor(props) {
    super(props);

    this.onClickDate = this.onClickDate.bind(this);
    this.onClickTime = this.onClickTime.bind(this);
    this.onClickProf = this.onClickProf.bind(this);
    this.back = this.back.bind(this);
    this.next = this.next.bind(this);
    this.checkNext = this.checkNext.bind(this);
    this.resetProfSlots = this.resetProfSlots.bind(this);
    this.handleSlideChanged = this.handleSlideChanged.bind(this);
    this.getInitialDaySliderIndex = this.getInitialDaySliderIndex.bind(this);
    this.getTimeSlotsContent = this.getTimeSlotsContent.bind(this);
    this.getInitialHourSliderIndex = this.getInitialHourSliderIndex.bind(this);
    this.setDateSliderRef = this.setDateSliderRef.bind(this);
  }

  componentWillMount() {
    this.props.dispatch(loadDates());
    this.props.dispatch(loadTimes());
  }

  componentDidMount() {
    this.props.setBack(this.back);
    this.props.setNext(this.next);
    this.props.setCheckNext(this.checkNext);
  }

  onClickDate(dateSlot) {
    if (!dateSlot) {
      return;
    }

    if (dateSlot.isSameOrBefore(moment(), 'day')) {
      return;
    }

    let screenSlidesAmount = this.getNumberOfDaysScreenSlides();
    let index = this.props.dateSlots.findIndex((currentDateSlot) => {
      return dateSlot.isSame(moment(currentDateSlot, 'MM-DD-YYYY'), 'day')
    });

    (index !== -1) && this.daySlider && this.daySlider.slickGoTo(Math.ceil(index - (screenSlidesAmount - 1) / 2));

    this.props.dispatch(changeActiveDate(dateSlot.format('MM-DD-YYYY')));
    this.props.dispatch(loadDates());
    this.props.dispatch(loadTimes());
    this.props.dispatch(updateApp());
  }

  onClickTime(timeSlot) {
    this.props.dispatch(changeActiveTime(timeSlot));
    this.props.dispatch(updateApp());
  }

  onClickProf(profSlot) {
    this.props.dispatch(toggleActiveProfs(profSlot));
    this.props.dispatch(updateApp());
  }

  back(e) {
    e.preventDefault();
    this.props.dispatch(push(`${PATH_CART}`));
  }

  next(e) {
    e.preventDefault();
    this.props.dispatch(checkSlotsAvailability());
  }

  checkNext() {
    return !!this.props.activeTimeSlot;
  }

  resetProfSlots(e) {
    e.preventDefault();
    this.props.dispatch(resetProfFilter());
    this.props.dispatch(updateApp());
  }

  handleSlideChanged(e) {
    if (!e && e !== 0) {
      return;
    }

    let screenSlidesAmount = this.getNumberOfDaysScreenSlides();
    let indexToSelect = e + (screenSlidesAmount - 1) / 2;

    let dateSlotToSelect = this.props.dateSlots[indexToSelect];
    if (!dateSlotToSelect) {
      return;
    }

    if (moment(dateSlotToSelect, 'MM-DD-YYYY').isSameOrBefore(moment(), 'day')) {
      let tomorrowDateIndex = this.props.dateSlots.findIndex((currentDateSlot) => {
        return moment(currentDateSlot, 'MM-DD-YYYY').isSame(moment().add(1, 'days'), 'day');
      });

      if (tomorrowDateIndex !== -1) {
        dateSlotToSelect = this.props.dateSlots[tomorrowDateIndex];
        setTimeout((() => { 
          this.daySlider && this.daySlider.slickGoTo(Math.ceil(tomorrowDateIndex - (screenSlidesAmount - 1) / 2)); 
        }).bind(this)); 
      }
    }

    this.props.dispatch(changeActiveDate(dateSlotToSelect));
    this.props.dispatch(loadDates());
    this.props.dispatch(loadTimes());
    this.props.dispatch(updateApp());
  }

  getNumberOfDaysScreenSlides() {
    const breakpoints = daysResponsiveDetails;
    let viewportWidth = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
    let breakpointWidths = breakpoints.filter((respBp) => {
      return respBp.breakpoint > viewportWidth;
    }).sort((bpA, bpB) => {
      return bpA.breakpoint - bpB.breakpoint;
    });

    return breakpointWidths.length ? breakpointWidths[0].settings.slidesToShow : breakpoints[breakpoints.length - 1].settings.slidesToShow;
  }

  getInitialDaySliderIndex() {
    if (!this.props.activeDateSlot) {
      return 0;
    }

    let screenSlidesAmount = this.getNumberOfDaysScreenSlides();

    let index = this.props.dateSlots.findIndex((currentDateSlot) => {
      return moment(this.props.activeDateSlot, 'MM-DD-YYYY').isSame(moment(currentDateSlot, 'MM-DD-YYYY'), 'day')
    });

    if (index === -1) {
      return 0;
    }

    return Math.max(Math.ceil(index - (screenSlidesAmount - 1) / 2), 0);
  }

  getInitialHourSliderIndex() {
    if (!this.props.activeTimeSlot) {
      return 0;
    }

    let index = this.props.timeSlots.findIndex((currentTimeSlot) => {
      return moment(this.props.activeTimeSlot.time).isSame(moment(currentTimeSlot.time), 'minute')
    });

    if (index === -1) {
      return 0;
    }

    return Math.floor(index / 12);
  }

  getTimeSlotsContent() {
    if (this.props.timeSlots.length !== 30) {
      return null;
    }

    let slotScreens = [];
    for (let i = 0; i < 3; i++) {
      slotScreens[i] = [...this.props.timeSlots.slice(i * 12, (i + 1) * 12)];
    }

    return (
      <div className="hours-container spacer-section margin-center">
        <Slider
          infinite={false} centerPadding={0}
          initialSlide={this.getInitialHourSliderIndex()} 
          ref={slider => this.timeSlider = slider}
          dots={true}
        >
          {slotScreens.map((slotScreen, index) => (
            <div key={index}>
              {slotScreen.map((timeItem) => (
                <TimeItem key={timeItem.id} timeItem={timeItem} onChange={this.onClickTime} />
              ))}
            </div>
          ))}
        </Slider>
      </div>
    );
  }

  setDateSliderRef(slider) {
    this.daySlider = slider;
    setTimeout(() => {
      slider && slider.slickGoTo(this.getInitialDaySliderIndex());
    });
  }

  getNightFeeNote() {
    if (!this.props.activeTimeSlot || !this.props.activeTimeSlot.time) {
      return null;
    }

    if (moment(this.props.activeTimeSlot.time).hours() >= 20) {
      return <p className="tit-small-night-fee spacer-section">Taxa noturna de 3€ apartir das 20h</p>
    } else {
      return null;
    }
  }

  render() {
    return (
      <article className="padding-top padding-bottom-large">
        <div className="item-type_service-geral-small">
          <img src={girl} alt="" />
        </div>
        <section>
          <svg className="wave_top" id="Layer_1" x="0px" y="0px"
            viewBox="0 0 380 26" enableBackground="new 0 0 380 26">
            <path className="wave_section" d="M364,23.4C345.7,22.3,342.5,0,320.6,0h-262C36.6,0,33.5,22.3,15.1,23.4H0V26h380v-2.6H364z" />
          </svg>
          <h2 className="sub-tit-section">QUANDO DESEJA O SERVIÇO</h2>
        </section>
        <form>
          <div className="max-width-container spacer-section center margin-center padding-bottom">
            <div className="days-container">
              <Slider
                slidesToShow={11} slidesToScroll={8} infinite={false} centerPadding={0}
                initialSlide={this.getInitialDaySliderIndex()} afterChange={this.handleSlideChanged}
                responsive={daysResponsiveDetails}
                ref={this.setDateSliderRef}
              >
                {this.props.dateSlots.map(dateItem => (
                  <div key={moment(dateItem, 'MM-DD-YYYY').toString()} >
                    <DateItem dateItem={moment(dateItem, 'MM-DD-YYYY')} onChange={this.onClickDate} />
                  </div>
                ))}
              </Slider>
            </div>

            {this.getTimeSlotsContent()}

            {this.getNightFeeNote()} 
            <p className="tit-small-prof spacer-section">PROFISSIONAIS</p>

            <div className="professionals-container">
              <button className="all-prof-btn" onClick={this.resetProfSlots}>
                <span>TODAS</span>
              </button>
              <div className="professionals-carousel-container">
                <Slider
                  slidesToShow={6} infinite={false} centerPadding={0}
                  slidesToScroll={3} responsive={profResponsiveDetails}
                >
                  {this.props.profSlots.map(professional => (
                    <div key={professional.id}>
                      <Professional professional={professional} onChange={this.onClickProf} />
                    </div>
                  ))}
                </Slider>
              </div>
            </div>
          </div>
        </form>
      </article>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    activeDateSlot: state.booking.activeDateSlot,
    activeTimeSlot: state.booking.activeTimeSlot,
    activeProfSlot: state.booking.activeProfSlot,
    dateSlots: state.booking.dateSlots,
    timeSlots: state.booking.timeSlots,
    profSlots: state.booking.profSlots
  };
};

export default connect(mapStateToProps)(Session);
export { Session as SessionNotConnected };
