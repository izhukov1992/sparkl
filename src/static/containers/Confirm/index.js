import React from 'react';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';

import { PATH_POSTAL } from '../../constants';
import { GridWrapper, Button } from '../../components';

class Confirm extends React.Component {
  constructor(props) {
    super(props);

    this.openPostalCodeScreen = this.openPostalCodeScreen.bind(this);
  }

  openPostalCodeScreen(e) {
    e.preventDefault();
    this.props.dispatch(push(`${PATH_POSTAL}`));
  }

  render() {
    return (
      <GridWrapper requireOverlayImg classes={"overlay-box"}>
          <div className="content content-shadow">
            <h1 className="tit-big-overlay">OBRIGADA PELA SUA RESERVA!</h1>
            <p className="padding-txt">Em breve será contactada por uma das nossas assistentes para validar a sua reserva.</p>
            <Button label="SAIR" big_style onClick={this.openPostalCodeScreen}/>
          </div>
      </GridWrapper>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
  }
};

export default connect(mapStateToProps)(Confirm);
export { Confirm as ConfirmNotConnected };
