import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { loadAllServices } from '../../actions/services'
import { getOrderedCategoryList } from '../../reducers/services'
import { Category } from '../../components';
import { PATH_POSTAL } from '../../constants';

class Categories extends React.Component {
  constructor(props) {
    super(props);

    this.back = this.back.bind(this);
    this.checkNext = this.checkNext.bind(this);
  }

  componentWillMount() {
    const { loaded, loading, loadAllServices } = this.props
    if (!loaded && !loading) loadAllServices();
  }

  componentDidMount() {
    this.props.setBack(this.back);
    this.props.setCheckNext(this.checkNext);
  }

  back(e) {
    e.preventDefault();
    this.props.dispatch(push(`${PATH_POSTAL}`));
  }

  checkNext() {
    return false;
  }

  render() {
    return (
      <article className="wrapper padding-top">
        {this.props.categories.map(category => (
          <Category key={category.id} name={category.name} overlayImg={category.overlay_img} id={category.id} />
        ))}
      </article>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    userName: state.auth.userName,
    statusText: state.auth.statusText,
    loaded: state.services.loadingState.loaded,
    loading: state.services.loadingState.loading,
    categories: getOrderedCategoryList(state)
  };
}

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    loadAllServices: bindActionCreators(loadAllServices, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Categories);
export { Categories as CategoriesNotConnected };
