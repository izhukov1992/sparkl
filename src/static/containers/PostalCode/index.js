import React from 'react';
import { connect } from 'react-redux';
import InputMask from 'react-input-mask';
import { GridWrapper, Button } from '../../components';
import { verifyPostalCodeAndCart } from '../../actions/customerDetails'

class PostalCode extends React.Component {
    constructor(props) {
        super(props);

        let { postal_code } = this.props.customerDetails;
        this.state = {
            postalCode: (postal_code.code && postal_code.extension) ? `${postal_code.code}-${postal_code.extension}` : '',
            cart: this.props.shoppingCart
        }

        this.handleChange = this.handleChange.bind(this);
        this.submitPostalCode = this.submitPostalCode.bind(this);
    }

    handleChange(e) {
        this.setState({
            postalCode: e.target.value
        });
    }

    submitPostalCode(e) {
        e.preventDefault();

        const { postalCode, cart } = this.state;
        this.props.verifyPostalCodeAndCart(postalCode, cart);
    }

    render() {
        return (
            <GridWrapper requireOverlayImg classes={"overlay-box"}>
                <div className="content-shadow">
                    <div className="content_input grey">
                        <h1 className="tit-medium-overlay">ONDE DESEJA FAZER O SEU SERVIÇO?</h1>
                    </div>
                    <div className="content_postal-code">
                        <form onSubmit={this.submitPostalCode}>
                            <InputMask mask="9999-999" pattern="[0-9]{4}-[0-9]{3}" required placeholder="Inserir Código Postal" className="postal-code input-text" maskChar={null} value={this.state.postalCode} onChange={this.handleChange} />
                            <svg className="wave_bottom" id="Layer_1" x="0px" y="0px"
                                viewBox="0 0 310 22" enableBackground="new 0 0 310 22">
                                <path className="wave_field" d="M297,2.2C282.1,3.1,279.4,22,261.5,22H47.8C29.9,22,27.3,3.1,12.3,2.2H0V0h310v2.2H297z" />
                            </svg>
                            <Button label="Seguinte" big_style button_submit />
                        </form>
                    </div>
                </div>
            </GridWrapper>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    let { customerDetails, shoppingCart } = state;
    return {
        customerDetails,
        shoppingCart
    }
};

export default connect(mapStateToProps, { verifyPostalCodeAndCart })(PostalCode);
export { PostalCode as PostalCodeNotConnected };
