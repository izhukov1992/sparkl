import React from 'react';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import moment from 'moment';

import { ServiceItem } from '../../components';
import { PATH_BOOKING, PATH_CONFIRM, girl } from '../../constants';
import { getTotalCartPrice } from '../../reducers';
import { completeOrder } from '../../actions/booking';

class Checkout extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      paymentMethod: 'VISA'
    };

    this.back = this.back.bind(this);
    this.next = this.next.bind(this);
    this.getFormattedCheckoutDate = this.getFormattedCheckoutDate.bind(this);
    this.getBookingWeekDay = this.getBookingWeekDay.bind(this);
    this.getFormattedAddress = this.getFormattedAddress.bind(this);
    this.getFormattedPostalCode = this.getFormattedPostalCode.bind(this);
    this.getCommentBlock = this.getCommentBlock.bind(this);
    this.setPaymentMethod = this.setPaymentMethod.bind(this)
  }

  componentDidMount() {
    this.props.setBack(this.back);
    this.props.setNext(this.next);
  }

  back(e) {
    e.preventDefault();
    this.props.dispatch(push(`${PATH_BOOKING}`));
  }

  next(e) {
    e.preventDefault();
    this.props.dispatch(completeOrder(this.state.paymentMethod));
  }

  getFormattedCheckoutDate() {
    let { activeDateSlot, activeTimeSlot } = this.props.booking;
    if (!activeDateSlot || !activeTimeSlot) {
      return '';
    }

    return `${moment(activeDateSlot, 'MM-DD-YYYY').format('DD MMMM')} ÀS ${moment(activeTimeSlot.time).format('HH:mm')}`;
  }

  getBookingWeekDay() {
    let { activeDateSlot } = this.props.booking;
    if (!activeDateSlot) {
      return '';
    }

    return moment(activeDateSlot, 'MM-DD-YYYY').format('dddd');
  }

  getFormattedAddress() {
    let { address } = this.props.customerDetails;
    return _.trim(`${address.branch_name || ''} ${address.number.number || ''} ${address.address_unit || ''}`);
  }

  getFormattedPostalCode() {
    return `${this.props.customerDetails.postal_code.code}-${this.props.customerDetails.postal_code.extension}`;
  }

  getPaymentTypes() {
    return [
      {
        value: 'VISA',
        caption: 'visa'
      },
      {
        value: 'SIBS_MULTIBANCO',
        caption: 'ent. referencia'
      },
      {
        value: 'MBWAY',
        caption: 'mbway'
      }
    ];
  }

  setPaymentMethod(type) {
    this.setState({
      paymentMethod: type ? type.value : type
    });
  }

  render() {
    return (
      <article className="padding-top">
        <div className="image-section">
          <div className="item-type_service-geral">
            <img src={girl} alt="" />
          </div>
        </div>

        <section>
          <svg className="wave_top" id="Layer_1" x="0px" y="0px"
            viewBox="0 0 380 26" enableBackground="new 0 0 380 26">
            <path className="wave_section" d="M364,23.4C345.7,22.3,342.5,0,320.6,0h-262C36.6,0,33.5,22.3,15.1,23.4H0V26h380v-2.6H364z" />
          </svg>
          <h1 className="sub-tit-section">O SEU AGENDAMENTO</h1>
          <h2 className="tit-section-checkout">{this.getFormattedCheckoutDate()}</h2>
          <p className="week-day-checkout">{this.getBookingWeekDay()}</p>

          <ul className="table-services no-bullets">
            {this.props.checkoutItems.map(checkoutItem => (
              <ServiceItem key={checkoutItem.service.id} serviceItem={checkoutItem} hideCounts={true} />
            ))}
          </ul>

          <div className="max-width-container">
            <p className="item-total item-total-dashed">TOTAL: {this.props.totalPrice}€</p>
          </div>
        </section>
        <section>
          <h2 className="tit-section-addon">FORMA DE PAGAMENTO</h2>          
          <div className="max-width-container">
            <div className="payment-type-dashed">
              <div className="payment-btn-container">
                {this.getPaymentTypes().map((type) => {
                  return (
                    <div 
                      key={type.value}
                      className={'payment-btn' + (type.value === this.state.paymentMethod ? ' active' : '')} 
                      onClick={() => this.setPaymentMethod(type)}>{type.caption}
                    </div>
                  )
                })}         
              </div>
            </div>
          </div>
        </section>
        <section>
          <h2 className="tit-section-addon">OS SEU DADOS</h2>
          <div className="padding-bottom max-width-container spacer-section">
            <div className="form-container">
              <div className="form-container-single group">
                <div className="col span_1_of_2">
                  <p className="form-label">Nome</p>
                  <p className="form-field-order">{this.props.customerDetails.first_name}</p>
                </div>
                <div className="col span_1_of_2">
                  <p className="form-label">Apelido</p>
                  <p className="form-field-order">{this.props.customerDetails.last_name}</p>
                </div>
              </div>
              <div className="form-container-single group">
                <div className="col span_2_of_2">
                  <p className="form-label">Email</p>
                  <p className="form-field-order">{this.props.customerDetails.email}</p>
                </div>
              </div>
              <div className="form-container-single group">
                <div className="col span_2_of_2">
                  <p className="form-label">Morada</p>
                  <p className="form-field-order">{this.getFormattedAddress()}</p>
                </div>
              </div>
              <div className="form-container-single group">
                <div className="col span_1_of_2">
                  <p className="form-label">Código-postal</p>
                  <p className="form-field-order">{this.getFormattedPostalCode()}</p>
                </div>
                <div className="col span_1_of_2">
                  <p className="form-label">Localidade</p>
                  <p className="form-field-order">{this.props.customerDetails.address.area.name}</p>
                </div>
              </div>
              <div className="form-container-single group">
                <div className="col span_1_of_2">
                  <p className="form-label">Telefone</p>
                  <p className="form-field-order">{this.props.customerDetails.mobile_number}</p>
                </div>
                <div className="col span_1_of_2">
                  <p className="form-label">NIF</p>
                  <p className="form-field-order">{this.props.customerDetails.nif}</p>
                </div>
              </div>
              {this.getCommentBlock()}
            </div>
          </div>
        </section>
      </article>
    );
  }

  getCommentBlock() {
    if (!this.props.customerDetails.comment || !this.props.customerDetails.comment.length) {
      return null;
    }

    return (
      <div className="form-container-single group">
        <div className="col span_2_of_2">
          <p className="form-label">Comentário</p>
          <p className="form-field-order">{this.props.customerDetails.comment}</p>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  let { customerDetails, booking } = state;
  return {
    customerDetails,
    booking,
    checkoutItems: state.shoppingCart.services,
    totalPrice: getTotalCartPrice(state)
  };
};

export default connect(mapStateToProps)(Checkout);
export { Checkout as CheckoutNotConnected };
