import React from 'react';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { Redirect } from 'react-router';

import { ServiceItem } from '../../components';
import { getRelatedAddons } from '../../reducers';
import { updateCart } from '../../actions/shoppingCart';
import { PATH_STATIC, PATH_CATEGORIES, PATH_CART, img_manicure } from '../../constants';

class Addons extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      addonItems: this.props.addonItems.map((addonItem) => {
        return {
          ...addonItem
        }
      }),
      actionCounter: 0
    };

    this.back = this.back.bind(this);
    this.next = this.next.bind(this);

    this.onIncrease = this.onIncrease.bind(this);
    this.onDecrease = this.onDecrease.bind(this);
  }

  componentDidMount() {
    this.props.setBack(this.back);
    this.props.setNext(this.next);
  }

  back(e) {
    e.preventDefault();
    this.props.dispatch(push(`${PATH_CATEGORIES}`));
  }

  next(e) {
    e.preventDefault();
    this.props.dispatch(updateCart({
      serviceItems: this.state.addonItems, 
      nextScreen: PATH_CART,
      actionCounter: this.state.actionCounter,
      increment: true
    }));
  }

  onIncrease(item) {
    if (!item) {
      return;
    }

    this.setState({
      addonItems: this.state.addonItems.map((addonItem) => {
        return {
          ...addonItem,
          quantity: (addonItem === item) ? ++addonItem.quantity : addonItem.quantity
        }
      }),
      actionCounter: ++this.state.actionCounter
    });
  }

  onDecrease(item) {
    if (!item || item.quantity === 0) {
      return;
    }

    this.setState({
      addonItems: this.state.addonItems.map((addonItem) => {
        return {
          ...addonItem,
          quantity: (addonItem === item) ? --addonItem.quantity : addonItem.quantity
        }
      }),
      actionCounter: ++this.state.actionCounter
    });
  }

  render() {
    if (!this.props.addonItems.length) {
      return <Redirect to={PATH_CART} />
    }

    return (
      <article className="padding-top padding-bottom">
        <div className="item-type_service-small">
          <img src={`${PATH_STATIC}/${this.props.overlayImg}`} alt="" />
        </div>

        <section>
          <svg className="wave_top" id="Layer_1" x="0px" y="0px"
            viewBox="0 0 380 26" enableBackground="new 0 0 380 26">
            <path className="wave_section" d="M364,23.4C345.7,22.3,342.5,0,320.6,0h-262C36.6,0,33.5,22.3,15.1,23.4H0V26h380v-2.6H364z" />
          </svg>
          <h2 className="sub-tit-section">DESEJA ADICIONAR EXTRAS?</h2>
          <form>
            <ul className="table-services spacer-section no-bullets item-pointer">
              {this.state.addonItems.map(addonItem => (
                <ServiceItem key={addonItem.service.id} serviceItem={addonItem} onIncrease={this.onIncrease} onDecrease={this.onDecrease} />
              ))}
            </ul>
          </form>
        </section>
      </article>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    addonItems: getRelatedAddons(state),
    overlayImg: state.services.categories[0] ? state.services.categories[0].overlay_img : img_manicure
  };
};

export default connect(mapStateToProps)(Addons);
export { Addons as AddonsNotConnected };
