import React from 'react';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'react-router-redux';
import PropTypes from 'prop-types';

import DevTools from './DevTools';
import App from '../../app';
import Routes from '../../routes';
import { ModalWrapper, LoadingSpinner } from '../../components';

import { PersistGate } from 'redux-persist/integration/react';


export default class Root extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      onBack: null,
      onNext: null,
      cartModalCallback: () => true,
      getModalServices: () => [],
      checkNext: () => true,
      modal: null
    }

    this.setBack = this.setBack.bind(this);
    this.setNext = this.setNext.bind(this);
    this.setModal = this.setModal.bind(this);
    this.setCheckNext = this.setCheckNext.bind(this);
    this.setCartModalCallback = this.setCartModalCallback.bind(this);
    this.setGetModalServices = this.setGetModalServices.bind(this);

    let touchsupport = ('ontouchstart' in window) || (navigator.maxTouchPoints > 0) || (navigator.msMaxTouchPoints > 0);
    if (!touchsupport) { // browser doesn't support touch
      document.documentElement.className += " non-touch"
    }
  }

  setBack(f) {
    this.setState({
      onBack: f
    });
  }

  setNext(f) {
    this.setState({
      onNext: f
    });
  }

  setCheckNext(f) {
    this.setState({
      checkNext: f
    });
  }

  setModal(type) {
    this.setState({
      modal: type
    });
  }

  setCartModalCallback(f) {
    this.setState({
      cartModalCallback: f
    });
  }

  setGetModalServices(f) {
    this.setState({
      getModalServices: f
    });
  }

  static propTypes = {
    store: PropTypes.shape().isRequired,
    history: PropTypes.shape().isRequired
  };

  render() {
    return (
      <div id="provider">
        <Provider store={this.props.store}>
          <PersistGate loading={null} persistor={this.props.persistor}>
            <div id="app" className={this.state.modal ? "right" : ""}>
              <App onBack={this.state.onBack} onNext={this.state.onNext} checkNext={this.state.checkNext}>
                <ConnectedRouter history={this.props.history}>
                  <Routes store={this.props.store} setBack={this.setBack} setNext={this.setNext} setCheckNext={this.setCheckNext}
                    setGetModalServices={this.setGetModalServices} setCartModalCallback={this.setCartModalCallback}
                  />
                </ConnectedRouter>
              </App>
              <ModalWrapper setModal={this.setModal} cartModalCallback={this.state.cartModalCallback} getModalServices={this.state.getModalServices} />
              <LoadingSpinner />
              <DevTools />
            </div>
          </PersistGate>
        </Provider>
      </div>
    );
  }
}
