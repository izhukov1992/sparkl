import React from 'react';
import { Route, Switch, Redirect } from 'react-router';
import { HomeView, LoginView, ProtectedView, NotFoundView, PostalCode, Categories, Services, Addons, Cart, Session, Booking, Checkout, Confirm } from './containers';
import requireAuthentication from './utils/requireAuthentication';
import { connect } from 'react-redux';

import { PATH_ROOT, PATH_OTHER, PATH_POSTAL, PATH_CATEGORIES, PATH_SERVICES, PATH_ADDONS, PATH_CART, PATH_SESSION, PATH_BOOKING, PATH_CHECKOUT, PATH_CONFIRM } from './constants'

class Routes extends React.Component {
  validatePostalCode = () => {
    const state = this.props.store.getState();
    if (state.customerDetails.postal_code.code === '') {
      return false;
    }
    return true;
  }

  getPostalCode = () => {
    return <PostalCode />
  }

  getCategories = () => {
    if (!this.validatePostalCode()) {
      return <Redirect to={PATH_POSTAL} />
    }
    return <Categories setBack={this.props.setBack} setCheckNext={this.props.setCheckNext} />
  }

  getWrongServices = ({ match }) => {
    if (!this.validatePostalCode()) {
      return <Redirect to={PATH_POSTAL} />
    }
    return <Redirect to={PATH_CATEGORIES} />
  }

  getServices = ({ match }) => {
    if (!this.validatePostalCode()) {
      return <Redirect to={PATH_POSTAL} />
    }
    const { id } = match.params;
    return <Services categoryId={id} setBack={this.props.setBack} setNext={this.props.setNext} setCheckNext={this.props.setCheckNext} />
  }

  getAddons = () => {
    if (!this.validatePostalCode()) {
      return <Redirect to={PATH_POSTAL} />
    }
    return <Addons setBack={this.props.setBack} setNext={this.props.setNext} />
  }

  getCart = () => {
    if (!this.validatePostalCode()) {
      return <Redirect to={PATH_POSTAL} />
    }
    return (
      <Cart setBack={this.props.setBack} setNext={this.props.setNext} setCheckNext={this.props.setCheckNext}
        setGetModalServices={this.props.setGetModalServices} setCartModalCallback={this.props.setCartModalCallback}
      />
    )
  }

  getBooking = () => {
    if (!this.validatePostalCode()) {
      return <Redirect to={PATH_POSTAL} />
    }
    return <Booking setBack={this.props.setBack} setNext={this.props.setNext} setCheckNext={this.props.setCheckNext} />
  }

  getSession = () => {
    if (!this.validatePostalCode()) {
      return <Redirect to={PATH_POSTAL} />
    }
    return <Session setBack={this.props.setBack} setNext={this.props.setNext} setCheckNext={this.props.setCheckNext} />
  }

  getCheckout = () => {
    if (!this.validatePostalCode()) {
      return <Redirect to={PATH_POSTAL} />
    }
    return <Checkout setBack={this.props.setBack} setNext={this.props.setNext} />
  }

  getConfirm = () => {
    if (!this.validatePostalCode()) {
      return <Redirect to={PATH_POSTAL} />
    }
    return <Confirm />
  }

  getNotFoundView = () => {
    return <NotFoundView />
  }

  render() {
    return (
      <div id='route-container'>
        <Switch>
          <Route exact path={PATH_ROOT} render={() => (
            <Redirect to={PATH_POSTAL} />
          )} />
          <Route path="/login" component={LoginView} />
          <Route path={PATH_POSTAL} render={this.getPostalCode} />
          <Route path={PATH_CATEGORIES} render={this.getCategories} />
          <Route exact path={PATH_SERVICES} render={this.getWrongServices} />
          <Route path={PATH_SERVICES + '/:id'} render={this.getServices} />
          <Route path={PATH_ADDONS} render={this.getAddons} />
          <Route path={PATH_CART} render={this.getCart} />
          <Route path={PATH_SESSION} render={this.getSession} />
          <Route path={PATH_BOOKING} render={this.getBooking} />
          <Route path={PATH_CHECKOUT} render={this.getCheckout} />
          <Route path={PATH_CONFIRM} render={this.getConfirm} />
          <Route path="/protected" component={requireAuthentication(ProtectedView)} />
          <Route path={PATH_OTHER} render={this.getNotFoundView} />
        </Switch>
      </div>
    );
  }
}

export default Routes;
