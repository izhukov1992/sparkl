import _ from 'lodash';
import {
  ADD_SERVICE, ADD_PENDING_SERVICE, UPDATE_CART,
  REMOVE_PENDING_SERVICE, CONFIRM_PENDING_SERVICE,
  CONFIRM_CHECKOUT, UPDATE_PENDING_REQUIRED_ADDONS,
  START, SUCCESS, FAIL
} from '../constants';

const initialState = {
  services: []
};

const shoppingCart = (state = initialState, action) => {
  let serviceList = [], serviceInfoToAdd;

  switch (action.type) {
    case UPDATE_CART:
      let pendingItems = action.pendingServiceItems;

      let newServiceList = pendingItems.reduce((result, item) => {
        let index = result.findIndex((serviceItem) => {
          return serviceItem.service.id === item.service.id;
        });

        if (index !== -1) {
          let quantity = (action.increment || item.alwaysIncrement) ? (result[index].quantity + item.quantity) : item.quantity;
          result[index] = Object.assign(result[index], {
            quantity
          });
        } else if (item.quantity) {
          result = [...result, {
            ...item
          }];
        }
        return result;
      }, [...state.services]);

      return {
        ...state,
        services: [...newServiceList]
      }

    case CONFIRM_CHECKOUT + SUCCESS:
      return initialState;

    default:
      return state;
  }
}

export default shoppingCart;