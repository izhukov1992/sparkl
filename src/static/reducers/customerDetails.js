import { 
    VERIFY_POSTAL_CODE, SUCCESS, SAVE_CUSTOMER_DETAILS, 
    SET_POSTAL_CODE, CHANGE_ADDRESS_UNIT, CHANGE_BOOKING_DETAILS, 
    CHANGE_ADDRESS_DETAILS, SET_BRANCH_INFO
} from '../constants';

const initialState = {
    first_name: '',
    last_name: '',
    mobile_number: null,
    email: '',
    nif: null,
    token: '',
    postal_code: {
        code: '',
        extension: '',
        name: ''
    },
    comment: '',
    numbers: [],
    full_address: '',
    address: {
        address_unit: '',
        discrict_code: '',
        county_code: '',
        branch_code: '',
        branch_name: '',
        branch_extra: '',
        venue_name: '',
        number: '',
        area: {
            code: '',
            name: '',
            status: null
        },
        postal_code: {
            code: '',
            extension: '',
            name: ''
        },
        source: ''
    }
};

const customerDetails = (state = initialState, action) => {
    switch (action.type) {
        case SET_POSTAL_CODE:
            return action.postal_code ? {
                ...state,
                postal_code: {
                    ...state.postal_code,
                    code: action.postal_code.substr(0, 4),
                    extension: action.postal_code.substr(5, 3)
                }
            } : state;

        case SAVE_CUSTOMER_DETAILS + SUCCESS:
            return action.response ? {
                ...state,
                ...action.response.customer
            } : state;

        case VERIFY_POSTAL_CODE + SUCCESS:
            let { response } = action;

            if (!response || !response.customer) {
                return state;
            }

            let { postal_code, token, address } = response.customer;

            return response ? {
                ...state,
                postal_code,
                token,
                address: {
                    ...state.address,
                    area: address.area,
                    branch_name: address.branch_name
                }
            } : state;

        case CHANGE_ADDRESS_UNIT:
            return {
                ...state,
                address: {
                    ...state.address,
                    address_unit: action.newValue
                }
            };

        case CHANGE_BOOKING_DETAILS: 
            return {
                ...state,
                ...action.data
            };

        case CHANGE_ADDRESS_DETAILS: 
            let { address_unit, number } = action;
            return {
                ...state,
                address: {
                    ...state.address,
                    address_unit,
                    number
                }
            };

        case SET_BRANCH_INFO:
            return {
                ...state,
                numbers: action.data.numbers.sort((numberInfoA, numberInfoB) => (Number(numberInfoA.number) - Number(numberInfoB.number))),
                address: {
                    ...state.address,
                    branch_name: action.data.branch_name
                }
            };

        default:
            return state;
    }
}

export default customerDetails;