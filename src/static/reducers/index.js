import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import _ from 'lodash';
import moment from 'moment';

import { removeArrayDuplicates } from '../utils';
import authReducer from './auth';
import dataReducer from './data';
import servicesReducer from './services';
import { getMainServicesByCategoryId } from './services';
import customerDetailsReducer from './customerDetails';
import modalReducer from './modal';
import shoppingCartReducer from './shoppingCart';
import bookingReducer from './booking';
import helperReducer from './helper';
import { nightFeeService } from '../constants';

export default combineReducers({
    auth: authReducer,
    data: dataReducer,
    routing: routerReducer,
    services: servicesReducer,
    customerDetails: customerDetailsReducer,
    modal: modalReducer,
    shoppingCart: shoppingCartReducer,
    booking: bookingReducer,
    helper: helperReducer
});

export const getServiceItemsByCategoryId = (state, id) => {
    const { shoppingCart } = state;
    let mainServices = getMainServicesByCategoryId(state, id) || [];

    return mainServices.map((service) => {
        return {
            service: service,
            quantity: 0
        };
    });
}

export const getDefaultRequiredAddon = (state, id, extraServices = []) => {
    let service = state.services.servicesById[id], requiredAddons = _.result(service, 'required_addons[0]');
    if (!service || _.isEmpty(requiredAddons)) return {};

    let { fallback_if_any_service, fallback_if_all_services } = requiredAddons;
    fallback_if_any_service = _.map(fallback_if_any_service, 'id');
    fallback_if_all_services = _.map(fallback_if_all_services, 'id');

    let category = state.services.servicesById[service.category.id];

    let filteredCartServices = state.shoppingCart.services.concat(...extraServices).filter((serviceInfo) => {
        return serviceInfo && serviceInfo.service && service.category.id === category.id;
    });

    filteredCartServices = _.map(filteredCartServices, 'service');

    let toReturn = {
        defaultAddon: state.services.servicesById[requiredAddons["fallback_choice"].id],
        applyFallback: false
    };

    if (filteredCartServices.length && filteredCartServices.some((cartService) => {
        return cartService && (fallback_if_any_service.indexOf(cartService.id) !== -1);
    })) {
        return Object.assign(toReturn, {
            applyFallback: false
        });
    }

    if (filteredCartServices.length && filteredCartServices.every((cartService) => {
        return cartService && (fallback_if_all_services.indexOf(cartService.id) !== -1);
    })) {
        return Object.assign(toReturn, {
            applyFallback: false
        });
    }

    return toReturn;
}

export const getRelatedAddons = (state) => {
    const serviceItems = state.shoppingCart.services;
    let cartServices = _.map(serviceItems, 'service');

    if (!cartServices.length) {
        return [];
    }

    return cartServices.reduce((result, service) => {
        let addons = _.map(service.related_services, 'id').map((id) => state.services.servicesById[id]);
        return [...result, ...addons];
    }, []).filter((addon, index, collection) => {
        return collection.findIndex((item) => {
            return item.id === addon.id;
        }) === index;
    }).map((addon) => {
        return {
            service: addon,
            quantity: 0
        };
    });
}

export const getTotalCartPrice = (state) => {
    let result = state.shoppingCart.services.reduce((amount, serviceInfo) => {
        let { service, quantity, defaultAddon } = serviceInfo;
        amount += (service.price * quantity);
        defaultAddon && (amount += defaultAddon.price)
        return amount;
    }, 0);

    // Add night fee price
    if (state.booking.activeTimeSlot && moment(state.booking.activeTimeSlot.time).hours() >= 20) {
        result += nightFeeService.service.price;
    }

    return result;
}

export const getServicePrice = (serviceInfo) => {
    if (!serviceInfo) {
        return 0;
    }

    let { service } = serviceInfo;
    if (!service) {
        return 0;
    }

    return service.price;
}

export const getServiceSumPrice = (serviceInfo) => {
    if (!serviceInfo) {
        return 0;
    }

    let { service } = serviceInfo;
    if (!service) {
        return 0;
    }

    return service.price * serviceInfo.quantity;
}

export const getCartTotalAmount = (state) => {
    return state.shoppingCart.services.reduce((amount, serviceInfo) => {
        return serviceInfo.service.price ? (amount + serviceInfo.quantity) : amount;
    }, 0);
}

export const getCartOrder = (state) => {
    return state.shoppingCart.services.reduce((arr, serviceInfo) => {
        let { service, defaultAddon } = serviceInfo;
        if (!service) {
            return arr;
        }

        for (let i = 0; i < serviceInfo.quantity; i++) {
            arr.push(service.id);
            defaultAddon && arr.push(defaultAddon.id);
        }
        return arr;
    }, []);
}
