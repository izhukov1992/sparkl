import { combineReducers } from 'redux';
import _ from 'lodash';
import {
  LOAD_ALL_SERVICES,
  START, SUCCESS, FAIL
} from '../constants';

const initialState = {
  loadingState: {
    loading: false,
    loaded: false
  },
  categories: {},
  servicesById: {},
  servicesByCategoryId: {}
};

const categories = (state = initialState.categories, action) => {
  switch (action.type) {
    case LOAD_ALL_SERVICES + SUCCESS:
      return Object.assign({}, ...action.response.reduce((obj, service) => {
        service.category && (obj[service.category.id] = service.category);
        return obj;
      }, {}));

    case LOAD_ALL_SERVICES + FAIL:
      return initialState.categories;

    default:
      return state;
  }
}

const servicesById = (state = initialState.servicesById, action) => {
  switch (action.type) {
    case LOAD_ALL_SERVICES + SUCCESS:
      return Object.assign({}, ...action.response.reduce((obj, service) => {
        !_.isUndefined(service.id) && (obj[service.id] = service);
        return obj;
      }, {}));

    case LOAD_ALL_SERVICES + FAIL:
      return initialState.servicesById;

    default:
      return state;
  }
}

const servicesByCategoryId = (state = initialState.servicesByCategoryId, action) => {
  switch (action.type) {
    case LOAD_ALL_SERVICES + SUCCESS:
      return Object.assign({}, ...action.response.reduce((obj, service) => {
        let categoryId = _.result(service.category, 'id');
        if (!_.isUndefined(categoryId) && !_.isUndefined(service.id)) {
          obj[categoryId] = [...obj[categoryId], service];
        }
        return obj;
      }, {}));

    case LOAD_ALL_SERVICES + FAIL:
      return initialState.servicesByCategoryId;

    default:
      return state;
  }
}

const loadingState = (state = initialState.servicesByCategoryId, action) => {
  switch (action.type) {
    case LOAD_ALL_SERVICES + START:
      return {
        loading: true,
        loaded: false
      };
    case LOAD_ALL_SERVICES + SUCCESS:
      return {
        loading: false,
        loaded: true
      };

    case LOAD_ALL_SERVICES + FAIL:
      return {
        loading: false,
        loaded: false
      };

    default:
      return state;
  }
}

export default combineReducers({
  categories,
  servicesById,
  servicesByCategoryId,
  loadingState
});

export const getOrderedCategoryList = state =>
  Object.values(state.services.categories).sort((a, b) => (a.weight - b.weight))

export const getCategoryById = (state, id) =>
  state.services.categories[id] || {}

export const getMainServicesByCategoryId = (state, id) => {
  if (!id in Object.keys(state.services.categories)) {
    return [];
  }

  state.services.servicesByCategoryId[id] = state.services.servicesByCategoryId[id] || [];

  return state.services.servicesByCategoryId[id].filter(service => (service && service["addon_only"] !== true)).sort((a, b) => (a.weight - b.weight))
}

export const getRequiredAddonsForPendingServices = (state, pendingServiceItems, ignoreNullPrice) => {
  if (!pendingServiceItems || !pendingServiceItems.length) {
    return [];
  }

  return pendingServiceItems.reduce((result, serviceItem) => {
    if (!serviceItem || !serviceItem.service || !serviceItem.quantity) {
      return result;
    }

    const { required_addons } = serviceItem.service;
    const requiredAddonsLength = _.result(serviceItem.service.required_addons, 'length', 0);

    if (!requiredAddonsLength || serviceItem.applyFallback) {
      return result;
    }

    let addons = required_addons[0].choices.map(option => state.services.servicesById[option.id]).filter((addon) => {
      if (!ignoreNullPrice) {
        return true;
      }
      return addon && addon.price; 
    });

    return result.concat(...addons)
  }, []).filter((addon, index, collection) => {
    return collection.findIndex((addonToSearch) => {
      return addonToSearch.id === addon.id;
    }) === index;
  }).map((addon) => {
    return {
      service: addon,
      quantity: 0
    };
  });
}

export const getRelatedServicesByServiceList = (state, ids) => {
  let relatedServiceIds = ids.map(id => state.services.servicesById[id]).reduce((result, service) => {
    return [...result, ...service["related_services"]]
  }, []);

  return relatedServiceIds.map(id => state.services[id]).sort((a, b) => (a.weight - b.weight));
}