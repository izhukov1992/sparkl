import moment from 'moment';
import _ from 'lodash';

import {
  CHANGE_ACTIVE_DATE, CHANGE_ACTIVE_TIME, CHANGE_ACTIVE_PROFS,
  LOAD_DATES, LOAD_TIMES, LOAD_PROFS,
  RESET_AVAILABLE_TIME_SLOTS, SET_AVAILABLE_TIME_SLOTS,
  LOAD_INITIAL_ORDER_DATA, RESET_PROF_FILTER, RESET_ACTIVE_TIME,
  RESET_SLOTS, CONFIRM_CHECKOUT, SUCCESS, SET_PAYMENT_METHOD
} from '../constants';

const initialState = {
  activeDateSlot: null,
  activeTimeSlot: null,
  activeProfSlot: null,
  dateSlots: [],
  timeSlots: [],
  profSlots: [],
  availableTimeSlots: [],
  paymentMethod: 'visa'
};

const booking = (state = initialState, action) => {
  let startDateSlot;

  switch (action.type) {
    case CHANGE_ACTIVE_DATE:
      let wasDateSlotChanged = !moment(state.activeDateSlot, 'MM-DD-YYYY').isSame(moment(action.dateSlot, 'MM-DD-YYYY'), 'day');
      return {
        ...state,
        activeDateSlot: action.dateSlot,
        activeTimeSlot: wasDateSlotChanged ? null : state.activeTimeSlot
      };

    case CHANGE_ACTIVE_TIME:
      return {
        ...state,
        activeTimeSlot: action.timeSlot
      };

    case CHANGE_ACTIVE_PROFS:
      return {
        ...state,
        activeProfSlot: (state.activeProfSlot === action.profSlot) ? null : action.profSlot
      };

    case RESET_PROF_FILTER:
      return {
        ...state,
        activeProfSlot: null
      };

    case RESET_ACTIVE_TIME:
      return {
        ...state,
        activeTimeSlot: null
      };

    case RESET_SLOTS:
      return {
        ...state,
        activeTimeSlot: null,
        activeDateSlot: null,
      };

    case LOAD_DATES:
      let tomorrowDate = moment().add(1, 'days').format('MM-DD-YYYY')
      startDateSlot = state.activeDateSlot ? state.activeDateSlot : tomorrowDate
      let dateSlots = [];

      let minDate, maxDate;

      if (moment(startDateSlot, 'MM-DD-YYYY') > moment(tomorrowDate, 'MM-DD-YYYY')) {
        minDate = tomorrowDate, maxDate = startDateSlot;
      } else {
        minDate = startDateSlot, maxDate = tomorrowDate;
      }

      let daysDiff = Math.abs(moment(minDate, 'MM-DD-YYYY').diff(moment(maxDate, 'MM-DD-YYYY'), 'days'));

      for (let i = 5; i > 0; i--) {
        dateSlots.push(moment(minDate, 'MM-DD-YYYY').subtract(i, 'days').format('MM-DD-YYYY'));
      }

      for (let i = 0; i < 100 + daysDiff; i++) {
        dateSlots.push(moment(minDate, 'MM-DD-YYYY').add(i, 'days').format('MM-DD-YYYY'));
      }

      return {
        ...state,
        activeDateSlot: startDateSlot,
        dateSlots
      };

    case LOAD_TIMES:
      startDateSlot = state.activeDateSlot ? state.activeDateSlot : moment().add(1, 'days');

      let startTimeSlot = moment(startDateSlot, 'MM-DD-YYYY').add(8, 'hours').format();

      let timeSlots = [];
      for (let i = 0; i < 30; i++) {
        let time = moment(startTimeSlot).add(30 * i, 'minutes');

        let isActive = state.availableTimeSlots.some((slot) => {
          return time.isSame(slot, 'minute');
        });

        timeSlots.push({
          id: time.format('HH:mm'),
          time,
          isActive
        });
      }

      return {
        ...state,
        timeSlots
      };

    case LOAD_INITIAL_ORDER_DATA:
      let { slots, sps } = action.data;
      return {
        ...state,
        availableTimeSlots: slots,
        profSlots: sps
      };

    case RESET_AVAILABLE_TIME_SLOTS:
      return {
        ...state,
        availableTimeSlots: []
      };

    case SET_AVAILABLE_TIME_SLOTS:
      return {
        ...state,
        availableTimeSlots: action.slots
      };

    case CONFIRM_CHECKOUT + SUCCESS:
      return {
        ...(_.omit(initialState, ['profSlots']))
      };

    case SET_PAYMENT_METHOD:
      return {
        ...state,
        paymentMethod: action.paymentMethod
      }

    default:
      return state;
  }
}

export default booking;