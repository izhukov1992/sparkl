import { UPDATE_APP, FETCH_STARTED, FETCH_COMPLETED } from '../constants';

const initialState = {
    rootRendererCount: 0,
    fetchInProgress: false
}

export default (state = initialState, action) => {
    switch (action.type) {
        case UPDATE_APP:
            return {
                ...state,
                rootRendererCount: ++state.rootRendererCount
            }

        case FETCH_STARTED:
            return {
                ...state,
                fetchInProgress: true
            }

        case FETCH_COMPLETED:
            return {
                ...state,
                fetchInProgress: false
            }

        default:
            return state
    }
}