import { SHOW_MODAL, HIDE_MODAL } from '../constants';

const initialState = {
    modalType: null,
    modalProps: {}
}

export default (state = initialState, action) => {
    let { modalType, modalProps } = action;

    switch (action.type) {
        case SHOW_MODAL:
            return {
                modalType,
                modalProps
            }
        case HIDE_MODAL:
            return initialState
        default:
            return state
    }
}