import os

from django.conf import settings
from django.http import HttpResponse
from django.views.generic import View
# from knox.auth import TokenAuthentication
# from rest_framework import status
# from rest_framework.generics import GenericAPIView
# from rest_framework.permissions import IsAuthenticated
# from rest_framework.response import Response


class IndexView(View):
    """Render main page."""

    def get(self, request):
        """Return html for main application page."""

        abspath = open(os.path.join(settings.BASE_DIR,
                                    'static_dist/index.html'), 'r')
        response = HttpResponse(content=abspath.read())

        # if order.customer.get_token() != ct:
        #     response.set_cookie('ct', order.customer.get_token())

        return response


# class ProtectedDataView(GenericAPIView):
#     """Return protected data main page."""

#     authentication_classes = (TokenAuthentication,)
#     permission_classes = (IsAuthenticated,)

#     def get(self, request):
#         """Process GET request and return protected data."""

#         data = {
#             'data': 'THIS IS THE PROTECTED STRING FROM SERVER',
#         }

#         return Response(data, status=status.HTTP_200_OK)
