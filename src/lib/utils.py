import base64
from datetime import (
    date,
    datetime,
    time,
    timedelta,
)
from dateutil import rrule
from disposable_email_checker.validators import validate_disposable_email
# import mandrill
import os
import string
import random
import requests
from time import gmtime, strftime

from django.conf import settings
from django.core.exceptions import ValidationError
from django.core.files.base import ContentFile
from django.core.validators import validate_email as django_validate_email
from django.db import transaction

UPCOMING_WEEK_DEFAULT = getattr(settings, 'UPCOMING_WEEK_RANGE', 4)  # 4 weeks


# --------------------
# Account registration
# --------------------


def validate_email(value):
    """Validate a single email."""
    if not value:
        return False
    # Check the regex, using the validate_email from django.
    try:
        django_validate_email(value)
    except ValidationError:
        return False
    else:
        # Check with the disposable list.
        try:
            validate_disposable_email(value)
        except ValidationError:
            return False
        else:
            return True


class AtomicMixin(object):
    """
    Ensure we rollback db transactions on exceptions.

    From https://gist.github.com/adamJLev/7e9499ba7e436535fd94
    """

    @transaction.atomic()
    def dispatch(self, *args, **kwargs):
        """Atomic transaction."""
        return super(AtomicMixin, self).dispatch(*args, **kwargs)

    def handle_exception(self, *args, **kwargs):
        """Handle exception with transaction rollback."""
        response = super(AtomicMixin, self).handle_exception(*args, **kwargs)

        if getattr(response, 'exception'):
            # We've suppressed the exception but still need to rollback
            # any transaction.
            transaction.set_rollback(True)

        return response


def image_file_info(filename_ext):
    """
    Return image type and extension.

    :param app: filename and extension
    :return: object with image type and extension
    """

    if filename_ext in ['jpg', 'jpeg']:  # pragma: no cover - tests cover .gif
        pil_type = 'jpeg'
        file_ext = '.jpg'
    elif filename_ext == 'png':  # pragma: no cover - tests cover .gif
        pil_type = 'png'
        file_ext = '.png'
    elif filename_ext == 'gif':
        pil_type = 'gif'
        file_ext = '.gif'

    return {'type': pil_type, 'extension': file_ext}


def serialize_image(instance, file_attr='file', filename=None):
    """
    Serialize a Picture instance into a dict.

    :param instance: Picture instance
    :param file_attr: attribute name that contains the FileField or ImageField
    :return: Dictionary with data for display in frontend
    """

    obj = getattr(instance, file_attr)

    return {
        'url': obj.url,
        'name': filename or os.path.basename(obj.name),
        'thumbnailUrl': obj.url,
        'size': obj.size,
    }


def base64_to_image(data):
    """Parse base64 image data to a image file."""
    if data and isinstance(data, str) and data.startswith('data:image'):
        image_info, image_content = data.split(';base64,')
        file_extension = image_info.split('/')[-1]
        data = ContentFile(base64.b64decode(image_content),
                           name='dummie.' + file_extension)
        return data
    return


def random_password_generator(size=10,
                              chars=string.ascii_lowercase + string.digits):
    return ''.join(random.choice(chars) for x in range(size))


# -----------
# Time travel
# -----------

def next_day(d):
    return d + timedelta(1)


def next_week(d):
    return d + timedelta(weeks=1)


def next_month(d):
    return d + timedelta(30)


def next_year(d):
    return d + timedelta(365)


def calculate_age(date_of_birth):
    """Calculate age from date of birth.
    http://stackoverflow.com/questions/4876370/django-date-format-dd-mm-yyyy

    :param date_of_birth: datetime.date(YYYY, MM, DD)
    """
    if date_of_birth:
        today = date.today()
        return today.year - date_of_birth.year - \
            ((today.month, today.day) <
             (date_of_birth.month, date_of_birth.day))
    return None


def upcoming_booking_dates():
    """Calculate upcoming bookings dates range."""
    now = date_whole_day()
    return (now[0], now[1] + timedelta(weeks=UPCOMING_WEEK_DEFAULT))


def date_whole_day(dt=[datetime.now().date()] * 2):
    try:
        d = (datetime.combine(dt[0], time.min),
             datetime.combine(dt[1], time.max))
    except (AttributeError, TypeError):
        d = (datetime.combine(datetime.strptime(
            dt[0], '%Y-%m-%d'), time.min),
            datetime.combine(datetime.strptime(
                dt[1], '%Y-%m-%d'), time.max))
    return d


def align_intervals(start, end, inc_start=True, inc_end=True):
    slots = []
    if inc_start and (start.minute == 0 or start.minute == 30):
        slots.append(start)
    rule = rrule.rrule(rrule.MINUTELY, interval=30, bysecond=0, dtstart=start)
    for s in rule.between(start, end, inc=False):
        if s.minute == 0 or s.minute == 30:
            slots.append(s)
    if inc_end and (end.minute == 0 or end.minute == 30):
        slots.append(end)

    return slots


def closest_interval(dt):
    minute = (dt.minute // 30 + 1) * 30
    return dt.replace(minute=0, second=0) + timedelta(minutes=minute)


def has_same_date(datetime1, datetime2):
    return datetime1.date() == datetime2.date()


def seconds_to_pretty(seconds):
    t = int(seconds) if isinstance(seconds, str) else seconds
    return strftime('%Hh%Mm', gmtime(t))


def nearest(items, pivot):
    return min(items, key=lambda x: abs(x - pivot))


def standardize_dt_seconds(dt):
    """Set seconds and microseconds to 0 to make sure calculations are
    correct."""
    return dt.replace(second=0, microsecond=0)


def dt_add_service_extra_time(dt):
    """Add service extra time to include in booking time."""
    return dt + timedelta(seconds=settings.ARBITRARY_SERVICE_EXTRA_TIME)


def dt_sub_service_min_interval(dt):
    """Subtract minimum interval between services to include in travel time."""
    return dt - timedelta(seconds=settings.ARBITRARY_SERVICES_INTERVAL_MIN)


def is_rush_hour(datetime):
    for t in settings.rush_hours:
        if t.get('from') < datetime.time() < t.get('until'):
            return t
    return False


# -----------
# Send emails
# -----------


def send_mandrill_mail(email_to, template_name, context):
    """
    Send mails using mandrill.

    :param email_to: Recipient
    :param template_name: Name of Mandrill template
    :param context: Dictionary with extra information
    """
    message = {
        'to': [],
        'global_merge_vars': [],
        'merge_language': 'mailchimp'
    }

    for user in email_to:
        message['to'].append(
            {'email': user.email, 'name': user.get_full_name()}
        )
    for k, msg in context.items():
        message['global_merge_vars'].append(
            {'name': k, 'content': msg}
        )

    # mandrill_client = mandrill.Mandrill(settings.MANDRILL_API_KEY)
    # mandrill_client.messages.send_template(template_name, [], message)


def send_confirm_account_email(user, link):
    """Send email to recover password."""
    ctx_dict = {
        'FULL_NAME': user.get_full_name(),
        'LINK': link
    }
    send_mandrill_mail([user], 'confirm-registration', ctx_dict)


def send_status_email(user, message):
    """Send email with confirm or reject message."""
    ctx_dict = {
        'FULL_NAME': user.get_full_name(),
        'MESSAGE': message
    }
    send_mandrill_mail([user], 'status-email', ctx_dict)


def send_password_recovery_email(user, password):
    """Send email to recover password."""
    ctx_dict = {
        'FULL_NAME': user.get_full_name(),
        'PASSWORD': password
    }
    send_mandrill_mail([user], 'password-recovery-email', ctx_dict)


# --------
# Send SMS
# --------

def send_sms(mobile_number, body):
    r = requests.post(settings.SMS_GATEWAY_URL,
                      settings.SMS_API_CREDENTIALS.extend({
                        'number': mobile_number,
                        'message': body}))

    return r.content.json()


def send_sms_confirm_mobile_number(profile):
    send_sms(profile.mobile_number,
             settings.CONFIRM_MOBILE_NUMBER_TEXT.format(
                profile.user.mobile_number_confirmation_code))


def send_sms_sp_upcoming_booking_reminder(slot):
    send_sms(slot.service_provider.mobile_number,
             settings.UPCOMING_BOOKING_TEXT.format())


def send_sms_customer_upcoming_booking(slot):
    send_sms(slot.service_provider.mobile_number,
             settings.UPCOMING_BOOKING_CUSTOMER_TEXT.format())


def send_sms_customer_sp_delayed(slot, delay_time):
    pass


# --------
# Facebook
# --------

def get_fb_profile(fb_id):
    r = requests.get(
        settings.FB_GRAPH_URL.format(fb_id),
        {
            'access_token': settings.ACCESS_TOKEN,
            'fields': 'first_name,last_name,gender,profile_pic,locale,timezone'
        }
    )

    return r.json()


# ------------------
# Admin custom views
# ------------------

def get_template_path(module, template_file):
    return "admin/{0}/{1}".format(module.split('.')[1], template_file)


# for getattr
def multi_getattr(obj, attr, default):
    attributes = attr.split(".")
    for i in attributes:
        try:
            obj = getattr(obj, i)
        except AttributeError:
            return default
    return obj
