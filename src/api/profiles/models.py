# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from datetime import datetime
import uuid

from django.conf import settings
from django.contrib.postgres.fields import ArrayField
from django.db import models
from django.db.models.signals import post_save
from django.utils.translation import ugettext_lazy as _

from lib.utils import (
    get_fb_profile,
    random_password_generator
)

from api.schedule import constants as schedule_constants
from api.schedule.models import Slot
from api.services.models import get_service_provider_services

from .constants import *


def generate_token():
    token = random_password_generator(size=20)
    if not CustomerExtraDetails.objects.filter(token=token):
        return token
    else:
        return generate_token()


def upload_user_image(instance, filename):
    """Used in User model to save image file."""
    formatted_name = '{}_{}_{}.{}'.format(instance.profile.first_name,
                                          instance.profile.last_name,
                                          datetime.now().strftime('%f'),
                                          filename.split('.')[1])
    return '{}/users/images/{}'.format(settings.MEDIA_ROOT,
                                       formatted_name.lower())


class ProfileManager(models.Manager):
    def create_from_facebook(self, fb_id):
        data = get_fb_profile(fb_id)

        return Customer.objects.create(
            first_name=data.get('first_name'),
            last_name=data.get('last_name'),
            gender=data.get('gender'),
            facebook_id=fb_id,
            image=data.get('profile_pic'),
            # locale=data.get('locale',),
            # timezone=data.get('timezone')
        )


class Profile(models.Model):
    id = models.UUIDField(primary_key=True,
                          unique=True,
                          default=uuid.uuid4,
                          editable=False)

    created_at = models.DateTimeField(_('Created at'), auto_now_add=True)
    updated_at = models.DateTimeField(_('Updated at'), auto_now=True)

    first_name = models.CharField(_('First Name'),
                                  max_length=50,
                                  default=_(""))
    last_name = models.CharField(_('Last Name'),
                                 max_length=50,
                                 default=_(""))

    image = models.ImageField(upload_to=upload_user_image,
                              null=True, blank=True)

    gender = models.CharField(_('Gender'), max_length=1,
                              choices=GENDER_CHOICES,
                              null=True, blank=True)
    mobile_number = models.CharField(_('Mobile number'), max_length=20,
                                     null=True, blank=True)

    date_of_birth = models.DateField(_('Date of Birth'), null=True, blank=True)
    vat = models.CharField(_('VAT'), max_length=9, null=True, blank=True)
    facebook_id = models.CharField(_('Facebook ID'), max_length=20,
                                   null=True, blank=True)

    medical_condition = models.CharField(_('Medical conditions'),
                                         max_length=200,
                                         null=True, blank=True)
    allergies = models.CharField(_('Allergies'), max_length=300,
                                 null=True, blank=True)

    objects = ProfileManager()

    class Meta:
        abstract = True

    def __str__(self):
        """
        Unicode representation for Profile model.

        :return: string
        """
        return self.get_full_name()

    def get_full_name(self):
        """
        Return the first_name plus the last_name, with a space in between.
        :return: string
        """
        return u"{0} {1}".format(self.first_name, self.last_name)

    def __unicode__(self):
        return self.get_full_name()

    def get_short_name(self):
        """
        Return the first_name.
        :return: string
        """
        return u"{}".format(self.first_name)

    def assign_facebook_data(self, data):
        self.first_name = data.get('first_name')
        self.last_name = data.get('last_name')
        self.gender = data.get('gender')
        self.image = data.get('profile_pic')
        # self.locale = data.get('locale')
        # self.timezone = data.get('timezone')
        self.save()

    def fetch_facebook_details(self):
        data = get_fb_profile(self.facebook_id)
        if data:
            self.assign_facebook_data(data)


class Customer(Profile):
    # user = models.ForeignKey('accounts.User',
    #                          related_name='profile',
    #                          null=True)

    level = models.CharField(_('Customer Level'),
                             max_length=2,
                             choices=LEVEL_CHOICES,
                             default=1)

    def get_address(self):
        last_address = self.addresses.first()
        if last_address.is_complete():
            return last_address.address
        else:
            return last_address.postal_code

    def get_postal_code(self):
        return self.addresses.first().postal_code

    def get_default_address(self):
        return self.addresses.filter(default=True).first()

    def assign_postal_code(self, postal_code):
        AddressEntry.objects.create(
            profile=self,
            postal_code=postal_code)

        return postal_code

    def assign_address(self, address):
        incomplete_entry = self.addresses.filter(
            profile=self,
            postal_code=address.postal_code,
            address=None).first()
        if incomplete_entry:
            incomplete_entry.address = address
            incomplete_entry.save()
        else:
            AddressEntry.objects.create(
                profile=self,
                address=address,
                postal_code=address.postal_code)

        return address

    def get_token(self):
        return self.extra_details.first().token

    def update_customer_level(customer):
        count = customer.booking_set.count()
        if customer.level == 1 and count == 1:
            # guest becomes new customer
            customer.level = 2
        elif customer.level == 2 and count > 1:
            # new customer becomes member
            customer.level = 3
        elif customer.level == 3 and count > 10:
            # member becomes pro customer
            customer.level = 4
        customer.save()


class ServiceProviderManager(models.Manager):
    def get_queryset(self):
        return super(ServiceProviderManager, self).get_queryset().filter(
            status=ACTIVE)


class ServiceProvider(Profile):
    address = models.ForeignKey('addresses.Address')
    status = models.CharField(_('Status'), choices=STATUS_CHOICES,
                              max_length=2,
                              default=INACTIVE)
    enrollment = models.CharField(_('Enrollment'), choices=ENROLLMENT_CHOICES,
                                  max_length=2,
                                  default=PENDING)

    travel_method = models.CharField(
        _('Travel method'), choices=TRAVEL_CHOICES,
        max_length=2,
        default=TRANSIT)

    objects = ServiceProviderManager()

    def performs_all_services(self, services):
        return set(services).issubset([s.service for s in
                                       get_service_provider_services(self)])

    def get_last_address(self, datetime=datetime.now()):
        # checkin = CheckIn.objects.filter(
        #     timestamp__gte=datetime)
        # if checkin:
        #     address = checkin.last().get_address()
        # else:
        slot = Slot.objects.filter(
            service_provider=self,
            slot_from__lte=datetime,
            slot_until__gte=datetime).order_by('slot_from').first()
        if slot:
            address = slot.get_slot_address()
        else:
            address = self.address
        return address

    def get_available_slot(self, datetime):
        return Slot.objects.filter(
            service_provider=self,
            slot_type=schedule_constants.AVAILABLE,
            slot_from__lte=datetime,
            slot_until__gte=datetime).order_by('slot_from').first()

    def is_available_between(self, slot_from, slot_until):
        available_slots = Slot.objects.filter(
            service_provider=self,
            slot_type=schedule_constants.AVAILABLE,
            slot_from__lte=slot_from,
            slot_until__gte=slot_until)
        return any(map(lambda slot: slot.get_slot_duration() >=
                       (slot_until - slot_from).seconds,
                   available_slots))


class CustomerExtraDetails(models.Model):
    customer = models.ForeignKey('Customer', related_name='extra_details')
    main_service_provider = models.ForeignKey('ServiceProvider',
                                              null=True, blank=True)
    has_children = models.BooleanField(_('Has children'), default=False)
    has_pets = models.BooleanField(_('Has pets'), default=False)
    work_schedule = models.CharField(_('Work schedule'),
                                     max_length=200, null=True, blank=True)
    work_profile = models.CharField(_('Work profile'),
                                    max_length=300, null=True, blank=True)
    family_profile = models.CharField(_('Family profile'),
                                      max_length=300, null=True, blank=True)
    source = models.CharField(_('Source'),
                              max_length=100, null=True, blank=True)
    token = models.CharField(_('Customer Token'), max_length=50,
                             null=True, blank=True)

    class Meta:
        verbose_name_plural = "Customer Extra Details"


class ServiceProviderExtraDetails(models.Model):
    service_provider = models.ForeignKey('ServiceProvider',
                                         related_name='extra_details')
    source = models.CharField(_('Source'),
                              max_length=100, null=True, blank=True)
    max_distance_home = models.PositiveIntegerField(
        _('Maximum distance from home'), default=25000,
        help_text=_("In meters"),
        null=True, blank=True)
    max_daily_distance = models.PositiveIntegerField(
        _('Maximum daily distance'), help_text=_("In meters"),
        null=True, blank=True)
    max_service_distance = models.PositiveIntegerField(
        _('Maximum distance per service'), help_text=_("In meters"),
        null=True, blank=True)
    min_service_profit = models.PositiveIntegerField(
        _('Minimum profit to accept service'), help_text=_("In Euros"),
        null=True, blank=True)
    min_distance_profit = models.PositiveIntegerField(
        _('Minimum profit to exceed maximum distance from home'),
        help_text=_("In Euros"), null=True, blank=True)
    revenue_percentage = models.DecimalField(_('Revenue percentage'),
                                             max_digits=4, decimal_places=2,
                                             null=True, blank=True,
                                             default=0.75)
    min_notify_interval = models.PositiveIntegerField(
        _('Minimum notify interval'),
        default=settings.MIN_NOTIFY_INTERVAL, null=True, blank=True,
        help_text=_('Minimum interval to be notified (in seconds) for last \
minute services'))
    experience_years = models.PositiveIntegerField(
        _('Experience years'), default=1, null=True, blank=True)
    languages_spoken = ArrayField(models.CharField(max_length=30), default=[])
    description = models.TextField(_('Profile description'),
                                   null=True, blank=True)
    avg_rating = models.DecimalField(_('Revenue percentage'),
                                     max_digits=3, decimal_places=2,
                                     null=True, blank=True)
    google_calendar_id = models.CharField(
        _('Google Calendar ID'), default=settings.PENDING_CALENDAR_ID,
        max_length=100, null=True, blank=True)

    class Meta:
        verbose_name_plural = "Service Provider Extra Details"


def assign_customer_extra_details(sender, instance, *args, **kwargs):
    if not CustomerExtraDetails.objects.filter(customer=instance):
        uniq_token = generate_token()
        CustomerExtraDetails.objects.create(customer=instance,
                                            token=uniq_token)


def assign_service_provider_extra_details(sender, instance, *args, **kwargs):
    if not ServiceProviderExtraDetails.objects.filter(
            service_provider=instance):
        ServiceProviderExtraDetails.objects.create(service_provider=instance)


post_save.connect(assign_customer_extra_details,
                  sender=Customer)
post_save.connect(assign_service_provider_extra_details,
                  sender=ServiceProvider)


class Staff(Profile):
    pass


class AddressEntry(models.Model):
    created_at = models.DateTimeField(_('Created at'), auto_now_add=True)
    updated_at = models.DateTimeField(_('Updated at'), auto_now=True)

    profile = models.ForeignKey('Customer', related_name='addresses')
    address = models.ForeignKey('addresses.Address', null=True, blank=True)
    postal_code = models.ForeignKey('addresses.PostalCode')
    address_unit = models.CharField(
        _('Address Unit'), max_length=200, null=True, blank=True,
        help_text='Flat, suite, unit, building, floor')
    default = models.BooleanField(_('Default address'), default=False)
    label = models.CharField(_('Label'), max_length=20, null=True, blank=True)

    class Meta:
        ordering = ['-updated_at']

    def is_complete(self):
        return bool(self.address)
    is_complete.boolean = True

    def __str__(self):
        """
        Unicode representation for Profile model.

        :return: string
        """
        if self.is_complete():
            return self.address.pretty_format()
        else:
            self.postal_code

    def __unicode__(self):
        """
        Unicode representation for Profile model.

        :return: string
        """
        return self.__str__()
