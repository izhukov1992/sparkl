# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
# from django.utils.safestring import mark_safe
# from django.utils.translation import ugettext_lazy as _
from api.profiles.models import ServiceProvider, Customer, AddressEntry


class ServiceProviderAdmin(admin.ModelAdmin):
    # search_fields = ('user__first_name', 'user__last_name', 'user__email',)
    # list_display = ('display_full_name', 'display_email',
    #                 'display_registration_timestamp', 'display_status',)

    # def display_full_name(self, obj):
    #     """Get user full name."""
    #     return obj.user.get_full_name()
    # display_full_name.short_description = _('Full name')

    # def display_email(self, obj):
    #     """Get user email."""
    #     return obj.user.email
    # display_email.short_description = _('Email')

    # def display_registration_timestamp(self, obj):
    #     """Get registration timestamp."""
    #     return obj.user.date_joined
    # display_registration_timestamp.admin_order_field = \
    #     '-display_registration_timestamp'
    # display_registration_timestamp.short_description = _('Registration')

    # def display_status(self, obj):
    #     """Check if user is accepted or rejected"""
    #     if obj.rejected:
    #         return mark_safe(
    #             '<img src="/static/admin/img/icon-no.gif" alt="True">')
    #     elif obj.accepted:
    #         return mark_safe(
    #             '<img src="/static/admin/img/icon-yes.gif" alt="True">')
    #     return ''
    # display_status.short_description = _('Status')
    pass


class CustomerAdmin(admin.ModelAdmin):
    pass


class AddressEntryAdmin(admin.ModelAdmin):
    pass


admin.site.register(Customer, CustomerAdmin)
admin.site.register(ServiceProvider, ServiceProviderAdmin)
admin.site.register(AddressEntry, AddressEntryAdmin)
