GENDER_MALE = 'M'
GENDER_FEMALE = 'F'
GENDER_OTHER = 'O'
GENDER_CHOICES = (
    (GENDER_MALE, 'Male'),
    (GENDER_FEMALE, 'Female'),
    (GENDER_OTHER, 'Other')
)

ACTIVE = 'A'
INACTIVE = 'I'
BLOCKED = 'B'

STATUS_CHOICES = (
    (ACTIVE, 'active'),
    (INACTIVE, 'inactive'),
    (BLOCKED, 'blocked')
)

ACCEPTED = 'A'
PENDING = 'P'
DECLINED = 'D'

ENROLLMENT_CHOICES = (
    (ACCEPTED, 'accepted'),
    (PENDING, 'pending'),
    (DECLINED, 'declined')
)

n = [str(x) for x in range(1, 5)]

LEVEL_CHOICES = (
    (1, 'Guest'),
    (2, 'New'),
    (3, 'Member'),
    (4, 'Pro'),
    (5, 'VIP'))

DRIVING = 'D'
TRANSIT = 'T'
BICYCLE = 'B'
WALKING = 'W'

TRAVEL_CHOICES = (
    (DRIVING, 'driving'),
    (TRANSIT, 'transit'),
    (BICYCLE, 'bicycle'),
    (WALKING, 'walking')
)
