from rest_framework import serializers
from phonenumber_field.validators import validate_international_phonenumber

# from lib.utils import (
#     validate_email as email_is_valid,
#     calculate_age
# )
# from lib.validate_pt import controlNIF
# from accounts.models import User
# from lib.utils import random_password_generator
from api.addresses.serializers import (
  AddressSerializer,
  PostalCodeSerializer
)
from api.addresses.models import (
    Address,
    Area,
    PostalCode
)
from api.profiles.models import (
    Customer,
    ServiceProvider,
    CustomerExtraDetails,
    ServiceProviderExtraDetails
)


class CustomerExtraDetailsSerializer(serializers.ModelSerializer):
    class Meta:
        model = CustomerExtraDetails
        fields = ('id',
                  # 'customer',
                  'has_children',
                  'has_pets',
                  'work_schedule',
                  'work_profile',
                  'family_profile',
                  'token')

    def create(self, validated_data):  # pragma: no cover
        """Create a customer instance."""
        pass

    def update(self, instance, validated_data):
        """Update a customer instance."""
        pass

    def to_representation(self, instance):
        """Representation of a customer."""
        return {
            'id': instance.id,
            # 'customer': CustomerSerializer(instance.customer).data,
            'has_children': instance.has_children,
            'has_pets': instance.has_pets,
            'work_schedule': instance.work_schedule,
            'work_profile': instance.work_profile,
            'family_profile': instance.family_profile,
            'token': instance.token
        }


class CustomerPublicSerializer(serializers.ModelSerializer):
    address = AddressSerializer()

    class Meta:
        model = Customer
        fields = ('first_name',
                  'last_name',
                  'mobile_number',
                  'address',
                  'vat')

    def to_representation(self, instance):
        """Representation of a service provider."""
        customer_address = instance.get_address()
        if isinstance(customer_address, Address):
            address = customer_address
        else:
            address = None
        return {
            'first_name': instance.first_name,
            'last_name': instance.last_name,
            'mobile_number': instance.mobile_number,
            'address': AddressSerializer(address).data,
            'postal_code': PostalCodeSerializer(
                instance.get_postal_code()).data,
            'vat': instance.vat,
            'token': instance.extra_details.first().token,
        }


class CustomerSerializer(serializers.ModelSerializer):
    extra_details = CustomerExtraDetailsSerializer()
    address = AddressSerializer()

    class Meta:
        model = Customer
        fields = ('id',
                  'first_name',
                  'last_name',
                  'gender',
                  'mobile_number',
                  'address',
                  'date_of_birth',
                  'vat',
                  'facebook_id',
                  'medical_condition',
                  'allergies',
                  'level',
                  'extra_details')

    def create(self, validated_data):  # pragma: no cover
        """Create a customer instance."""
        address_data = validated_data.pop('address')
        postal_code, _ = PostalCode.objects.get_or_create(
            address_data.pop('postal_code'))
        area, _ = Area.objects.get_or_create(address_data.pop('area'))
        address_data['source'] = 'customer_input'
        address = Address.objects.create(postal_code=postal_code,
                                         area=area,
                                         **address_data)
        customer = Customer.objects.create(address=address,
                                           **validated_data)
        # CustomerExtraDetails.objects.create(
        #     customer=customer, **validated_data.pop('extra_details'))

        return customer

    def update(self, instance, validated_data):
        """Update a customer instance."""
        pass

    def to_representation(self, instance):
        """Representation of a service provider."""
        return {
            'id': instance.id,
            'first_name': instance.first_name,
            'last_name': instance.last_name,
            'gender': instance.get_gender_display(),
            'mobile_number': instance.mobile_number,
            'address': AddressSerializer(instance.address).data,
            'date_of_birth': instance.date_of_birth,
            'vat': instance.vat,
            'facebook_id': instance.facebook_id,
            'medical_condition': instance.medical_condition,
            'allergies': instance.allergies,
            'extra_details': CustomerExtraDetailsSerializer(
                instance.extra_details.first()).data
        }

    # def to_representation(self, instance):
    #     """Representation of a customer."""
    #     pass
    #     # return {
    #     #     'id': instance.id,
    #     #     'name': instance.name,
    #     #     'mobile_number': instance.mobile_number,
    #     #     'address': instance.address,
    #     #     'area': instance.area,
    #     #     'vat': instance.vat
    #     # }

    # def validate_mobile_number(self, value):
    #     """Validate if phone number is valid or if there is an user using the
    #     phone number."""
    #     return validate_international_phonenumber(value)

    # def validate_fin(cls, value):
    #     """Validate fin."""
    #     # check if the first character starts with "S", "T", "F" or "G"
    #     # check if the rest of the characters has 7 numeric digits
    #     # check if the last char is a letter
    #     # check if the length is 9
    #     if ((value[:1] != 'S' and
    #                  value[:1] != 'T' and
    #                  value[:1] != 'F' and
    #                  value[:1] != 'G') or
    #             not value[1:7].isdigit() or
    #             not value[-1:].isalpha() or
    #             len(value) != 9):
    #         raise serializers.ValidationError('Invalid FIN.')
    #     return value


class ServiceProviderPublicSerializer(serializers.Serializer):
    image = serializers.CharField(allow_null=True)

    class Meta:
        model = ServiceProvider
        fields = ('id',
                  'first_name',
                  'last_name',
                  'gender',
                  'image')

    def create(self, validated_data):
        pass

    def update(self, instance, validated_data):
        pass

    def to_representation(self, instance):
        """Representation of a service provider."""
        return {
            'id': instance.id,
            'first_name': instance.first_name,
            'last_name': instance.last_name,
            'gender': instance.gender,
            'image': instance.image.url if instance.image else None,
        }


class ServiceProviderExtraDetailsSerializer(serializers.Serializer):
    class Meta:
        model = ServiceProviderExtraDetails
        fields = ('id',
                  'max_distance_home',
                  'min_service_profit')

    def create(self, validated_data):  # pragma: no cover
        """Create a customer instance."""
        pass

    def update(self, instance, validated_data):
        """Update a customer instance."""
        pass

    def to_representation(self, instance):
        """Representation of a customer."""
        return {
            'id': instance.id,
            'max_distance_home': instance.max_distance_home,
            'min_service_profit': instance.min_service_profit
        }


class ServiceProviderSerializer(serializers.Serializer):
    address = AddressSerializer()
    extra_details = ServiceProviderExtraDetailsSerializer()

    class Meta:
        model = ServiceProvider
        fields = ('id',
                  'first_name',
                  'last_name',
                  'gender',
                  'mobile_number',
                  'address',
                  'date_of_birth',
                  'vat',
                  'facebook_id',
                  'medical_condition',
                  'allergies',
                  'status',
                  'enrollment',
                  'transportation_method',
                  'enrollment_message',
                  'extra_details')

    def create(self, validated_data):
        pass

    def update(self, instance, validated_data):
        pass

    def to_representation(self, instance):
        """Representation of a service provider."""
        return {
            'id': instance.id,
            'first_name': instance.first_name,
            'last_name': instance.last_name,
            'gender': instance.get_gender_display(),
            'mobile_number': instance.mobile_number,
            'image': instance.image.url,
            'address': AddressSerializer(instance.address).data,
            'date_of_birth': instance.date_of_birth,
            'vat': instance.vat,
            'facebook_id': instance.facebook_id,
            'medical_condition': instance.medical_condition,
            'allergies': instance.allergies,
            'status': instance.get_status_display(),
            'enrollment': instance.get_enrollment_display(),
            'enrollment_message': instance.enrollment_message,
            'transportation_method':
                instance.get_transportation_method_display(),
            'extra_details': ServiceProviderExtraDetailsSerializer(
                instance.extra_details.first()).data
        }
