# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2018-05-23 17:41
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('profiles', '0003_auto_20180503_1952'),
    ]

    operations = [
        migrations.AddField(
            model_name='serviceproviderextradetails',
            name='min_distance_profit',
            field=models.PositiveIntegerField(blank=True, help_text='In Euros', null=True, verbose_name='Minimum profit to exceed maximum distance from home'),
        ),
        migrations.AlterField(
            model_name='serviceproviderextradetails',
            name='max_daily_distance',
            field=models.PositiveIntegerField(blank=True, help_text='In meters', null=True, verbose_name='Maximum daily distance'),
        ),
        migrations.AlterField(
            model_name='serviceproviderextradetails',
            name='max_service_distance',
            field=models.PositiveIntegerField(blank=True, help_text='In meters', null=True, verbose_name='Maximum distance per service'),
        ),
        migrations.AlterField(
            model_name='serviceproviderextradetails',
            name='min_service_profit',
            field=models.PositiveIntegerField(blank=True, help_text='In Euros', null=True, verbose_name='Minimum profit to accept service'),
        ),
    ]
