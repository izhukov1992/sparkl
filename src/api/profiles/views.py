# from django.db.models import Q, Avg
from django.shortcuts import get_object_or_404
from rest_framework import status, viewsets
# from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.decorators import list_route, detail_route
# from rest_framework_jwt.authentication import JSONWebTokenAuthentication

from api.profiles.models import (
    Customer,
    ServiceProvider,
    CustomerExtraDetails,
    generate_token
)
from api.profiles.serializers import (
    CustomerSerializer,
    ServiceProviderPublicSerializer,
    ServiceProviderSerializer,
    CustomerExtraDetailsSerializer
)


class ServiceProviderView(viewsets.ViewSet):
    def list(self, request):
        """List of service providers."""
        service_providers = ServiceProvider.objects.all()

        serializer = ServiceProviderSerializer(
            service_providers, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def retrieve(self, request):
        """Retrieve the service provider."""
        service_provider = get_object_or_404(
            ServiceProvider, user=request.user.pk)
        serializer = ServiceProviderPublicSerializer(service_provider)
        return Response(serializer.data)

    # def create(self, request):
    #     serializer = ServiceProviderSerializer(data=request.data)
    #     serializer.is_valid(raise_exception=True)
    #     serializer.save()
    #     return Response(serializer.data, status=status.HTTP_201_CREATED)


class CustomerView(viewsets.ViewSet):
    """Customer view."""
    # authentication_classes = (JSONWebTokenAuthentication,)
    # permission_classes = (IsAuthenticated,)

    def list(self, request):
        """List of customers."""
        customers = Customer.objects.all()

        serializer = CustomerSerializer(
            customers, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def retrieve(self, request, pk):
        """Retrieve the customer."""
        customer = get_object_or_404(Customer, pk=pk)
        serializer = CustomerSerializer(customer)
        return Response(serializer.data)

    def create(self, request):
        if not request.data.get('extra_details'):
            request.data.update({'extra_details': {}})
        serializer = CustomerSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    @list_route(methods=['get'])
    def get_new_token(self, request):
        data = {'customer_token': generate_token()}
        response = Response(data, status=status.HTTP_200_OK)

        # if not ct:
        #     response.set_cookie('ct', generate_token())

        return response

    @list_route(methods=['get'])
    def get_from_token(self, request):
        customer_extra = get_object_or_404(
            CustomerExtraDetails,
            token=request.data.get('customer_token'))
        serializer = CustomerSerializer(customer_extra.customer)
        return Response(serializer.data, status=status.HTTP_200_OK)

    @list_route(methods=['post'])
    def get_or_create_from_token(self, request):
        customer_extra = CustomerExtraDetails.objects.filter(
            token=request.data.get('customer_token'))
        if customer_extra:
            customer = customer_extra.first().customer
        else:
            customer = Customer.objects.create()
            e = customer.extra_details.first()
            e.token = request.data.get('customer_token')
            e.save()
        serializer = CustomerSerializer(customer)
        return Response(serializer.data, status=status.HTTP_200_OK)
