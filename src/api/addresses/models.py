# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.utils.encoding import smart_text

from api.navigation.helpers import find_coordinates

from api.addresses.constants import *


class Address(models.Model):
    discrict_code = models.CharField(_('Discrict Code'), max_length=5,
                                     null=True, blank=True)
    county_code = models.CharField(_('County Code'), max_length=5,
                                   null=True, blank=True)
    branch_code = models.CharField(_('Branch Code'), max_length=20,
                                   null=True, blank=True)
    branch_name = models.CharField(_('Branch Name'), max_length=300,
                                   null=True, blank=True)
    branch_extra = models.CharField(_('Branch Extra'), max_length=100,
                                    null=True, blank=True)
    venue_name = models.CharField(_('Venue Name'), max_length=200,
                                  null=True, blank=True)
    number = models.CharField(_('Number'), max_length=100,
                              null=True, blank=True)
    latitude = models.DecimalField(_('Latitude'), max_digits=9,
                                   decimal_places=6, null=True,
                                   blank=True)
    longitude = models.DecimalField(_('Longitude'), max_digits=9,
                                    decimal_places=6, null=True,
                                    blank=True)
    area = models.ForeignKey('Area', related_name="area_addresses")
    postal_code = models.ForeignKey('PostalCode', related_name="addresses")
    source = models.CharField(_('Source'), max_length=50,
                              null=True, blank=True)

    def __str__(self):
        if self.branch_name:
            return smart_text(self.branch_name)
        else:
            return self.postal_code

    def __unicode__(self):
        return u"%s" % self.__str__()

    def pretty_format(self):
        name = self.number or self.venue_name or ""
        first = smart_text(" ".join([self.branch_name, name]))
        return "{0}, {1} {2}, {3}".format(first,
                                          self.postal_code,
                                          smart_text(self.area.name),
                                          "Portugal")

    def fill_coordinates(self):
        """Query coordinates and fill latitude and longitude fields."""
        if not all([self.latitude, self.longitude]):
            self.latitude, self.longitude = find_coordinates(
                self.pretty_format())
            self.save()

    def get_coordinates(self):
        """Return coordinates: (lat, long)."""
        return (float(self.latitude), float(self.longitude))

    class Meta:
        verbose_name_plural = "Addresses"


class Area(models.Model):
    code = models.CharField(_('Area Code'), max_length=10)
    name = models.CharField(_('Area Name'), max_length=100)
    status = models.CharField(_('Status'),
                              max_length=2,
                              choices=STATUS_CHOICES,
                              default=ACTIVE)

    def __str__(self):
        return smart_text(self.name)

    def __unicode__(self):
        return u"%s" % self.__str__()


class PostalCode(models.Model):
    code = models.CharField(_('Postal Code'), max_length=10)
    extension = models.CharField(_('Postal Code Extension'), max_length=10)
    name = models.CharField(_('Postal Code Name'), max_length=100)

    def __str__(self):
        return "{}-{}".format(self.code, self.extension)

    def __unicode__(self):
        return u"%s" % self.__str__()
