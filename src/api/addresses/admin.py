# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import (
    Address,
    Area,
    PostalCode
)


class AddressAdmin(admin.ModelAdmin):
    pass


class AreaAdmin(admin.ModelAdmin):
    pass


class PostalCodeAdmin(admin.ModelAdmin):
    pass


admin.site.register(Address, AddressAdmin)
admin.site.register(Area, AreaAdmin)
admin.site.register(PostalCode, PostalCodeAdmin)
