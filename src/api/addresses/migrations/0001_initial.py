# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2018-03-27 21:55
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Address',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('discrict_code', models.CharField(blank=True, max_length=5, null=True, verbose_name='Discrict Code')),
                ('county_code', models.CharField(blank=True, max_length=5, null=True, verbose_name='County Code')),
                ('branch_code', models.CharField(blank=True, max_length=20, null=True, verbose_name='Branch Code')),
                ('branch_name', models.CharField(blank=True, max_length=300, null=True, verbose_name='Branch Name')),
                ('branch_extra', models.CharField(blank=True, max_length=100, null=True, verbose_name='Branch Extra')),
                ('venue_name', models.CharField(blank=True, max_length=200, null=True, verbose_name='Venue Name')),
                ('number', models.CharField(blank=True, max_length=100, null=True, verbose_name='Number')),
                ('latitude', models.DecimalField(blank=True, decimal_places=6, max_digits=9, null=True, verbose_name='Latitude')),
                ('longitude', models.DecimalField(blank=True, decimal_places=6, max_digits=9, null=True, verbose_name='Longitude')),
                ('source', models.CharField(blank=True, max_length=50, null=True, verbose_name='Source')),
            ],
            options={
                'verbose_name_plural': 'Addresses',
            },
        ),
        migrations.CreateModel(
            name='Area',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.CharField(max_length=10, verbose_name='Area Code')),
                ('name', models.CharField(max_length=100, verbose_name='Area Name')),
                ('status', models.CharField(choices=[(1, b'active'), (0, b'inactive')], default=1, max_length=2, verbose_name='Status')),
            ],
        ),
        migrations.CreateModel(
            name='PostalCode',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.CharField(max_length=10, verbose_name='Postal Code')),
                ('extension', models.CharField(max_length=10, verbose_name='Postal Code Extension')),
                ('name', models.CharField(max_length=100, verbose_name='Postal Code Name')),
            ],
        ),
        migrations.AddField(
            model_name='address',
            name='area',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='area_addresses', to='addresses.Area'),
        ),
        migrations.AddField(
            model_name='address',
            name='postal_code',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='addresses', to='addresses.PostalCode'),
        ),
    ]
