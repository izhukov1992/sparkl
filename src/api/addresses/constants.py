ACTIVE = 1
INACTIVE = 0

STATUS_CHOICES = (
    (ACTIVE, 'active'),
    (INACTIVE, 'inactive')
)
