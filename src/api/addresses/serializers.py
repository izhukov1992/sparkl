from rest_framework import serializers

from api.addresses.models import (
    Address,
    Area,
    PostalCode
)


class AreaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Area
        fields = ('id', 'code', 'name', 'status',)

    def create(self, validated_data):
        """Create a service instance."""
        return Area.objects.create(**validated_data)

    def update(self, instance, validated_data):
        """Update a service instance."""
        instance.code = validated_data.get('code', instance.code)
        instance.name = validated_data.get('name', instance.name)
        instance.status = validated_data.get('status', instance.status)
        instance.save()
        return instance

    def to_representation(self, instance):
        """Representation of an address."""
        return {
            'id': instance.id,
            'code': instance.code,
            'name': instance.name,
            'status': instance.get_status_display()
        }


class PostalCodeSerializer(serializers.ModelSerializer):
    class Meta:
        model = PostalCode
        fields = ('id', 'code', 'extension', 'name',)

    def create(self, validated_data):
        """Create a service instance."""
        return PostalCode.objects.create(**validated_data)

    def update(self, instance, validated_data):
        """Update a service instance."""
        instance.code = validated_data.get('code', instance.code)
        instance.extension = validated_data.get('extension',
                                                instance.extension)
        instance.name = validated_data.get('name', instance.name)
        instance.save()
        return instance

    def to_representation(self, instance):
        """Representation of an address."""
        return {
            'id': instance.id,
            'code': instance.code,
            'name': instance.name,
            'extension': instance.extension
        }


class AddressSerializer(serializers.ModelSerializer):
    postal_code = PostalCodeSerializer()
    area = AreaSerializer()

    class Meta:
        model = Address
        fields = ('id',
                  'discrict_code',
                  'county_code',
                  'branch_code',
                  'branch_name',
                  'branch_extra',
                  'venue_name',
                  'number',
                  'area',
                  'postal_code',
                  'source',)

    def create(self, validated_data):
        """Create a service instance."""
        postal_code, _ = PostalCode.objects.get_or_create(
            **validated_data.pop('postal_code'))

        area, _ = Area.objects.get_or_create(**validated_data.pop('area'))

        address = Address.objects.create(area=area,
                                         postal_code=postal_code,
                                         **validated_data)

        return address

    def update(self, instance, validated_data):
        """Update a service instance."""
        instance.discrict_code = validated_data.get(
            'discrict_code', instance.discrict_code)
        instance.county_code = validated_data.get(
            'county_code', instance.county_code)
        instance.branch_code = validated_data.get(
            'branch_code', instance.branch_code)
        instance.branch_name = validated_data.get(
            'branch_name', instance.branch_name)
        instance.branch_extra = validated_data.get(
            'branch_extra', instance.branch_extra)
        instance.venue_name = validated_data.get(
            'venue_name', instance.venue_name)
        instance.number = validated_data.get(
            'number', instance.number)
        instance.area = validated_data.get(
            'area', instance.area)
        instance.postal_code = validated_data.get(
            'postal_code', instance.postal_code)
        instance.source = validated_data.get(
            'source', instance.source)
        instance.save()
        return instance

    def to_representation(self, instance):
        """Representation of an address."""
        return {
            'id': instance.id,
            'discrict_code': instance.discrict_code,
            'county_code': instance.county_code,
            'branch_code': instance.branch_code,
            'branch_name': instance.branch_name,
            'branch_extra': instance.branch_extra,
            'venue_name': instance.venue_name,
            'number': instance.number,
            'area': AreaSerializer(instance.area).data,
            'postal_code': PostalCodeSerializer(instance.postal_code).data,
            'source': instance.source
        }


class AddressNumberSerializer(serializers.ModelSerializer):

    class Meta:
        model = Address
        fields = ('id',
                  'number')

    def to_representation(self, instance):
        """Representation of an address."""
        return {
            'id': instance.id,
            'number': instance.number,
        }


class AddressPublicSerializer(serializers.ModelSerializer):
    postal_code = PostalCodeSerializer()
    area = AreaSerializer()

    class Meta:
        model = Address
        fields = ('id',
                  'branch_name',
                  'venue_name',
                  'number',
                  'area',
                  'postal_code')

    def to_representation(self, instance):
        """Representation of an address."""
        return {
            'branch_name': instance.branch_name,
            'branch_extra': instance.branch_extra,
            'venue_name': instance.venue_name,
        }


class AddressSimpleSerializer(serializers.ModelSerializer):
    postal_code = PostalCodeSerializer()
    area = AreaSerializer()

    class Meta:
        model = PostalCode
        fields = ('id', 'code', 'extension', 'name',)

    def to_representation(self, instance):
        """Representation of an address."""
        return {
            'id': instance.id,
            'code': instance.code,
            'extension': instance.extension,
            'area': instance.name,
            'address': AddressPublicSerializer(
                instance.addresses.first()).data,
            'numbers': AddressNumberSerializer(
                instance.addresses.all(), many=True).data,
        }
