from rest_framework import status
from rest_framework.decorators import list_route, detail_route
from rest_framework.response import Response
from rest_framework.viewsets import ViewSet

from django.shortcuts import get_object_or_404

from api.addresses.models import (
    Address,
    Area,
    PostalCode
)

from api.addresses.serializers import (
    AddressSerializer,
    AddressSimpleSerializer,
    AreaSerializer,
    PostalCodeSerializer
)


class AddressView(ViewSet):
    def list(self, request):
        """List of addresses."""
        addresses = Address.objects.all()

        # require postal code param in order to query branch as well
        postalcode = request.query_params.get('postalcode', None)
        postalcode_id = request.query_params.get('postalcode_id', None)
        autocomplete = request.query_params.get('autocomplete', None)

        if postalcode_id:
            addresses = addresses.filter(postal_code_id=postalcode_id)
        if postalcode:
            branch = request.query_params.get('branch', None)
            code, extension = postalcode.split('-')
            if branch:
                addresses = addresses.filter(postal_code__code=code,
                                             postal_code__extension=extension,
                                             branch_name=branch)
            else:
                addresses = addresses.filter(postal_code__code=code,
                                             postal_code__extension=extension)
        elif autocomplete:
            # NOTE: you could use ElasticSearch for this, just saying (5)
            # https://medium.freecodecamp.org/elasticsearch-with-django-the-easy-way-909375bc16cb
            addresses = addresses.filter(branch_name__icontains=autocomplete)

        serializer = AddressSerializer(addresses, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def retrieve(self, request):
        """Retrieve the service provider."""
        address = get_object_or_404(request.pk)
        serializer = AddressSerializer(address)
        return Response(serializer.data)

    @list_route()
    def get_numbers(self, request):
        """Return list of numbers for given postal code (1000-100 format) or
        postal_code_id."""
        # require postal code param in order to query branch as well
        postalcode = request.query_params.get('postalcode', None)
        postalcode_id = request.query_params.get('postalcode_id', None)
        postal_code = None

        if postalcode_id:
            postal_code = PostalCode.objects.get(id=postalcode_id)
        elif postalcode:
            postal_code = PostalCode.objects.get(
                *zip(['code', 'extension'], postalcode.split('-')))

        serializer = AddressSimpleSerializer(postal_code)
        return Response(serializer.data, status=status.HTTP_200_OK)

    # def create(self, request):
    #     serializer = AddressSerializer(data=request.data)
    #     serializer.is_valid(raise_exception=True)
    #     serializer.save()
    #     return Response(serializer.data, status=status.HTTP_201_CREATED)

    # @list_route()
    # def get_addresses_for_postalcode(self, request):
    #     postalcode = request.query_params.get('postalcode', None)
    #     if postalcode:
    #         code, extension = postalcode.split('-')
    #         addresses = Address.objects.filter(
    #             postal_code__code=code, postal_code__extension=extension)
    #         s = status.HTTP_200_OK
    #     else:
    #         addresses = []
    #         s = status.HTTP_404_NOT_FOUND

    #     serializer = AddressSerializer(data=addresses, many=True)
    #     return Response(serializer.data, status=s)
