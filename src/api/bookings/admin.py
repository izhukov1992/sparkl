# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
# from django import forms
# from django.shortcuts import render
from django.utils.translation import ugettext_lazy as _

# from lib.utils import get_template_path

from api.bookings.constants import *
from api.bookings.models import (
    Booking,
    # BookingCM,
    BookingEnquiry,
    BookingPostalCodeEnquiry,
)


class BookingAdmin(admin.ModelAdmin):
    list_display = ('datetime',
                    'customer',
                    'address_area',
                    'channel',
                    'service_provider',
                    'status',
                    'to_pay',
                    'amount_paid',
                    'paid',
                    'delivered',)
    list_display_links = ('datetime', 'customer')
    search_fields = ('address', 'datetime')
    exclude = ('address',)
    readonly_fields = ('address_pretty',)
    # filter_horizontal = ('services',)
    list_filter = ('datetime',
                   'status',
                   'service_provider',
                   'delivered',
                   'channel',
                   'venue',
                   'paid',
                   'order',
                   # 'services',
                   'paid_to_service_provider',)
    list_per_page = 30
    date_hierarchy = 'datetime'
    actions = ['accept_and_mark_as_paid',
               'lock_calendar',
               'recalc_itineraries']
    # class Media:
    #         css = {
    #             "screen": ("css/items/items.css",)
    #         }
    #         js = ("js/items/items.js",)

    def queryset(self, request):
        # filter available service providers
        qs = super(BookingAdmin, self).queryset(request)
        booking_itinerary = BookingItinerary.objects.get(booking=qs)
        if booking_itinerary:
            sps = [booking_itinerary.best_itinerary.service_provider]
            qs.service_provider.queryset = sps + \
                [it.service_provider for it in
                 booking_itinerary.alternative_itineraries.all()]
        return qs

    def address_area(self, obj):
        return obj.address.area

    def address_pretty(self, obj):
        return obj.address.pretty_format()
    address_pretty.short_description = _("Address")

    def search_address(self, obj):
        # implement search funcion!!
        pass

    def to_pay(self, obj):
        return "{} €".format(
            obj.calculate_total_cost()) if not obj.paid else "-"

    def amount_paid(self, obj):
        return "{} €".format(obj.total_cost) if obj.paid else "-"

    def lock_calendar(modeladmin, request, queryset):
        # lock Service Provider's calendar
        for booking in queryset:
            booking.action_lock_calendar()

    def accept_and_mark_as_paid(modeladmin, request, queryset):
        for booking in queryset:
            if booking.is_pending():
                booking.mark_as_paid()
                booking.accept()

    def recalc_itineraries(modeladmin, request, queryset):
        for booking in queryset:
            booking.recalculate_itineraries()

    lock_calendar.short_description = _(
        'Lock calendar for service provider and alocate slots')
    accept_and_mark_as_paid.short_description = _('Accept and mark as paid')
    recalc_itineraries.short_description = _('Recalculate itineraries')

# change SP and recalculate itinerary
# on save, if SP != booking_itinerary.best_itinerary.service_provider
# recalculate schedule and raise if cannot


# class BookingAdminForm(forms.ModelForm):
#     def clean_status(self):
#         if self.booking and not (self.booking.is_enquiry() |
#                                  self.booking.is_ready()):
#             raise forms.ValidationError(_("Can't change booking when"))


class BookingEnquiryAdmin(admin.ModelAdmin):
    # form = BookingAdminForm
    readonly_fields = ('address',)


class BookingPostalCodeEnquiryAdmin(admin.ModelAdmin):
    pass


# class BookingCMAdmin(admin.ModelAdmin):
#     change_list_template = 'admin/cm_change_list.html'
#     list_display = ('datetime',
#                     'customer',
#                     'address_area',
#                     'channel',
#                     'service_provider',
#                     'status',
#                     'total_cost',
#                     'paid',
#                     'delivered',)
#     list_display_links = ('datetime', 'customer')
#     search_fields = ('address', 'datetime')
#     filter_horizontal = ('services',)
#     list_per_page = 30
#     date_hierarchy = 'datetime'
#     # class Media:
#     #         css = {
#     #             "screen": ("css/items/items.css",)
#     #         }
#     #         js = ("js/items/items.js",)

#     def address_area(self, obj):
#         return obj.address.area

#     # def view_address(self, obj):
#     #     return obj.address.encode('utf-8')

#     def search_address(self, obj):
#         # implement search funcion!!
#         pass


# https://www.webforefront.com/django/admincustomlayout.html
# https://medium.com/@hakibenita/how-to-turn-django-admin-into-a-lightweight-dashboard-a0e0bbf609ad
# @admin.site.register_view('hello',
#                           urlname='custom_hello',
#                           name='Greets you with a hello')
# def custom_hello(request):
#     context = dict(
#         admin.site.each_context(request),
#         hello_from="oxalorg"
#     )
#     return render(request,
#                   get_template_path(__name__, 'hello.html'),
#                   context)


admin.site.register(Booking, BookingAdmin)
# admin.site.register(BookingCM, BookingCMAdmin)
admin.site.register(BookingEnquiry, BookingEnquiryAdmin)
admin.site.register(BookingPostalCodeEnquiry, BookingPostalCodeEnquiryAdmin)
