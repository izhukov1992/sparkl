from collections import OrderedDict


def prepare_chart(data, idx=None, r=False, i=0, sort=True, trim=None):
    if sort:
        if idx:
            data = OrderedDict(sorted(data.items(),
                               key=lambda k: idx.index(k[i]), reverse=r))
        else:
            data = OrderedDict(sorted(data.items(),
                               key=lambda k: k[i], reverse=r))
    result = zip(*data.items())
    if bool(trim):
        result[0] = result[0][:trim]
        result[1] = result[1][:trim]
    return result
