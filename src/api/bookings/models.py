# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from datetime import (
    datetime as dttime,
    timedelta
)


from django.conf import settings
from django.db import models
from django.db.models.signals import pre_save
from django.utils import dateformat
from django.utils.translation import ugettext_lazy as _

from lib.utils import (
    dt_add_service_extra_time,
    dt_sub_service_min_interval,
    has_same_date,
    standardize_dt_seconds
)

from api.navigation.models import BookingItinerary
from api.schedule import constants as schedule_constants
from api.schedule.google_calendar_helpers import *
from api.schedule.models import (
    BookingEnquirySuggestionSlots,
    Slot
)
from api.services.models import get_items_duration

from api.bookings.constants import *


def get_night_fee(datetime):
    """Return night fee if any."""
    # TODO: come up with a dynamic way of doing this (4)
    return settings.NIGHT_FEE if (
        datetime >= datetime.replace(hour=20, minute=0, second=0)) else 0


class Booking(models.Model):
    created_at = models.DateTimeField(_('Created at'), auto_now_add=True)
    updated_at = models.DateTimeField(_('Updated at'), auto_now=True)
    datetime = models.DateTimeField(_('Datetime'))
    # TODO: add job to remove these bookings and mark as expired (0)
    expiration_datetime = models.DateTimeField(_('Expiration datetime'),
                                               null=True, blank=True)

    address = models.ForeignKey('addresses.Address')

    order = models.ForeignKey('business.Order', related_name='order_booking')

    total_cost = models.DecimalField(_('Amount paid'),
                                     max_digits=7, decimal_places=2,
                                     null=True, blank=True)
    paid = models.BooleanField(_('Paid'), default=False)
    paid_to_service_provider = models.BooleanField(
        _('Paid to service provider'), default=False)

    # extra_fees = models.ManyToManyField('ExtraServiceFee')
    night_fee = models.DecimalField(_('Night taxation'),
                                    max_digits=7, decimal_places=2,
                                    null=True, blank=True, default=0)
    venue = models.ForeignKey('services.Venue', null=True, blank=True)
    channel = models.ForeignKey('services.Channel', null=True, blank=True)

    customer = models.ForeignKey('profiles.Customer',
                                 related_name='customer')
    service_provider = models.ForeignKey('profiles.ServiceProvider',
                                         related_name='service_provider',
                                         null=True, blank=True)

    status = models.CharField(_('Status'),
                              choices=STATUS_CHOICES,
                              max_length=3,
                              default=ENQUIRY)
    delivered = models.BooleanField(_('Delivered'), default=False)
    details = models.TextField(_('Details'), max_length=300,
                               null=True, blank=True)

    source = models.CharField(_('Source'), max_length=100,
                              null=True, blank=True)

    def save(self, *args, **kwargs):
        self.datetime = standardize_dt_seconds(self.datetime)
        super(Booking, self).save(*args, **kwargs)

    def is_enquiry(self):
        """Check if booking status is still enquiry."""
        return self.status == ENQUIRY

    def is_ready(self):
        """Check if booking status is ready (itineraries are calculated)."""
        return self.status == READY

    def is_pending(self):
        """Check if booking status is pending."""
        return self.status == PENDING

    def rollback_to_enquiry(self):
        """Rollback booking status to enquiry.

        Admin action."""
        self.status = ENQUIRY
        self.save()

    def mark_as_pending(self):
        """Mark booking as pending."""
        self.set_expiration_datetime()
        self.status = PENDING
        self.save()

    def mark_as_paid(self):
        """Mark booking as paid.

        Admin action."""
        # TODO: confirm calculated amount paid (3)
        self.paid = True
        self.total_cost = self.calculate_total_cost()
        self.save()

    def mark_as_delivered(self):
        """Mark booking as delivered."""
        self.delivered = True
        self.save()

    def accept(self):
        """Accept booking upon payment."""
        # validate if user has paid all the previous bookings
        self.status = ACCEPTED
        self.save()

        google_calendar = start_service()
        # update_slot_event(google_calendar, self.slots.first().prev_slot)
        update_slot_event(google_calendar, self.slots.first())
        # FIXME: update_slot_event for itinerary as well (1)

    def reject(self):
        """Decline booking."""
        self.status = REJECTED
        self.save()

        google_calendar = start_service()
        update_slot_event(google_calendar, self.slots.first())

    def cancel(self):
        """Cancel booking."""
        self.status = CANCELED
        self.save()

        google_calendar = start_service()
        update_slot_event(google_calendar, self.slots.first())

    def expire(self):
        """Expire booking due to non payment."""
        self.status = EXPIRED
        self.save()

        google_calendar = start_service()
        update_slot_event(google_calendar, self.slots.first())

    def get_services(self):
        """Return services related to this order."""
        return [item.service for item in self.order.get_items()]

    def get_services_duration(self):
        """Return total services duration."""
        return get_items_duration(self.order.get_items(),
                                  self.service_provider)

    def calculate_extra_fees(self):
        fees = 0
        # night fee
        night_fee = get_night_fee(self.datetime)
        if self.night_fee != night_fee:
            self.night_fee = night_fee
            self.save()
        if self.night_fee:
            fees += self.night_fee
        return fees

    def calculate_total_cost(self):
        return self.get_services_price() + self.calculate_extra_fees()

    def set_expiration_datetime(self):
        """Set an expiration datetime based on parameters form config."""
        # TODO: come up with a dynamic way of doing this (4)
        # if last minute (same day)
        if has_same_date(datetime.now(), self.datetime):
            exp = datetime.now() + timedelta(
                seconds=settings.EXPIRATION_SAME_DAY)
        # if less than than 48h
        elif self.datetime <= (datetime.now() + timedelta(hours=48)):
            exp = datetime.now() + timedelta(
                seconds=settings.EXPIRATION_LT_48H)
        # if great than or equal to 48h
        else:
            exp = datetime.now() + timedelta(
                seconds=settings.EXPIRATION_GTE_48H)
        self.expiration_datetime = standardize_dt_seconds(exp)
        self.save()

    def assign_service_provider(self, service_provider):
        """Assign service provider to booking."""
        self.service_provider = service_provider
        self.save()

    def alocate_slots(self):
        """Alocate the slots regarding the travel time and booking time to the
        selected service provider.

        Creates the slot for travel and another for booking, each referencing
        that FK. Creates combined event in Service Provider's Google Calendar.
        """
        services_duration = self.order.services_duration

        # allocate travel slot
        booking_itinerary = BookingItinerary.objects.filter(
            booking=self, active=True).last()
        travel_start = dt_sub_service_min_interval(self.datetime) - timedelta(
            seconds=(
                booking_itinerary.chosen_itinerary.calculated_transit_time))
        travel_slot = Slot.objects.create(
            slot_from=travel_start,
            slot_until=self.datetime,
            itinerary=booking_itinerary.chosen_itinerary,
            service_provider=self.service_provider,
            slot_type=schedule_constants.TRAVEL)

        # allocate booking slot
        booking_end = dt_add_service_extra_time(self.datetime) + timedelta(
            seconds=services_duration)
        booking_slot = Slot.objects.create(
            slot_from=self.datetime,
            slot_until=booking_end,
            booking=self,
            service_provider=self.service_provider,
            slot_type=schedule_constants.BOOKING,
            prev_slot=travel_slot)

        # split full slot
        main_slot = self.service_provider.get_available_slot(self.datetime)

        slot_before = getattr(main_slot, 'prev_slot', None)
        slot_after = getattr(main_slot, 'next_slot', None)
        main_slot_to_update = False
        main_slot_to_delete = False

        google_calendar = start_service()

        if (main_slot.slot_from < travel_start and
                main_slot.slot_until > booking_end):
            # if available slot starts before and ends after
            slot_after = Slot.objects.create(
                slot_from=booking_end,
                slot_until=main_slot.slot_until,
                service_provider=self.service_provider,
                slot_type=schedule_constants.AVAILABLE,
                prev_slot=booking_slot)

            # TODO: add to queue (2)
            create_availability_event(google_calendar, slot_after)

            main_slot.slot_until = travel_start
            main_slot.save()
            slot_before = main_slot

            main_slot_to_update = True
        elif main_slot.slot_from < travel_start:
            # if available slot only starts before
            main_slot.slot_until = travel_start
            main_slot.save()

            main_slot_to_update = True
        elif main_slot.slot_until > booking_end:
            # if available slot only ends after
            main_slot.slot_from = booking_end
            main_slot.save()
            slot_after = main_slot

            main_slot_to_update = True
        else:
            # if available slot is completely filled
            main_slot_to_update = True
            main_slot_to_delete = True

        # main_slot <-> travel_slot <-> booking_slot <-> slot_after
        travel_slot.next_slot = booking_slot
        if bool(slot_before):
            travel_slot.prev_slot = slot_before
            slot_before.next_slot = travel_slot
            slot_before.save()
        travel_slot.save()

        if bool(slot_after):
            booking_slot.next_slot = slot_after
            slot_after.prev_slot = booking_slot
            slot_after.save()
        booking_slot.save()

        # create_travel_event(google_calendar, travel_slot)
        # create_booking_event(google_calendar, booking_slot)
        # TODO: add to queue (2)
        create_combined_booking_event(google_calendar, booking_slot)
        if main_slot_to_update:
            update_slot_event(google_calendar, main_slot)

        if main_slot_to_delete:
            main_slot.delete()

    def generate_itineraries(self):
        """Calculates the optimal itinerary for the available service providers
        for this specific booking."""
        booking_itinerary = BookingItinerary.objects.create(booking=self)
        chosen_service_provider = getattr(self, 'service_provider', None)
        if chosen_service_provider:
            booking_itinerary.calculate_service_provider(
                [chosen_service_provider])
        else:
            booking_itinerary.calculate_service_provider()
        if booking_itinerary.best_itinerary:
            self.status = READY
            self.save()

        return booking_itinerary

    def deprecate_previous_itineraries(self):
        """If there are previously calculated itineraries, the optimal
        itinerary should be recalculated and the previous ones deprecated."""
        booking_itineraries = BookingItinerary.objects.filter(
            booking=self).order_by('-id')[1:]
        for b_it in booking_itineraries:
            b_it.deprecate()

    def recalculate_itineraries(self):
        """Recalculate itineraries."""
        self.bookingenquiry_set.last().handle_booking()

    def action_lock_calendar(self):
        """Locks service provider calendar for this booking, alocating the
        slots and updating the calendar."""
        if self.service_provider and self.is_ready():
            # lock SP schedule
            self.alocate_slots()
            # pending payment
            self.mark_as_pending()
        # recalculate ready bookings itineraries
        bookings_ready = Booking.objects.filter(
            status=READY,
            datetime__range=(
                datetime.combine(self.datetime.date(), dttime.min.time()),
                datetime.combine(self.datetime.date(), dttime.max.time())))
        for booking in bookings_ready:
            booking.recalculate_itineraries()

    def get_services_price(self):
        """Get total price for the related order."""
        return self.order.get_total_price()

    def get_amount_revenue(self, service_provider=None, revenue_p=None):
        """Calculates service provider's revenue for this booking.

        This is used as a key criteria to exclude the service provider
        selection upon alocating the booking."""
        if not revenue_p:
            sp = service_provider or self.service_provider
            sp_extra_details = sp.extra_details.first()
            revenue_p = revenue_p or sp_extra_details.revenue_percentage
        return self.get_services_price() * revenue_p

    def __str__(self):
        return "Booking #{0} - {1} ({2})".format(self.id,
                                                 self.customer,
                                                 self.get_status_display())

    def __unicode__(self):
        return u"Booking #%d - %s (%s)" % (self.id,
                                           self.customer,
                                           self.get_status_display())


def fill_address(sender, instance, *args, **kwargs):
    # FIXME: check why this isn't working (2)
    if not instance.pk and not instance.address:
        instance.address = instance.customer.address


pre_save.connect(fill_address, sender=Booking)


# class BookingCM(Booking):
#     class Meta:
#         proxy = True
#         verbose_name = 'Community Management Booking'
#         verbose_name_plural = 'Community Management Bookings'
#         # default_permissions = ('booking_cm')
#         # permissions = (('booking_cm', 'Community Management'),)


class BookingEnquiry(models.Model):
    created_at = models.DateTimeField(_('Created at'), auto_now_add=True)
    datetime = models.DateTimeField(_('Chosen Datetime'))
    service_provider = models.ForeignKey('profiles.ServiceProvider',
                                         null=True, blank=True)
    address = models.ForeignKey('addresses.Address', null=True, blank=True)
    address_unit = models.CharField(_('Address Unit'),
                                    max_length=200, null=True, blank=True)

    order = models.ForeignKey('business.Order',
                              related_name='order_booking_enq')

    customer = models.ForeignKey('profiles.Customer')
    source = models.CharField(_('Source'),
                              max_length=100, null=True, blank=True)

    booking = models.ForeignKey('Booking', null=True, blank=True)
    details = models.TextField(_('Details'), max_length=300,
                               null=True, blank=True)

    class Meta:
        verbose_name_plural = "Booking Enquiries"

    def save(self, *args, **kwargs):
        self.datetime = standardize_dt_seconds(
            self.datetime)
        super(BookingEnquiry, self).save(*args, **kwargs)

    def prepare_booking(self):
        """Create booking related to enquiry, fills selected service provider
        if any."""
        booking = Booking.objects.create(
            datetime=self.datetime,
            customer=self.customer,
            address=self.address,
            order=self.order
        )
        self.booking = booking
        if getattr(self, 'service_provider', None):
            booking.service_provider = self.service_provider
            booking.save()
        self.save()

        return booking

    def handle_booking(self, created=False):
        """Creates booking related to enquiry, generates itineraries,
        locks service provider calendar."""
        if created:
            self.prepare_booking()

        self.booking.generate_itineraries()
        self.booking.deprecate_previous_itineraries()
        self.booking.action_lock_calendar()

        if not self.booking.bookingitinerary_set.first().best_itinerary:
            self.booking.rollback_to_enquiry()
            self.generate_alternative_schedules()

    def generate_alternative_schedules(self, dt=None):
        """DEPRECATED: for admin use only.

        Generate alternative itineraries for given booking, if the
        preferred datetime isn't available anymore."""
        # TODO: base available slots on other bookings and distances (1)
        dt = dt or self.booking.datetime
        slots = Slot.objects.filter(
            slot_type=schedule_constants.AVAILABLE,
            slot_from__gte=dt.replace(
                hour=0, minute=0, second=0),
            slot_until__lte=dt.replace(
                hour=23, minute=59, second=59))
        b_enq_slots = BookingEnquirySuggestionSlots.objects.create(
            booking_enquiry=self)
        b_enq_slots.possible_slots.add(*slots)
        b_enq_slots.generate_datetime_proposals()
        # b_enq_slots.set_start()

        return b_enq_slots

    def get_extra_fees(self):
        """Get extra fees related to booking."""
        return get_night_fee(self.datetime)

    def __str__(self):
        return "#{} {} {}".format(
            self.id,
            self.customer,
            self.datetime)

    def __unicode__(self):
        return "#%d %s %s" % (self.id,
                              self.customer,
                              dateformat.format(self.datetime,
                                                'j F Y, H:i'),)


class BookingPostalCodeEnquiry(models.Model):
    created_at = models.DateTimeField(_('Created at'), auto_now_add=True)
    postal_code = models.CharField(_('Postal Code'), max_length=20)
    address = models.CharField(_('Address'), max_length=100)
    email = models.CharField(_('E-mail'), max_length=50)
    customer = models.ForeignKey('profiles.Customer', null=True, blank=True)
    source = models.CharField(_('Source'),
                              max_length=100, null=True, blank=True)

    class Meta:
        verbose_name_plural = "Booking Postal Code Enquiries"
