STARTED = 'STR'
ENQUIRY_0 = 'NQ0'
ENQUIRY_1 = 'NQ1'
ENQUIRY_2 = 'NQ2'
ENQUIRY_3 = 'NQ3'
ENQUIRY_4 = 'NQ4'
ENQUIRY_5 = 'NQ5'
ENQUIRY = 'ENQ'
READY = 'RDY'
PENDING = 'PEN'
ACCEPTED = 'ACC'
DECLINED = 'DEC'
CANCELED = 'CAN'
EXPIRED = 'EXP'
RESCHEDULED = 'RSH'

STATUS_CHOICES = (
    (STARTED, 'started'),
    (ENQUIRY, 'enquiry'),
    (READY, 'ready'),
    (PENDING, 'pending'),
    (ACCEPTED, 'accepted'),
    (DECLINED, 'declined'),
    (CANCELED, 'canceled'),
    (EXPIRED, 'expired'),
    (RESCHEDULED, 'rescheduled')
)
