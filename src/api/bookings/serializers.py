from rest_framework import serializers

# from lib.utils import (
#     validate_email as email_is_valid,
#     calculate_age
# )
# from lib.validate_pt import controlNIF
# from lib.utils import random_password_generator

# from accounts.models import User
from api.addresses.models import Address
from api.addresses.serializers import AddressSerializer
from api.business.serializers import OrderPublicSerializer
from api.business.views import order_from_token
from api.payments.serializers import PaymentPublicSerializer
from api.profiles.models import ServiceProvider
from api.profiles.serializers import (
    CustomerPublicSerializer,
    ServiceProviderPublicSerializer
)
from api.bookings.models import Booking, BookingEnquiry


class BookingSerializer(serializers.ModelSerializer):
    class Meta:
        model = Booking
        fields = ('id',
                  'row_idx',
                  'datetime',
                  'order',
                  'customer',
                  'service_provider',
                  'amount_paid',
                  'amount_revenue',
                  'amount_profit',
                  'type_of_service',
                  'percentage',
                  'paid_to_service_provider',
                  'paid',
                  'repeated',
                  'night_tax',
                  'reservation_weekday',
                  'services_count',
                  'visits_count',
                  'venue',
                  'channel',
                  'details',
                  'opportunities',
                  'feedback')

    def create(self, validated_data):  # pragma: no cover
        """Create a booking instance."""
        pass

    def update(self, instance, validated_data):
        """Update a booking instance."""
        pass

    def to_representation(self, instance):
        """Representation of a booking."""
        return {
            'id': instance.id,
            'row_idx': instance.row_idx,
            'datetime': instance.datetime,
            'order': instance.order,
            'customer': instance.customer,
            'service_provider': instance.service_provider,
            'amount_paid': instance.amount_paid,
            'amount_revenue': instance.amount_revenue,
            'amount_profit': instance.amount_profit,
            'type_of_service': instance.type_of_service,
            'percentage': instance.percentage,
            'paid_to_service_provider': instance.paid_to_service_provider,
            'paid': instance.paid,
            'repeated': instance.repeated,
            'night_tax': instance.night_tax,
            'reservation_weekday': instance.reservation_weekday,
            'services_count': instance.services_count,
            'visits_count': instance.visits_count,
            'venue': instance.venue,
            'channel': instance.channel,
            'details': instance.details,
            'opportunities': instance.opportunities,
            'feedback': instance.feedback
        }


class BookingEnquirySerializer(serializers.Serializer):
    datetime = serializers.DateTimeField()
    sp = serializers.IntegerField()
    address_id = serializers.IntegerField()
    first_name = serializers.CharField()
    last_name = serializers.CharField()
    mobile_number = serializers.CharField()
    vat = serializers.CharField()

    class Meta:
        model = BookingEnquiry
        fields = ('customer',
                  'datetime',
                  'service_provider',
                  'address',
                  'address_unit',
                  'order',
                  'booking',
                  'details',
                  'source')

    def create(self, validated_data):  # pragma: no cover
        """Create a customer instance."""
        # if validated_data.get('facebook_id'):
        #     fb_id = validated_data.pop('facebook_id')
        #     c = Customer.objects.filter(facebook_id=fb_id)
        #     if c:
        #         customer = c.first()
        #     else:
        #         customer = Customer.objects.create_from_facebook(fb_id)
        #         customer.mobile_number = validated_data.pop('mobile_number')
        #         customer.save()
        #         e = customer.extra_details.first()
        #         e.source = 'facebook_messenger'
        #         e.save()
        # else:
        #     mobile_number = validated_data.pop('mobile_number')
        #     customer = Customer.objects.filter(mobile_number=mobile_number)
        #     if not customer:
        #         customer = Customer.objects.create(
        #             first_name=validated_data.pop('first_name'),
        #             last_name=validated_data.pop('last_name'),
        #             mobile_number=mobile_number)
        #     else:
        #         customer = customer.first()

        # address = Address.objects.get(id=validated_data.pop('address_id'))

        #     # FIXME: allow add address (1)
        #     # TODO: calculate default address - Max() (2)
        #     if not customer.address:
        #         customer.address = address
        #         customer.address_unit = validated_data.pop('address_unit')
        #         customer.save()
        #     # elif customer.has_address(address):

        #     services = Service.objects.filter(
        #         id__in=validated_data.pop('services'))

        #     booking_enquiry = BookingEnquiry.objects.create(
        #         preferred_datetime=standardize_dt_seconds(
        #             validated_data.pop('datetime')),
        #         address=address,
        #         address_unit=customer.address_unit,
        #         customer=customer)
        #     booking_enquiry.services.add(*[s for s in services.all()])
        #     # TODO: enqueue this (1)
        #     booking_enquiry.handle_booking(created=True)

        #     return booking_enquiry

        order = order_from_token(self.context.get('ct'))

        # TODO: validate datetime >= now() (1)
        if 'sp' in validated_data:
            sp = ServiceProvider.objects.get(
                id=validated_data.pop('sp'))
            return BookingEnquiry.objects.create(
                datetime=validated_data.pop('datetime'),
                customer=order.customer,
                order=order,
                service_provider=sp)
        else:
            return BookingEnquiry.objects.create(
                datetime=validated_data.pop('datetime'),
                customer=order.customer,
                order=order)

    def update(self, instance, validated_data):
        order = order_from_token(self.context.get('ct'))
        booking_enq = order.order_booking_enq.first()

        if self.context.get('actions'):
            if 'update_sp' in self.context.get('actions'):
                sp = ServiceProvider.objects.get(
                    id=validated_data.pop('sp'))
                booking_enq.service_provider = sp
            elif 'discard_sp' in self.context.get('actions'):
                booking_enq.service_provider = None
            if 'update_dt' in self.context.get('actions'):
                booking_enq.datetime = validated_data.pop(
                    'datetime')

            booking_enq.save()
        else:
            address = Address.objects.get(
                id=validated_data.pop('address_id'))

            order.customer.assign_address(address)
            order.customer.first_name = validated_data.pop('first_name')
            order.customer.last_name = validated_data.pop('last_name')
            order.customer.mobile_number = validated_data.pop('mobile_number')
            order.customer.vat = validated_data.pop('vat')

            booking_enq.address = address

            if 'address_unit' in validated_data:
                order.customer.address_unit = validated_data.pop(
                    'address_unit')
                booking_enq.address_unit = order.customer.address_unit

            order.customer.save()

            if 'source' in validated_data:
                booking_enq.source = validated_data.pop('source')

            if 'details' in validated_data:
                booking_enq.details = validated_data.pop('details')

            booking_enq.save()

            booking_enq.order.close()
            booking_enq.handle_booking(created=True)

            booking_enq.order.trigger_payment(
                payment_method=self.context.get('payment_method'))
            booking_enq.order.payments.last().assign_customer_ip(
                self.context.get('customer_ip'))

        return booking_enq

    def to_representation(self, instance):
        """Representation of a booking."""
        return {
            'id': instance.id,
            'order': OrderPublicSerializer(instance.order).data,
            'customer': CustomerPublicSerializer(instance.customer).data,
            'datetime': instance.datetime,
            'service_provider': ServiceProviderPublicSerializer(
                instance.service_provider).data,
            'address': AddressSerializer(instance.address).data,
            'address_unit': instance.address_unit,
            'source': instance.source,
            'payment': PaymentPublicSerializer(
                instance.order.payments.last()).data
        }


# class ExtraFeeSerializer(serializers.Serializer):
#     title = serializers.CharField()
#     amount = serializers.FloatField()

#     def to_representation(self, instance):
#         """Representation of a booking."""
#         return {
#             'title': instance.title,
#             'amount': instance.amount,
#         }
