from django.conf.urls import url
from django.utils.translation import ugettext_lazy as _

import views as booking_views

urlpatterns = [
    url(_(r'^booking/list/$'),
        booking_views.BookingView.as_view({'gel': 'list'}),
        name='bookings'),
    url(_(r'^booking/$'),
        booking_views.BookingView.as_view({'get': 'retrieve'}),
        name='booking_retrieve'),
]
