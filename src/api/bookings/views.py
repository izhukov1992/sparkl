#!/usr/bin/python
# -*- coding: utf-8 -*-

from datetime import (
    datetime,
    date,
    timedelta
)
from calendar import monthrange
# import numpy as np
from collections import defaultdict

from rest_framework import status, viewsets
# from rest_framework.permissions import IsAuthenticated
from rest_framework.decorators import list_route, detail_route
from rest_framework.response import Response
# from rest_framework_jwt.authentication import JSONWebTokenAuthentication

# from django.db.models import Q, Avg, Sum
from django.db.models import Count
from django.shortcuts import get_object_or_404
from django.utils.translation import ugettext_lazy as _

# from api.profiles.models import Customer

from api.bookings.helpers import (
    prepare_chart
)

from lib.constants import (
    MONTHS as m,
    WEEKDAYS as w
)

from api.addresses.models import Address
from api.profiles.models import (
    Customer,
    ServiceProvider
)
from api.bookings.models import (
    Booking,
    BookingEnquiry
)

from api.business.models import pre_calculate_slots
from api.business.views import order_from_token
from api.services.models import Service
from api.bookings.serializers import (
    BookingSerializer,
    BookingEnquirySerializer,
    # ExtraFeeSerializer
)


class BookingView(viewsets.ViewSet):
    def list(self, request):
        """List of service providers."""
        bookings = Booking.objects.all()

        serializer = BookingSerializer(bookings, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def retrieve(self, request):
        """Retrieve the service provider."""
        booking = get_object_or_404(Booking)
        serializer = BookingSerializer(booking)
        return Response(serializer.data)

    # def start_order(self, request):
    #     ct = request.COOKIES.get('ct')
    #     order = order_from_token(ct)

    #     postalcode = request.data.get('postal_code', '-')

    #     code, ext = postalcode.split('-')
    #     postal_codes = PostalCode.objects.filter(
    #         code=code, extension=ext)

    #     if postal_codes:
    #         postal_code = postal_codes.first()

    #         order.assign_postal_code(postal_code)

    #         # TODO: queue (0)
    #         pre_calculate_slots(order)

    #         return HttpResponseRedirect(reverse('services:category_list'))
    #     else:
    #         return render(request, 'addresses/postal_code_enquiry.html')

    # def add_items(self, request):
    #     ct = request.COOKIES.get('ct')
    #     order = order_from_token(ct)

    #     services = request.data.getlist('services')

    #     if services:
    #         order.create_items(services)

    #         pre_calculate_slots(order)

    #         response = HttpResponseRedirect(reverse('schedule:choose_slot'))
    #     else:
    #         response = HttpResponseRedirect(reverse('services:category_list'))

    #     return response


class BookingEnquiryView(viewsets.ViewSet):
    @list_route(methods=['post'])
    def assign_datetime(self, request):
        # TODO: ver se ainda está disponivel, se nao estiver 410 gone (0)
        ct = request.COOKIES.get('ct')

        order = order_from_token(ct)

        dt = request.data.get('datetime')
        sp = request.data.get('sp')
        if order.order_booking_enq:
            booking_enq = order.order_booking_enq.first()
            data = {}
            context = {'ct': ct, 'actions': []}
            if sp and (
                getattr(booking_enq, 'service_provider', None) is None or
                getattr(booking_enq, 'service_provider', None) is not None and
                    sp != booking_enq.service_provider.pk):
                data.update({'sp': sp})
                context['actions'].append('update_sp')
            elif not sp and getattr(booking_enq, 'service_provider', None):
                context['actions'].append('discard_sp')
            if dt and getattr(booking_enq, 'datetime', None) != dt:
                # TODO: check if slot is still valid, raise if not
                data.update({'datetime': dt})
                context['actions'].append('update_dt')

            serializer = BookingEnquirySerializer(
                booking_enq,
                data=data,
                context=context,
                partial=True)
        else:
            serializer = BookingEnquirySerializer(
                data=request.data,
                context={'ct': ct})

        if serializer.is_valid(raise_exception=True):
            booking_enq = serializer.save()
            night_fee = booking_enq.get_extra_fees()
            fees = {}
            if night_fee:
                fees = {
                    'message': _("Night fee"),
                    'amount': night_fee
                }
            data = {
                'booking': serializer.data,
                'fees': fees
            }
            # if booking_enq.has_extra_fees():
            #     fees = [ExtraFeeSerializer(extra_fee).data for extra_fee in
            #             booking_enq.get_extra_fees()]
            return Response(data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors,
                        status=status.HTTP_400_BAD_REQUEST)

    @list_route(methods=['post'])
    def end(self, request):
        ct = request.COOKIES.get('ct')
        order = order_from_token(ct)

        serializer = BookingEnquirySerializer(
            order.order_booking_enq.first(),
            data=request.data,
            context={
                'ct': ct,
                'payment_method': request.data.get('payment_method'),
                'customer_ip': request.META['REMOTE_ADDR']},
            partial=True)

        if serializer.is_valid(raise_exception=True):
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors,
                        status=status.HTTP_400_BAD_REQUEST)
