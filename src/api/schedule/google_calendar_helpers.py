# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from datetime import (
    datetime,
    timedelta
)
import pytz
from sparkl_web.google_oauth2_startservice import build_service

from django.conf import settings

from lib.utils import (
    dt_add_service_extra_time,
    dt_sub_service_min_interval,
    multi_getattr
)

from api.schedule.constants import *
from api.schedule.models import GoogleCalendarEvent


def start_service():
    service = build_service()

    return service


def get_service_provider_calendar_id(slot):
    return slot.service_provider.extra_details.first().google_calendar_id or \
        settings.CALENDAR_ID


def create_combined_booking_event(service, slot):
    # travel + booking slots together
    summary = "{0} - {1}".format(slot.booking.address.area.name.upper(),
                                 slot.booking.customer.get_full_name())
    description = (
        "Services: {0}\n" +
        "Service provider: {1}\n\n" +
        "Customer mobile: {2}\n" +
        "Customer VAT: {3}\n\n" +
        "Itinerary: \n" +
        "From: {4}\n" +
        "To: {5}\n" +
        "By: {6}\n" +
        "Distance: {7}\n" +
        "Time: {8}").format(
        ", ".join(["%dx %s (%s)" % (
            i.quantity, i.service.name, i.service.category) for i
            in slot.booking.order.get_items()]),
        slot.booking.service_provider.get_full_name(),
        slot.booking.customer.mobile_number,
        slot.booking.customer.vat,
        slot.prev_slot.itinerary.departure_address.pretty_format(),
        slot.prev_slot.itinerary.destination_address.pretty_format(),
        slot.prev_slot.itinerary.get_travel_method_display(),
        slot.prev_slot.itinerary.calculated_transit_distance_pretty(),
        slot.prev_slot.itinerary.calculated_transit_time_pretty())

    slot_from = dt_sub_service_min_interval(slot.slot_from) - timedelta(
        seconds=slot.prev_slot.itinerary.calculated_transit_time)
    slot_until = dt_add_service_extra_time(slot.slot_until)

    status = dict(GOOGLE_CALENDAR_EVENT_STATUS).get(
        slot.booking.status)
    calendar_id = settings.PENDING_CALENDAR_ID if \
        status == GCALENDAR_PENDING else \
        get_service_provider_calendar_id(slot)
    calendar_id = settings.PENDING_CALENDAR_ID
    event = service.events().insert(calendarId=calendar_id, body={
        'summary': summary,
        'start': {'dateTime': slot_from.replace(
            tzinfo=pytz.utc).isoformat()},
        'end': {'dateTime': slot_until.replace(
            tzinfo=pytz.utc).isoformat()},
        'location': slot.booking.address.pretty_format(),
        'description': description,
        'status': dict(GOOGLE_CALENDAR_EVENT_STATUS).get(
            slot.booking.status)
    }).execute()

    create_gcalendar_event(slot, event, calendar_id)


def create_booking_event(service, slot):
    summary = "{0} - {1}".format(slot.booking.address.area.name.upper(),
                                 slot.booking.customer.get_full_name())
    description = ("Services: {0}\n" +
                   "Service provider: {1}\n\n" +
                   "Customer mobile: {2}\n" +
                   "Customer VAT: {3}").format(
                   ", ".join(["%dx %s (%s)" % (i.quantity,
                                               i.service.name,
                                               i.service.category) for i
                   in slot.booking.order.get_items()]),
                   slot.booking.service_provider.get_full_name(),
                   slot.booking.customer.mobile_number,
                   slot.booking.customer.vat)

    slot_from = slot.slot_from
    slot_until = dt_add_service_extra_time(slot.slot_until)

    status = dict(GOOGLE_CALENDAR_EVENT_STATUS).get(
        slot.booking.status)
    calendar_id = settings.PENDING_CALENDAR_ID if \
        status == GCALENDAR_PENDING else \
        get_service_provider_calendar_id(slot)

    event = service.events().insert(calendarId=calendar_id, body={
        'summary': summary,
        'start': {'dateTime': slot_from.replace(
            tzinfo=pytz.utc).isoformat()},
        'end': {'dateTime': slot_until.replace(
            tzinfo=pytz.utc).isoformat()},
        'location': slot.booking.address.pretty_format(),
        'description': description,
        'status': dict(GOOGLE_CALENDAR_EVENT_STATUS).get(
            slot.booking.status)
    }).execute()

    create_gcalendar_event(slot, event, calendar_id)


def create_travel_event(service, slot):
    summary = "[{0}] {1} -> {2}".format(
        slot.itinerary.get_travel_method_display(),
        slot.itinerary.departure_address.postal_code.name,
        slot.itinerary.destination_address.postal_code.name)
    description = (
        "Service Provider: {0}\n\n" +
        "From: {1}\n" +
        "To: {2}\n" +
        "By: {3}\n" +
        "Distance: {4}\n" +
        "Time: {5}").format(
        slot.service_provider.get_full_name(),
        slot.itinerary.departure_address.pretty_format(),
        slot.itinerary.destination_address.pretty_format(),
        slot.itinerary.get_travel_method_display(),
        slot.itinerary.calculated_transit_distance_pretty(),
        slot.itinerary.calculated_transit_time_pretty())

    slot_from = dt_sub_service_min_interval(slot.slot_from)
    slot_until = slot.slot_until

    status = dict(GOOGLE_CALENDAR_EVENT_STATUS).get(
        slot.next_slot.booking.status)
    calendar_id = settings.PENDING_CALENDAR_ID if \
        status == GCALENDAR_PENDING else \
        get_service_provider_calendar_id(slot)

    event = service.events().insert(calendarId=calendar_id, body={
        'summary': summary,
        'start': {'dateTime': slot_from.replace(
            tzinfo=pytz.utc).isoformat()},
        'end': {'dateTime': slot_until.replace(
            tzinfo=pytz.utc).isoformat()},
        'location': slot.itinerary.destination_address.pretty_format(),
        'description': description,
        'status': status
    }).execute()

    create_gcalendar_event(slot, event, calendar_id)


def create_availability_event(service, slot):
    calendar_id = get_service_provider_calendar_id(slot)

    if bool(slot.prev_slot) and slot.prev_slot.slot_type == BOOKING:
        slot_from = dt_add_service_extra_time(slot.slot_from)
    else:
        slot_from = slot.slot_from

    event = service.events().insert(
        calendarId=calendar_id,
        body={
            'summary': "AVAILABLE - {}".format(slot.service_provider),
            'start': {'dateTime': slot_from.replace(
                tzinfo=pytz.utc).isoformat()},
            'end': {'dateTime': slot.slot_until.replace(
                tzinfo=pytz.utc).isoformat()},
            'status': dict(GOOGLE_CALENDAR_EVENT_STATUS).get(AVAILABLE)
        }).execute()

    create_gcalendar_event(slot, event, calendar_id)


# def create_time_off_event(service, slot):
#     # TODO: update availability slot to remove that time interval
#     event = service.events().insert(
#         calendarId=get_service_provider_calendar_id(slot),
#         body={
#             'summary': "{} OFF".format(slot.service_provider),
#             'start': {'dateTime': slot.slot_from.replace(
#                 tzinfo=pytz.utc).isoformat()},
#             'end': {'dateTime': slot.slot_until.replace(
#                 tzinfo=pytz.utc).isoformat()},
#             'status': dict(GOOGLE_CALENDAR_EVENT_STATUS).get(OFF)
#         }).execute()

#     create_gcalendar_event(slot, event)


def update_slot_event(service, slot, **kwargs):
    slot_event = slot.google_calendar_event.first()

    event = service.events().get(calendarId=slot_event.google_calendar_id,
                                 eventId=slot_event.event_id).execute()

    slot_from = slot.slot_from
    slot_until = slot.slot_until

    if slot.slot_type == BOOKING:
        slot_from = dt_sub_service_min_interval(slot.slot_from) - timedelta(
            seconds=slot.prev_slot.itinerary.calculated_transit_time)
        slot_until = dt_add_service_extra_time(slot.slot_until)
    elif slot.slot_type == TRAVEL:
        slot_from = dt_sub_service_min_interval(slot.slot_from)
    elif slot.slot_type == AVAILABLE:
        if getattr(slot, 'prev_slot', None) and \
                slot.prev_slot.slot_type == BOOKING:
            slot_from = dt_add_service_extra_time(slot.slot_from)

    event['start'] = {'dateTime': slot_from.replace(
        tzinfo=pytz.utc).isoformat()}
    event['end'] = {'dateTime': slot_until.replace(
        tzinfo=pytz.utc).isoformat()}

    status = {
        AVAILABLE: AVAILABLE,
        BOOKING: multi_getattr(slot, 'booking.status', None),
        OFF: OFF,
        TRAVEL: multi_getattr(slot, 'next_slot.booking.status', None)
    }.get(slot.slot_type)

    previous_status = event['status']
    event['status'] = dict(GOOGLE_CALENDAR_EVENT_STATUS).get(status)

    service.events().update(calendarId=slot_event.google_calendar_id,
                            eventId=slot_event.event_id,
                            body=event).execute()

    # if it was pending and now it's confirmed
    if (previous_status == GCALENDAR_PENDING and
            event['status'] == GCALENDAR_CONFIRMED):
        previous_calendar = slot_event.google_calendar_id
        slot_event.google_calendar_id = get_service_provider_calendar_id(slot)
        slot_event.save()

        service.events().move(calendarId=previous_calendar,
                              destination=slot_event.google_calendar_id,
                              eventId=slot_event.event_id).execute()
    # if it was confirmed and now it's canceled
    elif (previous_status == GCALENDAR_CONFIRMED and
            event['status'] == GCALENDAR_CANCELLED):
        # FIXME: update gcalendar on cancel booking (2)
        # 1. recalculate and merge slots
        # 2. update slots in google calendar
        pass
    # if it was pending and now it's canceled/expired/declined


def create_gcalendar_event(slot, event, calendar_id):
    return GoogleCalendarEvent.objects.create(
        slot=slot,
        google_calendar_id=calendar_id,
        event_id=event.get('id'),
        event_link=event.get('htmlLink'),
        status=event.get('status'),
        ical_uid=event.get('iCalUID'),
        last_updated=datetime.strptime(
            event.get('updated').split('.')[0], '%Y-%m-%dT%H:%M:%S'))
