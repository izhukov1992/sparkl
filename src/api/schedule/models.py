# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from datetime import timedelta

from django.conf import settings
from django.db import models
from django.utils import dateformat
from django.utils.translation import ugettext_lazy as _

from lib.utils import (
    align_intervals,
    # closest_interval,
    has_same_date,
    is_rush_hour,
    multi_getattr,
    standardize_dt_seconds
)

from api.addresses.models import PostalCode

from api.schedule.constants import *


class Slot(models.Model):
    slot_from = models.DateTimeField(_('From'))
    slot_until = models.DateTimeField(_('Until'))
    prev_slot = models.ForeignKey('self', blank=True, null=True,
                                  related_name='prev')
    next_slot = models.ForeignKey('self', blank=True, null=True,
                                  related_name='next')

    service_provider = models.ForeignKey('profiles.ServiceProvider')
    booking = models.ForeignKey('bookings.Booking', null=True, blank=True,
                                related_name='slots')
    itinerary = models.ForeignKey('navigation.Itinerary',
                                  null=True, blank=True)

    slot_type = models.CharField(
        _('Slot type'), choices=SLOT_TYPE_CHOICES,
        max_length=2,
        default=AVAILABLE)

    class Meta:
        ordering = ['slot_from']
        # unique_together = (('slot_from', 'service_provider'),
        #                    ('slot_until', 'service_provider'),)

    # FIXME: update available slots if slot off is created (1)

    def is_free(self):
        """Check if slot is free."""
        return self.slot_type == AVAILABLE

    def fill(self, obj_reference, slot_type=BOOKING):
        """Fill slot with respective type object (slot_type)."""
        self.slot_type = slot_type
        if self.slot_type == BOOKING:
            self.booking = obj_reference
        elif self.slot_type == TRAVEL:
            self.itinerary = obj_reference
        self.save()

    # def free_up(self):
    #     self.taken = False
    #     self.save()

    def get_slot_address(self):
        """Return addres related to slot."""
        if self.slot_type == BOOKING:
            return self.booking.address
        elif self.slot_type == TRAVEL:
            return self.itinerary.departure_address
        elif (self.is_free() and
              getattr(self, 'prev_slot', None) and
              has_same_date(self.slot_from, self.prev_slot.slot_from)):
                return self.prev_slot.get_slot_address()
        else:
            # if OFF or no other slots for that date, we assume SP is home
            return self.service_provider.address

    def get_slot_duration(self):
        """Return total slot duration in seconds."""
        return (self.slot_until - self.slot_from).seconds

    def save(self, *args, **kwargs):
        self.slot_from = standardize_dt_seconds(self.slot_from)
        self.slot_until = standardize_dt_seconds(self.slot_until)
        super(Slot, self).save(*args, **kwargs)

    def __str__(self):
        return "{0} - {1} {2}".format(self.slot_from,
                                      self.slot_until,
                                      self.service_provider)

    def __unicode__(self):
        return u"%s" % self.__str__()


class BookingSuggestionTimes(models.Model):
    """Quickfix: admin action only."""
    suggestion_slot = models.ForeignKey('BookingEnquirySuggestionSlots')
    datetime_choice = models.DateTimeField(_('Time Choice'))

    class Meta:
        ordering = ['datetime_choice']

    def __str__(self):
        return "{}".format(self.datetime_choice)

    def __unicode__(self):
        return u"%s" % self.__str__()


class BookingEnquirySuggestionSlots(models.Model):
    """Quickfix: admin action only."""
    booking_enquiry = models.ForeignKey('bookings.BookingEnquiry')
    possible_slots = models.ManyToManyField('Slot')
    chosen_datetime = models.ForeignKey('BookingSuggestionTimes',
                                        help_text=_(
                                            "New datetime choice for booking"),
                                        null=True, blank=True)

    class Meta:
        ordering = ['booking_enquiry__created_at']
        verbose_name_plural = "Booking Enquiry Suggestion Slots"

    def get_chosen_datetime(self):
        return multi_getattr(self, 'chosen_datetime.datetime_choice', None)

    def get_possible_starts(self):
        possible_starts = set()
        for slot in self.possible_slots.all():
            ending_slot = slot.slot_until - timedelta(
                seconds=self.booking_enquiry.booking.get_services_duration())
            # starting_slot = closest_interval(slot.slot_start)
            possible_starts.update(
                align_intervals(slot.slot_from, ending_slot))

        return list(possible_starts)

    def set_start(self):
        if not self.chosen_datetime:
            # if not all(map(lambda d: has_same_date(
                # self.booking_enquiry.preferred_datetime,
                # possible_date))
            self.chosen_datetime = nearest(
                self.get_possible_starts(),
                self.booking_enquiry.datetime)
            self.save()

    def generate_datetime_proposals(self):
        for dt in self.get_possible_starts():
            BookingSuggestionTimes.objects.create(
                suggestion_slot=self,
                datetime_choice=standardize_dt_seconds(dt))

    def __str__(self):
        return "#{} {} | {} ({})".format(
            self.booking_enquiry.id,
            self.booking_enquiry.customer,
            dateformat.format(self.booking_enquiry.datetime,
                              'j F Y, H:i'),
            self.booking_enquiry.booking.get_status_display())

    def __unicode__(self):
        return u"%s" % self.__str__()


class OrderSuggestionSlots(models.Model):
    """Possible slots calculated for a given order."""
    order = models.ForeignKey('business.Order',
                              related_name='suggestion_slots')
    possible_slots = models.ManyToManyField(
        'Slot',
        related_name='order_possible_slots')
    possible_itineraries = models.ManyToManyField(
        'navigation.Itinerary',
        related_name='order_possible_itineraries')
    stat_copied_itineraries = models.PositiveIntegerField(
        _('Copied itineraries count'),
        null=True, blank=True, default=0)
    stat_calculated_itineraries = models.PositiveIntegerField(
        _('Calculated itineraries count'),
        null=True, blank=True, default=0)

    # before alocating a booking:
    # get the slot that is being split and alocated
    # possible_slots = slot.order_possible_slots__set.all()
    # order_possible_slots = OrderSuggestionSlots.objects.filter(
    #     order__closed=False,
    #     possible_slots__in=possible_slots)
    # for slot in order_possible_slots
    # update it
    # make method and endpoint for datetime is still valid
    # cross with last_updated_at and current datetime

    class Meta:
        ordering = ['order__created_at', 'pk']
        verbose_name_plural = "Order Suggestion Slots"

    def get_chosen_datetime(self):
        """Quickfix: admin action only."""
        return multi_getattr(
            self, 'order.order_booking.first().datetime', None)

    def get_related_itineraries(self):
        pass

    def get_possible_starts(self, inc_itn_delay=True, inc_min_interval=False, sp=None):
        # add calculated_transit_time if any
        possible_starts = set()
        destination_address = self.order.get_customer_address()
        if isinstance(destination_address, PostalCode):
            # assume address is PostalCode
            destination_query = {
                'destination_address__postal_code': destination_address}
        else:
            destination_query = {
                'destination_address': destination_address}
        if sp:
            possible_slots = self.possible_slots.filter(
                service_provider__pk=sp)
        else:
            possible_slots = self.possible_slots.all()

        for slot in possible_slots:
            delay_time = 0
            advance_time = self.order.get_services_duration(sp)
            slot_from = slot.slot_from
            slot_until = slot.slot_until - timedelta(seconds=advance_time)

            # TODO: missing some criteria such as max distance per SP e.g. (2)
            # do this using the itinerary ratings (create itinerary, then rate it)
            if slot.prev_slot and slot.prev_slot.slot_type == BOOKING:
                departure_address = slot.get_slot_address()
                itineraries = self.possible_itineraries.filter(
                    departure_address=departure_address,
                    **destination_query).exclude(
                    calculated_transit_time=None)
                if not itineraries:
                    from api.navigation.models import Itinerary
                    itineraries = Itinerary.objects.filter(
                        departure_address=departure_address,
                        **destination_query)

                # TODO: include itinerary time if there's a booking after (0)

                transit_time = 0
                itinerary = None

                # narrow travel method
                travel_method = slot.service_provider.travel_method
                if inc_itn_delay and itineraries.filter(
                        travel_method=travel_method):
                    itinerary = itineraries.filter(
                        travel_method=travel_method).last()
                    transit_time = itinerary.calculated_transit_time
                elif itineraries:
                    itinerary = itineraries.last()
                    transit_time = itinerary.calculated_transit_time

                if (itinerary and itinerary.calculated_transit_distance >=
                        settings.MAX_DISTANCE_BY_FOOT):
                    delay_time += transit_time

                    rush_extra = is_rush_hour(slot.slot_from)
                    delay_time += rush_extra.get('delay').get(
                        travel_method) if rush_extra else 0

                if inc_min_interval:
                    delay_time += settings.ARBITRARY_SERVICES_INTERVAL_MIN

                slot_from += timedelta(seconds=delay_time)
                possible_starts.update(align_intervals(
                    slot_from, slot_until, inc_end=False))
            else:
                possible_starts.update(align_intervals(
                    slot_from, slot_until, inc_end=False))

        return sorted(list(possible_starts))

    def get_proposals_for_date(self, date):
        """Quickfix: admin action only."""
        for dt in self.get_possible_starts():
            BookingSuggestionTimes.objects.create(
                suggestion_slot=self,
                datetime_choice=standardize_dt_seconds(dt))

    def __str__(self):
        return "#{} {} | {}".format(
            self.order.id,
            self.order.customer,
            dateformat.format(self.order.created_at,
                              'j F Y, H:i'))

    def __unicode__(self):
        return u"%s" % self.__str__()


class GoogleCalendarEvent(models.Model):
    """Google Calendar Event object."""
    slot = models.ForeignKey('Slot', related_name='google_calendar_event')
    google_calendar_id = models.CharField(_('Calendar ID'), max_length=200)
    event_id = models.CharField(_('Event ID'), max_length=1024)
    event_link = models.URLField(_('Event Link'), max_length=300)
    status = models.CharField(_('Event Status'), max_length=100)
    ical_uid = models.CharField(_('Event iCal UID'), max_length=1024)
    last_updated = models.DateTimeField(_('Last Updated'))

    def __str__(self):
        return "{0} - #{1} ({2})".format(self.slot,
                                         self.event_id,
                                         self.status)

    def __unicode__(self):
        return u"%s" % self.__str__()
