# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from django.db import models
from django.forms.widgets import RadioSelect
from django.utils.html import format_html

from lib.utils import seconds_to_pretty

from .constants import *
from .models import (
    BookingEnquirySuggestionSlots,
    BookingSuggestionTimes,
    Slot,
    GoogleCalendarEvent
)


class SlotAdmin(admin.ModelAdmin):
    list_display = ('service_provider',
                    'slot_from',
                    'slot_until',
                    'customer',
                    'destination',
                    'slot_duration',
                    'slot_type',)
    list_display_links = ('slot_from', 'service_provider')
    search_fields = ('booking', 'itinerary')
    list_filter = ('slot_from',
                   'service_provider',
                   'slot_type',)
    list_per_page = 30
    date_hierarchy = 'slot_from'
    ordering = ['service_provider', 'slot_from']

    def customer(self, obj):
        if obj.slot_type == BOOKING:
            return obj.booking.customer
        else:
            return "-"

    def slot_duration(self, obj):
        return seconds_to_pretty(obj.get_slot_duration())

    def destination(self, obj):
        if obj.slot_type == TRAVEL:
            return "%s (%s)" % (obj.itinerary.destination_address,
                                obj.itinerary.destination_address.postal_code)
        else:
            return "-"


class BookingEnquirySuggestionSlotsAdmin(admin.ModelAdmin):
    exclude = ('booking_enquiry', 'possible_slots',)
    formfield_overrides = {
        models.ForeignKey: {'widget': RadioSelect},
    }

    def render_change_form(self, request, context, *args, **kwargs):
        context['adminform'].form.fields['chosen_datetime'].queryset = \
            BookingSuggestionTimes.objects.filter(
                suggestion_slot=kwargs.get('obj'))
        return super(
            BookingEnquirySuggestionSlotsAdmin,
            self).render_change_form(request, context, *args, **kwargs)

    def save_model(self, request, obj, form, change):
        if (change and
            obj.booking_enquiry.booking.datetime !=
                obj.get_chosen_datetime()):
            obj.booking_enquiry.booking.datetime = obj.get_chosen_datetime()
            obj.booking_enquiry.booking.save()
            obj.booking_enquiry.booking.recalculate_itineraries()
            if obj.booking_enquiry.booking.bookingitinerary_set.first().best_itinerary:
                obj.booking_enquiry.booking.action_lock_calendar()
        # TODO: predict booking (status==enquiry) datetime intersection (2)
        super(BookingEnquirySuggestionSlotsAdmin, self).save_model(
            request, obj, form, change)


class BookingSuggestionTimesAdmin(admin.ModelAdmin):
    pass


class GoogleCalendarEventAdmin(admin.ModelAdmin):
    list_display = ('slot_from',
                    'url',
                    'last_updated',
                    'status',)
    search_fields = ('slot',)
    list_filter = ('status',
                   'last_updated',)
    list_per_page = 40

    def slot_from(self, obj):
        return obj.slot.slot_from

    def url(self, obj):
        template = u'<a href="{url}" target="_blank">{text}</a>'
        return format_html(template, url=obj.event_link, text="link")

    url.allow_tags = True
    url.short_description = 'Link to event'


admin.site.register(BookingEnquirySuggestionSlots,
                    BookingEnquirySuggestionSlotsAdmin)
admin.site.register(BookingSuggestionTimes, BookingSuggestionTimesAdmin)
admin.site.register(GoogleCalendarEvent, GoogleCalendarEventAdmin)
admin.site.register(Slot, SlotAdmin)
