# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2018-05-03 18:52
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('schedule', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='ordersuggestionslots',
            options={'ordering': ['order__created_at', 'pk'], 'verbose_name_plural': 'Order Suggestion Slots'},
        ),
    ]
