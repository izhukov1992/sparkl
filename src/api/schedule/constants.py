from api.bookings import constants as booking_constants


AVAILABLE = 'A'
BOOKING = 'B'
OFF = 'O'
TRAVEL = 'T'

SLOT_TYPE_CHOICES = (
    (AVAILABLE, 'available'),
    (BOOKING, 'booking'),
    (OFF, 'off'),
    (TRAVEL, 'travel')
)

GCALENDAR_PENDING = 'tentative'
GCALENDAR_CONFIRMED = 'confirmed'
GCALENDAR_CANCELLED = 'cancelled'

GOOGLE_CALENDAR_EVENT_STATUS = (
    (booking_constants.ACCEPTED, GCALENDAR_CONFIRMED),
    (AVAILABLE, GCALENDAR_CONFIRMED),
    (OFF, GCALENDAR_CONFIRMED),
    (booking_constants.READY, GCALENDAR_PENDING),
    (booking_constants.PENDING, GCALENDAR_PENDING),
    (booking_constants.DECLINED, GCALENDAR_CANCELLED),
    (booking_constants.CANCELED, GCALENDAR_CANCELLED),
    (booking_constants.EXPIRED, GCALENDAR_CANCELLED)
)
