# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render


# def category_list(request):
#     # latest_question_list = Question.objects.order_by('-pub_date')[:5]
#     ct = request.COOKIES.get('ct')
#     order = order_from_token(ct)

#     context = {'services': Service.objects.get_services()}

#     response = render(request, 'services/category_list.html', context)

#     if not ct:
#         response.set_cookie('ct', order.customer.get_token())

#     return response


# def choose_slot(request):
#     ct = request.COOKIES.get('ct')
#     order = order_from_token(ct)

#     pre_calculate_slots(order)

#     order_slots = order.suggestion_slots.last()

#     context = {'slots': order_slots.get_possible_starts()}

#     response = render(request, 'schedule/choose_datetime.html', context)

#     return response


# def filter_datetime(request):
#     ct = request.COOKIES.get('ct')
#     order = order_from_token(ct)

#     q_from = request.query_params.get('q_from')
#     q_until = request.query_params.get('q_until')
#     sp = request.query_params.get('sp')

#     if request.query_params.get('date'):
#         d = map(lambda p: int(p),
#                 request.query_params.get('date').split('-'))
#         try:
#             q_from = datetime(d[0], d[1], d[2], 0, 0, 0)
#             q_until = datetime(d[0], d[1], d[2], 23, 59, 59)
#         except ValueError:
#             # ValueError: day is out of range for month
#             pass

#     pre_calculate_slots(
#         order, q_from=q_from, q_until=q_until, sp=sp)

#     order_slots = order.suggestion_slots.last()
#     # PossibleSlotsSerializer()
#     starts = order_slots.get_possible_starts(sp=sp)
#     # for key, group in itertools.groupby(dates, key=lambda x: x[1][:11]):
#     #     data

#     context = {'slots': starts}

#     response = render(request, 'schedule/choose_datetime.html', context)

#     return response


# def alocate_slot(request):
#     ct = request.COOKIES.get('ct')
#     order = order_from_token(ct)

#     dt = datetime.strptime(request.POST.get('datetime'),
#                            '%Y-%m-%d %H:%M')

#     address = order.customer.get_address()
#     if not isinstance(address, Address):
#         address = Address.objects.filter(
#             postal_code=address).last()

#     BookingEnquiry.objects.create(
#         preferred_datetime=dt,
#         customer=order.customer,
#         order=order,
#         address=address
#     )

#     address = Address.objects.filter(
#         postal_code=order.customer.postal_code)
#     numbers = [a.number for a in address]

#     context = {
#         'customer': order.customer,
#         'order': order,
#         'branch': address.first().branch_name,
#         'postal_code': order.customer.postal_code,
#         'area': order.customer.postal_code.name,
#         'numbers': numbers
#     }

#     response = render(request, 'booking/booking_enquiry.html', context)

#     return response
