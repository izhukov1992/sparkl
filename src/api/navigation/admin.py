# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from .models import (
    Itinerary,
    BookingItinerary,
    ItineraryRating
)


class ItineraryAdmin(admin.ModelAdmin):
    list_display = ('service_provider',
                    'calculated_transit_distance',
                    'calculated_transit_time',
                    'vincenty_distance',
                    'departure_address',
                    'destination_address',
                    'is_active',)
    list_display_links = ('service_provider',)
    search_fields = ('departure_address', 'destination_address')
    list_filter = ('booking_itinerary__booking__datetime',
                   'travel_method',
                   'calculations_source',
                   'booking_itinerary__active',)
    list_per_page = 30
    date_hierarchy = 'booking_itinerary__booking__datetime'

    def active(self, obj):
        return obj.booking_itinerary.active


class ItineraryRatingAdmin(admin.ModelAdmin):
    list_display = ('itinerary',
                    'service_provider',
                    'overall',
                    'services',
                    'max_range',
                    'availability',
                    'km_distance',
                    'time_distance',
                    'customer_pairing',
                    'is_active',)
    # date_hierarchy = 'itinerary__booking__datetime'

    def service_provider(self, obj):
        return obj.itinerary.service_provider

    # def datetime(self, obj):
    #     return obj.itinerary.booking.datetime


class BookingItineraryAdmin(admin.ModelAdmin):
    list_display = ('booking',
                    'best_itinerary',
                    'chosen_itinerary',
                    'active',)
    list_display_links = ('booking',)
    search_fields = ('booking__address',)
    list_filter = ('booking__datetime',
                   'active')
    list_per_page = 30
    date_hierarchy = 'booking__datetime'


admin.site.register(Itinerary, ItineraryAdmin)
admin.site.register(ItineraryRating, ItineraryRatingAdmin)
admin.site.register(BookingItinerary, BookingItineraryAdmin)
