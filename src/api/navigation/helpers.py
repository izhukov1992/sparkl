# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from geopy.distance import vincenty
import requests
from time import sleep

from django.conf import settings
#  (
#     GMAPS_KEY,
#     GMAPS_GEOCODE_URL,
#     GMAPS_DISTANCE_URL,
#     TIME_BETWEEN_REQUESTS
# )

RESPECT_RATIO = settings.TIME_BETWEEN_REQUESTS


def calc_vincenty(coords_from, coord_to):
    distance = vincenty(coords_from, coord_to)

    return distance.meters


def ask_gmaps(url, params):
    resp = requests.get(url, params=params)
    sleep(RESPECT_RATIO)

    return resp.json()


def massage_coords_from_gmaps(resp):
    coords = resp.get('results')[0].get('geometry').get('location')

    return (coords.get('lat'), coords.get('lng'))


def massage_itinerary_from_gmaps(resp):
    """return distance (meters), duration (seconds)"""
    # TODO: handle response status (2)
    # https://google-developers.appspot.com/maps/documentation/distance-matrix/intro#StatusCodes
    if resp.get('status') == 'OK':
        r = []
        for row in resp.get('rows'):
            el = row.get('elements')[0]
            if el.get('status') == 'OK':
                r.append((el.get('distance').get('value'),
                          el.get('duration').get('value')))
            else:
                # log(r.get('status'))
                r.append((None, None))
        return r
    else:
        # log(resp.get('error_message'))
        return 'retry'


def calc_itinerary(address_from, address_to, mode='transit'):
    # TODO: arrival_time or departure_time per booking (1)
    if len(address_from) > 1:
        origins = "|".join(["{},{}".format(*t) for t in address_from])
    else:
        origins = "{},{}".format(*address_from[0])

    request = ask_gmaps(settings.GMAPS_DISTANCE_URL,
                        {'key': settings.GMAPS_KEY,
                         'units': 'metric',
                         'mode': mode,
                         'origins': origins,
                         'destinations': "{},{}".format(*address_to)})

    result = massage_itinerary_from_gmaps(request)

    if result == 'retry':
        # time between requests increases 25% after each retry
        # RESPECT_RATIO *= 1.25
        # https://google-developers.appspot.com/maps/documentation/distance-matrix/web-service-best-practices
        # FIXME: defined in enclosing scope, not doing shit (5)
        RESPECT_RATIO *= 2
        print("Waiting {} seconds before retrying.".format(RESPECT_RATIO))
        return calc_itinerary(address_from, address_to, mode)
    else:
        return result


def find_coordinates(address):
    gmaps_coord = ask_gmaps(settings.GMAPS_GEOCODE_URL,
                            {'key': settings.GMAPS_KEY,
                             'address': address})
    coordinates = massage_coords_from_gmaps(gmaps_coord)
    return coordinates


def closest_service_provider(self, service_providers):
    return min(service_providers, key=service_providers.get)
