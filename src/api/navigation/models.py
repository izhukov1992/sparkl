# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from collections import OrderedDict
from datetime import datetime, timedelta
from time import gmtime, strftime

from django.conf import settings
from django.db import models
from django.db.models import Max, Min
from django.db.models.signals import pre_save
from django.utils.translation import ugettext_lazy as _

from lib.utils import multi_getattr

from api.profiles.constants import (
    ACTIVE as SP_ACTIVE,
    TRAVEL_CHOICES
)
from api.profiles.models import ServiceProvider

from api.navigation.helpers import *


def pre_calculate_distances(order_slots):
    """Pre calculates distances for itineraries for given order slots."""
    itineraries = order_slots.possible_itineraries.filter(
        calculated_transit_time=None).order_by('pk')
    pre_calcs = OrderedDict.fromkeys([i.get_travel_method_display() for
                                      i in itineraries], {})
    for i in itineraries:
        pre_calcs[i.get_travel_method_display()].update(
            {i.service_provider: i})

    dist_calcs = []
    for travel_mode in pre_calcs.keys():
        dist_calcs.extend(zip(pre_calcs[travel_mode].keys(),
                              calc_itinerary(
            [i.get_departure_coords() for i
             in pre_calcs[travel_mode].values()],
            i.get_destination_coords(), travel_mode)))

    for it, (sp, calcs) in zip(itineraries, dict(dist_calcs).iteritems()):
        if it.service_provider != sp:
            # FIXME: test itinerary query (0)
            import ipdb; ipdb.set_trace()
        it.calculated_transit_distance = calcs[0]
        it.calculated_transit_time = calcs[1]
        it.calculated_distance_source = 'google_maps'
        it.dest_address_based_on_guess = False
        it.save()


class ItineraryManager(models.Manager):
    """Created manager to override bulk_create() to calculate service
    provider."""
    def bulk_create(self, itineraries, batch_size=None):
        for itinerary in itineraries:
            itinerary.fill_default_fields()
        super(ItineraryManager, self).bulk_create(itineraries)


class Itinerary(models.Model):
    booking_itinerary = models.ForeignKey('BookingItinerary',
                                          null=True, blank=True)
    vincenty_distance = models.FloatField(
        _('Vincenty distance'), help_text=_('In meters'),
        null=True, blank=True)
    calculated_transit_distance = models.PositiveIntegerField(
        _('Calculated transit distance'), help_text=_('In meters'),
        null=True, blank=True)
    calculated_transit_time = models.PositiveIntegerField(
        _('Estimated transit time'), help_text=_('In seconds'),
        null=True, blank=True)
    calculations_source = models.CharField(
        _('Calculations source'), max_length=20, default='google_maps')
    travel_method = models.CharField(_('Travel mode'), choices=TRAVEL_CHOICES,
                                     max_length=2)

    departure_address = models.ForeignKey('addresses.Address',
                                          related_name='departure')
    destination_address = models.ForeignKey('addresses.Address',
                                            related_name='destination')
    dest_address_based_on_guess = models.BooleanField(
        _('Destination address based on guessing'),
        help_text='True when address is based on postal-code alone',
        default=False)

    service_provider = models.ForeignKey('profiles.ServiceProvider',
                                         null=True, blank=True)

    objects = ItineraryManager()

    class Meta:
        verbose_name_plural = "Itineraries"
        ordering = ['-id']

    def is_active(self):
        return getattr(self.booking_itinerary, 'active', None)
    is_active.boolean = True

    def get_departure_coords(self):
        """Return departure address coordinates: (lat, lng)."""
        return (self.departure_address.latitude,
                self.departure_address.longitude)

    def get_destination_coords(self):
        """Return destination address coordinates: (lat, lng)."""
        return (self.destination_address.latitude,
                self.destination_address.longitude)

    def fill_coordinates(self):
        """Fill departure and destination address coordinates."""
        self.departure_address.fill_coordinates()
        self.destination_address.fill_coordinates()

    def calculate_fields(self):
        """Calculate required fields:
            * Departure and destination address coordinates
            * Vincenty distance
        """
        # TODO: handle exceptions (2)
        self.fill_coordinates()
        self.vincenty_distance = calc_vincenty(
            self.get_departure_coords(), self.get_destination_coords())

    def has_calculated_fields(self):
        """Check if the dinstance and time are alredy calculated."""
        return all(self.calculated_transit_distance,
                   self.calculated_transit_time)

    def calculated_transit_distance_pretty(self):
        """Print transit distance in pretty format."""
        if self.calculated_transit_distance:
            return "{0:.2f} km".format(self.calculated_transit_distance / 1000)
        else:
            return ""

    def calculated_transit_time_pretty(self):
        """Print transit time in pretty format."""
        if self.calculated_transit_time:
            return strftime("%M min", gmtime(self.calculated_transit_time))
        else:
            return ""

    def fill_default_fields(self):
        """Fill travel method and departure address."""
        if not self.travel_method:
            self.travel_method = self.service_provider.travel_method
        if not hasattr(self, 'departure_address'):
            self.departure_address = self.service_provider.get_last_address(
                self.booking_itinerary.booking.datetime)
        self.calculate_fields()

    def __str__(self):
        return "[{0} -> {1}] ({2} / {3})".format(
            self.departure_address,
            self.destination_address,
            self.calculated_transit_time_pretty(),
            self.calculated_transit_distance_pretty())

    def __unicode__(self):
        return u"%s" % self.__str__()


def fill_default_fields_signal(sender, instance, *args, **kwargs):
    if not instance.pk:
        instance.fill_default_fields()


pre_save.connect(fill_default_fields_signal, sender=Itinerary)


class BookingItinerary(models.Model):
    best_itinerary = models.ForeignKey(
        'Itinerary', related_name='best', null=True, blank=True)
    chosen_itinerary = models.ForeignKey(
        'Itinerary', related_name='chosen', null=True, blank=True)
    alternative_itineraries = models.ManyToManyField(
        'Itinerary', related_name='alternatives')
    booking = models.ForeignKey('bookings.Booking')
    active = models.BooleanField(_('Active'), default=True)

    class Meta:
        verbose_name_plural = "Booking Itineraries"
        ordering = ['-id']

    def get_itineraries(self):
        """Return itineraries related to this booking."""
        return Itinerary.objects.filter(booking_itinerary=self)

    def get_departure_addresses(self):
        """Return itineraryies' departure addresses."""
        return [itinerary.departure_address for
                itinerary in self.get_itineraries()]

    def get_departure_coords(self):
        """Return itinerary departures' coordinates."""
        return [i.get_departure_coords() for
                i in self.get_itineraries()]

    def get_destination_address(self):
        """Return itinerary destination address."""
        return self.get_itineraries()[0].destination_address or \
            self.booking.address

    def get_destination_coords(self):
        """Return itinerary destination coordinates."""
        return self.get_itineraries()[0].get_destination_coords() or \
            self.booking.address.get_coordinates()

    def get_longest_time(self):
        """Return the longest time calculated given all the itineraries."""
        return self.get_itineraries().aggregate(
            Max('calculated_transit_time')).get(
            'calculated_transit_time__max')

    def get_shortest_time(self):
        """Return the shortest time calculated given all the itineraries."""
        return self.get_itineraries().aggregate(
            Min('calculated_transit_time')).get(
            'calculated_transit_time__min')

    def get_longest_distance(self):
        """Return the longest distance calculated given all the itineraries."""
        return self.get_itineraries().aggregate(
            Max('calculated_transit_distance')).get(
            'calculated_transit_distance__max')

    def get_shortest_distance(self):
        """Return the longest distance calculated given all the itineraries."""
        return self.get_itineraries().aggregate(
            Min('calculated_transit_distance')).get(
            'calculated_transit_distance__max')

    def get_largest_slot_gap(self):
        """Ignored criteria."""
        return (self.get_longest_time() +
                self.booking.get_services_duration())

    def get_smallest_slot_gap(self):
        """Ignored criteria."""
        return (self.get_shortest_time() +
                self.booking.get_services_duration())

    def calculate_distances(self, itineraries, mode='default', source='google_maps'):
        pre_calcs = OrderedDict.fromkeys([i.get_travel_method_display() for
                                          i in itineraries], {})
        for i in itineraries:
            pre_calcs[i.get_travel_method_display()].update(
                {i.service_provider: i})

        if source == 'google_maps':
            # check default mode method
            dist_calcs = []
            for travel_mode in pre_calcs.keys():
                t = travel_mode if mode == 'default' else mode
                dist_calcs.extend(zip(pre_calcs[travel_mode].keys(),
                                      calc_itinerary(
                    [i.get_departure_coords() for i
                     in pre_calcs[travel_mode].values()],
                    self.get_destination_coords(), t)))

            for sp, calcs in dict(dist_calcs).iteritems():
                it = itineraries.get(service_provider=sp.pk)
                it.calculated_transit_distance = calcs[0]
                it.calculated_transit_time = calcs[1]
                it.calculated_distance_source = source
                it.save()

        # it_attrs = []
        # for s in candidates:
        #     it_attrs.append(Itinerary(booking_itinerary=self,
        #                               destination_address=self.booking.address,
        #                               service_provider=s))
        #     next_booking = s.next_booking(self.booking.datetime)
        #     if bool(next_booking):
        #         it_attrs.append(Itinerary(
        #             booking_itinerary=self,
        #             destination_address=next_booking,
        #             service_provider=s))

        # it_attrs = [Itinerary(booking_itinerary=self,
        #                       destination_address=self.booking.address,
        #                       service_provider=s) for s in candidates]
        # # TODO: check return keys from bulk_create to save up queries (2)
        # Itinerary.objects.bulk_create(it_attrs)
        # itineraries = self.get_itineraries()

        # self.calculate_distances(itineraries)

    # TODO: passar isto para o Booking (5)
    def calculate_service_provider(self, candidates=None):
        booking_from = self.booking.datetime - timedelta(
            seconds=settings.ARBITRARY_SERVICES_INTERVAL_MIN)
        booking_until = self.booking.datetime + timedelta(
            seconds=self.booking.get_services_duration() +
            settings.ARBITRARY_SERVICE_EXTRA_TIME)

        if not candidates:
            candidates = filter(None, map(
                lambda s: s if s.is_available_between(
                    booking_from, booking_until) else None,
                ServiceProvider.objects.filter(status=SP_ACTIVE)))

        # it_attrs = []
        # for s in candidates:
        #     it_attrs.append(Itinerary(booking_itinerary=self,
        #                               destination_address=self.booking.address,
        #                               service_provider=s))
        #     next_booking = s.next_booking(self.booking.datetime)
        #     if bool(next_booking):
        #         it_attrs.append(Itinerary(
        #             booking_itinerary=self,
        #             destination_address=next_booking,
        #             service_provider=s))

        it_attrs = [Itinerary(booking_itinerary=self,
                              destination_address=self.booking.address,
                              service_provider=s) for s in candidates]
        # TODO: check return keys from bulk_create to save up queries (2)
        Itinerary.objects.bulk_create(it_attrs)
        itineraries = self.get_itineraries()

        self.calculate_distances(itineraries)

        it_ratings = [ItineraryRating(itinerary=it) for it in itineraries]
        ItineraryRating.objects.bulk_create(it_ratings)
        longest_distance = self.get_longest_distance()
        longest_time = self.get_longest_time()

        itinerary_ratings = ItineraryRating.objects.filter(
            itinerary_id__in=itineraries)
        calculated_ratings = []
        for rating in itinerary_ratings:
            rating.pre_rate_itinerary()
            if rating.overall != 0:
                calculated_ratings.append(rating)

        for rating in calculated_ratings:
            rating.rate_km_distance(longest_distance)
            rating.rate_time_distance(longest_time)
            rating.calc_overall()

        best_itinerary = self.get_best_itinerary(calculated_ratings)

        if best_itinerary:
            self.best_itinerary = best_itinerary
            self.chosen_itinerary = best_itinerary
            self.save()
            self.alternative_itineraries.add(
                *self.get_alternative_itineraries(calculated_ratings))
            self.booking.assign_service_provider(
                self.best_itinerary.service_provider)
            # self.booking.alocate_slots()

            # incluir rating para booking seguinte, se existir
            # TODO: se necessário, recalcular itinerario seguinte (0)

            # self.booking.accept()

    def get_sorted_itineraries(self, ratings=None):
        if not ratings:
            return [rating.itinerary for rating in
                    ItineraryRating.objects.filter(
                        itinerary_id__in=self.get_itineraries()).order_by(
                        '-overall')]
        else:
            return [rating.itinerary for rating in
                    sorted(ratings, key=lambda k: k.overall, reverse=True)]

    def get_best_itinerary(self, ratings=None):
        it = self.get_sorted_itineraries(ratings)[:1]
        if it:
            return it[0]
        return None

    def get_alternative_itineraries(self, ratings=None):
        if not ratings:
            return ItineraryRating.objects.filter(
                itinerary_id__in=self.get_itineraries()).order_by(
                '-overall')[1:]
        else:
            return self.get_sorted_itineraries(ratings)[1:]

    def deprecate(self):
        self.active = False
        self.save()


# class ItineraryRatingManager(models.Manager):
#     """Created manager to override bulk_create() to calculate service
#     provider."""
#     def bulk_create(self, ratings, batch_size=None):
#         for rating in ratings:
#             rating.pre_rate_itinerary()
#         super(ItinerayRatingManager, self).bulk_create(ratings)


class ItineraryRating(models.Model):
    """Ratings from 0 to settings.ITINERARY_MAX_SCORE."""
    itinerary = models.ForeignKey('Itinerary', related_name='rating')
    overall = models.PositiveIntegerField(
        _('Overall'), null=True, blank=True)
    availability = models.PositiveIntegerField(
        _('Availability'), null=True, blank=True)
    services = models.PositiveIntegerField(
        _('Services'), null=True, blank=True)
    max_range = models.PositiveIntegerField(
        _('Maximum range'), null=True, blank=True)
    km_distance = models.PositiveIntegerField(
        _('Km distance'), null=True, blank=True)
    time_distance = models.PositiveIntegerField(
        _('Time distance'), null=True, blank=True)
    customer_pairing = models.PositiveIntegerField(
        _('Customer pairing'), null=True, blank=True)

    # objects = ItinerayRatingManager()

    class Meta:
        ordering = ['-id', '-overall']

    def is_active(self):
        return multi_getattr(self.itinerary, 'booking_itinerary.active', None)
    is_active.boolean = True

    def rate_services(self):
        if not self.itinerary.service_provider.performs_all_services(
                self.itinerary.booking_itinerary.booking.get_services()):
            self.services = 0
            self.overall = 0
        else:
            self.services = settings.ITINERARY_MAX_SCORE
        # FIXME: ver como gravar isto depois, por causa do bulk_create (5)
        # self.save()

    def rate_max_range(self):
        it = self.itinerary
        extra_details = it.service_provider.extra_details.first()
        # FIXME: calculate booking address distance from service provider home!
        prev_calculated_it = Itinerary.objects.filter(
            departure_address__postal_code=it.departure_address.postal_code,
            destination_address__postal_code=it.destination_address.postal_code,
            travel_method=it.travel_method).first()
        if prev_calculated_it:
            total_distance = prev_calculated_it.calculated_transit_distance
        else:
            total_distance = calc_vincenty(it.get_departure_coords(),
                                           it.get_destination_coords())
        if (extra_details.max_distance_home and
                total_distance > extra_details.max_distance_home):
            if it.booking_itinerary.booking.get_amount_revenue(
                service_provider=it.service_provider,
                revenue_p=extra_details.revenue_percentage) >= \
                    extra_details.min_service_profit:
                # TODO: put this in confidence level config file (5)
                self.max_range = 80
            else:
                self.max_range = 0
                self.overall = 0
        else:
            self.max_range = settings.ITINERARY_MAX_SCORE
        # FIXME: ver como gravar isto depois, por causa do bulk_create (5)
        # self.save()

    def rate_km_distance(self, longest_distance):
        try:
            self.km_distance = int(settings.ITINERARY_MAX_SCORE - (
                (self.itinerary.calculated_transit_distance *
                 settings.ITINERARY_MAX_SCORE) / longest_distance))
        except ZeroDivisionError:
            if longest_distance != 0:
                self.km_distance = int(settings.ITINERARY_MAX_SCORE - (
                    (settings.ITINERARY_MAX_SCORE) / longest_distance))
            else:
                # TODO: check this (0)
                self.km_distance = settings.ITINERARY_MAX_SCORE
        self.save()

    def rate_time_distance(self, longest_time):
        try:
            self.time_distance = int(settings.ITINERARY_MAX_SCORE - (
                (self.itinerary.calculated_transit_time *
                 settings.ITINERARY_MAX_SCORE) / longest_time))
        except ZeroDivisionError:
            if longest_time != 0:
                self.time_distance = int(settings.ITINERARY_MAX_SCORE - (
                    (settings.ITINERARY_MAX_SCORE) / longest_time))
            else:
                # TODO: check this (0)
                self.time_distance = settings.ITINERARY_MAX_SCORE
        self.save()

    def rate_availability(self, services_duration=None):
        """
        Rates service provider availability.

        Total time is the sum of services duration, transit time plus
        Ideal time is equal to services duration plus
        arbitrary services interval maximum (1h)
        """
        services_duration = services_duration or \
            self.itinerary.booking_itinerary.booking.get_services_duration()

        total_time = (services_duration +
                      settings.ARBITRARY_SERVICE_EXTRA_TIME +
                      self.itinerary.calculated_transit_time)
        start_time = self.itinerary.booking_itinerary.booking.datetime
        available_slot = self.itinerary.service_provider.get_available_slot(
            start_time)
        ideal_time = total_time + settings.ARBITRARY_SERVICES_INTERVAL_MAX
        if available_slot.get_slot_duration() <= total_time:
            self.availability = 0
            self.overall = 0
        elif available_slot.get_slot_duration() >= ideal_time:
            self.availability = 100
        else:
            self.availability = int(settings.ITINERARY_MAX_SCORE - (
                (total_time * settings.ITINERARY_MAX_SCORE) /
                ideal_time))
        self.save()

    def rate_customer_pairing(self):
        # TODO: change ITINERARY_CRITERIA_LEN (5)
        booking = self.itinerary.booking_itinerary.booking
        if booking.customer.extra_details.first().main_service_provider == \
                self.itinerary.service_provider:
            self.customer_pairing = 100
        else:
            pass
        # if rating.filter = 1, overall = 0
        # rating = Rating.objects.filter(
        #     customer=booking.customer,
        #     booking__service_provider=self.itinerary.service_provider)
        # get rating avg from 1 to 5 * 10, if not rating then assume 5
        # customer level - isto é inútil na cena global
        customer_level = booking.customer.level * 10
        # novas clientes com novas profissionais?

    def rate_service_distribution(self):
        # calculates amount of services or revenue compared to other providers
        pass

    def calc_overall(self):
        if not self.overall:
            self.overall = (self.services +
                            self.max_range +
                            self.km_distance +
                            self.time_distance +
                            self.availability)
        self.overall = int(self.overall *
                           settings.ITINERARY_MAX_SCORE /
                           settings.ITINERARY_SCALE)
        self.save()
        return self.overall

    def pre_rate_itinerary(self):
        for f in ['rate_services',
                  'rate_max_range',
                  'rate_availability']:
            # rate_customer_pairing
            # rate_service_distribution
            getattr(self, f)()


# def pre_rate_itinerary_signal(sender, instance, *args, **kwargs):
#     instance.pre_rate_itinerary()


# post_save.connect(pre_rate_itinerary_signal, sender=ItineraryRating)

class DailyQuota(models.Model):
    """."""
    service_provider = models.ForeignKey('profiles.ServiceProvider')
    starting_address = models.ForeignKey('addresses.Address')
    updated_at = models.DateTimeField(_('Updated at'), auto_now=True)
    max_range = models.PositiveIntegerField(
        _('Maximum distance'), help_text=_("In meters"), default=50000)
    total_distance = models.PositiveIntegerField(
        _('Total distance so far'),
        help_text=_("In meters"),
        default=0)

    def update_last_distance(self, distance=None):
        if not distance:
            last_check_in = CheckIn.objects.filter(
                service_provider=self.service_provider,
                timestamp__gt=self.updated_at)
            distance = last_check_in.get_distance()
        self.total_distance += distance
        self.save()


class CheckIn(models.Model):
    """Ratings from 0 to settings.ITINERARY_MAX_SCORE."""
    service_provider = models.ForeignKey('profiles.ServiceProvider')
    address = models.ForeignKey('addresses.Address',
                                null=True, blank=True)
    latitude = models.DecimalField(_('Latitude'), max_digits=9,
                                   decimal_places=6, null=True,
                                   blank=True)
    longitude = models.DecimalField(_('Longitude'), max_digits=9,
                                    decimal_places=6, null=True,
                                    blank=True)
    timestamp = models.DateTimeField(_('Timestamp'),
                                     auto_now_add=True)

    def get_address(self):
        pass
        # if not self.address:
        #     self.address = 
        # return self.address

    def get_distance(self):
        pass
