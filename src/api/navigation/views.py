# from django.db.models import Q, Avg
from django.shortcuts import get_object_or_404
from rest_framework import status, viewsets
# from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
# from rest_framework_jwt.authentication import JSONWebTokenAuthentication

from models import Itinerary
from serializers import (
    ItinerarySerializer
)


class ItineraryView(viewsets.ViewSet):
    """Itinerary view."""
    # authentication_classes = (JSONWebTokenAuthentication,)
    # permission_classes = (IsAuthenticated,)

    def list(self, request):
        """List of customers."""
        itineraries = Itinerary.objects.all()

        serializer = ItinerarySerializer(
            itineraries, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def retrieve(self, request, pk):
        """Retrieve the customer."""
        itinerary = get_object_or_404(Itinerary, pk=pk)
        serializer = ItinerarySerializer(itinerary)
        return Response(serializer.data)

    # def create(self, request):
    #     serializer = CustomerSerializer(data=request.data)
    #     serializer.is_valid(raise_exception=True)
    #     serializer.save()
    #     return Response(serializer.data, status=status.HTTP_201_CREATED)

    def calculate(self, request):
        pass
