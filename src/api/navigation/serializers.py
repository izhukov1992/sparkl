from rest_framework import serializers

# from lib.utils import (
#     validate_email as email_is_valid,
#     calculate_age
# )
# from lib.validate_pt import controlNIF
# from accounts.models import User
# from lib.utils import random_password_generator
from api.profiles.serializers import ServiceProviderPublicSerializer
from api.profiles.models import (
    ServiceProvider
)
from models import (
    Itinerary
)


class ItinerarySerializer(serializers.ModelSerializer):
    class Meta:
        model = Itinerary
        fields = ('id',
                  '',
                  '')

    def create(self, validated_data):  # pragma: no cover
        """Create a customer instance."""
        pass

    def update(self, instance, validated_data):
        """Update a customer instance."""
        pass

    def to_representation(self, instance):
        """Representation of a customer."""
        return {
            'id': instance.id,
            '': instance.has_children,
        }
