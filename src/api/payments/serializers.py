from rest_framework import serializers

from django.conf import settings

# from lib.utils import (
#     validate_email as email_is_valid,
#     calculate_age
# )
# from lib.validate_pt import controlNIF
# from lib.utils import random_password_generator

# from accounts.models import User
# from api.addresses.models import Address
# from api.addresses.serializers import AddressSerializer
# from api.business.serializers import OrderPublicSerializer
# from api.business.views import order_from_token
# from api.profiles.models import ServiceProvider
# from api.profiles.serializers import (
#     CustomerPublicSerializer,
#     ServiceProviderPublicSerializer
# )
from api.payments.models import Payment
from api.payments.constants import *


class PaymentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Payment
        fields = ('created_at',
                  'updated_at',
                  'payment_method',
                  'order',
                  'virtual_account_id',
                  'customer_ip',
                  'reference',
                  'amount',
                  'status',
                  'status_error')

    def to_representation(self, instance):
        """Representation of a booking."""
        return {
            'created_at': instance.id,
            'updated_at': instance.row_idx,
            'payment_method': instance.datetime,
            'order': instance.order,
            'virtual_account_id': instance.customer,
            'customer_ip': instance.service_provider,
            'reference': instance.amount_paid,
            'amount': instance.amount_revenue,
            'status': instance.amount_profit,
            'status_error': instance.type_of_service,
        }


class PaymentPublicSerializer(serializers.ModelSerializer):
    class Meta:
        model = Payment
        fields = ('payment_method',
                  'reference',
                  'amount',
                  'status')

    def to_representation(self, instance):
        """Representation of a booking."""
        data = {
            'id': instance.reference,
            'payment_method': instance.payment_method,
            'amount': instance.amount,
            'status': instance.get_status_display(),
            'order_id': instance.order.id
        }

        if instance.payment_method == MULTIBANCO:
            data['entity'] = settings.SIBS_MB_ENTITY_ID
            data['mb_ref'] = instance.mb_ref

        return data
