from django.conf.urls import url

from api.payments.views import *


urlpatterns = [
    # url(r'^webhook/(?P<reference>.*)$',
    url(r'^webhook/$',
        PaymentView.as_view({'post': 'webhook'}),
        name='payment_webhook'),
    url(r'^mbway_request/$',
        PaymentView.as_view({'post': 'mbway_request'}),
        name='mbway_request'),
    url(r'^get_fake_checkout_id/$',
        PaymentView.as_view({'post': 'get_fake_checkout_id'}),
        name='get_fake_checkout_id'),
]
