# DIRECT_DEBIT = 'DDB'
# MASTERCARD = 'CCM'
MBWAY = 'MBWAY'
MULTIBANCO = 'SIBS_MULTIBANCO'
VISA = 'VISA'

PAYMENT_METHODS = (
    (MBWAY, 'MBWAY'),
    (MULTIBANCO, 'Multibanco'),
    (VISA, 'Visa'))
    # (MASTERCARD, 'Mastercard')
    # (DIRECT_DEBIT, 'Direct Debit'))

PAYMENT_TYPE_OPTIONS = {
    MULTIBANCO: 'PA',
    MBWAY: 'DB',
    VISA: 'PA'
}

# preparation to check if needs to get any details before performing payment
# for server-2-server method
PAYMENT_TYPE_PREPARE = {
    MULTIBANCO: True,
    # MBWAY: False,
    MBWAY: True,
    VISA: True
}

SUCCESS = 'S'
SUCCESS_FLAGGED = 'SF'
PENDING = 'P'
PENDING_FLAGGED = 'PF'
REJECTED = 'R'
CHARGEBACK = 'C'
FAILED = 'F'

PAYMENT_STATUS_CHOICES = (
    (SUCCESS, 'Success'),
    (SUCCESS_FLAGGED, 'Success flagged'),
    (PENDING, 'Pending'),
    (PENDING_FLAGGED, 'Pending flagged'),
    (REJECTED, 'Rejected'),
    (CHARGEBACK, 'Chargeback'),
    (FAILED, 'Failed request'))

PAYMENT_OPT = 'P'
REGISTRATION_OPT = 'R'

NOTIFICATION_TYPE_OPTIONS = (
    (PAYMENT_OPT, 'PAYMENT'),
    (REGISTRATION_OPT, 'REGISTRATION'))

NOTIFICATION_TYPE_DICT = {
    'PAYMENT': PAYMENT_OPT,
    'REGISTRATION': REGISTRATION_OPT
}

ACTION_NONE = 'N'
ACTION_CREATED = 'C'
ACTION_UPDATED = 'U'
ACTION_DELETED = 'D'

ACTION_OPTIONS = (
    (ACTION_NONE, 'None'),
    (ACTION_CREATED, 'Created'),
    (ACTION_UPDATED, 'Updated'),
    (ACTION_DELETED, 'Deleted'))
