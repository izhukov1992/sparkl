# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import binascii
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.ciphers import (
    Cipher,
    algorithms,
    modes
)
from datetime import (
    datetime,
    timedelta
)
import json
import re
import requests

from django.conf import settings
from django.contrib.postgres.fields import JSONField
from django.db import models
from django.utils.translation import ugettext_lazy as _

from api.payments.constants import *


class Payment(models.Model):
    created_at = models.DateTimeField(_('Created at'), auto_now_add=True)
    updated_at = models.DateTimeField(_('Updated at'), auto_now=True)
    payment_method = models.CharField(_('Payment Method'), max_length=25,
                                      choices=PAYMENT_METHODS,
                                      default=MBWAY)
    order = models.ForeignKey('business.Order', related_name='payments')
    virtual_account_id = models.CharField(
        _('MBWAY virtual account id'), help_text=_(
            "Mobile number is preferred."), max_length=100,
        null=True, blank=True)
    customer_ip = models.CharField(_('Customer IP address'), max_length=100,
                                   null=True, blank=True)
    reference = models.CharField(_('reference'), max_length=100,
                                 null=True, blank=True)
    mb_ref = models.CharField(_('MULTIBANCO reference'), max_length=20,
                              null=True, blank=True)
    amount = models.DecimalField(_('Price'), max_digits=7, decimal_places=2)
    status = models.CharField(_('Status'), max_length=2,
                              choices=PAYMENT_STATUS_CHOICES,
                              default=PENDING)
    status_error = models.CharField(_('Status error'), max_length=300,
                                    null=True, blank=True)

    class Meta:
        ordering = ['updated_at']

    def get_expiration_details(self):
        # if self.order.order_booking.exists():
        #     exp_dt = self.order.order_booking.first().expiration_datetime
        # FIXME: remove this, for test purposes only (0)
        exp_dt = datetime.now() + timedelta(seconds=3600 * 4)
        return exp_dt.strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3] + "+00:00"

    def get_auth_details(self):
        return {
            'authentication.userId': settings.SIBS_AUTH_USER_ID,
            'authentication.entityId': settings.SIBS_AUTH_ENTITY_ID,
            'authentication.password': settings.SIBS_AUTH_PASSWORD,
        }

    def get_common_details(self):
        return {
            'notificationUrl': settings.SIBS_WEBHOOK,
            'currency': settings.SIBS_CURRENCY,
            'paymentType': PAYMENT_TYPE_OPTIONS.get(self.payment_method),
            'amount': "{}".format(self.amount),
            'testMode': "EXTERNAL",
            'customParameters[SIBS_ENV]': "QLY"
        }

    def get_mbway_details(self):
        # https://www.mbway.pt/
        # perguntas/consultas/e-possivel-aceitar-pagamentos-internacionais-com-mb-way/
        # v_id = self.virtual_account_id or self.order.customer.mobile_number

        return {
            'paymentBrand': self.payment_method,
            'merchantTransactionId':
                settings.SIBS_MBWAY_MERCHANT_TRANSACTION_ID,
            # assume it's always a mobile number
            # 'virtualAccount.accountId': "351#{}".format(v_id[-9:])
        }

    def get_multibanco_details(self):
        return {
            'paymentBrand': self.payment_method,
            # 'customer.ip': self.customer_ip,
            # 'customer.surname': self.order.customer.first_name,
            # 'customer.givenName': self.order.customer.last_name,
            'billing.country': settings.BILLING_COUNTRY,
            'customParameters[SIBSMULTIBANCO_PtmntEntty]':
                settings.SIBS_MB_ENTITY_ID,
            'customParameters[SIBSMULTIBANCO_RefIntlDtTm]':
                datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3] + "+00:00",
            'customParameters[SIBSMULTIBANCO_RefLmtDtTm]':
                self.get_expiration_details()
        }

    def get_visa_details(self):
        return {
            'merchantTransactionId': settings.SIBS_VISA_MERCHANT_TRANSACTION_ID
        }

    def get_details(self):
        extra_details_func = {
            MBWAY: 'get_mbway_details',
            MULTIBANCO: 'get_multibanco_details',
            VISA: 'get_visa_details'
        }

        details = self.get_auth_details()
        details.update(self.get_common_details())

        details.update(getattr(
            self, extra_details_func.get(self.payment_method))())

        # if self.payment_method == MBWAY:
        #     details.update(self.get_mbway_details())
        # elif self.payment_method == MULTIBANCO:
        #     details.update(self.get_multibanco_details())

        return details

    def get_result_code_details(self, result_code=None):
        # TODO: test this (3)
        if not result_code:
            result_code = self.status_request.last().result_code
        result_codes = json.load(open(settings.SIBS_RESULT_CODES_FILENAME))
        code_details = filter(
            lambda result: result['code'] == result_code, result_codes)

        return code_details

    def needs_preparation(self):
        return PAYMENT_TYPE_PREPARE.get(self.payment_method)

    def get_request_url(self):
        if self.payment_method == SIBS_MULTIBANCO:
            return settings.SIBS_MULTIBANCO_URL
        else:
            return settings.SIBS_PAYMENT_URL

    def get_status_url(self):
        # if self.payment_method == VISA:
        #     return settings.SIBS_CHECKOUTID_URL.format(self.id)
        # else:
        #     return "/".join([settings.SIBS_PAYMENT_URL, self.id])
        return settings.SIBS_STATUS_URL.format(self.id)

    def create_status_request(self, response):
        status_request = PaymentStatusRequest.objects.create(
            payment=self,
            result_code=response.get('result').get('code'),
            result_description=response.get('result').get('description'),
            build_number=response.get('buildNumber'),
            timestamp=response.get('timestamp'),
            ndc=response.get('ndc'),
            payload=response)

        if response.get('paymentBrand') == SIBS_MULTIBANCO:
            self.mb_ref = response.get('resultDetails').get('pmtRef')
            self.save()

        self.assign_status(status_request.result_code)

        return status_request

    def create_notification(self, response):
        PaymentStatusRequest.objects.create(
            payment=self,
            notification_type=NOTIFICATION_TYPE_DICT.get(response.get('type')),
            action=response.get('payload').get('action'),
            payload=response)

        mb_ref = response.get('payload').get('resultDetails').get('pmtRef')
        if (response.get('payload').get('paymentBrand') == SIBS_MULTIBANCO and
                mb_ref is not None):
            self.mb_ref = mb_ref
            self.save()

        self.assign_status(response.get('payload').get('result').get('code'))

        return self.status

    def get_payment_status(self):
        try:
            resp = requests.get(self.get_status_url(),
                                self.get_auth_details())

            response = resp.json()

            self.create_status_request(response)
            self.assign_status(response.get('result').get('code'))
        except Exception as e:
            self.status_error = e
            self.save()

    def send_request(self):
        try:
            resp = requests.post(self.get_request_url(),
                                 self.get_details())

            response = resp.json()
            self.reference = response.get('id')

            if self.payment_method == SIBS_MULTIBANCO:
                self.mb_ref = response.get('resultDetails').get('pmtRef')
                self.save()
            self.assign_status(response.get('result').get('code'))
        except Exception as e:
            self.status_error = e

        self.save()

    def assign_customer_ip(self, customer_ip):
        self.customer_ip = customer_ip
        self.save()

    def assign_status(self, result_code=None):
        # TODO: test this (2)
        if not result_code:
            result_code = self.status_request.last().result_code

        # https://sibs.docs.onlinepayments.pt/reference/resultCodes
        if re.match(r'^(000\.000\.|000\.100\.1|000\.[36])', result_code):
            status_code = SUCCESS
        elif re.match(r'^(000\.400\.0[^3]|000\.400\.100)', result_code):
            status_code = SUCCESS_FLAGGED
        elif re.match(r'^(000\.200)', result_code):
            status_code = PENDING
        elif re.match(r'^(800\.400\.5|100\.400\.500)', result_code):
            status_code = PENDING_FLAGGED
        elif re.match(r'^(000\.100\.2)', result_code):
            status_code = CHARGEBACK
        else:
            status_code = REJECTED

        self.status = status_code
        self.save()

        return self.status

    def update_status(self):
        resp = requests.get(self.get_status_url())
        response = resp.json()

        self.create_status_request(response)
        self.assign_status(response.get('result').get('code'))

    def is_paid(self):
        return any(map(lambda s: self.status == s,
                       [SUCCESS, SUCCESS_FLAGGED]))
    is_paid.boolean = True

    def __str__(self):
        return "{} ({}) | {}".format(self.reference,
                                     self.payment_method,
                                     self.get_status_display())


class PaymentStatusRequest(models.Model):
    payment = models.ForeignKey('Payment', related_name='status_request')
    result_code = models.CharField(_('Result code'), max_length=15)
    result_description = models.CharField(_('Result description'),
                                          max_length=300)
    build_number = models.CharField(_('Build number'), max_length=100)
    timestamp = models.DateTimeField(_('Timestamp'))
    ndc = models.CharField(_('NDC'), max_length=100)
    payload = JSONField()

    class Meta:
        ordering = ['timestamp']


def decrypt_notification(header_auth_tag, header_iv, payload):
    key = binascii.unhexlify(settings.SIBS_WEBHOOK_KEY)
    auth_tag = binascii.unhexlify(header_auth_tag)
    iv = binascii.unhexlify(header_iv)
    cipher_text = binascii.unhexlify(payload)

    decryptor = Cipher(
        algorithms.AES(key),
        modes.GCM(iv, auth_tag),
        backend=default_backend()
    ).decryptor()

    return decryptor.update(cipher_text) + decryptor.finalize()


class PaymentNotification(models.Model):
    created_at = models.DateTimeField(_('Created at'), auto_now_add=True)
    updated_at = models.DateTimeField(_('Updated at'), auto_now=True)
    notification_type = models.CharField(_('Notification Type'), max_length=1,
                                         choices=NOTIFICATION_TYPE_OPTIONS,
                                         default=PAYMENT_OPT)
    action = models.CharField(_('Action'), max_length=1,
                              choices=ACTION_OPTIONS,
                              null=True, blank=True)
    payload = JSONField()

    class Meta:
        ordering = ['updated_at']

    def __str__(self):
        p_notification = "[{}] {}".format(self.notification_type,
                                          self.updated_at)
        if self.notification_type == REGISTRATION_TYPE:
            p_notification += " | {}".format(self.get_action_display())

        return p_notification

    def __unicode__(self):
        return u"%s" % self.__str__()
