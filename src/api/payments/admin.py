# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from api.payments.models import (
    Payment,
    PaymentNotification,
    PaymentStatusRequest
)


class PaymentAdmin(admin.ModelAdmin):
    pass


class PaymentNotificationAdmin(admin.ModelAdmin):
    pass


class PaymentStatusRequestAdmin(admin.ModelAdmin):
    pass


admin.site.register(Payment, PaymentAdmin)
admin.site.register(PaymentNotification, PaymentNotificationAdmin)
admin.site.register(PaymentStatusRequest, PaymentStatusRequestAdmin)
