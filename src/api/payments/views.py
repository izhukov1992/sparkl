# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django_rest_logger import log
import json
from rest_framework import status, viewsets
from rest_framework.decorators import list_route
# from rest_framework.decorators import action
# from rest_framework.generics import GenericAPIView
from rest_framework.response import Response

from django.shortcuts import get_object_or_404

from api.business.models import Order

from api.payments.constants import *

from django.conf import settings
import requests
from datetime import datetime, timedelta
from api.payments.models import (
    decrypt_notification,
    Payment
)


class PaymentView(viewsets.ViewSet):

    # @action(methods=['get'], detail=True)
    @list_route(methods=['post'])
    def webhook(self, request):
        """
        View to receive payment update

        Receive data from SIBS regarding payment and update it.
        """
        if ('action' in request.data and
                request.data['action'] == 'webhook activation'):
            # Answer with HTTP(200) if successful
            return Response(status=status.HTTP_200_OK)

        decrypted_response = decrypt_notification(
            request.META.get('HTTP_X_AUTHENTICATION_TAG'),
            request.META.get('HTTP_X_INITIALIZATION_VECTOR'),
            request.data.get('encryptedBody'))
        data = json.loads(decrypted_response)
        print('\n')
        print('*' * 50)
        print(data)
        print('*' * 50)
        print('\n')

        try:
            payment = Payment.objects.filter(
                reference=data.get('payload').get('id')).last()
            payment.create_notification(data)

            return Response(status=status.HTTP_200_OK)
        except (Payment.DoesNotExist, AttributeError):
            log.warning(message='Payment not found.',
                        details={
                            'http_status_code': status.HTTP_404_NOT_FOUND})
            return Response(status=status.HTTP_404_NOT_FOUND)

    # @list_route(methods=['post'])
    # def mbway_request(self, request):
    #     order = get_object_or_404(Order, pk=request.data.get('order_id'))

    #     payment = order.payments.last()
    #     payment.virtual_account_id = request.data.get('v_id')
    #     payment.payment_method = MBWAY
    #     payment.save()
    #     payment.send_request()

    #     return Response(status=status.HTTP_200_OK)

    @list_route(methods=['post'])
    def get_fake_checkout_id(self, request):
        payment_method = request.data.get('payment_method')

        details = {
            'authentication.userId': settings.SIBS_AUTH_USER_ID,
            'authentication.entityId': settings.SIBS_AUTH_ENTITY_ID,
            'authentication.password': settings.SIBS_AUTH_PASSWORD,
            'notificationUrl': settings.SIBS_WEBHOOK,
            'currency': settings.SIBS_CURRENCY,
            'paymentType': PAYMENT_TYPE_OPTIONS.get(payment_method),
            'amount': "12.00",
            'testMode': "EXTERNAL",
            'customParameters[SIBS_ENV]': "QLY"
        }

        url = settings.SIBS_PAYMENT_URL

        if payment_method == MULTIBANCO:
            url = settings.SIBS_MULTIBANCO_URL
            dt = datetime.now()
            exp_dt = dt + timedelta(seconds=3600 * 4)
            details.update({
                'paymentBrand': MULTIBANCO,
                'billing.country': settings.BILLING_COUNTRY,
                'customParameters[SIBSMULTIBANCO_PtmntEntty]':
                    settings.SIBS_MB_ENTITY_ID,
                'customParameters[SIBSMULTIBANCO_RefIntlDtTm]':
                    dt.strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3] + "+00:00",
                'customParameters[SIBSMULTIBANCO_RefLmtDtTm]':
                    exp_dt.strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3] + "+00:00"
            })
        elif payment_method == VISA:
            details.update({
                'merchantTransactionId':
                    settings.SIBS_VISA_MERCHANT_TRANSACTION_ID
            })

        resp = requests.post(url, details)
        print('\n')
        print('*' * 50)
        print(details)
        print('*' * 50)
        print('\n')
        sibs_response = resp.json()
        print('\n')
        print('*' * 50)
        print(sibs_response)
        print('*' * 50)
        print('\n')

        payment = Payment.objects.create(
            reference=sibs_response.get('id'),
            payment_method=payment_method,
            amount=12.0,
            status=PENDING,
            order=Order.objects.last())

        data = {
            'id': payment.reference,
            'payment_method': payment_method,
            'amount': "12.00",
            'status': payment.get_status_display(),
            'order_id': payment.order.id
        }
        if payment_method == MULTIBANCO:
            payment.mb_ref = sibs_response.get(
                'resultDetails').get('pmtRef')
            payment.save()
            data['entity'] = settings.SIBS_MB_ENTITY_ID
            data['mb_ref'] = payment.mb_ref

        print('\n')
        print('*' * 50)
        print(data)
        print('*' * 50)
        print('\n')

        return Response(data, status=status.HTTP_200_OK)


# class PaymentWebhookView(GenericAPIView):
#     serializer_class = None
#     authentication_classes = ()

#     def get(self, request, reference):
#         """
#         View to receive payment update

#         Receive data from SIBS regarding payment and update it.
#         """
#         payment = Payment.objects.filter(reference=reference)
#         print("shit")
#         import ipdb; ipdb.set_trace()
#         if payment:
#             return Response(status=status.HTTP_200_OK)

#         log.warning(message='Payment not found.',
#                     details={'http_status_code': status.HTTP_404_NOT_FOUND})
#         return Response(status=status.HTTP_404_NOT_FOUND)
