# from django.db.models import Q, Avg
from datetime import datetime

from django.shortcuts import get_object_or_404, get_list_or_404
from django.utils.translation import ugettext_lazy as _

from rest_framework import status, viewsets
# from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.decorators import list_route, detail_route
# from rest_framework_jwt.authentication import JSONWebTokenAuthentication

from lib.utils import has_same_date

from api.addresses.models import (
    PostalCode
)
from api.profiles.models import (
    Customer,
    CustomerExtraDetails
)
from api.profiles.serializers import (
    # CustomerSerializer,
    CustomerPublicSerializer,
    ServiceProviderPublicSerializer
)

from api.business.models import (
    Order,
    # OrderItem
    pre_calculate_slots
)
from api.business.serializers import (
    OrderPublicSerializer,
    # OrderItemSerializer
)


def order_from_token(token):
    order = None
    customer_extra = CustomerExtraDetails.objects.filter(
        token=token)
    if customer_extra:
        customer = customer_extra.first().customer

        orders = Order.objects.filter(customer=customer,
                                      closed=False)

        if orders:
            order = orders.last()
        else:
            order = Order.objects.create(customer=customer)

    return order


def order_for_new_customer():
    customer = Customer.objects.create()
    order = Order.objects.create(customer=customer)

    return order


class OrderView(viewsets.ViewSet):
    """Order view."""
    # authentication_classes = (JSONWebTokenAuthentication,)
    # permission_classes = (IsAuthenticated,)

    # def list(self, request):
    #     """List of customers."""
    #     orders = Order.objects.all()

    #     serializer = OrderPublicSerializer(
    #         orders, many=True)
    #     return Response(serializer.data, status=status.HTTP_200_OK)

    # def retrieve(self, request, pk):
    #     """Retrieve the customer."""
    #     order = get_object_or_404(Order, pk=pk)
    #     serializer = OrderPublicSerializer(order)
    #     return Response(serializer.data)

    # def create(self, request):
    #     if not request.data.get('extra_details'):
    #         request.data.update({'extra_details': {}})
    #     serializer = CustomerSerializer(data=request.data)
    #     serializer.is_valid(raise_exception=True)
    #     serializer.save()
    #     return Response(serializer.data, status=status.HTTP_201_CREATED)

    # @list_route(methods=['get'])
    # def get_new_token(self, request):
    #     data = {'customer_token': generate_token()}
    #     return Response(data, status=status.HTTP_200_OK)

    @list_route(methods=['get'])
    def get_from_token(self, request):
        ct = request.COOKIES.get('ct')
        customer_extra = get_object_or_404(CustomerExtraDetails, token=ct)
        order = get_list_or_404(Order,
                                customer=customer_extra.customer,
                                closed=False)
        serializer = OrderPublicSerializer(order.last())
        response = Response(serializer.data, status=status.HTTP_200_OK)

        return response

    # @list_route(methods=['post'])
    # def get_or_create_from_token(self, request):
    #     order = order_from_token(request.data.get('customer_token'))
    #     serializer = OrderPublicSerializer(order)
    #     return Response(serializer.data, status=status.HTTP_200_OK)

    @list_route(methods=['post'])
    def assign_postal_code(self, request):
        ct = request.COOKIES.get('ct')
        postalcode = request.data.get('postal_code', '-')
        order = order_from_token(ct)
        if not order:
            order = order_for_new_customer()

        code, ext = postalcode.split('-')
        postal_code = get_object_or_404(PostalCode, code=code, extension=ext)

        order.assign_postal_code(postal_code)

        # TODO: queue (0)
        pre_calculate_slots(order)

        data = {'customer': CustomerPublicSerializer(order.customer).data}

        response = Response(data, status=status.HTTP_200_OK)
        if order.customer.get_token() != ct:
            response.set_cookie('ct', order.customer.get_token())

        return response

    @list_route(methods=['get'])
    def get_available_slots(self, request):
        ct = request.COOKIES.get('ct')
        q_from = request.query_params.get('q_from')
        q_until = request.query_params.get('q_until')
        sp = request.query_params.get('sp')

        order = order_from_token(ct)
        if not order:
            order = order_for_new_customer()

        if q_from and q_until:
            q_from = datetime.strptime(
                "{}-{}-{} 00h00".format(*q_from.split('-')),
                '%Y-%m-%d %Hh%M')
            q_until = datetime.strptime(
                "{}-{}-{} 23h59".format(*q_until.split('-')),
                '%Y-%m-%d %Hh%M')
        elif request.query_params.get('date'):
            d = map(lambda p: int(p),
                    request.query_params.get('date').split('-'))
            try:
                q_from = datetime(d[0], d[1], d[2], 0, 0, 0)
                q_until = datetime(d[0], d[1], d[2], 23, 59, 59)
            except ValueError:
                # ValueError: day is out of range for month
                pass
        if q_from and (q_from < datetime.now() or
                       has_same_date(q_from, datetime.now())):
            q_from = None
        if q_until and q_until < datetime.now():
            q_until = None

        pre_calculate_slots(
            order, q_from=q_from, q_until=q_until, sp=sp)

        order_slots = order.suggestion_slots.last()
        # PossibleSlotsSerializer()
        starts = order_slots.get_possible_starts(sp=sp)
        data = {'slots': starts}
        sps = [ServiceProviderPublicSerializer(it.service_provider).data for it
               in order_slots.possible_itineraries.all()]
        data.update({"sps": sps})

        response = Response(data, status=status.HTTP_200_OK)

        if order.customer.get_token() != ct:
            response.set_cookie('ct', order.customer.get_token())

        return response

    @list_route(methods=['post'])
    def update_cart(self, request):
        ct = request.COOKIES.get('ct')
        order = order_from_token(ct)
        order.create_items(request.data.get('services'))

        return Response(status=status.HTTP_200_OK)
