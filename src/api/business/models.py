# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from collections import Counter
from datetime import (
    datetime,
    timedelta
)

from django.conf import settings
from django.db import models
from django.utils.translation import ugettext_lazy as _

from api.addresses.models import Address
from api.schedule.constants import AVAILABLE
from api.schedule.models import (
    OrderSuggestionSlots,
    Slot
)
from api.navigation.models import (
    Itinerary,
    pre_calculate_distances
)
from api.services.models import (
    get_items_duration,
    Service
)
from api.payments.models import Payment
from api.profiles.constants import ACTIVE as SP_ACTIVE
from api.profiles.models import ServiceProvider


def guess_schedule_slots(query_from=None, query_until=None, sp=None):
    """Get available slots for given datetime range and filter service
    provider, if any."""

    # datetime between today and the next two days
    if not query_from:
        query_from = datetime.now() + timedelta(
            seconds=settings.MIN_NOTIFY_INTERVAL)
    if not query_until:
        query_until = (query_from + timedelta(days=2)).replace(
            hour=23, minute=59, second=59)

    # apanhar slots livres de todas as profissionais para essas datas
    slots = []
    service_providers = None
    if sp:
        service_providers = ServiceProvider.objects.filter(id=sp,
                                                           status=SP_ACTIVE)
    if not (sp or service_providers):
        service_providers = ServiceProvider.objects.filter(status=SP_ACTIVE)
    for service_provider in service_providers:
        s = Slot.objects.filter(
            service_provider=service_provider,
            slot_type=AVAILABLE,
            slot_from__gte=query_from,
            slot_until__lte=query_until).order_by('slot_from')
        if s:
            slots.append(*s) if len(s) == 1 else slots.extend(s)

    return slots


def pre_calculate_itineraries(order_slots, slots_to_calc=None):
    """Calculate itineraries for selected slots in a given order."""
    # pre calcular itinerarios aqui, sem estarem acoplados ao booking
    # assume address is PostalCode
    address = order_slots.order.customer.get_address()

    if isinstance(address, Address):
        destination = address
        guess_address = False
    else:
        destination = Address.objects.filter(
            postal_code=address).last()
        guess_address = True

    slots = slots_to_calc or order_slots.possible_slots.all()

    for slot in slots:
        prev_itineraries = Itinerary.objects.filter(
            departure_address=slot.get_slot_address(),
            destination_address=destination,
            service_provider=slot.service_provider,
            travel_method=slot.service_provider.travel_method)
        if prev_itineraries:
            order_slots.possible_itineraries.add(prev_itineraries.last())
            order_slots.stat_copied_itineraries += 1
            order_slots.save()
        else:
            order_slots.possible_itineraries.add(Itinerary.objects.create(
                departure_address=slot.get_slot_address(),
                destination_address=destination,
                service_provider=slot.service_provider,
                dest_address_based_on_guess=guess_address))
            order_slots.stat_calculated_itineraries += 1
            order_slots.save()

    pre_calculate_distances(order_slots)


def get_slots_to_update(order_slots, q_from=None, q_until=None, sp=None):
    """Check if there are slots to be updated within a given order."""
    if not q_from:
        q_from = order_slots.possible_slots.first().slot_from
    if not q_until:
        q_until = order_slots.possible_slots.last().slot_until
    last_queried_slots = guess_schedule_slots(q_from, q_until, sp)

    if set(order_slots.possible_slots.all()) != set(last_queried_slots):
        return last_queried_slots
    else:
        return []


def update_available_slots(order_slots, last_queried_slots=None):
    """Update previously calculated slots for given order, if needed."""
    if not last_queried_slots and order_slots.possible_slots.all():
        last_queried_slots = get_slots_to_update(order_slots)

    if order_slots.possible_slots.all():
        slots_to_calculate = set(last_queried_slots).difference(
            set(order_slots.possible_slots.all()))
        slots_to_remove = set(order_slots.possible_slots.all()).difference(
            set(last_queried_slots))

        order_slots.possible_slots.remove(*slots_to_remove)
        order_slots.possible_slots.add(*slots_to_calculate)
        if slots_to_calculate:
            pre_calculate_itineraries(order_slots, slots_to_calculate)


def pre_calculate_slots(order, q_from=None, q_until=None, sp=None):
    """Pre-calculates and updates itineraries for given order."""
    # previously calculated suggestions
    prev_suggestions = order.suggestion_slots.last()

    if (prev_suggestions and
            prev_suggestions.possible_slots.exists()):
        # check if slots are still valid or adds new ones if period changes
        if sp:
            prev_suggestions.possible_slots.remove(
                *prev_suggestions.possible_slots.exclude(
                    service_provider__id=sp))
            prev_suggestions.possible_itineraries.remove(
                *prev_suggestions.possible_itineraries.exclude(
                    service_provider__id=sp))
        slots_to_update = get_slots_to_update(
            prev_suggestions, q_from, q_until, sp)
        if slots_to_update:
            update_available_slots(
                prev_suggestions, slots_to_update)
            pre_calculate_itineraries(
                prev_suggestions, slots_to_update)
    else:
        available_slots = guess_schedule_slots(q_from, q_until, sp)
        order_slots = OrderSuggestionSlots.objects.create(order=order)
        order_slots.possible_slots.add(*available_slots)
        pre_calculate_itineraries(order_slots)


class OrderItem(models.Model):
    order = models.ForeignKey('Order', related_name='items')
    service = models.ForeignKey('services.Service')
    quantity = models.PositiveIntegerField(_('Quantity'), default=1)

    def get_price(self):
        return self.service.price * self.quantity

    def __count__(self):
        return self.quantity

    def __str__(self):
        return self.service.name.encode('utf-8')

    def __unicode__(self):
        return u"%s" % self.__str__()


class Order(models.Model):
    created_at = models.DateTimeField(_('Started at'), auto_now_add=True)
    last_updated_at = models.DateTimeField(_('Last updated at'), auto_now=True)
    closed = models.BooleanField(_('Closed'), default=False)
    customer = models.ForeignKey('profiles.Customer')
    services_duration = models.PositiveIntegerField(
        _('Estimated services duration'), help_text=_('In seconds'),
        null=True, blank=True, default=0)

    def add_item(self, item, quantity=1):
        return OrderItem.objects.create(
            order=self, service=item, quantity=quantity)

    def create_items(self, service_ids):
        """Create order from cart services list.

        service_ids are [1, 1, 2, 5, 7, 1, 2]
        """
        cart = Counter(service_ids)
        # TODO: clear cart every time (1)
        for service_id, quantity in cart.most_common():
            self.add_item(Service.objects.get(id=service_id), quantity)

    def remove_item(self, service, quantity=1):
        items = OrderItem.objects.filter(
            order=self,
            service=service)
        for item in items:
            if quantity < 0:
                break
            item.delete()
            quantity -= 1

        return self.items.all()

    def get_amount(self):
        return self.get_total_price()

    def get_total_price(self):
        return sum([item.get_price() for item in self.get_items()])

    def get_items(self):
        return self.items.all()

    def get_quantity(self):
        return sum([item.quantity for item in self.get_items()])

    def get_related_booking(self):
        return self.order_booking.last()

    def assign_postal_code(self, postal_code):
        return self.customer.assign_postal_code(postal_code)

    def get_customer_address(self):
        return self.customer.get_address()

    def get_services_duration(self, service_provider=None):
        sp = service_provider or getattr(self.order_booking_enq,
                                         'service_provider',
                                         None)
        return get_items_duration(self.get_items(), sp)

    def close(self):
        self.services_duration = self.get_services_duration()
        self.closed = True
        self.save()

    def trigger_payment(self, payment_method=None):
        payment = Payment.objects.create(
            order=self,
            payment_method=payment_method,
            amount=self.get_amount())
        if payment.needs_preparation():
            payment.send_request()

    def __str__(self):
        return "%s (%s)" % (self.get_related_booking(),
                            self.get_quantity())

    def __unicode__(self):
        return u"%s" % self.__str__()
