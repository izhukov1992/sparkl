from rest_framework import serializers
from django.utils.translation import ugettext_lazy as _

# from lib.utils import (
#     validate_email as email_is_valid,
#     calculate_age
# )
# from lib.validate_pt import controlNIF
# from accounts.models import User
# from lib.utils import random_password_generator
from api.profiles.serializers import (
    CustomerPublicSerializer,
    ServiceProviderPublicSerializer
)
from api.services.serializers import ServiceSerializer

from api.business.models import (
    Order,
    OrderItem
)


class OrderItemSerializer(serializers.Serializer):
    service = ServiceSerializer()

    class Meta:
        model = OrderItem
        fields = ('id',
                  'service',
                  'quantity')

    def create(self, validated_data):
        pass

    def update(self, instance, validated_data):
        pass

    def to_representation(self, instance):
        """Representation of a service provider."""
        if instance:
            data = {
                'id': instance.id,
                'service': ServiceSerializer(instance.service).data,
                'quantity': instance.quantity
            }
        else:
            data = {'info': _('Order is empty.')}
        return data


class OrderSerializer(serializers.Serializer):
    customer = CustomerPublicSerializer()
    items = OrderItemSerializer(many=True)

    class Meta:
        model = Order
        fields = ('id',
                  'closed',
                  'customer',
                  'service_provider',
                  'services_duration')

    def create(self, validated_data):
        pass

    def update(self, instance, validated_data):
        pass

    def to_representation(self, instance):
        """Representation of a service provider."""
        return {
            'id': instance.id,
            'created_at': instance.created_at,
            'closed': instance.closed,
            'customer': CustomerPublicSerializer(instance.customer).data,
            'services_duration': instance.services_duration,
            'service_provider': ServiceProviderPublicSerializer(
                instance.service_provider).data,
            'items': OrderItemSerializer(instance.items.all(), many=True).data
        }


class OrderPublicSerializer(serializers.Serializer):
    customer = CustomerPublicSerializer()
    items = OrderItemSerializer(many=True)
    quantity = serializers.IntegerField()

    class Meta:
        model = Order
        fields = ('customer')

    def create(self, validated_data):
        pass

    def update(self, instance, validated_data):
        pass

    def to_representation(self, instance):
        """Representation of a service provider."""
        return {
            'id': instance.id,
            'customer': CustomerPublicSerializer(instance.customer).data,
            'items': OrderItemSerializer(instance.items.all(), many=True).data,
            'quantity': instance.get_quantity()
        }
