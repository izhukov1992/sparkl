from django.conf.urls import url
from django.utils.translation import ugettext_lazy as _

from accounts import views as account_views

urlpatterns = [
    url(_(r'^create/$'),
        account_views.UserRegistrationView.as_view({'post': 'create'}),
        name='create'),
    url(_(r'^update-profile/$'),
        account_views.UserRegistrationView.as_view({'post': 'update_profile'}),
        name='update_profile'),
    url(_(r'^crop-image/$'),
        account_views.UserRegistrationView.as_view({'post': 'crop_image'}),
        name='crop_image'),
    url(_(r'^login/$'),
        account_views.UserLoginView.as_view(),
        name='login'),
    url(_(r'^social/login/$'),
        account_views.SocialLoginView.as_view(),
        name='social-login'),
    url(_(r'^recover-password/$'),
        account_views.UserRecoverPasswordView.as_view(),
        name='recover_password'),
    url(_(r'^change-password/$'),
        account_views.UserChangePasswordView.as_view(),
        name='change_password'),
    url(_(r'^confirm/email/(?P<email_activation_key>.*)/$'),
        account_views.UserConfirmEmailView.as_view(),
        name='confirm_email'),
    url(_(r'^status/email/$'),
        account_views.UserEmailConfirmationStatusView.as_view(),
        name='status_email'),
    url(_(r'^confirm/mobile/(?P<mobile_activation_key>.*)/$'),
        account_views.UserConfirmMobileView.as_view(),
        name='confirm_mobile'),
    url(_(r'^status/mobile/$'),
        account_views.UserMobileConfirmationStatusView.as_view(),
        name='status_mobile'),
    url(_(r'^token/mobile/$'),
        account_views.UserSendMobileTokenView.as_view(),
        name='send_mobile_token')
]
