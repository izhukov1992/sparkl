from io import BytesIO
from PIL import Image
import requests
import uuid

from django.conf import settings
from django.contrib.auth.tokens import default_token_generator
from django.http import JsonResponse
from django.core.files.base import ContentFile
from django.shortcuts import get_object_or_404
from django.utils.encoding import force_text, force_bytes
from django.utils.http import urlsafe_base64_encode
from django_rest_logger import log
from rest_framework import (
    parsers,
    renderers,
    status,
    viewsets
)
from rest_framework.decorators import permission_classes
from rest_framework.generics import GenericAPIView
from rest_framework.permissions import (
    BasePermission,
    IsAuthenticated
)
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from rest_framework_jwt.serializers import JSONWebTokenSerializer
from rest_framework_jwt.utils import (
    jwt_response_payload_handler,
    jwt_payload_handler,
    jwt_encode_handler
)
from django.contrib.auth import authenticate

from accounts.models import (
    User,
    ACCOUNT_TYPE_SERVICE_PROVIDER,
    ACCOUNT_TYPE_Customer
)
from accounts.serializers import (
    UserImageCropSerializer,
    UserRegistrationSerializer,
    UserRegistrationProfileSerializer,
    SocialLoginSerializer,
    UserRecoverPasswordSerializer, UserChangePasswordSerializer)
from lib.utils import (
    AtomicMixin,
    image_file_info,
    random_password_generator,
    serialize_image,
    send_password_recovery_email,
    send_confirm_account_email)
from profiles.models import (
    ServiceProvider,
    Customer
)


# class IsTakingCare(BasePermission):
#     """
#     Allows access to a Customer's Caregiver.
#     """

#     def has_permission(self, request, view, obj):
#         return obj.caregiver.user.pk == request.user.pk


class UserRegistrationView(viewsets.ViewSet):
    """User registration view."""
    authentication_classes = (JSONWebTokenAuthentication,)

    def create(self, request):
        serializer = UserRegistrationSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        user = User.objects.get(email=serializer.data['email'])
        send_confirm_account_email(
            user,
            '{}/account/activate/{}'.format(
                settings.SITE_NAME, user.email_activation_key))

        return Response(status=status.HTTP_201_CREATED)

    @permission_classes((IsAuthenticated,))
    def update_profile(self, request):
        """Update user profile."""
        serializer = UserRegistrationProfileSerializer(request.user,
                                                       data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response({'results': serializer.data},
                        status=status.HTTP_200_OK)

    # @permission_classes((IsAuthenticated, IsTakingCare))
    @permission_classes((IsAuthenticated,))
    def crop_image(self, request):
        """Crop and upload the user image."""
        serializer = UserImageCropSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        field = serializer.validated_data['field']
        if 'create_customer' in request.data:
            user = User.objects.create(
                email='{}:{}'.format(uuid.uuid1(), request.user.email))
        else:
            user = request.user

        # delete if there is an old file
        getattr(user, field).delete()

        uploaded_file = request.FILES[field]
        uploaded_file.open()  # we need to open it or else Pillow can not load
        image = Image.open(uploaded_file)

        # make the thumbnail
        px1, py1 = (int(float(serializer.validated_data['x'])),
                    int(float(serializer.validated_data['y'])))
        px2, py2 = (int(float(serializer.validated_data['x2'])),
                    int(float(serializer.validated_data['y2'])))
        image = image.crop((px1, py1, px2, py2))

        # save the thumbnail to memory
        temp_handle = BytesIO()
        filename_ext = uploaded_file.name.split('.')[-1].lower()
        pil_info = image_file_info(filename_ext)

        image.save(temp_handle, pil_info['type'])
        temp_handle.seek(0)  # rewind the file

        # save to the thumbnail field
        suf = ContentFile(temp_handle.read())

        # save temporary file
        filename_full = '{}{}'.format(user.id, pil_info['extension'])

        getattr(user, field).save(filename_full, suf, save=True)

        data = {'files': [serialize_image(user, field)], 'user': user.pk}
        response = JsonResponse(data)
        response['Content-Disposition'] = 'inline; filename=files.json'

        return response


class SocialLoginView(APIView):
    throttle_classes = ()
    permission_classes = ()
    authentication_classes = ()
    parser_classes = (parsers.FormParser, parsers.JSONParser,)
    renderer_classes = (renderers.JSONRenderer,)
    serializer_class = SocialLoginSerializer

    def post(self, request):
        """
        User login view.

        Based on JSONWebTokenAPIView from rest_framework_jwt.
        """
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            user_obj = serializer.save()
            user = authenticate(username=user_obj.email)
            payload = jwt_payload_handler(user)
            token = jwt_encode_handler(payload)
            token_data = jwt_response_payload_handler(token, user, request)

            # Check which profiles the user have
            # TODO: Find a better way (5)
            profiles = []
            if ServiceProvider.objects.filter(user=user):
                profiles.append(ACCOUNT_TYPE_SERVICE_PROVIDER)
            if Customer.objects.filter(user=user):
                profiles.append(ACCOUNT_TYPE_Customer)

            response_data = {
                'account_type': user.default_account,
                'first_name': user.first_name,
                'last_name': user.last_name,
                'profiles': profiles
            }
            response_data.update(token_data)
            return Response(response_data)

        log.warning(message='Authentication failed.',
                    details={'http_status_code': status.HTTP_401_UNAUTHORIZED})
        return Response(serializer.errors, status=status.HTTP_401_UNAUTHORIZED)


class UserLoginView(APIView):
    throttle_classes = ()
    permission_classes = ()
    authentication_classes = ()
    parser_classes = (parsers.FormParser, parsers.JSONParser,)
    renderer_classes = (renderers.JSONRenderer,)
    serializer_class = JSONWebTokenSerializer

    def post(self, request):
        """
        User login view.

        Based on JSONWebTokenAPIView from rest_framework_jwt.
        """
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            user = serializer.object.get('user') or request.user
            token = serializer.object.get('token')
            token_data = jwt_response_payload_handler(token, user, request)

            # Check which profiles the user have
            # TODO: Find a better way (5)
            profiles = []
            if ServiceProvider.objects.filter(user=user):
                profiles.append(ACCOUNT_TYPE_SERVICE_PROVIDER)
            if Customer.objects.filter(user=user):
                profiles.append(ACCOUNT_TYPE_Customer)

            response_data = {
                'account_type': user.default_account,
                'first_name': user.first_name,
                'last_name': user.last_name,
                'profiles': profiles
            }
            response_data.update(token_data)
            return Response(response_data)

        log.warning(message='Authentication failed.',
                    details={'http_status_code': status.HTTP_401_UNAUTHORIZED})
        return Response(serializer.errors, status=status.HTTP_401_UNAUTHORIZED)


class UserConfirmEmailView(AtomicMixin, GenericAPIView):
    serializer_class = None
    authentication_classes = ()

    def get(self, request, email_activation_key):
        """
        View for confirm email.

        Receive an activation key as parameter and confirm email.
        """
        user = get_object_or_404(User, email_activation_key=str(
            email_activation_key))
        if user.confirm_email():
            return Response(status=status.HTTP_200_OK)

        log.warning(message='Email confirmation key not found.',
                    details={'http_status_code': status.HTTP_404_NOT_FOUND})
        return Response(status=status.HTTP_404_NOT_FOUND)


class UserEmailConfirmationStatusView(GenericAPIView):
    serializer_class = None
    authentication_classes = (JSONWebTokenAuthentication,)

    def get(self, request):
        """Retrieve user current confirmed_email status."""
        user = self.request.user
        return Response({'status': user.confirmed_email},
                        status=status.HTTP_200_OK)


class UserSendMobileTokenView(GenericAPIView):
    serializer_class = None
    authentication_classes = (JSONWebTokenAuthentication,)

    def get(self, request):
        """Send mobile confirmation message.

        TODO: standarize all numbers in the database (5)
        National: 9 digits (e.g.: 60129007788)
        International: (prepend '00' followed by country code and mobile
        number, e.g.: 0066129007788)
        """
        params = {
            "un": settings.ISMS_USERNAME,
            "pwd": settings.ISMS_PASSWORD,
            "dstno": request.user.mobile_number,
            "msg": "{}{}".format(settings.ISMS_TEXT,
                                 request.user.mobile_confirmation_token),
            "type": 1
        }

        try:
            req = requests.get(settings.ISMS_URL,
                               params=params,
                               timeout=settings.ISMS_TIMEOUT)
        except requests.exceptions.Timeout:
            status = False

        if req.content.startswith('2000') or req.content == "":
            return Response(status=status.HTTP_200_OK)

        log.warning(message=req.text,
                    details={'http_status_code': status.HTTP_404_NOT_FOUND})
        return Response({'status': req.text},
                        status=status.HTTP_404_NOT_FOUND)


class UserConfirmMobileView(AtomicMixin, GenericAPIView):
    serializer_class = None
    authentication_classes = ()

    def get(self, request, mobile_activation_key):
        """
        View for confirm phone.

        Receive an activation key as parameter and confirm email.
        """
        user = get_object_or_404(User, mobile_activation_key=str(
            mobile_activation_key))
        if user.confirm_mobile():
            return Response(status=status.HTTP_200_OK)

        log.warning(message='Mobile confirmation key not found.',
                    details={'http_status_code': status.HTTP_404_NOT_FOUND})
        return Response({'status': user.confirmed_mobile},
                        status=status.HTTP_404_NOT_FOUND)


class UserMobileConfirmationStatusView(GenericAPIView):
    serializer_class = None
    authentication_classes = (JSONWebTokenAuthentication,)

    def get(self, request):
        """Retrieve user current confirmed_email status."""
        user = self.request.user
        return Response({'status': user.confirmed_mobile},
                        status=status.HTTP_200_OK)


class UserRecoverPasswordView(GenericAPIView):
    serializer_class = UserRecoverPasswordSerializer
    authentication_classes = (JSONWebTokenAuthentication,)

    def post(self, request):
        """Send an email with a new password."""
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)

        # Get user.
        user = get_object_or_404(
            User, email=serializer.validated_data['email'])
        # Generates a random password.
        new_password = random_password_generator(
            8, '23456789ABCDEFGHJKLMNPQRSTUVWXYZ')
        # Set the new password.
        user.set_password(new_password)
        user.save()

        send_password_recovery_email(user, new_password)
        return Response(status=status.HTTP_200_OK)


class UserChangePasswordView(GenericAPIView):
    serializer_class = UserChangePasswordSerializer
    authentication_classes = (JSONWebTokenAuthentication,)

    @permission_classes((IsAuthenticated,))
    def post(self, request):
        """Send an email with a link for the user to recover his password."""
        serializer = self.serializer_class(data=request.data, instance=self.request.user)
        serializer.is_valid(raise_exception=True)
        self.request.user.set_password(serializer.validated_data['password'])
        self.request.user.save()
        return Response(status=status.HTTP_200_OK)
