from rest_framework import serializers
from phonenumber_field.validators import validate_international_phonenumber

from django.utils.translation import ugettext_lazy as _

from accounts.models import User
from profiles.models import Customer, ServiceProvider
from lib.utils import validate_email as email_is_valid
from lib.validate_pt import controlNIF


class UserRegistrationSerializer(serializers.Serializer):
    first_name = serializers.CharField()
    last_name = serializers.CharField()
    mobile_number = serializers.CharField()
    vat = serializers.CharField()
    email = serializers.EmailField()
    account_type = serializers.CharField()
    password = serializers.CharField(allow_blank=False, write_only=True)
    password_confirmation = serializers.CharField()

    def create(self, validated_data):
        """Create a user instance."""
        data = {
            'email': validated_data.pop('email'),
            'default_account': validated_data.get('account_type', 'C'),
        }

        user = User.objects.create(**data)
        user.set_password(validated_data['password'])
        user.mobile_activation_key = User.objects.make_random_password(
            length=6, allowed_chars='0123456789')
        user.save()

        Customer.objects.create(
            user=user,
            first_name=validated_data.pop('first_name'),
            last_name=validated_data.pop('last_name'),
            mobile_number=validated_data.pop('mobile_number'),
            vat=validated_data.pop('vat'))
        # if validated_data['account_type'] == 'C':
        #     Customer.objects.create(user=user)
        # elif validated_data['account_type'] == 'P':
        #     ServiceProvider.objects.create(user=user)

        return user

    def update(self, instance, validated_data):  # pragma: no cover
        pass

    def validate_email(self, value):
        """
        Validate if email is valid or there is an user using the email.

        :param value: string
        :return: string
        """
        if not email_is_valid(value):
            raise serializers.ValidationError(
                _('Please use a different email address provider.'))
        elif User.objects.filter(email=value).exists():
            raise serializers.ValidationError(_(
                'Email already in use, please use a different email address.'))

        return value

    def validate(self, data):
        """
        Validate if passwords match.

        :param data:
        :return:
        """
        if data['password'] != data['password_confirmation']:
            raise serializers.ValidationError(_('Passwords don\'t match.'))
        return data

    def to_representation(self, instance):
        """Representation of a message."""

        data = {
            'email': instance.email
        }

        return data


class SocialLoginSerializer(serializers.Serializer):
    email = serializers.EmailField()
    account_type = serializers.CharField(allow_blank=True, required=False)
    name = serializers.CharField(allow_blank=True, required=False)
    access_token = serializers.CharField()

    def create(self, validated_data):
        """Create a user instance."""
        data = {
            'first_name': validated_data.get('first_name', ''),
            'last_name': validated_data.get('last_name', ''),
            'email': validated_data['email'],
            'default_account': validated_data.get('account_type', 'C'),
            'mobile_number': validated_data.get('mobile_number', ''),
            'vat': validated_data.get('vat', '')
        }
        user = User.objects.filter(email=data['email'])
        if user:
            user = user.first()
            user.profile.first_name = data['first_name']
            user.profile.first_name = data['first_name']
            user.profile.save()
        else:
            user = User.objects.create(
                email=data['email'],
                default_account=data['default_account'],
            )
            user.mobile_activation_key = User.objects.make_random_password(
                length=6, allowed_chars='0123456789')
            user.save()

            # if validated_data['account_type'] == 'C':
            #     Customer.objects.create(user=user)
            # elif validated_data['account_type'] == 'P':
            #     ServiceProvider.objects.create(user=user)

            Customer.objects.create(
                user=user,
                first_name=data['first_name'],
                last_name=data['last_name'],
                mobile_number=data['mobile_number'],
                vat=data['vat'])

        return user

    def update(self, instance, validated_data):  # pragma: no cover
        pass

    def validate_email(self, value):
        """
        Validate if email is valid or there is an user using the email.

        :param value: string
        :return: string
        """
        if not email_is_valid(value):
            raise serializers.ValidationError(
                _('Please use a different email address provider.'))
        elif User.objects.filter(email=value).exists():
            raise serializers.ValidationError(_(
                'Email already in use, please use a different email address.'))

        return value


class UserRegistrationProfileSerializer(serializers.ModelSerializer):
    image = serializers.CharField()
    gender = serializers.CharField()
    mobile_number = serializers.CharField()
    address = serializers.CharField()
    date_of_birth = serializers.DateField()
    vat = serializers.CharField()

    class Meta:
        model = User
        fields = ('image', 'gender', 'mobile_number', 'address',
                  'date_of_birth', 'vat')

    def create(self, validated_data):  # pragma: no cover
        """Create function."""
        pass

    def update(self, instance, validated_data):  # pragma: no cover
        """Update a user instance."""
        instance.profile.gender = validated_data.get(
            'gender', instance.profile.gender)
        instance.profile.date_of_birth = validated_data.get(
            'date_of_birth', instance.profile.date_of_birth)
        instance.profile.mobile_number = validated_data.get(
            'mobile_number', instance.profile.mobile_number)
        # instance.address = validated_data.get('address',
        #                                       instance.address)
        instance.profile.vat = validated_data.get('vat', instance.profile.vat)

        instance.profile.save()
        return instance

    def validate_mobile_number(self, value):
        """Validate if phone number is valid or if there is an user using the
        phone number."""
        validate_international_phonenumber(value)

        if Customer.objects.filter(mobile_number=value):
            raise serializers.ValidationError(
                _('Please use a different phone number.'))
        return value

    # def validate_vat(cls, value):
    #     if not controlNIF(value):
    #         raise serializers.ValidationError(_('Invalid VAT.'))
    #     return value


class UserImageCropSerializer(serializers.Serializer):
    field = serializers.CharField()
    x = serializers.FloatField()
    y = serializers.FloatField()
    x2 = serializers.FloatField()
    y2 = serializers.FloatField()
    create_customer = serializers.BooleanField(required=False)

    def create(self, validated_data):  # pragma: no cover
        """Create function."""
        pass

    def update(self, instance, validated_data):  # pragma: no cover
        """Update function."""
        pass


class UserRecoverPasswordSerializer(serializers.Serializer):
    email = serializers.EmailField()

    def create(self, validated_data):  # pragma: no cover
        """Create function."""
        pass

    def update(self, instance, validated_data):  # pragma: no cover
        """Update function."""
        pass


class UserChangePasswordSerializer(serializers.Serializer):
    old_password = serializers.CharField()
    password = serializers.CharField(allow_blank=False, write_only=True)
    password_confirmation = serializers.CharField()

    def create(self, validated_data):  # pragma: no cover
        """Create function."""
        pass

    def update(self, instance, validated_data):  # pragma: no cover
        """Update function."""
        pass

    def validate(self, data):
        """
        Validate if passwords match.

        :param data:
        :return:
        """
        if not self.instance.check_password(data['old_password']):
            raise serializers.ValidationError(_('Invalid password.'))

        if data['password'] != data['password_confirmation']:
            raise serializers.ValidationError(_('Passwords don\'t match.'))
        return data
