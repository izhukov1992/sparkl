import uuid
from datetime import datetime, timedelta
from operator import add, lt

from django.conf import settings
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager
from django.db import models
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _

from profiles.models import (
    Customer,
    ServiceProvider
)

from .constants import *


class MyUserManager(BaseUserManager):
    def _create_user(self, email, password, first_name, last_name, is_staff,
                     is_superuser, **extra_fields):
        """Create and save an User with the given email, password, name and
        phone number."""
        now = timezone.now()
        email = self.normalize_email(email)
        user = self.model(email=email,
                          first_name=first_name,
                          last_name=last_name,
                          is_staff=is_staff,
                          is_active=True,
                          is_superuser=is_superuser,
                          last_login=now,
                          date_joined=now, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)

        return user

    def create_user(self, email, first_name, last_name, password,
                    **extra_fields):
        """Create and save an User with the given email, password,
        name and phone number."""
        return self._create_user(email, password, first_name, last_name,
                                 is_staff=False, is_superuser=False,
                                 **extra_fields)

    def update_default_account(self, account_type=None):
        """Update the user's default account by order of importance."""
        try:
            self.default_account = ACCOUNT_TYPE_CHOICES[account_type]
        except TypeError:
            P_TYPES = (
                (ACCOUNT_TYPE_SERVICE_PROVIDER, ServiceProvider),
                (ACCOUNT_TYPE_CUSTOMER, Customer)
            )
            self.default_account = [p[0] for p in P_TYPES if
                                    p[1].objects.get(user=self)][0]

        self.save()


class User(AbstractBaseUser):
    """
    Model that represents an user.
    To be active, the user must register and confirm his email.
    """
    user_uuid = models.UUIDField(unique=True,
                                 default=uuid.uuid4,
                                 editable=False)
    email = models.EmailField(_('Email address'), unique=True)
    default_account = models.CharField(max_length=2,
                                       choices=ACCOUNT_TYPE_CHOICES,
                                       default=ACCOUNT_TYPE_CUSTOMER)

    confirmed_email = models.BooleanField(default=False)
    confirmed_mobile = models.BooleanField(default=False)

    is_staff = models.BooleanField(_('staff status'), default=False)
    is_superuser = models.BooleanField(_('superuser status'), default=False)
    is_active = models.BooleanField(_('active'), default=True)

    date_joined = models.DateTimeField(_('date joined'), auto_now_add=True)

    email_activation_key = models.UUIDField(unique=True, default=uuid.uuid4)
    mobile_activation_key = models.CharField(max_length=6, default='000000')

    USERNAME_FIELD = 'email'

    objects = MyUserManager()

    def __str__(self):
        """
        Unicode representation for an user model.
        :return: string
        """
        return self.email

    def get_full_name(self):
        """
        Return the first_name plus the last_name, with a space in between.
        :return: string
        """
        return "%s %s" % (self.profile.first_name, self.profile.last_name)

    def get_short_name(self):
        """
        Return the first_name.
        :return: string
        """
        return self.profile.first_name

    def activation_expired(self):
        """
        Check if user's activation has expired.
        :return: boolean
        """
        return lt(add(self.date_joined,
                      timedelta(days=settings.ACCOUNT_ACTIVATION_DAYS)),
                  timezone.now())

    def confirm_email(self):
        """
        Confirm email.
        :return: boolean
        """
        if not self.activation_expired() and not self.confirmed_email:
            self.confirmed_email = True
            self.save()
            return True
        return False

    def confirm_mobile(self):
        """
        Confirm mobile.
        :return: boolean
        """
        if self.mobile_number and not self.confirmed_mobile:
            self.confirmed_mobile = True
            self.save()
            return True
        return False

    def has_perm(self, perm, obj=None):
        """
        Returns True if the user has the specified permission.

        TODO: This is hardcoded to allow all superusers (5)
        """
        return self.is_superuser

    def has_module_perms(self, app_label):
        """
        Returns True if the user has each of the specified permissions.

        TODO: This is hardcoded to allow all superusers (5)
        """
        return self.is_superuser
