# from django.db.models import Avg
# from phonenumber_field.validators import validate_international_phonenumber
from rest_framework import serializers

# from lib.utils import (
#     validate_email as email_is_valid,
#     calculate_age
# )
# from lib.validate_pt import controlNIF
# from accounts.models import User
# from lib.utils import random_password_generator
from api.services.models import (
    Service,
    ServiceCategory,
    ExtraServiceGroup,
    ServicePreferencesGroup,
    Venue,
    Channel
)


class ServiceCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = ServiceCategory
        fields = ('id',
                  'name',
                  # 'image',
                  'weight')

    def create(self, validated_data):  # pragma: no cover
        """Create a customer instance."""
        pass

    def update(self, instance, validated_data):
        """Update a customer instance."""
        pass

    def to_representation(self, instance):
        """Representation of a customer."""
        services = Service.objects.filter(category__pk=instance.pk)

        return {
            'id': instance.id,
            'name': instance.name,
            # 'image': instance.image,
            'weight': instance.weight,
            # 'services': ServiceSimpleSerializer(services, many=True).data
            'services': ServiceByCategorySerializer(services, many=True).data
        }


class ServiceCategorySimpleSerializer(serializers.ModelSerializer):
    class Meta:
        model = ServiceCategory
        fields = ('id',
                  'name',
                  # 'image',
                  'weight')

    def to_representation(self, instance):
        """Representation of a customer."""
        return {
            'id': instance.id,
            'name': instance.name,
            # 'image': instance.image,
            'weight': instance.weight
        }


class ServiceSimpleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Service
        fields = ('id',
                  'name',
                  'help_text',
                  'price',
                  'time_duration',
                  'weight')

    def create(self, validated_data):  # pragma: no cover
        """Create a customer instance."""
        pass

    def update(self, instance, validated_data):
        """Update a customer instance."""
        pass

    def to_representation(self, instance):
        """Representation of a service."""
        return {
            'id': instance.pk,
            'name': instance.name,
            'price': instance.price,
            'help_text': instance.help_text,
            'time_duration': instance.time_duration,
            'weight': instance.weight
        }


class ServiceIdSerializer(serializers.ModelSerializer):
    class Meta:
        model = Service
        fields = ('id')

    def to_representation(self, instance):
        """Representation of a service."""
        return {
            'id': instance.pk,
        }


class ExtraServiceGroupSerializer(serializers.ModelSerializer):
    choices = ServiceIdSerializer(many=True)
    fallback_choice = ServiceIdSerializer()
    fallback_if_all_services = ServiceIdSerializer(many=True)
    fallback_if_any_service = ServiceIdSerializer(many=True)

    class Meta:
        model = ExtraServiceGroup
        fields = ('id',
                  'name',
                  'help_text',
                  'choices',
                  'fallback_choice',
                  'fallback_if_all_services',
                  'fallback_if_any_service')

    def to_representation(self, instance):
        """Representation of a service."""
        return {
            'id': instance.pk,
            'name': instance.name,
            'help_text': instance.help_text,
            'choices': ServiceIdSerializer(
                instance.choices, many=True).data,
            'fallback_choice': ServiceIdSerializer(
                instance.fallback_choice).data,
            'fallback_if_all_services': ServiceIdSerializer(
                instance.fallback_if_all_services, many=True).data,
            'fallback_if_any_service': ServiceIdSerializer(
                instance.fallback_if_any_service, many=True).data
        }


class ServicePreferencesGroupSerializer(serializers.ModelSerializer):
    default_choice = ServiceIdSerializer()
    yes_choice = ServiceIdSerializer()
    no_choice = ServiceIdSerializer()

    class Meta:
        model = ServicePreferencesGroup
        fields = ('id',
                  'name',
                  'help_text',
                  'default_choice',
                  'yes_choice',
                  'no_choice')

    def to_representation(self, instance):
        """Representation of a service."""
        return {
            'id': instance.pk,
            'name': instance.name,
            'help_text': instance.help_text,
            'default_choice': ServiceIdSerializer(
                instance.default_choice).data,
            'yes_choice': ServiceIdSerializer(
                instance.yes_choice).data,
            'no_choice': ServiceIdSerializer(
                instance.no_choice).data,
        }


class ServiceByCategorySerializer(serializers.ModelSerializer):
    subcategory = ServiceCategorySerializer()
    related_services = ServiceIdSerializer(many=True)
    required_addons = ExtraServiceGroupSerializer(many=True)
    related_addons = ExtraServiceGroupSerializer(many=True)
    related_preferences = ServicePreferencesGroupSerializer(many=True)

    class Meta:
        model = Service
        fields = ('id',
                  'name',
                  'help_text',
                  'price',
                  'time_duration',
                  'repetition_period',
                  'subcategory',
                  'addon_only',
                  'related_services',
                  'related_addons',
                  'required_addons',
                  'related_preferences',
                  'weight')

    def to_representation(self, instance):
        """Representation of a service."""
        extra = {}
        if not instance.addon_only:
            if instance.related_services.all():
                extra.update({
                    'related_services': ServiceIdSerializer(
                        instance.related_services, many=True).data,
                })
            if instance.related_addons.all():
                extra.update({
                    'related_addons': ExtraServiceGroupSerializer(
                        instance.related_addons, many=True).data
                })
            if instance.required_addons.all():
                extra.update({
                    'required_addons': ExtraServiceGroupSerializer(
                        instance.required_addons, many=True).data
                })
            if instance.related_preferences.all():
                extra.update({
                    'related_preferences': ServicePreferencesGroupSerializer(
                        instance.related_preferences, many=True).data
                })
        if instance.subcategory:
            extra.update({
                'subcategory': ServiceCategorySimpleSerializer(
                    instance.subcategory).data,
            })
        data = {
            'id': instance.pk,
            'name': instance.name,
            'price': instance.price,
            'help_text': instance.help_text,
            'time_duration': instance.time_duration,
            'addon_only': instance.addon_only,
            'weight': instance.weight,
        }
        data.update(extra)

        return data


class ServiceSerializer(serializers.ModelSerializer):
    category = ServiceCategorySerializer()
    subcategory = ServiceCategorySerializer()
    related_services = ServiceIdSerializer(many=True)
    required_addons = ExtraServiceGroupSerializer(many=True)
    related_addons = ExtraServiceGroupSerializer(many=True)
    related_preferences = ServicePreferencesGroupSerializer(many=True)

    class Meta:
        model = Service
        fields = ('id',
                  'name',
                  'help_text',
                  'price',
                  'time_duration',
                  'repetition_period',
                  'category',
                  'subcategory',
                  'addon_only',
                  'related_services',
                  'related_addons',
                  'required_addons',
                  'related_preferences',
                  'weight')

    def create(self, validated_data):  # pragma: no cover
        """Create a customer instance."""
        pass

    def update(self, instance, validated_data):
        """Update a customer instance."""
        pass

    def to_representation(self, instance):
        """Representation of a service."""
        extra = {}
        if not instance.addon_only:
            if instance.related_services.all():
                extra.update({
                    'related_services': ServiceIdSerializer(
                        instance.related_services, many=True).data,
                })
            if instance.related_addons.all():
                extra.update({
                    'related_addons': ExtraServiceGroupSerializer(
                        instance.related_addons, many=True).data
                })
            if instance.required_addons.all():
                extra.update({
                    'required_addons': ExtraServiceGroupSerializer(
                        instance.required_addons, many=True).data
                })
            if instance.related_preferences.all():
                extra.update({
                    'related_preferences': ServicePreferencesGroupSerializer(
                        instance.related_preferences, many=True).data
                })
        if instance.subcategory:
            extra.update({
                'subcategory': ServiceCategorySimpleSerializer(
                    instance.subcategory).data,
            })
        data = {
            'id': instance.pk,
            'name': instance.name,
            'price': instance.price,
            'help_text': instance.help_text,
            'time_duration': instance.time_duration,
            'category': ServiceCategorySimpleSerializer(
                instance.category).data,
            'addon_only': instance.addon_only,
            'weight': instance.weight,
        }
        data.update(extra)

        return data

    # def to_representation(self, instance):
    #     """Representation of a service."""
    #     return {
    #         'id': instance.pk,
    #         'name': instance.name,
    #         'price': instance.price,
    #         'help_text': instance.help_text,
    #         'time_duration': instance.time_duration,
    #         'category': ServiceCategorySimpleSerializer(
    #             instance.category).data,
    #         'subcategory': ServiceCategorySimpleSerializer(
    #             instance.subcategory).data,
    #         'addon_only': instance.addon_only,
    #         'related_services': ServiceIdSerializer(
    #             instance.related_services, many=True).data,
    #         'related_addons': ExtraServiceGroupSerializer(
    #             instance.related_addons, many=True).data,
    #         'required_addons': ExtraServiceGroupSerializer(
    #             instance.required_addons, many=True).data,
    #         'related_preferences': ServicePreferencesGroupSerializer(
    #             instance.related_preferences, many=True).data,
    #         'weight': instance.weight,
    #     }


class VenueSerializer(serializers.ModelSerializer):
    class Meta:
        model = Venue
        fields = ('name',)

    def create(self, validated_data):  # pragma: no cover
        """Create a customer instance."""
        pass

    def update(self, instance, validated_data):
        """Update a customer instance."""
        pass

    def to_representation(self, instance):
        """Representation of a customer."""
        return {
            'id': instance.id,
            'name': instance.name,
        }


class ChannelSerializer(serializers.ModelSerializer):
    class Meta:
        model = Channel
        fields = ('name',)

    def create(self, validated_data):  # pragma: no cover
        """Create a customer instance."""
        pass

    def update(self, instance, validated_data):
        """Update a customer instance."""
        pass

    def to_representation(self, instance):
        """Representation of a customer."""
        return {
            'id': instance.id,
            'name': instance.name,
        }
