from django.contrib import admin
# from django.utils.safestring import mark_safe
# from django.utils.translation import ugettext_lazy as _
from api.services.models import (
    Service,
    ServiceDelivery,
    ServiceCategory,
    Venue,
    Channel
)


class ServiceAdmin(admin.ModelAdmin):
    pass


class ServiceDeliveryAdmin(admin.ModelAdmin):
    pass


class ServiceCategoryAdmin(admin.ModelAdmin):
    pass


class VenueAdmin(admin.ModelAdmin):
    pass


class ChannelAdmin(admin.ModelAdmin):
    pass


admin.site.register(Service, ServiceAdmin)
admin.site.register(ServiceDelivery, ServiceDeliveryAdmin)
admin.site.register(ServiceCategory, ServiceCategoryAdmin)
admin.site.register(Venue, VenueAdmin)
admin.site.register(Channel, ChannelAdmin)
