# from django.db.models import Q, Avg
from django.shortcuts import get_object_or_404
from rest_framework import status, viewsets
# from rest_framework.permissions import IsAuthenticated
from rest_framework.decorators import list_route, detail_route
from rest_framework.response import Response
# from rest_framework_jwt.authentication import JSONWebTokenAuthentication

from api.services.models import (
    Service,
    ServiceCategory,
    Venue,
    Channel
)
from api.services.serializers import (
    ServiceSerializer,
    ServiceSimpleSerializer,
    ServiceCategorySerializer,
    VenueSerializer,
    ChannelSerializer
)


class ServiceView(viewsets.ViewSet):
    def list(self, request):
        """List of service providers."""
        services = Service.objects.all()

        serializer = ServiceSerializer(services, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def retrieve(self, request):
        """Retrieve the service provider."""
        service = get_object_or_404(Service)
        serializer = ServiceSerializer(service)
        return Response(serializer.data)

    @list_route(methods=['get'])
    def get_services(self, request):
        services = Service.objects.get_services()

        serializer = ServiceSimpleSerializer(services, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    @list_route(methods=['get'])
    def get_addons(self, request):
        services = Service.objects.get_addons()

        serializer = ServiceSimpleSerializer(services, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    @list_route(methods=['get'])
    def filter(self, request):
        ids = request.query_params.get('ids').split(',')
        services = Service.objects.filter(pk__in=ids)

        serializer = ServiceSerializer(services, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class ServiceCategoryView(viewsets.ViewSet):
    def list(self, request):
        """List of service providers."""
        service_categories = ServiceCategory.objects.all()

        serializer = ServiceCategorySerializer(
            service_categories, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def retrieve(self, request, pk):
        """Retrieve the services related to that category."""
        service_category = get_object_or_404(ServiceCategory, pk=pk)
        serializer = ServiceCategorySerializer(service_category)
        return Response(serializer.data)

    @list_route(methods=['get'])
    def filter(self, request):
        ids = request.query_params.get('ids').split(',')
        categories = ServiceCategory.objects.filter(pk__in=ids)

        serializer = ServiceCategorySerializer(categories, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class VenueView(viewsets.ViewSet):
    def list(self, request):
        """List of service providers."""
        venues = Venue.objects.all()

        serializer = VenueSerializer(venues, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def retrieve(self, request):
        """Retrieve the service provider."""
        venue = get_object_or_404(Venue)
        serializer = VenueSerializer(venue)
        return Response(serializer.data)


class ChannelView(viewsets.ViewSet):
    def list(self, request):
        """List of service providers."""
        channels = Channel.objects.all()

        serializer = ChannelSerializer(channels, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def retrieve(self, request):
        """Retrieve the service provider."""
        channel = get_object_or_404(Channel)
        serializer = ChannelSerializer(channel)
        return Response(serializer.data)
