# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from datetime import datetime

from django.conf import settings
from django.db import models
from django.db.models.signals import pre_save
from django.utils.translation import ugettext_lazy as _


def get_services_duration(services, service_provider=None):
    """Return total services duration.

    If service_provider, service time is relative to them."""
    if service_provider:
        services = ServiceDelivery.objects.filter(
            service_provider=service_provider,
            service__in=services.all())
    else:
        services = services.all()
    return sum([service.time_duration for service in services])


def get_items_duration(items, service_provider=None):
    """Return total services duration.

    If service_provider, service time is relative to them."""
    if service_provider:
        return sum([ServiceDelivery.objects.get(
            service_provider=service_provider,
            service=item.service).time_duration for
            item in items])
    else:
        return sum([item.service.time_duration for item in items])


def get_services_price(services):
    """Return total price for the services."""
    return sum([service.price for service in services])


def get_service_provider_services(sp):
    """Return services performed by service provider."""
    return ServiceDelivery.objects.filter(service_provider=sp)


def upload_category_image(instance, filename):
    """Used in User model to save image file."""
    formatted_name = '{}_{}.{}'.format(instance.name,
                                       datetime.now().strftime('%f'),
                                       filename.split('.')[1])
    return '{}/categories/images/{}'.format(settings.MEDIA_ROOT,
                                            formatted_name.lower())


class ServiceCategoryManager(models.Manager):
    def get_queryset(self):
        """Default queryset is limited to categories that are active."""
        return super(ServiceCategoryManager, self).get_queryset().filter(
            active=True)


class ServiceCategory(models.Model):
    name = models.CharField(_('Name'), max_length=100)
    image = models.ImageField(upload_to=upload_category_image,
                              null=True, blank=True)
    weight = models.PositiveIntegerField(
        _('Weight'), help_text=_('Sort order (heaviest last)'),
        null=True, blank=True, default=0)
    active = models.BooleanField(_('Active'), default=True)

    class Meta:
        verbose_name_plural = "Service Categories"
        ordering = ['weight']

    def __str__(self):
        return self.name.encode('utf-8')

    def __unicode__(self):
        return u"%s" % self.__str__()


class ServiceManager(models.Manager):
    def get_queryset(self):
        """Default queryset is limited to services that are active."""
        return super(ServiceManager, self).get_queryset().filter(
            active=True)

    def get_services(self):
        """Return services that are not add-ons only."""
        return super(ServiceManager, self).get_queryset().filter(
            addon_only=False)

    def get_addons(self):
        """Return services that are add-ons only."""
        return super(ServiceManager, self).get_queryset().filter(
            addon_only=True)


class Service(models.Model):
    name = models.CharField(_('Name'), max_length=100)
    help_text = models.CharField(_('Help text'), max_length=200,
                                 null=True, blank=True)
    price = models.DecimalField(_('Price'), max_digits=7, decimal_places=2)
    time_duration = models.PositiveIntegerField(
        _('Avg time duration'), help_text=_('In seconds'),
        null=True, blank=True)
    repetition_period = models.PositiveIntegerField(
        _('Avg repetition duration'), help_text=_('In seconds'),
        null=True, blank=True)
    category = models.ForeignKey('ServiceCategory', related_name='category')
    subcategory = models.ForeignKey('ServiceCategory',
                                    related_name='subcategory',
                                    null=True, blank=True)
    active = models.BooleanField(_('Active'), default=True)
    interoperable = models.BooleanField(
        _('Interoperability'),
        help_text=_("Can be performed simultaneously with another service"),
        default=True)
    addon_only = models.BooleanField(
        _('Add-on only'),
        help_text=_("If addon_only, only shows in the add-ons screen"),
        default=False)
    related_services = models.ManyToManyField(
        'self',
        help_text=_("Service(s) related to this service"),
        related_name='related_services')
    related_addons = models.ManyToManyField(
        'ExtraServiceGroup',
        help_text=_("Add-on(s) related to this service"),
        related_name='related_addons')
    # add-ons that are required based on the service and its rules
    required_addons = models.ManyToManyField(
        'ExtraServiceGroup',
        related_name='required_addons',
        help_text=_("Validate if at least one add-on is selected"))
    related_preferences = models.ManyToManyField(
        'ServicePreferencesGroup',
        help_text=_("Add-ons that are preferences"),
        related_name='related_preferences')
    # TODO: smart sort_order (5)
    # https://github.com/bfirsh/django-ordered-model
    weight = models.PositiveIntegerField(
        _('Weight'), help_text=_('Sort order (heaviest last)'),
        null=True, blank=True, default=0)

    objects = ServiceManager()

    class Meta:
        ordering = ['category', 'weight']

    def __str__(self):
        return self.name.encode('utf-8')

    def __unicode__(self):
        return u"%s (%s)" % (self.name, self.category.name)


class ServiceDelivery(models.Model):
    """Service performed by service provider, including relative revenue."""
    service = models.ForeignKey('Service')
    service_provider = models.ForeignKey('profiles.ServiceProvider')
    revenue_percentage = models.FloatField(_('Revenue percentage'),
                                           default=0.75)
    time_duration = models.PositiveIntegerField(
        _('Avg time duration'), help_text=_('In seconds'),
        null=True, blank=True)

    def __str__(self):
        return self.service.name.encode('utf-8')

    def __unicode__(self):
        return u"%s" % self.__str__()

    class Meta:
        unique_together = ('service', 'service_provider')
        verbose_name_plural = "Service Deliveries"


def fill_avg_time_field_signal(sender, instance, *args, **kwargs):
    """Feed service duration field from average value if none is provided."""
    if not instance.pk and not instance.time_duration:
        instance.time_duration = instance.service.time_duration


pre_save.connect(fill_avg_time_field_signal, sender=ServiceDelivery)


class ExtraServiceGroup(models.Model):
    name = models.CharField(_('Name'), max_length=200)
    choices = models.ManyToManyField('Service', related_name='choices')
    fallback_choice = models.ForeignKey('Service',
                                        related_name='fallback_choice')
    fallback_if_all_services = models.ManyToManyField(
        'Service', related_name='fallback_if_all_services',
        help_text=_("Fallback if all the selected services of that category\
        are in this field."))
    fallback_if_any_service = models.ManyToManyField(
        'Service', related_name='fallback_if_any_service',
        help_text=_("Fallback if any of the selected services of that category\
        are in this field."))
#     fallback_if_all_addons = models.ManyToManyField(
#         'Service', related_name='fallback_if_all_addons',
#         help_text='Fallback if all of the selected add-ons of that category\
# are in this field.')
#     fallback_if_any_addon = models.ManyToManyField(
#         'Service', related_name='fallback_if_one_addon',
#         help_text='Fallback if any of the selected add-ons of that category\
# are in this field.')
    help_text = models.CharField(_('Help text'), max_length=200,
                                 null=True, blank=True)
    weight = models.PositiveIntegerField(
        _('Weight'), help_text=_('Sort order (heaviest last)'),
        null=True, blank=True, default=0)

    class Meta:
        ordering = ['weight']

    def __str__(self):
        return self.name.encode('utf-8')

    def __unicode__(self):
        return u"%s" % self.__str__()


class ServicePreferencesGroup(models.Model):
    """Preferences related to a service.

    Each preference is a service (add-on only) as well."""
    name = models.CharField(_('Name'), max_length=200)
    default_choice = models.ForeignKey('Service',
                                       related_name='default_choice')
    yes_choice = models.ForeignKey('Service', related_name='yes_choice')
    no_choice = models.ForeignKey('Service', related_name='no_choice')
    help_text = models.CharField(_('Help text'), max_length=200,
                                 null=True, blank=True)
    weight = models.PositiveIntegerField(
        _('Weight'), help_text=_('Sort order (heaviest last)'),
        null=True, blank=True, default=0)

    class Meta:
        ordering = ['weight']

    def __str__(self):
        return self.name.encode('utf-8')

    def __unicode__(self):
        return u"%s" % self.__str__()


class ServicePackage(models.Model):
    """Services package."""
    name = models.CharField(_('Name'), max_length=100)
    price = models.DecimalField(_('Price'), max_digits=7, decimal_places=2)
    items = models.ManyToManyField('Service')

    def __str__(self):
        return self.name.encode('utf-8')

    def __unicode__(self):
        return u"%s" % self.__str__()


class Venue(models.Model):
    name = models.CharField(_('Name'), max_length=100)
    address = models.ForeignKey('addresses.Address', null=True, blank=True)

    def __str__(self):
        return self.name.encode('utf-8')

    def __unicode__(self):
        return u"%s" % self.__str__()


class Channel(models.Model):
    """Sales channel."""
    name = models.CharField(_('Channel'), max_length=100)
    address = models.ForeignKey('addresses.Address', null=True, blank=True)

    def __str__(self):
        return self.name.encode('utf-8')

    def __unicode__(self):
        return u"%s" % self.__str__()
