from django.conf.urls import url
from django.utils.translation import ugettext_lazy as _

import views as service_views

urlpatterns = [
    url(_(r'^service/list/$'),
        service_views.ServiceView.as_view({'gel': 'list'}),
        name='services'),
    url(_(r'^service/$'),
        service_views.ServiceView.as_view({'get': 'retrieve'}),
        name='service_retrieve'),
]

urlpatterns += [
    url(_(r'^service_category/list/$'),
        service_views.ServiceCategoryView.as_view({'gel': 'list'}),
        name='service_categories'),
    url(_(r'^service_category/$'),
        service_views.ServiceCategoryView.as_view({'get': 'retrieve'}),
        name='service_category_retrieve'),
]

urlpatterns += [
    url(_(r'^venue/list/$'),
        service_views.VenueView.as_view({'gel': 'list'}),
        name='venues'),
    url(_(r'^venue/$'),
        service_views.VenueView.as_view({'get': 'retrieve'}),
        name='venue_retrieve'),
]

urlpatterns += [
    url(_(r'^channel/list/$'),
        service_views.ChannelView.as_view({'gel': 'list'}),
        name='channels'),
    url(_(r'^channel/$'),
        service_views.ChannelView.as_view({'get': 'retrieve'}),
        name='channel_retrieve'),
]
