import os
from datetime import time

from api.profiles.constants import (
    DRIVING,
    TRANSIT
)

# TODO: remove /sparkl_web/settings to get base folder (3)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(__file__)))

PROJECT_PATH = os.path.realpath(os.path.dirname(__file__))

SECRET_KEY = 'n3@wsgyxr)65$+s%z=b7#@8460%t_t0&s*elevyu%h5w_0i9@@'

ALLOWED_HOSTS = []

# Application definition

DJANGO_APPS = (
    # 'django.contrib.admin',
    'django.contrib.admin.apps.SimpleAdminConfig',
    'django.contrib.sessions',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.staticfiles',
)


THIRD_PARTY_APPS = (
    'adminplus',
    'rest_framework',
    # 'knox',
    'corsheaders',
    'django_extensions',
    # 'rest_framework_jwt',
    # 'rest_framework_swagger',
)

LOCAL_APPS = (
    'base.apps.BaseConfig',
    'api.addresses.apps.AddressesConfig',
    'api.profiles.apps.ProfilesConfig',
    'api.navigation.apps.NavigationConfig',
    'api.schedule.apps.ScheduleConfig',

    'api.business.apps.BusinessConfig',
    'api.bookings.apps.BookingsConfig',
    'api.services.apps.ServicesConfig',
    'api.payments.apps.PaymentsConfig'
)

INSTALLED_APPS = DJANGO_APPS + THIRD_PARTY_APPS + LOCAL_APPS

MIDDLEWARE = (
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

# CORS_ORIGIN_WHITELIST = (
#     '127.0.0.1:8080',
# )

CORS_ORIGIN_ALLOW_ALL = True

ROOT_URLCONF = 'sparkl_web.urls'

WSGI_APPLICATION = 'sparkl_web.wsgi.application'

LANGUAGE_CODE = 'en-GB'

TIME_ZONE = 'Europe/Lisbon'

USE_I18N = True

USE_L10N = True

# make sure the server is in Portugal:
# https://docs.djangoproject.com/en/1.10/topics/i18n/timezones/#postgresql
# https://docs.djangoproject.com/en/1.10/ref/settings/#std:setting-USE_TZ
USE_TZ = True

# AUTH_USER_MODEL = 'api.accounts.User'

ACCOUNT_ACTIVATION_DAYS = 7  # days

STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, 'static_root')
STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'static_dist'),
)
# MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
# MEDIA_URL = '/media/'
MEDIA_ROOT = 'media'

# store static files locally and serve with whitenoise
# STATICFILES_STORAGE = 'whitenoise.django.GzipManifestStaticFilesStorage'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            BASE_DIR.replace('/sparkl_web', '/templates/'),
            os.path.join(BASE_DIR, 'static')
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'libraries': {
                'admin.urls': 'django.contrib.admin.templatetags.admin_urls',
            },
            'context_processors': [
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages'
            ],
            'debug': True
        },
    },
]

# ############# REST FRAMEWORK ###################

REST_FRAMEWORK = {
    'DEFAULT_PERMISSION_CLASSES': (),
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.SessionAuthentication',
        'rest_framework.authentication.BasicAuthentication',
        # 'rest_framework_jwt.authentication.JSONWebTokenAuthentication',
    ),
    'DEFAULT_PAGINATION_CLASS':
        'rest_framework.pagination.PageNumberPagination',
    'PAGE_SIZE': 20,
    'DEFAULT_PARSER_CLASSES': (
        'rest_framework.parsers.JSONParser',
        'rest_framework.parsers.FormParser',
        'rest_framework.parsers.MultiPartParser',
    ),
}

APPEND_SLASH = False

# # ############ JWT AUTHENTICATION ##################

# JWT_AUTH = {
#     'JWT_ENCODE_HANDLER':
#         'rest_framework_jwt.utils.jwt_encode_handler',

#     'JWT_DECODE_HANDLER':
#         'rest_framework_jwt.utils.jwt_decode_handler',

#     'JWT_PAYLOAD_HANDLER':
#         'rest_framework_jwt.utils.jwt_payload_handler',

#     'JWT_PAYLOAD_GET_USER_ID_HANDLER':
#         'rest_framework_jwt.utils.jwt_get_user_id_from_payload_handler',

#     'JWT_RESPONSE_PAYLOAD_HANDLER':
#         'rest_framework_jwt.utils.jwt_response_payload_handler',

#     'JWT_SECRET_KEY': SECRET_KEY,
#     'JWT_ALGORITHM': 'HS512',
#     'JWT_VERIFY': True,
#     'JWT_VERIFY_EXPIRATION': True,
#     'JWT_LEEWAY': 0,
#     'JWT_EXPIRATION_DELTA': datetime.timedelta(seconds=60 * 60 * 24),

#     'JWT_ALLOW_REFRESH': False,
#     'JWT_REFRESH_EXPIRATION_DELTA': datetime.timedelta(days=7),

#     'JWT_AUTH_HEADER_PREFIX': 'JWT',
# }

# Social Authentication

# AUTHENTICATION_BACKENDS = (
#     'accounts.auth_backend.PasswordlessAuthBackend',
#     'django.contrib.auth.backends.ModelBackend',
# )

# Google APIs
GMAPS_KEY = "AIzaSyCCSquQjr_RxRmBk9tu-hoyL6Ng01fO9sg"
GMAPS_URL = "https://maps.googleapis.com/maps/api/"
GMAPS_GEOCODE_URL = GMAPS_URL + "geocode/json?"
GMAPS_DISTANCE_URL = GMAPS_URL + "distancematrix/json?"
TIME_BETWEEN_REQUESTS = 0.1  # 100 ms

CALENDAR_KEY = "469325273320-ogqu6jdn2l02lsqm7kshiupvkb99mc4q.apps.googleusercontent.com"
CALENDAR_SERVICE_ACCOUNT_ID = "sparkl@sparkl-1509972949064.iam.gserviceaccount.com"
# CALENDAR_SERVICE_ACCOUNT_ID = "pgaudencio@sparkl-192311.iam.gserviceaccount.com"
CALENDAR_ID = "3qpotc8kv1ievkg8hlqrs2psb0@group.calendar.google.com"
# CALENDAR_ID = "sparkl.pt_o8iicu0h7h9ghnmve0q7cb58lk@group.calendar.google.com"
PENDING_CALENDAR_ID = "8glhmblcjadre9fa9pb05g5b58@group.calendar.google.com"
# PENDING_CALENDAR_ID = "sparkl.pt_9a08r2v4784mf3e7nkrhoa56rc@group.calendar.google.com"
CLIENT_SECRET_FILE = os.path.join(BASE_DIR, 'sparkl_web/settings/sparkl-f8de8d4752f2.p12')
# CLIENT_SECRET_FILE = os.path.realpath('sparkl_web/settings/sparkl-4926f48599a9.p12')

# Time measurements
SECONDS_PER_MINUTE = 60
SECONDS_PER_HOUR = 3600
SECONDS_PER_DAY = 86400

# Itinerary ratings
ITINERARY_MAX_SCORE = 100
ITINERARY_CRITERIA_LEN = 6
ITINERARY_SCALE = ITINERARY_MAX_SCORE * ITINERARY_CRITERIA_LEN

# Bookings night fee
NIGHT_FEE = 3  # 3 euro after 8pm

# Minimum order value
MIN_ORDER_VALUE = 12  # euros

# Services extra time
# 10m for service delay
ARBITRARY_SERVICE_EXTRA_TIME = SECONDS_PER_MINUTE * 10
# 30min for minimum time between services
ARBITRARY_SERVICES_INTERVAL_MIN = SECONDS_PER_MINUTE * 30
# 1h for maximum time between services
ARBITRARY_SERVICES_INTERVAL_MAX = SECONDS_PER_HOUR
# Maximum walkable distance
MAX_DISTANCE_BY_FOOT = 1000  # 1km

# rush hour definition
rush_hours = [
    {
        'from': time(8, 0),
        'until': time(10, 30),
        'delay': {
            DRIVING: SECONDS_PER_MINUTE * 20,  # 20 minutes
            TRANSIT: SECONDS_PER_MINUTE * 40,  # 40 minutes
        }
    },
    {
        'from': time(17, 30),
        'until': time(20, 30),
        'delay': {
            DRIVING: SECONDS_PER_MINUTE * 20,  # 20 minutes
            TRANSIT: SECONDS_PER_MINUTE * 40,  # 40 minutes
        }
    }
]

# notify service providers
MIN_NOTIFY_INTERVAL = SECONDS_PER_HOUR * 4  # 4 hours for last minute booking
# expire booking if payment not done in specified interval
EXPIRATION_SAME_DAY = SECONDS_PER_HOUR * 1  # 1h payment interval for same day
EXPIRATION_LT_48H = SECONDS_PER_HOUR * 5  # 5h payment interval < 48h
EXPIRATION_GTE_48H = SECONDS_PER_HOUR * 20  # 20h payment interval >= 48h

# SMS Gateway
SMS_GATEWAY_URL = "https://smsgateway.me/api/v3/messages/send"
SMS_API_CREDENTIALS = {'email': 'pmgaudencio@gmail.com',
                       'password': '',
                       'device': 'device_id'}
UPCOMING_BOOKING_SP_TEXT = ''
UPCOMING_BOOKING_CUSTOMER_TEXT = ''
CONFIRM_MOBILE_NUMBER_TEXT = ''

# FACEBOOK

FB_GRAPH_URL = "https://graph.facebook.com/{}"
FB_ACCESS_TOKEN = ""

# SIBS payments
SIBS_AUTH_USER_ID = "8a8294185b674555015b7c1928e81736"
SIBS_AUTH_ENTITY_ID = "8a8294175edcf864015ee32d7e501641"
SIBS_AUTH_PASSWORD = "Rr47eQesdW"
SIBS_MBWAY_MERCHANT_TRANSACTION_ID = "testesparkl"
SIBS_VISA_MERCHANT_TRANSACTION_ID = "teste_PA_CP"
SIBS_WEBHOOK = "https://342dfcc4.ngrok.io/api/v1/payment/webhook/"
SIBS_WEBHOOK_KEY = "5FBD4D603E74198BFEA1C5789FE41A0747C3CC6F7130136A3CE8A3FEF45B68B9"
SIBS_CURRENCY = "EUR"
BILLING_COUNTRY = "PT"
SIBS_MB_ENTITY_ID = "25002"

SIBS_PAYMENT_URL = "https://test.onlinepayments.pt/v1/checkouts"
SIBS_MULTIBANCO_URL = "https://test.onlinepayments.pt/v1/payments"
SIBS_STATUS_URL = "/".join([SIBS_PAYMENT_URL, "{}/payment"])
SIBS_RESULT_CODES_FILENAME = "lib/sibs_resultcodes.json"

# Channel settings
# CHANNEL_LAYERS = {
#     "default": {
#         "BACKEND": "asgi_redis.RedisChannelLayer",
#         "CONFIG": {
#             "hosts": [os.environ.get('REDIS_URL', 'redis://localhost:6379')],
#         },
#         "ROUTING": "sparkl_web.routing.channel_routing",
#     },
# }

# SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')

NOSE_ARGS = [
    '--with-coverage',
    '--cover-package=foo,bar',
]
