from sparkl_web.settings.base import *  # NOQA (ignore all errors on this line)

DEBUG = True

PAGE_CACHE_SECONDS = 1

ALLOWED_HOSTS = ["*"]

# TODO: Change to final path name (4)
SITE_NAME = 'http://54.169.230.134:8000'

if os.environ.get('RDS_DB_NAME'):
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.postgresql_psycopg2',
            'NAME': os.environ['RDS_DB_NAME'],
            'USER': os.environ['RDS_USERNAME'],
            'PASSWORD': os.environ['RDS_PASSWORD'],
            'HOST': os.environ['RDS_HOSTNAME'],
            'PORT': os.environ['RDS_PORT'],
        }
    }
else:
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.postgresql_psycopg2',
            'NAME': 'sparkl_stag',
            'USER': 'dev',
            'PASSWORD': 'dev',
            'HOST': 'localhost',
            'PORT': '',
        }
    }

# REST_FRAMEWORK['EXCEPTION_HANDLER'] = 'django_rest_logger.handlers.rest_exception_handler'

LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'root': {
        'level': 'DEBUG',
        'handlers': ['django_rest_logger_handler'],
    },
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(module)s '
                      '%(process)d %(thread)d %(message)s'
        },
    },
    'handlers': {
        'django_rest_logger_handler': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'verbose'
        }
    },
    'loggers': {
        'django.db.backends': {
            'level': 'ERROR',
            'handlers': ['django_rest_logger_handler'],
            'propagate': False,
        },
        'django_rest_logger': {
            'level': 'DEBUG',
            'handlers': ['django_rest_logger_handler'],
            'propagate': False,
        },
        'werkzeug': {
            'handlers': ['django_rest_logger_handler'],
            'level': 'DEBUG',
            'propagate': True,
        }
    },
}

DEFAULT_LOGGER = 'django_rest_logger'

LOGGER_EXCEPTION = DEFAULT_LOGGER
LOGGER_ERROR = DEFAULT_LOGGER
LOGGER_WARNING = DEFAULT_LOGGER

# AWS Storage
AWS_STORAGE_BUCKET_NAME = 'sparkl-stag'
# AWS_ACCESS_KEY_ID = 'AKIAIWNWMRWCMIW6WJHA'
# AWS_SECRET_ACCESS_KEY = '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'

MEDIA_URL = 'https://{}/'.format(SITE_NAME)
DEFAULT_FILE_STORAGE = 'storages.backends.s3boto.S3BotoStorage'

# Graph models
GRAPH_MODELS = {
    'all_applications': True,
    'group_models': True
}

# Facebook
# https://developers.facebook.com/apps/154397635379908/fb-login/settings/
FACEBOOK_APP_ID = "154397635379908"
FACEBOOK_CLIENT_TOKEN = "a5334fa31e0f0740a081f15515ef86cb"
# permissions: email, public_profile, user_birthday, user_friends,
# user_location, user_relationships, user_work_history

# SIBS payments
# SIBS_WEBHOOK = "https://dev.sparkl.pt/api/v1/payment/webhook/"
