from adminplus.sites import AdminSitePlus
from rest_framework.routers import DefaultRouter

from django.conf import settings
from django.conf.urls import include, url
from django.contrib import admin
# from django.contrib.auth.models import Permission
from django.views.decorators.cache import cache_page
from django.views.generic import TemplateView 

from base import views as base_views
import api.profiles.views as profile_views
import api.addresses.views as address_views
import api.services.views as service_views
import api.bookings.views as booking_views
import api.business.views as business_views
import api.payments.views as payment_views

# admin config
admin.site = AdminSitePlus()
admin.sites.site = admin.site
admin.autodiscover()
admin.site.site_title = 'Sparkl admin'
admin.site.site_header = 'Sparkl admin'
# admin.site.site_url = 'http://sparkl.pt/'
admin.site.index_title = 'Sparkl administration'
admin.empty_value_display = 'Empty'
# admin.site.register(Permission)


router = DefaultRouter(trailing_slash=False)

# profiles
router.register(r'customer', profile_views.CustomerView, base_name='customer')
router.register(r'service_provider', profile_views.ServiceProviderView,
                base_name='service_provider')

# addresses
router.register(r'address', address_views.AddressView, base_name='address')

# services
router.register(r'service', service_views.ServiceView, base_name='service')
router.register(r'service_category', service_views.ServiceCategoryView,
                base_name='service_category')
# router.register(r'venue', service_views.VenueView, base_name='venue')
# router.register(r'channel', service_views.ChannelView, base_name='channel')

# bookings
router.register(r'booking', booking_views.BookingView, base_name='booking')
router.register(r'booking_enquiry', booking_views.BookingEnquiryView,
                base_name='booking_enquiry')

# business
router.register(r'order', business_views.OrderView, base_name='order')
# router.register(r'payment', payment_views.PaymentView, base_name='payment')


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    # url(r'^docs/', include('rest_framework_swagger.urls')),
    # url(r'^api-docs/', include(
    #     'rest_framework.urls', namespace='rest_framework')),

    url(r'^api/v1/payment/', include('api.payments.urls', namespace='payments')),
    url(r'^api/v1/', include(router.urls)),

    # Service worker file must be placed into root folder of web app
    url(r'^sw.js', (TemplateView.as_view(template_name="sw.js", content_type='application/javascript')), name='sw.js'),
    
    # catch all others because of how history is handled by react router
    # cache this page because it will never change
    url(r'', cache_page(settings.PAGE_CACHE_SECONDS)(
        base_views.IndexView.as_view()), name='index'),
]
