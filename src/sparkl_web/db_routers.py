class AddressRouter:
    """
    A router to control all database operations on models in the
    api.addresses application.
    """
    def db_for_read(self, model, **hints):
        """
        Attempts to read api.addresses models go to address_db.
        """
        if model._meta.app_label == 'api.addresses':
            return 'address_db'
        return None

    def db_for_write(self, model, **hints):
        """
        Attempts to write api.addresses models go to address_db.
        """
        if model._meta.app_label == 'api.addresses':
            return 'address_db'
        return None

    def allow_relation(self, obj1, obj2, **hints):
        """
        Allow relations if a model in the api.addresses app is involved.
        """
        if (obj1._meta.app_label == 'api.addresses' or
                obj2._meta.app_label == 'api.addresses'):
            return True
        return None

    def allow_migrate(self, db, app_label, model_name=None, **hints):
        """
        Make sure the api.addresses app only appears in the 'address_db'
        database.
        """
        if app_label == 'api.addresses':
            return db == 'address_db'
        return None
