from django.core.wsgi import get_wsgi_application
import os

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "sparkl_web.settings.dev")

from django.conf import settings
import httplib2
from googleapiclient.discovery import build
from oauth2client.service_account import ServiceAccountCredentials


def build_service():
    credentials = ServiceAccountCredentials.from_p12_keyfile(
        service_account_email=settings.CALENDAR_SERVICE_ACCOUNT_ID,
        filename=settings.CLIENT_SECRET_FILE,
        scopes=['https://www.googleapis.com/auth/calendar']
    )

    http = credentials.authorize(httplib2.Http())

    service = build('calendar', 'v3', http=http)

    return service
