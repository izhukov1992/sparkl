#!/bin/bash

set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" <<-EOSQL
    CREATE USER sparkl WITH PASSWORD 'password' CREATEDB;
    CREATE DATABASE sparkl_dev;
    GRANT ALL PRIVILEGES ON DATABASE sparkl_dev TO sparkl;
EOSQL
